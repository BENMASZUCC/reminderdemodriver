﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.VisualBasic.FileIO
Imports System.Net.Mail
Imports System.Configuration
Imports System.Globalization
Imports System.IO.FileStream
Imports System.Text.RegularExpressions
Imports System.Net
Imports Microsoft.Win32
Imports System.Management

'06/05/2021 MB 
'12/10/2021 SG

Public Class Form1

    Public listID As List(Of Integer)

    'Public strConnectionDB As String = "Data Source=srvsqlprod;Initial Catalog=ZCONF;Persist Security Info=True;User ID=sa;Password=Ditirambo1234!"
    Public strConnectionDB As String = "" 'Data Source=ISEBCTEST;Initial Catalog=ZCONF;Persist Security Info=True;User ID=sa;Password=P@ssw0rd"

    Public sInviaMail As String
    Public strDateFormat As String

    Public strConnect As String
    Public strConnect2 As String

    Public stPath As String
    Public stDataSource As String
    Public stInitialCatalog As String
    Public stPassword As String
    Public stUserID As String

    Private tProgessBar As Threading.Thread
    Delegate Function tProgressBar_at_Max() As Boolean
    Private suspended As Boolean
    Dim inputArgument As String = "/mode="
    Dim inputArgument2 As String = "/idutenteam="
    Dim inputName As String = ""
    Dim inputIDAM As String = ""

    Private myConn As SqlConnection
    Private myConn2 As SqlConnection
    Private myConn3 As SqlConnection
    Private myConn4 As SqlConnection
    Private myConn5 As SqlConnection
    Private myCmd As SqlCommand
    Private myCmdGEN As SqlCommand
    Private myCmdGENMAIL As SqlCommand
    Private myReader As SqlDataReader
    Private myReader2 As SqlDataReader
    Private myReader4 As SqlDataReader
    Private myReaderGEN As SqlDataReader
    Private myReaderGENMAIL As SqlDataReader

    Private results As String

    Public Mail_Host As String
    Public Mail_Name As String
    Public Mail_Address As String
    Public Mail_Port As String
    Public Mail_Username As String
    Public Mail_Password As String
    Public Mail_EnableSSL As String
    Public Mail_SmtpAuth As String
    Public stPasswordDecifrata As String

    Private Sub GetAnagrafica(strCodiceFiscale As String, anagList As ArrayList)
        Dim obj As Object
        Try
            If strCodiceFiscale.ToString <> String.Empty Then
                For Each obj In anagList
                    If anagList.IndexOf(obj) = 1 Then

                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Function validateData(data As String) As Boolean
        Dim check As Boolean
        check = True

        If data.Contains("<script>") Then
            check = False
        End If
        If data.Contains("<SCRIPT>") Then
            check = False
        End If

        Dim formula As String
        formula = "<(\''[^\'']*\''|'[^']*'|[^'\''>])*>"

        Dim rx As New Regex(formula)

        Dim isHTML As Boolean
        isHTML = rx.IsMatch(data)

        If (isHTML = True) Then check = False

        Dim formula2 As String
        formula2 = "(www|http:|https:)+[^\\s]+[\\w]"

        Dim rx2 As New Regex(formula2)

        Dim isURL As Boolean

        isURL = rx2.IsMatch(data)

        If (isURL = True) Then check = False

        Return check

    End Function


    Public Function apriDirettamenteSchedaDemo(GUIDConf As String, IDKey As Integer, Tabella As String)

        Dim ret As String
        ret = ""

        If (validateData(GUIDConf) = False) Then ret = "Errore"

        Dim codex As String

        codex = ""
        Dim parametri As String
        parametri = GUIDConf & ";" & IDKey.ToString() & ";" & Tabella

        codex = CifraturaDecifratura.CifraturaDecifratura.EncryptX(parametri)

        ret = codex

        Return ret


    End Function

    Private Function InviomailPresales() As Boolean
        Dim ContattiAgg As String
        Dim reformatted As String = DateTime.Now().ToString("dd/MM/yyyy")

        Dim IdDemo As Integer
        Dim IdDemoOLD As Integer
        Dim y As Integer
        Dim IDRichiesta As Integer
        Dim IDRichiestaOLD As Integer
        Dim IdStoricoDemo As Integer
        Dim IdStoricoDemoOLD As Integer
        Dim Descrizione As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim NomeMailContatto As String
        Dim DataDemo As String
        Dim strEmail As String
        Dim strOra As String
        Dim strDemo As String

        Dim DescrizioneOLD As String
        Dim NomeContattoOLD As String
        Dim MailContattoOLD As String
        Dim NomeMailContattoOLD As String
        Dim DataDemoOLD As String
        Dim strEmailOLD As String
        Dim strOraOLD As String
        Dim strDemoOLD As String

        Dim MailPresales As String

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        Dim strToday As String
        Dim strSQL As String
        Dim myReader1 As SqlDataReader
        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ReminderPresaleDemoToday" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        retImp = False

        IdStoricoDemo = 0
        IdStoricoDemoOLD = 0

        strTest = "N"

        strTest = System.Configuration.ConfigurationManager.AppSettings("TESTpresales").ToString()

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        sw.WriteLine("Inizio invio mail per i presales -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB) 'Create a Connection object.

        myCmd = myConn.CreateCommand 'Create a Command object.
        myCmd.CommandText = strSQL
        myConn.Open() 'Open the connection.

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop

        myReader1.Close() 'Close the reader and the database connection.

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = "Reminder demo per il #data per i presales"
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNotificaPresales'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNotificaPresales'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        ' caricamento dei presales
        Dim presales As New List(Of String)
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "SELECT ID, Descrizione, Email FROM Users where idwebprofile=3"
        myReader3 = myCmd3.ExecuteReader()
        Do While myReader3.Read()
            presales.Add(myReader3.Item("ID").ToString() & ";" & myReader3.Item("Descrizione").ToString() & ";" & myReader3.Item("Email").ToString())
        Loop
        myReader3.Close()

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)

        'Create a Command object.
        strSQL = "set dateformat dmy; ;Exec spGetDemoReminder @data"
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        MailPresales = ""
        NomeMailContatto = ""
        NomeMailContattoOLD = ""

        NomeContatto = ""
        MailContatto = ""
        DataDemo = ""
        strEmail = ""
        strOra = ""
        strDemo = ""
        IdDemo = 0
        IdStoricoDemo = 0

        NomeContattoOLD = ""
        MailContattoOLD = ""
        DataDemoOLD = ""
        strEmailOLD = ""
        strOraOLD = ""
        strDemoOLD = ""
        IdDemoOLD = 0
        IdStoricoDemoOLD = 0

        y = 0
        IdDemo = 0
        IdDemoOLD = 0

        myCmd.Parameters.AddWithValue("@data", reformatted)

        myConn.Open() 'Open the connection.
        Descrizione = ""
        DescrizioneOLD = ""

        'MsgBox("Exec spGetDemoReminder @data" & reformatted.ToString())

        myReader = myCmd.ExecuteReader()

        If myReader.HasRows Then
            'MsgBox("myReader.HasRows")
            Do While myReader.Read
                Try
                    IDRichiesta = myReader.Item("IDRichiesta")
                    IdStoricoDemo = myReader.Item("Id")
                    Descrizione = myReader.Item("Descrizione").ToString()
                    NomeContatto = myReader.Item("NomeContatto").ToString()
                    MailContatto = myReader.Item("MailContatto").ToString()
                    DataDemo = myReader.Item("DataDemoText").ToString()
                    strEmail = myReader.Item("mail")
                    strOra = myReader.Item("Ora")
                    strDemo = myReader.Item("voce")
                    IdDemo = myReader.Item("IdDemo")

                    sw.WriteLine("Mail Presales -- elenco : " & myReader.Item("Mail"))
                    sw.WriteLine("Descrizione Presales -- elenco : " & myReader.Item("Descrizione"))
                    sw.WriteLine("DataDemo -- " & myReader.Item("DataDemoText"))
                    sw.WriteLine("Ora -- " & myReader.Item("Ora"))
                    sw.WriteLine("voce -- " & myReader.Item("voce"))

                    Dim strVoce As String
                    strVoce = myReader.Item("voce")

                    If IdDemo <> IdDemoOLD And y > 0 Then

                        ContattiAgg = ""

                        Try

                            Dim myCmdContAgg As New SqlCommand
                            Dim strSqlContAgg As String
                            ContattiAgg = ""

                            strSqlContAgg = "Select Nome + ' ' + Cognome + ' ' + Mail + '<br>' Contatto from Preventivi_Richieste_Contatti where IDRichiesta= " & IDRichiestaOLD
                            strSqlContAgg = strSqlContAgg & " UNION Select Nominativo + ' ' + Mail + '<br>' Contatto from Storico_Demo_Contatti where IDStoricoDemo= " & IdStoricoDemoOLD

                            Try
                                myConn2 = New SqlConnection(strConnectionDB)
                                myConn2.Open()
                                myCmdContAgg = myConn2.CreateCommand
                                myCmdContAgg.CommandText = strSqlContAgg
                                myReader3 = myCmdContAgg.ExecuteReader()
                                Do While myReader3.Read()
                                    ContattiAgg = ContattiAgg & " " & myReader3.Item("Contatto").ToString() & "<br>"
                                Loop
                                myReader3.Close()
                                myConn2.Close()
                            Catch ex As Exception
                                sw.WriteLine("Errore su ContattiAgg -- " & ex.Message.ToString())
                                ContattiAgg = ""
                            End Try

                        Catch ex As Exception

                        End Try

                        strEmailOLD.Split(";").Count

                        For Each sMailPresales As String In strEmailOLD.Split(";")
                            If sMailPresales <> "" Then

                                Dim UsersID As String
                                Dim UsersDesc As String
                                Dim UsersEmail As String
                                Dim Users() As String
                                UsersID = ""
                                UsersDesc = ""
                                UsersEmail = ""
                                For Each presale As String In presales
                                    Users = presale.Split(";")

                                    If (presale.Contains(sMailPresales.ToString().Trim())) Then
                                        UsersID = Users(0).ToString()
                                        UsersDesc = Users(1).ToString()
                                        UsersEmail = Users(2).ToString()
                                        Console.Write(Users(0).ToString() & " " & Users(1).ToString() & " " & " " & Users(2).ToString() & " ")
                                        Exit For
                                    End If

                                Next

                                MailPresales = sMailPresales
                                If strTest.ToUpper().Trim().ToString() = "S" Then
                                    MailPresales = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                                End If

                                sw.WriteLine("Invio mail a presale " & sMailPresales.ToString())

                                Dim testo As String
                                testo = strDemoOLD & "<br>" & strOraOLD & "<br>" & NomeMailContattoOLD


                                If ContattiAgg <> "" Then

                                    If strDemoOLD.ToLower().Contains(Replace(MailContattoOLD.ToLower(), ";", "")) = False Then
                                        NomeMailContattoOLD = "" & NomeMailContattoOLD & ContattiAgg & "<br>"
                                    Else
                                        NomeMailContattoOLD = "" & ContattiAgg & "<br>"
                                    End If

                                End If

                                Try
                                    Inviamail(strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo), "", MailPresales, "", sw, testoHtml.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo).Replace("#ora", strOra), DataDemo, "", Nothing)


                                    Dim query As String
                                    strOggetto = "Reminder demo per il #data per i presales"
                                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                                    myConn2 = New SqlConnection(strConnectionDB)
                                    myConn2.Open()
                                    Using comm As New SqlCommand()
                                        With comm
                                            .Connection = myConn2
                                            .CommandType = CommandType.Text
                                            .CommandText = query
                                            .Parameters.AddWithValue("@Dest", MailPresales)
                                            .Parameters.AddWithValue("@CC", "")
                                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo))
                                            .Parameters.AddWithValue("@Esito", "OK")
                                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailPresales)
                                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                                            .Parameters.AddWithValue("@IDRichiesta", 0)
                                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo).Replace("#ora", strOra))
                                        End With
                                        Try
                                            comm.ExecuteNonQuery()
                                        Catch ex As Exception
                                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                        End Try
                                    End Using

                                Catch ex As Exception
                                    sw.WriteLine("Errore Invio mail a presale " & sMailPresales.ToString())

                                    Dim query As String
                                    strOggetto = "Reminder demo per il #data per i presales"
                                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                                    'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                                    myConn2 = New SqlConnection(strConnectionDB)
                                    myConn2.Open()
                                    Using comm As New SqlCommand()
                                        With comm
                                            .Connection = myConn2
                                            .CommandType = CommandType.Text
                                            .CommandText = query
                                            .Parameters.AddWithValue("@Dest", MailPresales)
                                            .Parameters.AddWithValue("@CC", "")
                                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo))
                                            .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailPresales)
                                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                                            .Parameters.AddWithValue("@IDRichiesta", 0)
                                            '.Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo).Replace("#ora", strOra))
                                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo & "<br><br>" & NomeMailContattoOLD).Replace("#ora", strOra))
                                        End With
                                        Try
                                            comm.ExecuteNonQuery()
                                        Catch ex1 As Exception
                                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                        End Try
                                    End Using

                                End Try


                            End If

                        Next

                        DescrizioneOLD = ""
                        NomeMailContattoOLD = ""
                        NomeContattoOLD = ""
                        MailContattoOLD = ""
                        DataDemoOLD = ""
                        strEmailOLD = ""
                        strOraOLD = ""
                        strDemoOLD = ""
                        IdDemoOLD = 0
                        IDRichiestaOLD = 0
                        IdStoricoDemoOLD = 0

                    End If

                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IdDemoOLD = IdDemo

                If Not (DescrizioneOLD.Contains(Descrizione)) Then
                    DescrizioneOLD = DescrizioneOLD & ";" & Descrizione
                End If


                NomeMailContatto = "" & NomeContatto & " " & MailContatto & " "
                NomeMailContattoOLD = "" & NomeMailContattoOLD & NomeMailContatto & "<br>"

                MailContattoOLD = MailContattoOLD & ";" & MailContatto
                NomeContattoOLD = NomeContattoOLD & ";" & NomeContatto
                DataDemoOLD = DataDemo
                If Not (strEmailOLD.Contains(strEmail)) Then
                    strEmailOLD = strEmailOLD & ";" & strEmail
                End If
                strOraOLD = strOra
                strDemoOLD = strDemo
                IdDemoOLD = IdDemo
                IDRichiestaOLD = IDRichiesta
                IdStoricoDemoOLD = IdStoricoDemo

                y = y + 1

            Loop
        End If



        ContattiAgg = ""

        Try

            Dim myCmdContAgg As New SqlCommand
            Dim strSqlContAgg As String
            ContattiAgg = ""

            strSqlContAgg = "Select Nome + ' ' + Cognome + ' ' + Mail + '<br>' Contatto from Preventivi_Richieste_Contatti where IDRichiesta= " & IDRichiestaOLD
            strSqlContAgg = strSqlContAgg & " UNION Select Nominativo + ' ' + Mail + '<br>' Contatto from Storico_Demo_Contatti where IDStoricoDemo= " & IdStoricoDemoOLD

            Try
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                myCmdContAgg = myConn2.CreateCommand
                myCmdContAgg.CommandText = strSqlContAgg
                myReader3 = myCmdContAgg.ExecuteReader()
                Do While myReader3.Read()
                    ContattiAgg = ContattiAgg & " " & myReader3.Item("Contatto").ToString() & "<br>"
                Loop
                myReader3.Close()
                myConn2.Close()
            Catch ex As Exception
                sw.WriteLine("Errore su ContattiAgg -- " & ex.Message.ToString())
                ContattiAgg = ""
            End Try

        Catch ex As Exception

        End Try

        strEmailOLD.Split(";").Count

        MailPresales = ""
        For Each sMailPresales As String In strEmailOLD.Split(";")
            If sMailPresales <> "" Then
                MailPresales = sMailPresales
                If strTest.ToUpper().Trim().ToString() = "S" Then
                    MailPresales = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                End If

                Dim UsersID As String
                Dim UsersDesc As String
                Dim UsersEmail As String
                Dim Users() As String

                For Each presale As String In presales
                    Users = presale.Split(";")
                    UsersID = ""
                    UsersDesc = ""
                    UsersEmail = ""
                    If (presale.Contains(sMailPresales.ToString().Trim())) Then
                        UsersID = Users(0).ToString()
                        UsersDesc = Users(1).ToString()
                        UsersEmail = Users(2).ToString()
                        Console.Write(Users(0).ToString() & " " & Users(1).ToString() & " " & " " & Users(2).ToString() & " ")
                        Exit For
                    End If

                Next


                sw.WriteLine("Invio mail a presale " & sMailPresales.ToString())

                Dim testo As String
                testo = strDemoOLD & "<br>" & strOraOLD & "<br>" & NomeMailContattoOLD
                testo = strDemoOLD & "<br>" & strOraOLD & "<br>"

                If ContattiAgg <> "" Then

                    If strDemoOLD.ToLower().Contains(Replace(MailContattoOLD.ToLower(), ";", "")) = False Then
                        NomeMailContattoOLD = "" & NomeMailContattoOLD & ContattiAgg & "<br>"
                    Else
                        NomeMailContattoOLD = "" & ContattiAgg & "<br>"
                    End If

                End If


                Try
                    Inviamail(strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo), "", MailPresales, "", sw, testoHtml.Replace("#data", DataDemoOLD.ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo & "<br><br>" & NomeMailContattoOLD).Replace("#ora", strOraOLD), DataDemoOLD, "", Nothing)

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailPresales)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo))
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailPresales)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)
                            '''.Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD.ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo).Replace("#ora", strOraOLD))
                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD.ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo & "<br><br>" & NomeMailContattoOLD).Replace("#ora", strOraOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                Catch ex As Exception
                    sw.WriteLine("Errore Invio mail a presale " & sMailPresales.ToString())

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailPresales)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo))
                            .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailPresales)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)
                            '.Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD.ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo).Replace("#ora", strOraOLD))
                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD.ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo & "<br><br>" & NomeMailContattoOLD).Replace("#ora", strOraOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                End Try

            End If

        Next

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function

    Private Function InviomailPresalesNEW() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        strTest = "N"

        strTest = System.Configuration.ConfigurationManager.AppSettings("TESTpresales").ToString()

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio invio mail per i presales -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()


        Dim testoHtml As String
        Dim testoHtmlCOMM As String
        Dim strOggetto As String
        Dim strOggettoComm As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = "Reminder demo per il #data per i presales"
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNotificaPresales'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNotificaPresales'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        testoHtmlCOMM = ""
        strOggettoComm = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNotificaCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtmlCOMM = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNotificaCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggettoComm = myReader3.Item("Testo")
        Loop
        myReader3.Close()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim presales As New List(Of String)
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "SELECT ID, Descrizione, Email FROM Users where idwebprofile=3"
        myReader3 = myCmd3.ExecuteReader()
        Do While myReader3.Read()
            presales.Add(myReader3.Item("ID").ToString() & ";" & myReader3.Item("Descrizione").ToString() & ";" & myReader3.Item("Email").ToString())
        Loop
        myReader3.Close()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)

        'Create a Command object.
        strSQL = "Exec spGetDemoReminder @data"
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        'myCmd.Parameters.AddWithValue("@data", "2021-02-18")

        Dim reformatted As String = DateTime.Now().ToString("yyyy-MM-dd")

        Dim IdDemo As Integer
        Dim IdDemoOLD As Integer
        Dim y As Integer

        Dim Descrizione As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim NomeMailContatto As String
        Dim DataDemo As String
        Dim strEmail As String
        Dim strOra As String
        Dim strDemo As String

        Dim DescrizioneOLD As String
        Dim NomeContattoOLD As String
        Dim MailContattoOLD As String
        Dim NomeMailContattoOLD As String
        Dim DataDemoOLD As String
        Dim strEmailOLD As String
        Dim strOraOLD As String
        Dim strDemoOLD As String

        Dim MailPresales As String

        Dim IDRichiesta As Integer
        Dim IDCommerciale As Integer
        Dim NominativoComm As String
        Dim MailComm As String
        Dim TelefonoComm As String
        Dim AziendaComm As String

        IDRichiesta = 0
        IDCommerciale = 0
        NominativoComm = ""
        MailComm = ""
        TelefonoComm = ""
        AziendaComm = ""

        MailPresales = ""
        NomeMailContatto = ""
        NomeMailContattoOLD = ""

        NomeContatto = ""
        MailContatto = ""
        DataDemo = ""
        strEmail = ""
        strOra = ""
        strDemo = ""
        IdDemo = 0

        NomeContattoOLD = ""
        MailContattoOLD = ""
        DataDemoOLD = ""
        strEmailOLD = ""
        strOraOLD = ""
        strDemoOLD = ""
        IdDemoOLD = 0

        y = 0
        IdDemo = 0
        IdDemoOLD = 0

        myCmd.Parameters.AddWithValue("@data", reformatted)

        'Open the connection.
        myConn.Open()
        Descrizione = ""
        DescrizioneOLD = ""

        myReader = myCmd.ExecuteReader()

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    Descrizione = myReader.Item("Descrizione").ToString()
                    NomeContatto = myReader.Item("NomeContatto").ToString()
                    MailContatto = myReader.Item("MailContatto").ToString()
                    DataDemo = myReader.Item("DataDemoText").ToString()
                    strEmail = myReader.Item("mail")
                    strOra = myReader.Item("Ora")
                    strDemo = myReader.Item("voce")
                    IdDemo = myReader.Item("IdDemo")

                    IDRichiesta = myReader.Item("IDRichiesta")
                    IDCommerciale = myReader.Item("IDCommerciale")
                    NominativoComm = myReader.Item("NominativoComm").ToString()
                    MailComm = myReader.Item("MailComm").ToString()
                    TelefonoComm = myReader.Item("TelefonoComm").ToString()
                    AziendaComm = myReader.Item("AziendaComm").ToString()

                    Dim NomeAzienda As String
                    Dim myCmdAZ As New SqlCommand
                    NomeAzienda = ""

                    Try
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        myCmdAZ = myConn2.CreateCommand
                        myCmdAZ.CommandText = "Select isnull(NomeAzienda,'') NomeAzienda FROM Preventivi_Richieste where ID=" & IDRichiesta.ToString()
                        myReader3 = myCmdAZ.ExecuteReader()
                        Do While myReader3.Read()
                            NomeAzienda = myReader3.Item("NomeAzienda").ToString()
                        Loop
                        myReader3.Close()
                        myConn2.Close()
                    Catch ex As Exception
                        sw.WriteLine("Errore su recupero NomeAzienda -- " & ex.Message.ToString())
                    End Try





                    sw.WriteLine("Mail Presales -- elenco : " & myReader.Item("Mail"))
                    sw.WriteLine("Descrizione Presales -- elenco : " & myReader.Item("Descrizione"))
                    sw.WriteLine("DataDemo -- " & myReader.Item("DataDemoText"))
                    sw.WriteLine("Ora -- " & myReader.Item("Ora"))
                    sw.WriteLine("voce -- " & myReader.Item("voce"))

                    Dim strVoce As String
                    strVoce = myReader.Item("voce")




                    ''*****************************************************************************************
                    ''********************* Invio mail a commerciale di riferimento
                    ''*****************************************************************************************
                    ''testoHtmlCOMM
                    'Dim testoCOMM As String
                    'testoCOMM = strDemo & " " & strOra & "<br>" & NomeAzienda & "<br>" & NomeContatto & " " & MailContatto
                    ''testoCOMM = strDemo & "<br>" & strOra & "<br>" & NomeAzienda & "<br>" & NomeContatto & "<br>" & MailContatto

                    'strOggettoComm = "Invio reminder al commerciale per la demo del #data"

                    'sw.WriteLine("BEGIN Invio mail a commerciale di riferimento " & MailComm.ToString() & " " & Now.ToString())

                    'If strTest.ToUpper().Trim().ToString() = "S" Then
                    '    MailComm = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    '    sw.WriteLine("***TEST*** Invio mail a commerciale di riferimento " & MailComm.ToString())
                    'End If

                    'sw.WriteLine("***IDCommerciale = " & IDCommerciale.ToString())

                    'If IDCommerciale <> 0 Then
                    '    Try
                    '        Inviamail(strOggettoComm.Replace("#data", DataDemo), "", MailComm, "", sw, testoHtmlCOMM.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", NominativoComm).Replace("#testo", testoCOMM).Replace("#ora", strOra), DataDemo, "")
                    '        sw.WriteLine("Mail inviata a commerciale " & MailComm.ToString())

                    '        Dim query As String
                    '        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                    '        myConn2 = New SqlConnection(strConnectionDB)
                    '        myConn2.Open()
                    '        Using comm As New SqlCommand()
                    '            With comm
                    '                .Connection = myConn2
                    '                .CommandType = CommandType.Text
                    '                .CommandText = query
                    '                .Parameters.AddWithValue("@Dest", MailContatto)
                    '                .Parameters.AddWithValue("@CC", "")
                    '                .Parameters.AddWithValue("@Oggetto", strOggettoComm.Replace("#data", DataDemo))
                    '                .Parameters.AddWithValue("@Esito", "OK")
                    '                .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailContatto)
                    '                .Parameters.AddWithValue("@GUIDRichiesta", "")
                    '                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                    '            End With
                    '            Try
                    '                comm.ExecuteNonQuery()
                    '            Catch ex As Exception
                    '                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    '            End Try
                    '        End Using

                    '    Catch ex As Exception
                    '        sw.WriteLine("Errore Invio mail a commerciale " & MailComm.ToString())

                    '        Dim query As String
                    '        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"

                    '        myConn2 = New SqlConnection(strConnectionDB)
                    '        myConn2.Open()
                    '        Using comm As New SqlCommand()
                    '            With comm
                    '                .Connection = myConn2
                    '                .CommandType = CommandType.Text
                    '                .CommandText = query
                    '                .Parameters.AddWithValue("@Dest", MailContatto)
                    '                .Parameters.AddWithValue("@CC", "")
                    '                .Parameters.AddWithValue("@Oggetto", strOggettoComm.Replace("#data", DataDemo))
                    '                .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                    '                .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailContatto)
                    '                .Parameters.AddWithValue("@GUIDRichiesta", "")
                    '                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                    '            End With
                    '            Try
                    '                comm.ExecuteNonQuery()
                    '            Catch ex1 As Exception
                    '                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    '            End Try
                    '        End Using

                    '    End Try
                    'End If
                    'sw.WriteLine("END Invio mail a commerciale di riferimento " & MailComm.ToString() & " " & Now.ToString())

                    ''*****************************************************************************************
                    ''********************* Invio mail a commerciale di riferimento
                    ''*****************************************************************************************






                    If IdDemo <> IdDemoOLD And y > 0 Then

                        strEmailOLD.Split(";").Count

                        For Each sMailPresales As String In strEmailOLD.Split(";")
                            If sMailPresales <> "" Then

                                Dim UsersID As String
                                Dim UsersDesc As String
                                Dim UsersEmail As String
                                Dim Users() As String

                                For Each presale As String In presales
                                    Users = presale.Split(";")

                                    If (presale.Contains(sMailPresales.ToString().Trim())) Then
                                        UsersID = Users(0).ToString()
                                        UsersDesc = Users(1).ToString()
                                        UsersEmail = Users(2).ToString()
                                        Console.Write(Users(0).ToString() & " " & Users(1).ToString() & " " & " " & Users(2).ToString() & " ")
                                        Exit For
                                    End If

                                Next

                                MailPresales = sMailPresales
                                If strTest.ToUpper().Trim().ToString() = "S" Then
                                    MailPresales = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                                End If

                                sw.WriteLine("Invio mail a presale " & sMailPresales.ToString())

                                Dim testo As String
                                testo = strDemoOLD & "<br>" & strOraOLD & "<br>" & NomeMailContattoOLD

                                Try
                                    Inviamail(strOggetto.Replace("#data", DataDemo), "", MailPresales, "", sw, testoHtml.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo).Replace("#ora", strOra), DataDemo, "", Nothing)
                                    sw.WriteLine("Mail inviata a presale " & sMailPresales.ToString())

                                    Dim query As String
                                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                                    myConn2 = New SqlConnection(strConnectionDB)
                                    myConn2.Open()
                                    Using comm As New SqlCommand()
                                        With comm
                                            .Connection = myConn2
                                            .CommandType = CommandType.Text
                                            .CommandText = query
                                            .Parameters.AddWithValue("@Dest", MailContatto)
                                            .Parameters.AddWithValue("@CC", "")
                                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                            .Parameters.AddWithValue("@Esito", "OK")
                                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto)
                                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                                            .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                        End With
                                        Try
                                            comm.ExecuteNonQuery()
                                        Catch ex As Exception
                                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                        End Try
                                    End Using

                                Catch ex As Exception
                                    sw.WriteLine("Errore Invio mail a presale " & sMailPresales.ToString())

                                    Dim query As String
                                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                                    myConn2 = New SqlConnection(strConnectionDB)
                                    myConn2.Open()
                                    Using comm As New SqlCommand()
                                        With comm
                                            .Connection = myConn2
                                            .CommandType = CommandType.Text
                                            .CommandText = query
                                            .Parameters.AddWithValue("@Dest", MailContatto)
                                            .Parameters.AddWithValue("@CC", "")
                                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                            .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto)
                                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                                            .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                        End With
                                        Try
                                            comm.ExecuteNonQuery()
                                        Catch ex1 As Exception
                                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                        End Try
                                    End Using

                                End Try

                            End If

                        Next

                        DescrizioneOLD = ""
                        NomeMailContattoOLD = ""
                        NomeContattoOLD = ""
                        MailContattoOLD = ""
                        DataDemoOLD = ""
                        strEmailOLD = ""
                        strOraOLD = ""
                        strDemoOLD = ""
                        IdDemoOLD = 0

                    End If

                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IdDemoOLD = IdDemo

                If Not (DescrizioneOLD.Contains(Descrizione)) Then
                    DescrizioneOLD = DescrizioneOLD & ";" & Descrizione
                End If


                NomeMailContatto = "" & NomeContatto & " " & MailContatto & " "
                NomeMailContattoOLD = "" & NomeMailContattoOLD & NomeMailContatto & "<br>"

                MailContattoOLD = MailContattoOLD & ";" & MailContatto
                NomeContattoOLD = NomeContattoOLD & ";" & NomeContatto
                DataDemoOLD = DataDemo
                If Not (strEmailOLD.Contains(strEmail)) Then
                    strEmailOLD = strEmailOLD & ";" & strEmail
                End If
                strOraOLD = strOra
                strDemoOLD = strDemo
                IdDemoOLD = IdDemo

                y = y + 1

            Loop
        End If


        strEmailOLD.Split(";").Count

        MailPresales = ""
        For Each sMailPresales As String In strEmailOLD.Split(";")
            If sMailPresales <> "" Then
                MailPresales = sMailPresales
                If strTest.ToUpper().Trim().ToString() = "S" Then
                    MailPresales = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                End If

                Dim UsersID As String
                Dim UsersDesc As String
                Dim UsersEmail As String
                Dim Users() As String

                For Each presale As String In presales
                    Users = presale.Split(";")

                    If (presale.Contains(sMailPresales.ToString().Trim())) Then
                        UsersID = Users(0).ToString()
                        UsersDesc = Users(1).ToString()
                        UsersEmail = Users(2).ToString()
                        Console.Write(Users(0).ToString() & " " & Users(1).ToString() & " " & " " & Users(2).ToString() & " ")
                        Exit For
                    End If

                Next


                Dim testo As String
                testo = strDemoOLD & "<br>" & strOraOLD & "<br>" & NomeMailContattoOLD

                Try
                    Inviamail(strOggetto.Replace("#data", DataDemo), "", MailPresales, "", sw, testoHtml.Replace("#data", DataDemoOLD.ToString()).Replace("#nome", UsersDesc).Replace("#testo", testo).Replace("#ora", strOraOLD), DataDemoOLD, "", Nothing)
                    sw.WriteLine("Mail inviata a presale " & sMailPresales.ToString())

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailContatto)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                Catch ex As Exception
                    sw.WriteLine("Errore Invio mail a presale " & sMailPresales.ToString())

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailContatto)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                            .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                End Try



            End If

        Next

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function

    Private Function resetUserLocked() As Boolean

        Dim query As String
        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\resetUserLocked" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)

        query = "update Preventivi_Richieste set UserLock='', DataUserLock=null where UserLock<>'' and DATEDIFF(minute, DataUserLock, getdate())>30"
        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()
        Using comm As New SqlCommand()
            With comm
                .Connection = myConn2
                .CommandType = CommandType.Text
                .CommandText = query
            End With
            Try
                comm.ExecuteNonQuery()
            Catch ex As Exception
                Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))
                sw.WriteLine("*** ERRORE SU UserLocked " & DateTime.Now)
                sw.WriteLine(ex.Message.ToString())
                sw.Close()
            End Try
        End Using
        myConn2.Close()

    End Function


    Private Function InvioMailCommercialiDemo1to1Eseguita() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        strTest = "N"

        strTest = System.Configuration.ConfigurationManager.AppSettings("TESTpresales").ToString()

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio invio mail per i presales -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = "Reminder demo per il #data per i presales"
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNotificaCommercialiDemo1to1Eseguita'"
        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNotificaCommercialiDemo1to1Eseguita'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)

        ''Create a Command object.
        'strSQL = "set dateformat dmy;
        'Select IDCommerciale, max(ac.Nominativo) Commerciale, max(ac.Mail) MailCommerciale
        'From Storico_RichiesteDemo_COP s
        'Join Richieste_COP rc on rc.ID=s.IDRichiestaCop
        'Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale
        'Where DataConfermaEseguita >= Convert(varchar(10), getdate(), 103)
        'and DataConfermaEseguita < Convert(varchar(10), dateadd(day,1, getdate()), 103)
        '/*And ISNULL(s.note,'')='' */
        'And isnull(s.Eseguita,0)=1
        'group by IDCommerciale"

        strSQL = "set dateformat dmy;Select IDCommerciale, max(ac.Nominativo) Commerciale, max(ac.Mail) MailCommerciale 
        from RichiesteDemo_COP rd
        join Storico_RichiesteDemo_COP s on rd.IDRichiestaCop=s.IDRichiestaCop and rd.IDKey=s.IDKey and rd.Tabella=s.Tabella and rd.DataDemo = s.DataDemo and rd.OrarioDemo=s.OrarioDemo and rd.Progressivo=s.Progressivo
        Join Richieste_COP rc on rc.ID=s.IDRichiestaCop
        Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale
        --WHERE  rd.IDRichiestaCop IN (SELECT ID FROM Richieste_COP WHERE AziendaCliente LIKE 'SEIN%') and
        where s.Eseguita=1
        and DataConfermaEseguita >= Convert(varchar(10), getdate(), 103)
        and DataConfermaEseguita < Convert(varchar(10), dateadd(day,1, getdate()), 103) 
        And ISNULL(s.note,'')=''
        group by IDCommerciale"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        'Dim reformatted As String = DateTime.Now().AddDays(1).ToString("dd/MM/yyyy")


        Dim IdStoricoDemo As Integer
        Dim IdStoricoDemoOLD As Integer
        Dim IdDemo As Integer
        Dim IdDemoOLD As Integer
        Dim IdRichiestaOLD As Integer
        Dim y As Integer

        Dim Descrizione As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim NomeMailContatto As String
        Dim DataDemo As String
        Dim strEmail As String
        Dim strOra As String
        Dim strDemo As String
        Dim strVoce As String

        Dim DescrizioneOLD As String
        Dim NomeContattoOLD As String
        Dim MailContattoOLD As String
        Dim NomeMailContattoOLD As String
        Dim DataDemoOLD As String
        Dim strEmailOLD As String
        Dim strOraOLD As String
        Dim strDemoOLD As String
        Dim IDCommercialeOLD As Integer
        Dim strVoceOLD As String

        Dim MailCommerciale As String
        Dim Commerciale As String

        Dim IDRichiesta As Integer
        Dim IDCommerciale As Integer
        Dim NominativoComm As String
        Dim MailComm As String
        Dim NominativoCommOLD As String
        Dim MailCommOLD As String
        Dim TelefonoComm As String
        Dim AziendaComm As String

        Dim Str1 As String
        Dim Str2 As String

        Dim Str1OLD As String
        Dim Str2OLD As String

        IDRichiesta = 0
        IdRichiestaOLD = 0
        IDCommerciale = 0
        IDCommercialeOLD = 0
        NominativoComm = ""
        MailComm = ""
        NominativoCommOLD = ""
        MailCommOLD = ""
        TelefonoComm = ""
        AziendaComm = ""
        Str1 = ""
        Str2 = ""
        Str1OLD = ""
        Str2OLD = ""

        NomeMailContatto = ""
        NomeMailContattoOLD = ""

        NomeContatto = ""
        MailContatto = ""
        DataDemo = ""
        strEmail = ""
        strOra = ""
        strDemo = ""
        IdDemo = 0
        IdStoricoDemo = 0

        NomeContattoOLD = ""
        MailContattoOLD = ""
        DataDemoOLD = ""
        strEmailOLD = ""
        strOraOLD = ""
        strDemoOLD = ""
        IdDemoOLD = 0
        IdStoricoDemoOLD = 0
        strVoce = ""

        y = 0
        IdDemo = 0
        IdDemoOLD = 0
        IdStoricoDemo = 0
        IdStoricoDemoOLD = 0

        'myCmd.Parameters.AddWithValue("@data", reformatted)

        myConn.Open()
        Descrizione = ""
        DescrizioneOLD = ""
        strVoceOLD = ""

        myReader = myCmd.ExecuteReader()
        Dim codiceRichiesta As String
        Dim numRighe As Integer
        numRighe = 0
        codiceRichiesta = ""
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    numRighe = numRighe + 1

                    IDCommerciale = myReader.Item("IdCommerciale")
                    MailCommerciale = myReader.Item("MailCommerciale").ToString()
                    Commerciale = myReader.Item("Commerciale").ToString()

                    'IdStoricoDemo = myReader.Item("Id")
                    'DataDemo = myReader.Item("DataDemo").ToString()
                    'strOra = myReader.Item("OrarioDemo")
                    'MailCommerciale = myReader.Item("MailCommerciale").ToString()
                    'Commerciale = myReader.Item("Commerciale").ToString()
                    'AziendaCliente = myReader.Item("AziendaCliente").ToString()
                    'Argomento = myReader.Item("Argomento").ToString()

                    ''''''MailCommerciale = MailCommOLD
                    If strTest.ToUpper().Trim().ToString() = "S" Then
                        MailCommerciale = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        sw.WriteLine("Invio mail a commerciale " & Commerciale.ToString() & " in TEST")
                    Else
                        sw.WriteLine("Invio mail a commerciale " & Commerciale.ToString())
                    End If

                    Dim strSQLDettaglio As String
                    'strSQLDettaglio = "set dateformat dmy; "
                    'strSQLDettaglio = strSQLDettaglio & "Select Convert(varchar(10), DataDemo, 103) [DataDemo],[OrarioDemo], "
                    'strSQLDettaglio = strSQLDettaglio & "Case s.tabella "
                    'strSQLDettaglio = strSQLDettaglio & "when 'Preventivi_Sezioni' then (select sezione from Preventivi_Sezioni where ID=s.IDKey) "
                    'strSQLDettaglio = strSQLDettaglio & "when 'Preventivi_Funzioni' then (select Funzione from Preventivi_Funzioni where ID=s.IDKey) "
                    'strSQLDettaglio = strSQLDettaglio & "End Argomento, "
                    'strSQLDettaglio = strSQLDettaglio & "rc.AziendaCliente "
                    'strSQLDettaglio = strSQLDettaglio & "From Storico_RichiesteDemo_COP s "
                    'strSQLDettaglio = strSQLDettaglio & "Join Richieste_COP rc on rc.ID=s.IDRichiestaCop "
                    'strSQLDettaglio = strSQLDettaglio & "Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale "
                    'strSQLDettaglio = strSQLDettaglio & "Where DataConfermaEseguita >= Convert(varchar(10), getdate(), 103) "
                    'strSQLDettaglio = strSQLDettaglio & "and DataConfermaEseguita < Convert(varchar(10), dateadd(day,1, getdate()), 103) "
                    'strSQLDettaglio = strSQLDettaglio & "And isnull(s.Eseguita,0)=1 "
                    'strSQLDettaglio = strSQLDettaglio & "/*And ISNULL(s.note,'')='' */ "
                    'strSQLDettaglio = strSQLDettaglio & "And IDCommerciale = " & IDCommerciale.ToString() & " "
                    'strSQLDettaglio = strSQLDettaglio & "order by IDCommerciale "


                    strSQLDettaglio = "set dateformat dmy;Select Convert(varchar(10), rd.DataDemo, 103) DataDemo,rd.OrarioDemo, "
                    strSQLDettaglio = strSQLDettaglio & "Case s.tabella when 'Preventivi_Sezioni' then (select sezione from Preventivi_Sezioni where ID=s.IDKey) when 'Preventivi_Funzioni' then (select Funzione from Preventivi_Funzioni where ID=s.IDKey) End  Argomento, "
                    strSQLDettaglio = strSQLDettaglio & "rc.AziendaCliente "
                    strSQLDettaglio = strSQLDettaglio & "from RichiesteDemo_COP rd "
                    strSQLDettaglio = strSQLDettaglio & "join Storico_RichiesteDemo_COP s on rd.IDRichiestaCop=s.IDRichiestaCop and rd.IDKey=s.IDKey and rd.Tabella=s.Tabella and rd.DataDemo = s.DataDemo and rd.OrarioDemo=s.OrarioDemo and rd.Progressivo=s.Progressivo "
                    strSQLDettaglio = strSQLDettaglio & "Join Richieste_COP rc on rc.ID=s.IDRichiestaCop "
                    strSQLDettaglio = strSQLDettaglio & "Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale "
                    strSQLDettaglio = strSQLDettaglio & "where s.Eseguita=1 "
                    strSQLDettaglio = strSQLDettaglio & "and DataConfermaEseguita >= Convert(varchar(10), getdate(), 103) "
                    strSQLDettaglio = strSQLDettaglio & "and DataConfermaEseguita < Convert(varchar(10), dateadd(day,1, getdate()), 103) "
                    strSQLDettaglio = strSQLDettaglio & "And ISNULL(s.note,'')='' "
                    strSQLDettaglio = strSQLDettaglio & "And IDCommerciale = " & IDCommerciale.ToString() & " "





                    '
                    Dim sTabellaInteressi As String
                    sTabellaInteressi = ""
                    sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding:5px;text-align: left"">Azienda</th><th style=""padding:5px;text-align: left"">Argomento</th><th style=""padding:5px;text-align: left"">Data Demo</th>
<th style=""padding:5px;text-align: left"">Ora Demo</th>
</tr></thead><tbody>"
                    Dim myReaderDettaglio As SqlDataReader
                    Dim myCmdDettaglio As New SqlCommand
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    myCmdDettaglio = myConn2.CreateCommand
                    myCmdDettaglio.CommandText = strSQLDettaglio
                    myReaderDettaglio = myCmdDettaglio.ExecuteReader()
                    Do While myReaderDettaglio.Read()
                        'testoHtml = myReaderDettaglio.Item("DataDemo")
                        'testoHtml = myReaderDettaglio.Item("OrarioDemo")
                        'testoHtml = myReaderDettaglio.Item("Argomento")
                        'testoHtml = myReaderDettaglio.Item("AziendaCliente")

                        sTabellaInteressi = sTabellaInteressi + "<tr>"
                        sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & myReaderDettaglio.Item("AziendaCliente").ToString() & "</td>"
                        sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & myReaderDettaglio.Item("Argomento").ToString() & "</td>"
                        sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & myReaderDettaglio.Item("DataDemo").ToString() & "</td>"
                        sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & myReaderDettaglio.Item("OrarioDemo").ToString() & "</td>"
                        sTabellaInteressi = sTabellaInteressi + "</tr>"


                    Loop
                    myReaderDettaglio.Close()
                    myConn2.Close()
                    sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"

                    Dim testo As String
                    Dim testoHtml2 As String
                    testoHtml2 = ""
                    testo = strDemoOLD

                        Try

                        testoHtml2 = testoHtml.Replace("#intestazioneCommerciale", Commerciale).Replace("#TabellaDemo", sTabellaInteressi)

                        Inviamail(strOggetto.Replace("#data", DataDemo), "", MailCommerciale, "", sw, testoHtml2, DataDemo, "", Nothing)

                        sw.WriteLine("Mail inviata a commerciale " & MailCommerciale.ToString())

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                            Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailCommerciale)
                                .Parameters.AddWithValue("@CC", "")
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", 0)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml2)
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using

                        Catch ex As Exception
                            sw.WriteLine("Errore Invio mail a commerciali " & MailCommerciale.ToString())

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailCommerciale)
                                    .Parameters.AddWithValue("@CC", "")
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                    .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                    .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", 0)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml2)
                            End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex1 As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using

                        End Try

                        DescrizioneOLD = ""
                        NomeMailContattoOLD = ""
                        NomeContattoOLD = ""
                        MailContattoOLD = ""
                        DataDemoOLD = ""
                        strEmailOLD = ""
                        strOraOLD = ""
                        strDemoOLD = ""
                        IdDemoOLD = 0
                        IdStoricoDemoOLD = 0
                        IDCommercialeOLD = 0
                        IdRichiestaOLD = 0

                        NominativoCommOLD = ""
                        MailCommOLD = ""

                        Str1OLD = ""
                        Str2OLD = ""



                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IdStoricoDemoOLD = IdStoricoDemo
                IdDemoOLD = IdDemo
                IDCommercialeOLD = IDCommerciale
                MailCommOLD = MailComm
                IdRichiestaOLD = IDRichiesta

                If Not (DescrizioneOLD.Contains(Descrizione)) Then
                    DescrizioneOLD = DescrizioneOLD & ";" & Descrizione
                End If

                NominativoCommOLD = NominativoComm

                NomeMailContatto = "" & NomeContatto & " " & MailContatto & " "
                NomeMailContattoOLD = "" & NomeMailContattoOLD & NomeMailContatto & "<br>"


                Str1 = "<br>" & strDemo.ToUpper() & " " & strOra.ToUpper()
                Str2 = NomeMailContatto.ToLower()

                ''''''''''''''''strDemo = strDemo.ToUpper() & " " & strOra.ToUpper() ' & "<br>" & NomeMailContattoOLD.ToLower()

                MailContattoOLD = MailContattoOLD & ";" & MailContatto
                NomeContattoOLD = NomeContattoOLD & ";" & NomeContatto
                DataDemoOLD = DataDemo
                If Not (strEmailOLD.Contains(strEmail)) Then
                    strEmailOLD = strEmailOLD & ";" & strEmail
                End If
                strOraOLD = strOra

                If (Str1 <> Str1OLD) Then
                    If (strDemoOLD = "") Then
                        strDemoOLD = strDemoOLD & Str1 & "<br>" & Str2.ToLower()
                    Else
                        strDemoOLD = strDemoOLD & "<br>" & Str1 & "<br>" & Str2.ToLower()
                    End If
                Else
                    strDemoOLD = strDemoOLD & "<br>" & Str2.ToLower()
                End If

                IdStoricoDemoOLD = IdStoricoDemo
                IdDemoOLD = IdDemo
                strVoceOLD = strVoce

                Str1OLD = Str1
                Str2OLD = Str2

                y = y + 1

            Loop
        End If

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function



    Private Function InviomailCommerciali() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        strTest = "N"

        strTest = System.Configuration.ConfigurationManager.AppSettings("TESTpresales").ToString()

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ReminderCommercialiDemoToday" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio invio mail per i presales -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = "Reminder demo per il #data per i presales"
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNotificaCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNotificaCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)

        'Create a Command object.
        strSQL = "Set dateformat dmy Exec spGetDemoReminderCOMM @data"
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        'myCmd.Parameters.AddWithValue("@data", "2021-02-18")

        Dim reformatted As String = DateTime.Now().AddDays(1).ToString("dd/MM/yyyy")
        '''Dim reformatted As String = DateTime.Now().ToString("yyyy-MM-dd")
        ''''''''''''''''''''''''''''''''''''''''''''''Dim reformatted As String = DateTime.Now().ToString("dd/MM/yyyy")



        'reformatted = "2021-02-17"  'TEST
        Dim IdStoricoDemo As Integer
        Dim IdStoricoDemoOLD As Integer
        Dim IdDemo As Integer
        Dim IdDemoOLD As Integer
        Dim IdRichiestaOLD As Integer
        Dim y As Integer

        Dim Descrizione As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim NomeMailContatto As String
        Dim DataDemo As String
        Dim strEmail As String
        Dim strOra As String
        Dim strDemo As String
        Dim strVoce As String

        Dim DescrizioneOLD As String
        Dim NomeContattoOLD As String
        Dim MailContattoOLD As String
        Dim NomeMailContattoOLD As String
        Dim DataDemoOLD As String
        Dim strEmailOLD As String
        Dim strOraOLD As String
        Dim strDemoOLD As String
        Dim IDCommercialeOLD As Integer
        Dim strVoceOLD As String

        Dim MailCommerciale As String

        Dim IDRichiesta As Integer
        Dim IDCommerciale As Integer
        Dim NominativoComm As String
        Dim MailComm As String
        Dim NominativoCommOLD As String
        Dim MailCommOLD As String
        Dim TelefonoComm As String
        Dim AziendaComm As String

        Dim Str1 As String
        Dim Str2 As String

        Dim Str1OLD As String
        Dim Str2OLD As String

        IDRichiesta = 0
        IdRichiestaOLD = 0
        IDCommerciale = 0
        IDCommercialeOLD = 0
        NominativoComm = ""
        MailComm = ""
        NominativoCommOLD = ""
        MailCommOLD = ""
        TelefonoComm = ""
        AziendaComm = ""
        Str1 = ""
        Str2 = ""
        Str1OLD = ""
        Str2OLD = ""

        NomeMailContatto = ""
        NomeMailContattoOLD = ""

        NomeContatto = ""
        MailContatto = ""
        DataDemo = ""
        strEmail = ""
        strOra = ""
        strDemo = ""
        IdDemo = 0
        IdStoricoDemo = 0

        NomeContattoOLD = ""
        MailContattoOLD = ""
        DataDemoOLD = ""
        strEmailOLD = ""
        strOraOLD = ""
        strDemoOLD = ""
        IdDemoOLD = 0
        IdStoricoDemoOLD = 0
        strVoce = ""

        y = 0
        IdDemo = 0
        IdDemoOLD = 0
        IdStoricoDemo = 0
        IdStoricoDemoOLD = 0

        myCmd.Parameters.AddWithValue("@data", reformatted)

        myConn.Open()
        Descrizione = ""
        DescrizioneOLD = ""
        strVoceOLD = ""

        myReader = myCmd.ExecuteReader()
        Dim codiceRichiesta As String
        Dim numRighe As Integer
        numRighe = 0
        codiceRichiesta = ""
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    numRighe = numRighe + 1

                    IdStoricoDemo = myReader.Item("Id")

                    Descrizione = myReader.Item("Descrizione").ToString()
                    NomeContatto = myReader.Item("NomeContatto").ToString()
                    MailContatto = myReader.Item("MailContatto").ToString()
                    DataDemo = myReader.Item("DataDemoText").ToString()
                    strEmail = myReader.Item("mail")
                    strOra = myReader.Item("Ora")
                    strDemo = myReader.Item("voce")

                    IdDemo = myReader.Item("IdDemo")

                    IDRichiesta = myReader.Item("IDRichiesta")
                    codiceRichiesta = IDRichiesta.ToString("X")

                    IDCommerciale = myReader.Item("IDCommerciale")
                    NominativoComm = myReader.Item("NominativoComm").ToString()
                    MailComm = myReader.Item("MailComm").ToString()
                    TelefonoComm = myReader.Item("TelefonoComm").ToString()
                    AziendaComm = myReader.Item("AziendaComm").ToString()

                    Dim NomeAzienda As String
                    NomeAzienda = ""
                    NomeAzienda = myReader.Item("NomeAzienda").ToString()

                    If IdDemoOLD <> IdDemo Then
                        NomeMailContattoOLD = ""
                    End If

                    strVoce = myReader.Item("voce")

                    If IDCommerciale <> IDCommercialeOLD And y > 0 Then

                        Dim ContattiAgg As String
                        ContattiAgg = ""

                        Try

                            Dim myCmdContAgg As New SqlCommand
                            Dim strSqlContAgg As String
                            ContattiAgg = ""

                            strSqlContAgg = "Select Nome + ' ' + Cognome + ' ' + Mail + '<br>' Contatto from Preventivi_Richieste_Contatti where IDRichiesta= " & IdRichiestaOLD
                            strSqlContAgg = strSqlContAgg & " UNION Select Nominativo + ' ' + Mail + '<br>' Contatto from Storico_Demo_Contatti where IDStoricoDemo= " & IdStoricoDemoOLD

                            Try
                                myConn2 = New SqlConnection(strConnectionDB)
                                myConn2.Open()
                                myCmdContAgg = myConn2.CreateCommand
                                myCmdContAgg.CommandText = strSqlContAgg
                                myReader3 = myCmdContAgg.ExecuteReader()
                                Do While myReader3.Read()
                                    ContattiAgg = ContattiAgg & " " & myReader3.Item("Contatto").ToString() & "<br>"
                                Loop
                                myReader3.Close()
                                myConn2.Close()
                            Catch ex As Exception
                                sw.WriteLine("Errore su ContattiAgg -- " & ex.Message.ToString())
                                ContattiAgg = ""
                            End Try

                        Catch ex As Exception

                        End Try


                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''IDRichiesta = IdRichiestaOLD

                        MailCommerciale = MailCommOLD
                        If strTest.ToUpper().Trim().ToString() = "S" Then
                            MailCommerciale = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        End If

                        sw.WriteLine("Invio mail a commerciale " & MailCommerciale.ToString())

                        Dim testo As String

                        testo = strDemoOLD

                        Try

                            If ContattiAgg <> "" Then

                                If strDemoOLD.ToLower().Contains(Replace(MailContattoOLD.ToLower(), ";", "")) = False Then
                                    NomeMailContattoOLD = "" & NomeMailContattoOLD & ContattiAgg & "<br>"
                                Else
                                    NomeMailContattoOLD = "" & ContattiAgg & "<br>"
                                End If

                            End If

                            Inviamail(strOggetto.Replace("#data", DataDemo), "", MailCommerciale, "", sw, testoHtml.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", NominativoCommOLD).Replace("#testo", strDemoOLD & "<br><br>" & NomeMailContattoOLD), DataDemo, "", Nothing)
                            sw.WriteLine("Mail inviata a commerciale " & MailCommerciale.ToString())

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailCommerciale)
                                    .Parameters.AddWithValue("@CC", "")
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                    .Parameters.AddWithValue("@Esito", "OK")
                                    .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", 0)
                                    '.Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemo).Replace("#nome", NominativoCommOLD).Replace("#demo", strDemo).Replace("#ora", strOra).Replace("#testo", testo))
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", myReader.Item("DataDemoText").ToString()).Replace("#nome", NominativoCommOLD).Replace("#testo", strDemoOLD & "<br><br>" & NomeMailContattoOLD))

                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using

                        Catch ex As Exception
                            sw.WriteLine("Errore Invio mail a commerciali " & MailCommerciale.ToString())

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailCommerciale)
                                    .Parameters.AddWithValue("@CC", "")
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                    .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                    .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", 0)
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemo).Replace("#nome", NominativoCommOLD).Replace("#demo", strDemo).Replace("#ora", strOra).Replace("#testo", testo))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex1 As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using

                        End Try

                        DescrizioneOLD = ""
                        NomeMailContattoOLD = ""
                        NomeContattoOLD = ""
                        MailContattoOLD = ""
                        DataDemoOLD = ""
                        strEmailOLD = ""
                        strOraOLD = ""
                        strDemoOLD = ""
                        IdDemoOLD = 0
                        IdStoricoDemoOLD = 0
                        IDCommercialeOLD = 0
                        IdRichiestaOLD = 0

                        NominativoCommOLD = ""
                        MailCommOLD = ""

                        Str1OLD = ""
                        Str2OLD = ""

                    End If

                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IdStoricoDemoOLD = IdStoricoDemo
                IdDemoOLD = IdDemo
                IDCommercialeOLD = IDCommerciale
                MailCommOLD = MailComm
                IdRichiestaOLD = IDRichiesta

                If Not (DescrizioneOLD.Contains(Descrizione)) Then
                    DescrizioneOLD = DescrizioneOLD & ";" & Descrizione
                End If

                NominativoCommOLD = NominativoComm

                NomeMailContatto = "" & NomeContatto & " " & MailContatto & " "
                NomeMailContattoOLD = "" & NomeMailContattoOLD & NomeMailContatto & "<br>"


                Str1 = "<br>" & strDemo.ToUpper() & " " & strOra.ToUpper()
                Str2 = NomeMailContatto.ToLower()

                ''''''''''''''''strDemo = strDemo.ToUpper() & " " & strOra.ToUpper() ' & "<br>" & NomeMailContattoOLD.ToLower()

                MailContattoOLD = MailContattoOLD & ";" & MailContatto
                NomeContattoOLD = NomeContattoOLD & ";" & NomeContatto
                DataDemoOLD = DataDemo
                If Not (strEmailOLD.Contains(strEmail)) Then
                    strEmailOLD = strEmailOLD & ";" & strEmail
                End If
                strOraOLD = strOra

                If (Str1 <> Str1OLD) Then
                    If (strDemoOLD = "") Then
                        strDemoOLD = strDemoOLD & Str1 & "<br>" & Str2.ToLower()
                    Else
                        strDemoOLD = strDemoOLD & "<br>" & Str1 & "<br>" & Str2.ToLower()
                    End If
                Else
                    strDemoOLD = strDemoOLD & "<br>" & Str2.ToLower()
                End If

                IdStoricoDemoOLD = IdStoricoDemo
                IdDemoOLD = IdDemo
                strVoceOLD = strVoce

                Str1OLD = Str1
                Str2OLD = Str2

                y = y + 1

            Loop
        End If

        If numRighe > 0 Then

            MailCommerciale = MailCommOLD
            If strTest.ToUpper().Trim().ToString() = "S" Then
                MailCommerciale = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            End If

            sw.WriteLine("Invio mail a commerciale " & MailCommerciale.ToString())

            'If Not (strDemoOLD.Contains(strDemo)) Then
            '    strDemoOLD = strDemoOLD & "<br>" & strDemoOLD & " " & strOraOLD
            'End If

            'strDemoOLD = strDemoOLD & "<b>"

            Dim testo As String
            'testo = strDemoOLD & "<br>" & strOraOLD & "<br>" & NomeMailContattoOLD
            ''''''''''''''''''''testo = strDemoOLD & "<br><br>" & NomeMailContattoOLD

            testo = strDemoOLD

            Try
                Inviamail(strOggetto.Replace("#data", DataDemo), "", MailCommerciale, "", sw, testoHtml.Replace("#data", DataDemo.ToString()).Replace("#nome", NominativoCommOLD).Replace("#testo", testo).Replace("#ora", ""), DataDemo, "", Nothing)
                sw.WriteLine("Mail inviata a commerciale " & MailCommerciale.ToString())

                Dim query As String
                query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@Dest", MailCommerciale)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemo).Replace("#nome", NominativoCommOLD).Replace("#demo", strDemo).Replace("#ora", strOra).Replace("#testo", testo))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using

            Catch ex As Exception
                sw.WriteLine("Errore Invio mail a presale " & MailCommerciale.ToString())

                Dim query As String
                query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@Dest", MailCommerciale)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                        .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemo).Replace("#nome", NominativoCommOLD).Replace("#demo", strDemo).Replace("#ora", strOra).Replace("#testo", testo))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using

            End Try


        Else
            sw.WriteLine("Nessun invio per i commerciali -- " & DateTime.Now)
        End If


        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function


    Private Function ControlloRichiesteSenzaFunzioni() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio ControlloRichiesteSenzaFunzioni -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myCmd3 As New SqlCommand

        strOggetto = "Test ControlloRichiesteSenzaFunzioni"
        testoHtml = ""
        testoHtml = "<b>Query di controllo:<b> <br><br>"
        testoHtml = testoHtml & "set dateformat dmy; Select count(*) from Anagrafica a "
        testoHtml = testoHtml & "join Preventivi_Richieste pr on pr.ID=a.IDRichiesta <br> "
        'testoHtml = testoHtml & "where NomeContatto<>'' and NomeAzienda<>'' and MailContatto<>'' <br> "
        testoHtml = testoHtml & "where NomeContatto <>'' and NomeAzienda<>'' and MailContatto<>'' "
        testoHtml = testoHtml & "and (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) =0 "
        testoHtml = testoHtml & "and pr.id not in ( select idrichiesta from Storico_DemoClienti) "
        testoHtml = testoHtml & "and pr.id not in ( select idrichiesta from Richieste_COP) "

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()
        'Create a Command object.
        strSQL = "set dateformat dmy; Select count(*) Num from Anagrafica a join Preventivi_Richieste pr on pr.ID=a.IDRichiesta where NomeContatto<>'' and NomeAzienda<>'' and MailContatto<>'' and (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) =0 and pr.id not in ( select idrichiesta from Storico_DemoClienti) and pr.id not in ( select idrichiesta from Richieste_COP)"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        Dim codiceRichiesta As String
        Dim numRighe As Integer

        Dim sIDkey As String
        Dim sTabella As String
        Dim sDataDemo As String
        Dim sOrario As String

        Dim sTesto As String

        sTesto = ""
        sTesto = sTesto & "<br>Num Richieste senza funzioni<br>"

        numRighe = 0
        codiceRichiesta = ""
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    Try

                        sIDkey = myReader.Item("Num").ToString()
                        numRighe = myReader.Item("Num")

                        sTesto = sTesto & sIDkey + ";" + "<br>"

                    Catch ex As Exception
                        sw.WriteLine("Errore Invio mail ControlloRichiesteSenzaFunzioni ")
                    End Try
                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

            Loop
        End If

        testoHtml = testoHtml & "<br><br><br>" & "Richieste senza funzioni selezionate"

        testoHtml = testoHtml & "<br>" & sTesto

        'numRighe = Int32.Parse(myReader.Item("ret").ToString())

        If numRighe > 0 Then
            sw.WriteLine("Invio mail ControlloRichiesteSenzaFunzioni")
            Inviamail("Reminder DemoDriver: Rilevato problema su RICHIESTE SENZA FUNZIONI", "", "massimo.benini@zucchetti.it", "mandolfo@ebcconsulting.com;silvia.giovannini@zucchetti.it", sw, testoHtml, "", "", Nothing)
        Else
            sw.WriteLine("Messuna mail da inviare [ControlloRichiesteSenzaFunzioni]")
        End If

        sw.WriteLine("*** ControlloRichiesteSenzaFunzioni - RIGHE TROVATE = " & numRighe.ToString())

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function




    Private Function ControlloAnagDuplicate() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio ControlloAnagDuplicate -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myCmd3 As New SqlCommand

        strOggetto = "Test ControlloAnagDuplicate"
        testoHtml = ""
        testoHtml = "<b>Query di controllo:<b> <br><br>"
        testoHtml = testoHtml & "set dateformat dmy; Select pr.id, count(*) from Anagrafica a "
        testoHtml = testoHtml & "join Preventivi_Richieste pr on pr.ID=a.IDRichiesta <br> "
        testoHtml = testoHtml & "left join Anagrafica_Commerciali ac on ac.ID = pr.IDCommerciale <br> "
        testoHtml = testoHtml & "where NomeContatto <>'' and NomeAzienda<>'' and MailContatto<>'' "
        testoHtml = testoHtml & "and (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0 "
        testoHtml = testoHtml & "group by pr.id "
        testoHtml = testoHtml & "having count(*)>1 "

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()
        'Create a Command object.
        strSQL = "set dateformat dmy; Select pr.id, count(*) from Anagrafica a join Preventivi_Richieste pr on pr.ID=a.IDRichiesta left join Anagrafica_Commerciali ac on ac.ID = pr.IDCommerciale where NomeContatto<>'' and NomeAzienda<>'' and MailContatto<>'' and (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0 group by pr.id having count(*)>1"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        Dim codiceRichiesta As String
        Dim numRighe As Integer

        Dim sIDkey As String
        Dim sTabella As String
        Dim sDataDemo As String
        Dim sOrario As String

        Dim sTesto As String

        sTesto = ""
        sTesto = sTesto & "<br>IdAnagrafica<br>"

        numRighe = 0
        codiceRichiesta = ""
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    Try

                        sIDkey = myReader.Item("id").ToString()

                        sTesto = sTesto & sIDkey + ";" + "<br>"

                    Catch ex As Exception
                        sw.WriteLine("Errore Invio mail ControlloStoricoDemo ")
                    End Try
                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try
                numRighe = numRighe + 1

            Loop
        End If

        testoHtml = testoHtml & "<br><br><br>" & "Righe di Anagrafica duplicate per una richiesta"

        testoHtml = testoHtml & "<br>" & sTesto

        'numRighe = Int32.Parse(myReader.Item("ret").ToString())

        If numRighe > 0 Then
            sw.WriteLine("Invio mail ControlloStoricoDemo")
            Inviamail("Reminder DemoDriver: Rilevato problema su ANAGRAFICA DUPLICATA", "", "massimo.benini@zucchetti.it", "mandolfo@ebcconsulting.com;silvia.giovannini@zucchetti.it", sw, testoHtml, "", "", Nothing)
        Else
            sw.WriteLine("Messuna mail da inviare [ControlloAnagDuplicate]")
        End If

        sw.WriteLine("*** ControlloAnagDuplicate - RIGHE TROVATE = " & numRighe.ToString())

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function
    Private Function ControlloIdCommerciale() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ControlloIdCommerciale" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio ControlloIdCommerciale -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myCmd3 As New SqlCommand

        strOggetto = "Test ControlloIdCommerciale"
        testoHtml = ""
        'testoHtml = "<b>Query di controllo:<b> <br><br>"
        'testoHtml = testoHtml & "Select c.IDKey, C.Tabella, c.DataDemo, C.Note, C.Note2, s.IDkey, S.Tabella, S.DataDemo, S.Orario,* <br> "
        'testoHtml = testoHtml & "From storico_democlienti s <br> "
        'testoHtml = testoHtml & "Left Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo And c.Note=s.orario) <br> "
        'testoHtml = testoHtml & "WHERE C.IDKEY Is NULL "

        testoHtml = testoHtml & "Select rc.ID, rc.IDCommerciale, (Select ID from Anagrafica_Commerciali where Nominativo = rc.RagioneSocialePartner), ac.Nominativo, rc.RagioneSocialePartner, * from Richieste_COP rc "
        testoHtml = testoHtml & "Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale "
        testoHtml = testoHtml & "where ac.Nominativo <> rc.RagioneSocialePartner "
        testoHtml = testoHtml & "And (select ID from Anagrafica_Commerciali where Nominativo = rc.RagioneSocialePartner) Is Not null "
        testoHtml = testoHtml & "order by rc.DataRichiesta desc "


        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()
        'Create a Command object.
        ''strSQL = "select count(*) ret FROM storico_democlienti s left join Calendario_Demo c on (c.IDKey = s.IDkey and c.Tabella=s.tabella and c.DataDemo = s.DataDemo and c.Note=s.orario) WHERE C.IDKEY IS NULL"
        'strSQL = "Select c.IDKey, C.Tabella, c.DataDemo, C.Note, C.Note2, s.IDkey sIDkey, S.Tabella sTabella, S.DataDemo sDataDemo, S.Orario sOrario,* FROM storico_democlienti s left join Calendario_Demo c on (c.IDKey = s.IDkey and c.Tabella=s.tabella and c.DataDemo = s.DataDemo and c.Note=s.orario) WHERE C.IDKEY IS NULL"


        strSQL = "Select rc.ID, rc.IDCommerciale IDCommerciale, (Select ID from Anagrafica_Commerciali where Nominativo = rc.RagioneSocialePartner) IDCommerciale1, ac.Nominativo Nominativo, rc.RagioneSocialePartner Nominativo1, * from Richieste_COP rc "
        strSQL = strSQL + "Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale "
        strSQL = strSQL + "where ac.Nominativo <> rc.RagioneSocialePartner "
        strSQL = strSQL + "And (select ID from Anagrafica_Commerciali where Nominativo = rc.RagioneSocialePartner) Is Not null "
        strSQL = strSQL + "order by rc.DataRichiesta desc "


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        Dim codiceRichiesta As String
        Dim numRighe As Integer

        Dim sID As String
        Dim sIDCommerciale As String
        Dim sIDCommerciale1 As String
        Dim sNominativo1 As String
        Dim sNominativo As String

        Dim sTesto As String

        sTesto = ""
        sTesto = sTesto & "<br>ID;IDCommerciale;IDCommerciale1;Nominativo1;Nominativo<br>"

        numRighe = 0
        codiceRichiesta = ""
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    Try

                        sID = myReader.Item("ID").ToString()
                        sIDCommerciale = myReader.Item("IDCommerciale").ToString()
                        sIDCommerciale1 = myReader.Item("IDCommerciale1").ToString()
                        sNominativo1 = myReader.Item("Nominativo1").ToString()
                        sNominativo = myReader.Item("Nominativo").ToString()

                        sTesto = sTesto & sID + ";" + sIDCommerciale + ";" + sIDCommerciale1 + ";" + sNominativo1 + ";" + sNominativo + "<br>"

                    Catch ex As Exception
                        sw.WriteLine("Errore Invio mail ControlloIdCommerciale ")
                    End Try
                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try
                numRighe = numRighe + 1

            Loop
        End If

        testoHtml = testoHtml & "<br><br><br><br>" & "Problema su IdCommerciale"

        testoHtml = testoHtml & "<br>" & sTesto

        'numRighe = Int32.Parse(myReader.Item("ret").ToString())

        If numRighe > 0 Then
            sw.WriteLine("Invio mail ControlloIdCommerciale")
            Inviamail("Reminder DemoDriver: Rilevato problema su IDCOMMERCIALE", "", "massimo.benini@zucchetti.it", "mandolfo@ebcconsulting.com", sw, testoHtml, "", "", Nothing)
        Else
            sw.WriteLine("Messuna mail da inviare [ControlloIdCommerciale]")
        End If

        sw.WriteLine("*** ControlloIdCommerciale - RIGHE TROVATE = " & numRighe.ToString())

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function

    Private Function PopolaTabellaEventiAziende(IdAzienda As Integer, sw As StreamWriter) As Boolean

        Dim strSQL As String
        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\PopolaTabellaEventiAziende" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)

        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()

        strSQL = "Exec sp_ElencoEventiAzienda @idazienda, @data1, @data2"

        myCmd = myConn2.CreateCommand
        myCmd.CommandText = strSQL

        myCmd.Parameters.Add("@idazienda", SqlDbType.Int).Value = IdAzienda
        myCmd.Parameters.Add("@data1", SqlDbType.DateTime).Value = "01/01/1900"
        myCmd.Parameters.Add("@data2", SqlDbType.DateTime).Value = Now


        Dim IdRichiesta As String
        Dim Tipo As String
        Dim DataEvento As Date
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim NoteDemo As String
        Dim Demo As String
        Dim Anno As Integer
        Dim NumSettimana As Integer
        Dim IdCommerciale As Integer
        Dim partecipantiweekly As Integer
        Dim PresaleRiferimento As String

        myReader = Nothing
        myReader = myCmd.ExecuteReader()
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    Try

                        IdRichiesta = myReader.Item("IdRichiesta").ToString()
                        Tipo = myReader.Item("Tipo").ToString()
                        DataEvento = Convert.ToDateTime(myReader.Item("DataEvento").ToString())
                        NomeAzienda = myReader.Item("NomeAzienda").ToString()
                        NomeContatto = myReader.Item("NomeContatto").ToString()
                        MailContatto = myReader.Item("MailContatto").ToString()
                        NoteDemo = myReader.Item("NoteDemo").ToString()
                        Demo = myReader.Item("Demo").ToString()
                        Anno = Convert.ToInt32(myReader.Item("Anno").ToString())
                        NumSettimana = Convert.ToInt32(myReader.Item("NumSettimana").ToString())
                        IdCommerciale = Convert.ToInt32(myReader.Item("IdCommerciale").ToString())
                        partecipantiweekly = Convert.ToInt32(myReader.Item("partecipantiweekly").ToString())
                        PresaleRiferimento = myReader.Item("PresaleRiferimento").ToString()

                        'Dim strSQL1 As String

                        ''seleziono i product manager
                        'strSQL1 = "Select count(*) Ret from TabellaEventiAziende "
                        'If Tipo = "Configurazione" Then
                        '    strSQL1 = strSQL1 & "where IDRichiesta = @IDRichiesta AND IDTipo = 1 "
                        'End If
                        ''strSQL1 = strSQL1 & "where IDTipo = @Tipo and IdAzienda=@IDAZIENDA2 and year(DataEvento)=@year and month(DataEvento)=@month and day(DataEvento)=@day "
                        ''strSQL1 = strSQL1 & "And NomeAzienda=@NomeAzienda "
                        ''strSQL1 = strSQL1 & "And NoteDemo=@NoteDemo "

                        'Dim myConn1 As SqlConnection
                        'myConn1 = New SqlConnection(strConnectionDB)
                        'myConn1.Open()

                        'myCmdGEN = myConn1.CreateCommand
                        'myCmdGEN.CommandText = strSQL1
                        'myCmdGEN.CommandTimeout = 240

                        Select Case Tipo
                            Case "Configurazione"
                                Tipo = "1"
                            Case "Demo Weekly"
                                Tipo = "2"
                            Case "Demo 1to1"
                                Tipo = "3"
                        End Select

                        'myCmdGEN.Parameters.Add("IDRichiesta", SqlDbType.Int).Value = IdRichiesta

                        ''myCmdGEN.Parameters.Add("@Tipo", SqlDbType.VarChar).Value = Tipo
                        ''myCmdGEN.Parameters.Add("@idazienda2", SqlDbType.Int).Value = IdAzienda
                        ''myCmdGEN.Parameters.Add("@year", SqlDbType.Int).Value = Convert.ToDateTime(DataEvento).Year()
                        ''myCmdGEN.Parameters.Add("@month", SqlDbType.Int).Value = Convert.ToDateTime(DataEvento).Month()
                        ''myCmdGEN.Parameters.Add("@day", SqlDbType.Int).Value = Convert.ToDateTime(DataEvento).Day()
                        ''myCmdGEN.Parameters.Add("@NomeAzienda", SqlDbType.VarChar).Value = NomeAzienda
                        ''myCmdGEN.Parameters.Add("@NoteDemo", SqlDbType.VarChar).Value = NoteDemo

                        'Dim myReader1 As SqlDataReader
                        'Dim ret As Integer

                        'myReader1 = myCmdGEN.ExecuteReader()

                        'Do While myReader1.Read()
                        '    ret = myReader1.Item("ret")
                        'Loop
                        'myReader1.Close()

                        'If ret = 0 Then

                        sw.WriteLine("Inserita riga NomeAzienda = " & NomeAzienda & " Tipo = " & Tipo & " DataEvento = " & DataEvento.ToShortDateString.ToString())

                        Dim query As String
                        query = "INSERT INTO TabellaEventiAziende (IdRichiesta,IDTipo,DataEvento,IdAzienda,NomeAzienda,NomeContatto,MailContatto,NoteDemo,Demo,Anno,NumSettimana,idcommerciale,partecipantiweekly,PresaleRiferimento) "
                        query = query & "VALUES (@IdRichiesta,@IDTipo,@DataEvento,@IdAzienda,@NomeAzienda,@NomeContatto,@MailContatto,@NoteDemo,@Demo,@Anno,@NumSettimana,@idcommerciale,@partecipantiweekly,@PresaleRiferimento)"
                        myConn3 = New SqlConnection(strConnectionDB)
                        myConn3.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn3
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@IdRichiesta", IdRichiesta)
                                .Parameters.AddWithValue("@IDTipo", Tipo)
                                .Parameters.AddWithValue("@DataEvento", DataEvento)
                                .Parameters.AddWithValue("@IdAzienda", IdAzienda)
                                .Parameters.AddWithValue("@NomeAzienda", NomeAzienda)
                                .Parameters.AddWithValue("@NomeContatto", NomeContatto)
                                .Parameters.AddWithValue("@MailContatto", MailContatto)
                                .Parameters.AddWithValue("@NoteDemo", NoteDemo)
                                .Parameters.AddWithValue("@Demo", Demo)
                                .Parameters.AddWithValue("@Anno", Anno)
                                .Parameters.AddWithValue("@NumSettimana", NumSettimana)
                                .Parameters.AddWithValue("@idcommerciale", idcommerciale)
                                .Parameters.AddWithValue("@partecipantiweekly", partecipantiweekly)
                                .Parameters.AddWithValue("@PresaleRiferimento", PresaleRiferimento)
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("Errore PopolatabellaEventiAziende " & ex.Message)
                            End Try
                        End Using
                        myConn3.Close()

                        'End If

                        'myConn1.Close()


                    Catch ex As Exception
                        sw.WriteLine("Errore PopolatabellaEventiAziende " & ex.Message.ToString())
                    End Try
                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

            Loop
        End If


        'myCmd3.CommandText = "Exec spSTAT_PopolaTargetPresale @idutentepresale"
        'myCmd3.Parameters.AddWithValue("@idutentepresale", CInt(ID1.ToString()))


        'Close the reader and the database connection.
        myReader.Close()
        myConn2.Close()


        Me.Cursor = Cursors.Default

        Exit Function

    End Function




    Private Function ControlloAziende() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio ControlloAziende -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myCmd3 As New SqlCommand

        strOggetto = "Test ControlloAziende"
        testoHtml = ""
        testoHtml = "<b>Query di controllo:<b> <br><br>"
        testoHtml = testoHtml & "set dateformat dmy;
select r.IDAzienda, az.ID, r.id, r.NomeAzienda, convert(varchar(10), r.data,103) , az.NomeAzienda,az.Dominio, az.MailContatto, r.MailContatto, * from Aziende az
join  Preventivi_Richieste r on r.IDAzienda=az.ID
where (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=r.ID) >0 
and r.NomeAzienda<>'' and r.MailContatto<>'' and r.NomeContatto<>''
and ltrim(rtrim(az.NomeAzienda)) <> ltrim(rtrim(r.NomeAzienda))
and 
r.Data>=convert(varchar(10), dateadd(day,-1, getdate()),103)
"

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()
        'Create a Command object.
        'strSQL = "select count(*) ret FROM storico_democlienti s left join Calendario_Demo c on (c.IDKey = s.IDkey and c.Tabella=s.tabella and c.DataDemo = s.DataDemo and c.Note=s.orario) WHERE C.IDKEY IS NULL"
        strSQL = "set dateformat dmy;
select r.IDAzienda IDAziendaR, az.ID IDAziendaA, r.id IDRichiesta, r.NomeAzienda NomeAziendaR, convert(varchar(10), r.data,103) DataRichiesta , az.NomeAzienda NomeAziendaA,az.Dominio, az.MailContatto, r.MailContatto, * from Aziende az
join  Preventivi_Richieste r on r.IDAzienda=az.ID
where (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=r.ID) >0 
and r.NomeAzienda<>'' and r.MailContatto<>'' and r.NomeContatto<>''
and ltrim(rtrim(az.NomeAzienda)) <> ltrim(rtrim(r.NomeAzienda))
and 
r.Data>=convert(varchar(10), dateadd(day,-1, getdate()),103)"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        Dim codiceRichiesta As String
        Dim numRighe As Integer

        Dim NomeAziendaR As String
        Dim NomeAziendaa As String
        Dim IdAziendaR As String
        Dim IdAziendaA As String

        Dim sTesto As String

        sTesto = ""
        sTesto = sTesto & "<br>NomeAziendaR;NomeAziendaA;IdAziendaR;IdAziendaA<br>"

        numRighe = 0
        codiceRichiesta = ""
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    Try

                        NomeAziendaR = myReader.Item("NomeAziendaR").ToString()
                        NomeAziendaa = myReader.Item("NomeAziendaa").ToString()
                        IdAziendaR = myReader.Item("IdAziendaR").ToString()
                        IdAziendaA = myReader.Item("IdAziendaA").ToString()

                        sTesto = sTesto & NomeAziendaR + ";" + NomeAziendaa + ";" + IdAziendaR + ";" + IdAziendaA + "<br><br>"

                    Catch ex As Exception
                        sw.WriteLine("Errore Invio mail ControlloAziende ")
                    End Try
                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try
                numRighe = numRighe + 1

            Loop
        End If

        testoHtml = testoHtml & "<br>" & sTesto

        'numRighe = Int32.Parse(myReader.Item("ret").ToString())

        If numRighe > 0 Then
            sw.WriteLine("Invio mail ControlloAziende")
            Inviamail("Reminder DemoDriver: Rilevato problema su ControlloAziende", "", "massimo.benini@zucchetti.it", "mandolfo@ebcconsulting.com", sw, testoHtml, "", "", Nothing)
        Else
            sw.WriteLine("Messuna mail da inviare [ControlloAziende]")
        End If

        sw.WriteLine("*** ControlloAziende - RIGHE TROVATE = " & numRighe.ToString())

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function


    Private Function ControlloStoricoDemo() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        Dim strTest As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio ControlloStoricoDemo -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        strSQL = ""
        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "
        myConn = New SqlConnection(strConnectionDB)
        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myConn.Open()
        Dim myReader1 As SqlDataReader
        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myCmd3 As New SqlCommand

        strOggetto = "Test ControlloStoricoDemo"
        testoHtml = ""
        testoHtml = "<b>Query di controllo:<b> <br><br>"
        testoHtml = testoHtml & "Select c.IDKey, C.Tabella, c.DataDemo, C.Note, C.Note2, s.IDkey, S.Tabella, S.DataDemo, S.Orario,* <br> "
        testoHtml = testoHtml & "From storico_democlienti s <br> "
        testoHtml = testoHtml & "Left Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo And c.Note=s.orario) <br> "
        testoHtml = testoHtml & "WHERE C.IDKEY Is NULL "

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()
        'Create a Command object.
        'strSQL = "select count(*) ret FROM storico_democlienti s left join Calendario_Demo c on (c.IDKey = s.IDkey and c.Tabella=s.tabella and c.DataDemo = s.DataDemo and c.Note=s.orario) WHERE C.IDKEY IS NULL"
        strSQL = "Select c.IDKey, C.Tabella, c.DataDemo, C.Note, C.Note2, s.IDkey sIDkey, S.Tabella sTabella, S.DataDemo sDataDemo, S.Orario sOrario,* FROM storico_democlienti s left join Calendario_Demo c on (c.IDKey = s.IDkey and c.Tabella=s.tabella and c.DataDemo = s.DataDemo and c.Note=s.orario) WHERE C.IDKEY IS NULL"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        Dim codiceRichiesta As String
        Dim numRighe As Integer

        Dim sIDkey As String
        Dim sTabella As String
        Dim sDataDemo As String
        Dim sOrario As String

        Dim sTesto As String

        sTesto = ""
        sTesto = sTesto & "<br>IDkey;Tabella;DataDemo;Orario<br>"

        numRighe = 0
        codiceRichiesta = ""
        If myReader.HasRows Then
            Do While myReader.Read

                Try
                    Try

                        sIDkey = myReader.Item("sIDkey").ToString()
                        sTabella = myReader.Item("sTabella").ToString()
                        sDataDemo = myReader.Item("sDataDemo").ToString()
                        sOrario = myReader.Item("sOrario").ToString()

                        sTesto = sTesto & sIDkey + ";" + sTabella + ";" + sDataDemo + ";" + sOrario + "<br>"

                    Catch ex As Exception
                        sw.WriteLine("Errore Invio mail ControlloStoricoDemo ")
                    End Try
                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try
                numRighe = numRighe + 1

            Loop
        End If

        testoHtml = testoHtml & "<br><br><br>" & "Righe di storico_democlienti senza riga collegata in Calendario_Demo"

        testoHtml = testoHtml & "<br>" & sTesto

        'numRighe = Int32.Parse(myReader.Item("ret").ToString())

        If numRighe > 0 Then
            sw.WriteLine("Invio mail ControlloStoricoDemo")
            Inviamail("Reminder DemoDriver: Rilevato problema su STORICO DEMO", "", "massimo.benini@zucchetti.it", "mandolfo@ebcconsulting.com", sw, testoHtml, "", "", Nothing)
        Else
            sw.WriteLine("Messuna mail da inviare [ControlloStoricoDemo]")
        End If

        sw.WriteLine("*** ControlloStoricoDemo - RIGHE TROVATE = " & numRighe.ToString())

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        'checkCalendar = True

        Exit Function

    End Function

    Private Function BackupStoricoDemo() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio ControlloStoricoDemo -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String


        Dim myCmd3 As New SqlCommand

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()
        'Create a Command object.
        strSQL = "DECLARE @Q VARCHAR(MAX) SET @Q='SELECT * INTO storico_democlienti' + Convert(CHAR(8),GETDATE(),112) + ' FROM  storico_democlienti' SELECT @Q EXEC(@Q)"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        sw.WriteLine("*** Backup StoricoDemoClienti")

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine importazione ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        Exit Function

    End Function

    Private Function InviaMailNONPartecipantiDemo(pData As Date) As Boolean


        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDStoricoDemoClienti As String
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        Dim Commerciale As String
        Dim Argomento As String

        Dim DataDemo As Date
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        'Dim IDKey As Int32
        'Dim Tabella As String

        retImp = False

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & pData & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        'Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail ai NON partecipanti alle demo del " & strData & " -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNONPartecipazioneDemoGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNONPartecipazioneDemoGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim ret As Integer

        ret = 0

        strSQL = "Select count(*) ret from Storico_DemoClienti s "
        strSQL = strSQL & "Join InfoSalutiDemoAssenti ass On (ass.IDKey=s.IDKey And ass.Tabella=s.Tabella And isnull(ass.AbilitaInvio, 0)=1) "
        'strSQL = strSQL & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = Convert(NVARCHAR, GETDATE(), 103)"
        strSQL = strSQL & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = " & strData

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        If myReader.HasRows Then
            Do While myReader.Read
                ret = myReader.Item("ret").ToString()
            Loop
        End If
        myReader.Close()
        myReader = Nothing

        If ret = 0 Then
            InviaMailNONPartecipantiDemo = False
            'sw.WriteLine("Nessuna mail a NON partecipanti da inviare -- " & DateTime.Now)
            'sw.Close()
            Exit Function
        End If


        Dim idKey1 As Integer
        Dim tabella1 As String
        Dim datademo1 As Date
        Dim orario1 As String

        idKey1 = 0
        tabella1 = ""
        datademo1 = Now
        orario1 = ""



        strSQLgen = "set dateformat dmy "
        strSQLgen = strSQLgen & "select S.IDKey,S.Tabella,Convert(NVARCHAR, S.DataDemo, 103) DataDemo, Orario from Storico_DemoClienti s "
        strSQLgen = strSQLgen & "Join InfoSalutiDemoAssenti ass On (ass.IDKey=s.IDKey And ass.Tabella=s.Tabella And isnull(ass.AbilitaInvio, 0)=1) "
        'strSQLgen = strSQLgen & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = Convert(NVARCHAR, GETDATE(), 103) "
        strSQLgen = strSQLgen & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = " & strData
        strSQLgen = strSQLgen & "group by s.IDKey, s.Tabella, S.DataDemo, s.Orario"
        myCmdGEN = myConn2.CreateCommand
        myCmdGEN.CommandText = strSQLgen

        myReaderGEN = myCmdGEN.ExecuteReader()
        If myReaderGEN.HasRows Then
            Do While myReaderGEN.Read
                idKey1 = myReaderGEN.Item("IDKey")
                tabella1 = myReaderGEN.Item("Tabella").ToString()
                datademo1 = myReaderGEN.Item("DataDemo").ToString()
                orario1 = myReaderGEN.Item("Orario").ToString()

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                strSQL = "set dateformat dmy "
                strSQL = strSQL & "Select s.ID, pr.NomeAzienda, pr.NomeContatto, pr.MailContatto, "
                strSQL = strSQL & "isnull(a.Nominativo + ' ' + a.Mail + '', "
                strSQL = strSQL & "( "
                strSQL = strSQL & "Select top 1 a.Nominativo + ' ' + a.Mail + '' from preventivi_richieste r "
                strSQL = strSQL & "Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale "
                strSQL = strSQL & "where "
                strSQL = strSQL & "r.NomeAzienda = pr.NomeAzienda And "
                strSQL = strSQL & "IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc "
                strSQL = strSQL & ") "
                strSQL = strSQL & ") as Commerciale, "
                strSQL = strSQL & "Case "
                strSQL = strSQL & "WHEN ass.Tabella='Preventivi_Sezioni' THEN isnull(s.NoteDemo, sez.Sezione) "
                strSQL = strSQL & "WHEN ass.Tabella='Preventivi_Funzioni' THEN isnull(s.NoteDemo,funz.Funzione) "
                strSQL = strSQL & "End Argomento, "
                strSQL = strSQL & "Convert(varchar, S.DATADEMO, 103) DataDemo, S.ORARIO	"
                strSQL = strSQL & "From Storico_DemoClienti s "
                strSQL = strSQL & "Join InfoSalutiDemoAssenti ass on (ass.IDKey=s.IDKey And ass.Tabella=s.Tabella And ass.NoteDemo=s.NoteDemo And isnull(ass.AbilitaInvio,0)=1) "
                strSQL = strSQL & "Join Preventivi_Richieste pr on pr.id=s.IDRichiesta "
                strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = pr.IDCommerciale "
                strSQL = strSQL & "Left Join Preventivi_Sezioni sez on sez.ID=s.IDKey "
                strSQL = strSQL & "Left Join Preventivi_Funzioni funz on funz.ID=s.IDKey "
                strSQL = strSQL & "where isnull(s.completata, 0) = 0 "
                strSQL = strSQL & "And isnull(s.SendPartecip2Guest,0)=0 "
                strSQL = strSQL & "And isnull(s.SendNONPartecip2Guest,0)=0 "

                strSQL = strSQL & "And s.IDKey=" & idKey1.ToString()
                strSQL = strSQL & "And s.Tabella='" & tabella1 & "' "
                strSQL = strSQL & "And s.datademo='" & datademo1 & "' "
                strSQL = strSQL & "And s.Orario='" & orario1 & "' "

                strSQL = strSQL & "And pr.NomeAzienda<>'' AND pr.NomeContatto<>'' AND pr.MailContatto<>'' "

                myCmd = myConn.CreateCommand
                myCmd.CommandText = strSQL

                myReader = myCmd.ExecuteReader()

                strContatto = ""
                strMailGuest = ""
                strPresale = ""
                strDataDemoTxt = ""
                IDRichiestaOLD = 0
                IDRichiesta = 0
                IDStoricoDemoClienti = ""

                If myReader.HasRows Then
                    Do While myReader.Read

                        Try

                            IDStoricoDemoClienti = myReader.Item("ID").ToString()
                            NomeAzienda = myReader.Item("NomeAzienda").ToString()
                            NomeContatto = myReader.Item("NomeContatto").ToString()
                            MailContatto = myReader.Item("MailContatto").ToString()
                            Commerciale = myReader.Item("Commerciale").ToString()
                            Argomento = myReader.Item("Argomento").ToString()
                            strDemo = myReader.Item("Argomento").ToString()
                            DataDemo = myReader.Item("DataDemo")

                            strOra = myReader.Item("ORARIO").ToString()

                            Dim strTest As String
                            Dim strDest As String

                            strDest = ""
                            strTest = System.Configuration.ConfigurationManager.AppSettings("TESTGuestTomorrow").ToString()

                            '**************************************************************'
                            '*************** INVIO EMAIL PARTECIPAZIONE DEMO **************'
                            '**************************************************************'
                            MailContattoAgg = ""

                            If strTest.ToUpper().Trim().ToString() = "S" Then

                                strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                                'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                                MailContatto = strDest 'TEST

                            End If


                            Dim bInviaMail As Boolean

                            bInviaMail = True
                            Dim myReader4 As SqlDataReader
                            Dim myCmd4 As New SqlCommand

                            Dim oDate As DateTime
                            oDate = Convert.ToDateTime(DataDemo)
                            oDate = oDate.AddDays(-1)

                            Dim myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            bInviaMail = False

                            Dim strSqlCtr As String
                            strSqlCtr = "INVIO MAIL NON PARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & Argomento & " del " & DataDemo

                            myCmd4 = myConn2.CreateCommand
                            myCmd4.CommandText = "Select * from LogMailOperation where Oggetto='Demo Driver Zucchetti: Abbiamo altre date disponibili' and DestinatarioPrincipale=@DestinatarioPrincipale and Note=@note and IDRichiesta = " & IDStoricoDemoClienti
                            myCmd4.Parameters.Add("@DestinatarioPrincipale", SqlDbType.VarChar).Value = MailContatto
                            myCmd4.Parameters.Add("@note", SqlDbType.VarChar).Value = strSqlCtr
                            myReader4 = myCmd4.ExecuteReader()
                            If myReader4.HasRows = False Then
                                bInviaMail = True
                            End If

                            myReader4.Close()
                            myConn2.Close()

                            MailContattoAgg = ""
                            If bInviaMail = True Then

                                Try

                                    Inviamail(strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo), "", MailContatto, MailContattoAgg, Nothing, testoHtml.Replace("#data", DataDemo).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra).Replace("#commerciale", Commerciale), DataDemo, "", Nothing)
                                    'sw.WriteLine("Invio email partecipazione demo a " & MailContatto & " " & DateTime.Now)

                                    Dim query As String
                                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                                    myConn3 = New SqlConnection(strConnectionDB)
                                    myConn3.Open()
                                    Using comm As New SqlCommand()
                                        With comm
                                            .Connection = myConn3
                                            .CommandType = CommandType.Text
                                            .CommandText = query
                                            .Parameters.AddWithValue("@Dest", MailContatto)
                                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                            .Parameters.AddWithValue("@Esito", "OK")
                                            .Parameters.AddWithValue("@Note", "INVIO MAIL NON PARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & strDemo & " del " & DataDemo)
                                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                                            .Parameters.AddWithValue("@IDRichiesta", CInt(IDStoricoDemoClienti))
                                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemo).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#ora", strOra).Replace("#commerciale", Commerciale))
                                        End With
                                        Try
                                            comm.ExecuteNonQuery()
                                        Catch ex As Exception
                                            'sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                        End Try
                                    End Using
                                    myConn3.Close()


                                Catch ex As Exception
                                    'sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                                    Dim query As String
                                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                                    myConn3 = New SqlConnection(strConnectionDB)
                                    myConn3.Open()
                                    Using comm As New SqlCommand()
                                        With comm
                                            .Connection = myConn3
                                            .CommandType = CommandType.Text
                                            .CommandText = query
                                            .Parameters.AddWithValue("@Dest", MailContatto)
                                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                            .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                            .Parameters.AddWithValue("@Note", "INVIO MAILPARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & strDemo)
                                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                                            .Parameters.AddWithValue("@IDRichiesta", CInt(IDStoricoDemoClienti))
                                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                        End With
                                        Try
                                            comm.ExecuteNonQuery()
                                        Catch ex1 As Exception
                                            'sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                        End Try
                                    End Using
                                    myConn3.Close()


                                End Try

                                ' settare a 1 il flag SendNONPartecip2Guest
                                ' update StoricoDemoClienti set SendNONPartecip2Guest=1 where ID=IDStoricoDemoClienti

                                Dim queryU As String
                                queryU = "update Storico_DemoClienti set SendNONPartecip2Guest=1 where ID=@IDStoricoDemoClienti"
                                myConn3 = New SqlConnection(strConnectionDB)
                                myConn3.Open()
                                Using comm As New SqlCommand()
                                    With comm
                                        .Connection = myConn3
                                        .CommandType = CommandType.Text
                                        .CommandText = queryU
                                        .Parameters.AddWithValue("@IDStoricoDemoClienti", CInt(IDStoricoDemoClienti))
                                    End With
                                    Try
                                        comm.ExecuteNonQuery()
                                    Catch ex1 As Exception
                                        'sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                    End Try
                                End Using
                                myConn3.Close()

                            End If

                        Catch ex As Exception
                            'sw.WriteLine(ex.Message.ToString())
                        End Try

                        IDRichiestaOLD = IDRichiesta

                    Loop
                End If

                myReader.Close()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            Loop
        End If
        myReaderGEN.Close()
        myReaderGEN = Nothing

        myConn.Close()
        myConn2.Close()

        'sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        'sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailNONPartecipantiDemo = True

        Exit Function

    End Function



    Private Function InviaMailNONPartecipantiDemoComm(pData As Date) As Boolean


        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDStoricoDemoClienti As String
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim Contatto As String
        Dim ContattoOLD As String
        Dim MailCommerciale As String
        Dim MailCommercialeOLD As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        Dim Commerciale As String
        Dim CommercialeOLD As String
        Dim Argomento As String

        Dim strDemoold As String
        Dim DataDemoOLD As String
        Dim MailContattoAggOLD As String
        Dim NomeContattoOLD As String
        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim DataDemo As Date
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        'Dim IDKey As Int32
        'Dim Tabella As String

        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        MailContattoAggOLD = ""

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & pData & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        'Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail ai COMMERCIALE dei NON partecipanti alle demo del " & strData & " -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNONPartecipazioneDemoComm'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNONPartecipazioneDemoComm'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim ret As Integer

        ret = 0

        strSQL = "set dateformat dmy "
        strSQL = strSQL & "Select count(*) ret from Storico_DemoClienti s "
        strSQL = strSQL & "Join InfoSalutiDemoAssenti ass On (ass.IDKey=s.IDKey And ass.Tabella=s.Tabella And isnull(ass.AbilitaInvio, 0)=1) "
        'strSQL = strSQL & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = Convert(NVARCHAR, GETDATE(), 103)"
        strSQL = strSQL & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = " & strData

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        If myReader.HasRows Then
            Do While myReader.Read
                ret = myReader.Item("ret").ToString()
            Loop
        End If
        myReader.Close()
        myReader = Nothing

        If ret = 0 Then
            InviaMailNONPartecipantiDemoComm = False
            'sw.WriteLine("Nessuna mail ai commerciali da inviare -- " & DateTime.Now)
            'sw.Close()
            Exit Function
        End If

        Dim idKey1 As Integer
        Dim tabella1 As String
        Dim datademo1 As Date
        Dim orario1 As String

        Dim strTest As String
        Dim strDest As String

        strTest = ""
        strDest = ""
        idKey1 = 0
        tabella1 = ""
        datademo1 = Now
        orario1 = ""

        strSQLgen = "set dateformat dmy "
        strSQLgen = strSQLgen & "select S.IDKey,S.Tabella,Convert(NVARCHAR, S.DataDemo, 103) DataDemo, Orario from Storico_DemoClienti s "
        strSQLgen = strSQLgen & "Join InfoSalutiDemoAssenti ass On (ass.IDKey=s.IDKey And ass.Tabella=s.Tabella And isnull(ass.AbilitaInvio, 0)=1) "
        'strSQLgen = strSQLgen & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = Convert(NVARCHAR, GETDATE(), 103) "
        strSQLgen = strSQLgen & "where Convert(NVARCHAR, DataRegistrazionePartecipazione, 103) = " & strData
        strSQLgen = strSQLgen & "group by s.IDKey, s.Tabella, S.DataDemo, s.Orario"

        myCmdGEN = myConn2.CreateCommand
        myCmdGEN.CommandText = strSQLgen

        myReaderGEN = myCmdGEN.ExecuteReader()
        If myReaderGEN.HasRows Then
            Do While myReaderGEN.Read
                idKey1 = myReaderGEN.Item("IDKey")
                tabella1 = myReaderGEN.Item("Tabella").ToString()
                datademo1 = myReaderGEN.Item("DataDemo").ToString()
                orario1 = myReaderGEN.Item("Orario").ToString()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                strSQL = "set dateformat dmy "
                strSQL = strSQL & "Select distinct s.ID, pr.NomeAzienda, pr.NomeContatto, pr.MailContatto, isnull(a.Mail,'') MailCommerciale, "
                strSQL = strSQL & "isnull(a.Nominativo + ' ' + a.Mail + '', "
                strSQL = strSQL & "( "
                strSQL = strSQL & "Select top 1 a.Nominativo + ' ' + a.Mail + '' from preventivi_richieste r "
                strSQL = strSQL & "Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale "
                strSQL = strSQL & "where "
                strSQL = strSQL & "r.NomeAzienda = pr.NomeAzienda And "
                strSQL = strSQL & "IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc "
                strSQL = strSQL & ") "
                strSQL = strSQL & ") as Commerciale, "
                strSQL = strSQL & "Case "
                strSQL = strSQL & "WHEN ass.Tabella='Preventivi_Sezioni' THEN isnull(s.NoteDemo, sez.Sezione) "
                strSQL = strSQL & "WHEN ass.Tabella='Preventivi_Funzioni' THEN isnull(s.NoteDemo,funz.Funzione) "
                strSQL = strSQL & "End Argomento, "
                strSQL = strSQL & "Convert(varchar, S.DATADEMO, 103) DataDemo, S.ORARIO	"
                strSQL = strSQL & "From Storico_DemoClienti s "
                strSQL = strSQL & "Join InfoSalutiDemoAssenti ass on (ass.IDKey=s.IDKey And ass.Tabella=s.Tabella And isnull(ass.AbilitaInvio,0)=1) "
                strSQL = strSQL & "Join Preventivi_Richieste pr on pr.id=s.IDRichiesta "
                strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = pr.IDCommerciale "
                strSQL = strSQL & "Left Join Preventivi_Sezioni sez on sez.ID=s.IDKey "
                strSQL = strSQL & "Left Join Preventivi_Funzioni funz on funz.ID=s.IDKey "
                strSQL = strSQL & "where isnull(s.completata, 0) = 0 "
                strSQL = strSQL & "And isnull(s.SendPartecip2Guest,0)=0 "
                strSQL = strSQL & "And isnull(s.SendNONPartecip2Commerciale,0)=0 "
                strSQL = strSQL & "And isnull(pr.ConfigurazionePerInterni ,0)=0 "

                strSQL = strSQL & "And s.IDKey=" & idKey1.ToString() & " "
                strSQL = strSQL & "And s.Tabella='" & tabella1 & "' "
                strSQL = strSQL & "And s.datademo='" & datademo1 & "' "
                strSQL = strSQL & "And s.Orario='" & orario1 & "' "

                strSQL = strSQL & "And isnull(s.SendNONPartecip2Guest,0)=0 "
                strSQL = strSQL & "And pr.NomeAzienda<>'' AND pr.NomeContatto<>'' AND pr.MailContatto<>'' "


                strSQL = strSQL & "And a.id Is Not null "
                strSQL = strSQL & "And "
                strSQL = strSQL & "( "
                strSQL = strSQL & "Select Top 1 a.Nominativo + ' ' + a.Mail + '' from preventivi_richieste r "
                strSQL = strSQL & "Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale "
                strSQL = strSQL & "where "
                strSQL = strSQL & "r.NomeAzienda = pr.NomeAzienda And "
                strSQL = strSQL & "isnull(r.ConfigurazionePerInterni ,0)=0 And "
                strSQL = strSQL & "IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc "
                strSQL = strSQL & ") Is Not null "

                strSQL = strSQL & "order by Commerciale "

                myCmd = myConn.CreateCommand
                myCmd.CommandText = strSQL

                myReader = myCmd.ExecuteReader()

                strContatto = ""
                strMailGuest = ""
                strPresale = ""
                strDataDemoTxt = ""
                IDRichiestaOLD = 0
                IDRichiesta = 0
                IDStoricoDemoClienti = ""
                ContattoOLD = ""
                Contatto = ""
                Commerciale = ""
                strOraOLD = ""

                strDest = ""
                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If myReader.HasRows Then
                    Do While myReader.Read

                        IDStoricoDemoClienti = myReader.Item("ID").ToString()
                        NomeAzienda = myReader.Item("NomeAzienda").ToString()
                        NomeContatto = myReader.Item("NomeContatto").ToString()
                        MailContatto = myReader.Item("MailContatto").ToString()
                        Contatto = NomeContatto & "[" & MailContatto & "]"
                        MailCommerciale = myReader.Item("MailCommerciale").ToString()
                        Commerciale = myReader.Item("Commerciale").ToString()
                        MailCommerciale = myReader.Item("MailCommerciale").ToString()
                        Argomento = myReader.Item("Argomento").ToString()
                        strDemo = myReader.Item("Argomento").ToString()
                        DataDemo = myReader.Item("DataDemo")
                        strOra = myReader.Item("ORARIO").ToString()

                        Try

                            If CommercialeOLD <> Commerciale And CommercialeOLD <> "" Then

                                '**************************************************************'
                                '*************** INVIO EMAIL PARTECIPAZIONE DEMO A**************'
                                '**************************************************************'
                                MailContattoAgg = ""

                                If strTest.ToUpper().Trim().ToString() = "S" Then

                                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                                    MailContatto = strDest 'TEST

                                End If


                                Try

                                    If strTest.ToUpper().Trim().ToString() = "S" Then
                                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                                        MailCommercialeOLD = strDest 'TEST
                                    End If

                                    Inviamail(strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemoold), "", MailCommercialeOLD, "", Nothing, testoHtml.Replace("#data", DataDemoOLD).Replace("#nome", ContattoOLD).Replace("#demo", strDemoold).Replace("#linkwebex", strLinkWebexOLD).Replace("#ora", strOra).Replace("#commerciale", CommercialeOLD), DataDemoOLD, "", Nothing)
                                    'sw.WriteLine("Invio email partecipazione demo a " & MailContattoOLD & " " & DateTime.Now)

                                    Dim query As String
                                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                                    myConn2 = New SqlConnection(strConnectionDB)
                                    myConn2.Open()
                                    Using comm As New SqlCommand()
                                        With comm
                                            .Connection = myConn2
                                            .CommandType = CommandType.Text
                                            .CommandText = query
                                            .Parameters.AddWithValue("@Dest", MailCommercialeOLD)
                                            .Parameters.AddWithValue("@CC", "")
                                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemoOLD))
                                            .Parameters.AddWithValue("@Esito", "OK")
                                            .Parameters.AddWithValue("@Note", "INVIO MAIL NON PARTECIPAZIONE DEMO  " & MailContattoOLD & " per la demo " & strDemoold & " del " & DataDemoOLD)
                                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                                            .Parameters.AddWithValue("@IDRichiesta", CInt(IDStoricoDemoClienti))
                                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#nome", ContattoOLD).Replace("#demo", strDemoold).Replace("#ora", strOraOLD).Replace("#commerciale", CommercialeOLD))
                                        End With
                                        Try
                                            comm.ExecuteNonQuery()
                                        Catch ex As Exception
                                            'sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                        End Try
                                    End Using
                                    myConn2.Close()
                                Catch ex As Exception
                                    'sw.WriteLine("*** ERRORE SU INVIO MAIL A  " & MailCommercialeOLD & " " & DateTime.Now)
                                End Try

                                IDRichiestaOLD = 0
                                CommercialeOLD = ""
                                ContattoOLD = ""
                                MailCommercialeOLD = ""
                                strDemoold = ""
                                DataDemoOLD = ""
                                MailContattoAggOLD = ""
                                NomeContattoOLD = ""
                                strLinkWebexOLD = ""
                                strOraOLD = ""

                            End If



                        Catch ex As Exception
                            'sw.WriteLine(ex.Message.ToString())
                        End Try



                        ' settare a 1 il flag SendNONPartecip2Guest
                        ' update StoricoDemoClienti set SendNONPartecip2Guest=1 where ID=IDStoricoDemoClienti

                        Dim queryU As String
                        queryU = "update Storico_DemoClienti set SendNONPartecip2Commerciale=1 where ID=@IDStoricoDemoClienti"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = queryU
                                .Parameters.AddWithValue("@IDStoricoDemoClienti", CInt(IDStoricoDemoClienti))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                'sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()

                        IDRichiestaOLD = IDRichiesta
                        CommercialeOLD = Commerciale

                        ContattoOLD = ContattoOLD.ToString() & "<br>" & Contatto.ToString()
                        MailCommercialeOLD = MailCommerciale
                        strDemoold = strDemo
                        DataDemoOLD = DataDemo
                        MailContattoAggOLD = MailContattoAgg
                        NomeContattoOLD = NomeContatto
                        strLinkWebexOLD = strLinkWebex
                        strOraOLD = strOra

                    Loop
                End If

                Try

                    If strTest.ToUpper().Trim().ToString() = "S" Then
                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        MailCommercialeOLD = strDest 'TEST
                    End If

                    If (strDemoold <> "" And DataDemoOLD <> "" And ContattoOLD <> "") Then
                        Inviamail(strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemoold), "", MailCommercialeOLD, "", Nothing, testoHtml.Replace("#data", DataDemoOLD).Replace("#nome", ContattoOLD).Replace("#demo", strDemoold).Replace("#linkwebex", strLinkWebexOLD).Replace("#ora", strOra).Replace("#commerciale", CommercialeOLD), DataDemoOLD, "", Nothing)
                        'sw.WriteLine("Invio email partecipazione demo a " & MailCommercialeOLD & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailCommercialeOLD)
                                .Parameters.AddWithValue("@CC", "")
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemoOLD))
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL NON PARTECIPAZIONE DEMO  " & MailContattoOLD & " per la demo " & strDemoold & " del " & DataDemoOLD)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", CInt(IDStoricoDemoClienti))
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#nome", ContattoOLD).Replace("#demo", strDemoold).Replace("#ora", strOraOLD).Replace("#commerciale", CommercialeOLD))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                'sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()
                    End If


                Catch ex As Exception
                    'sw.WriteLine("*** ERRORE SU INVIO MAIL A  " & MailCommercialeOLD & " " & DateTime.Now)
                End Try

                myReader.Close()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            Loop
        End If
        myReaderGEN.Close()
        myReaderGEN = Nothing

        myConn.Close()
        myConn2.Close()

        'sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        'sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailNONPartecipantiDemoComm = True

        Exit Function

    End Function


    Private Function InviaMailPartecipantiDemoOLD() As Boolean


        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDStoricoDemoClienti As String
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        Dim Commerciale As String
        Dim Argomento As String

        Dim DataDemo As Date
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        'Dim IDKey As Int32
        'Dim Tabella As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio invio mail ai partecipanti alle demo -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoPartecipazioneDemoGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectPartecipazioneDemoGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        strSQL = "set dateformat dmy "
        strSQL = strSQL & "Select s.ID, pr.NomeAzienda, pr.NomeContatto, pr.MailContatto, "
        strSQL = strSQL & "isnull((select top 1 a.Nominativo + ' (' + a.mail + ') ' from preventivi_richieste r Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale where r.NomeAzienda = pr.NomeAzienda And IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc),'') as Commerciale, "
        'strSQL = strSQL & "isnull((select top 1 a.Nominativo from preventivi_richieste r Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale where r.NomeAzienda = pr.NomeAzienda And IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc),'')) as Commerciale, "
        'strSQL = strSQL & "isnull(a.Nominativo, "
        'strSQL = strSQL & "(select top 1 a.Nominativo from preventivi_richieste r "
        'strSQL = strSQL & "Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale "
        'strSQL = strSQL & "where r.NomeAzienda = pr.NomeAzienda And IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc)) as Commerciale, "
        strSQL = strSQL & "Case "
        strSQL = strSQL & "WHEN p.Tabella='Preventivi_Sezioni' THEN isnull(s.NoteDemo, sez.Sezione) "
        strSQL = strSQL & "WHEN p.Tabella='Preventivi_Funzioni' THEN isnull(s.NoteDemo,funz.Funzione) "
        strSQL = strSQL & "End Argomento, "
        strSQL = strSQL & "Convert(varchar, S.DATADEMO, 103) DataDemo, S.ORARIO	"
        strSQL = strSQL & "From Storico_DemoClienti s "
        strSQL = strSQL & "Join InfoSalutiDemoPresenti p on (p.IDKey=s.IDKey And p.Tabella=s.Tabella and p.NoteDemo=s.NoteDemo And isnull(p.AbilitaInvio,0)=1) "
        strSQL = strSQL & "Join Preventivi_Richieste pr on pr.id=s.IDRichiesta "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = pr.IDCommerciale "
        strSQL = strSQL & "Left Join Preventivi_Sezioni sez on sez.ID=s.IDKey "
        strSQL = strSQL & "Left Join Preventivi_Funzioni funz on funz.ID=s.IDKey "
        strSQL = strSQL & "where isnull(s.completata, 0) = 1 "
        strSQL = strSQL & "and isnull(s.SendPartecip2Guest,0)=0 "
        strSQL = strSQL & "and DataRegistrazionePartecipazione is not null  "


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0
        IDStoricoDemoClienti = ""

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    IDStoricoDemoClienti = myReader.Item("ID").ToString()
                    NomeAzienda = myReader.Item("NomeAzienda").ToString()
                    NomeContatto = myReader.Item("NomeContatto").ToString()
                    MailContatto = myReader.Item("MailContatto").ToString()
                    Commerciale = myReader.Item("Commerciale").ToString()
                    Argomento = myReader.Item("Argomento").ToString()
                    strDemo = myReader.Item("Argomento").ToString()
                    DataDemo = myReader.Item("DataDemo")

                    strOra = myReader.Item("ORARIO").ToString()

                    If (Commerciale = "" Or Commerciale Is Nothing) Then
                        Commerciale = " il tuo commerciale "
                    End If

                    Dim strTest As String
                    Dim strDest As String

                    strDest = ""
                    strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL PARTECIPAZIONE DEMO **************'
                    '**************************************************************'
                    MailContattoAgg = ""

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailContatto = strDest 'TEST

                    End If


                    Dim bInviaMail As Boolean
                    Dim myReader4 As SqlDataReader
                    Dim myCmd4 As New SqlCommand

                    Dim oDate As DateTime
                    oDate = Convert.ToDateTime(DataDemo)
                    oDate = oDate.AddDays(-1)

                    Dim myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    bInviaMail = False

                    Dim strSqlCtr As String
                    strSqlCtr = "INVIO MAILPARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & Argomento & " del " & DataDemo

                    myCmd4 = myConn3.CreateCommand
                    myCmd4.CommandText = "Select * from LogMailOperation where Oggetto='Configuratore Zucchetti: Grazie per avere partecipato alla demo' and DestinatarioPrincipale=@DestinatarioPrincipale and Note=@note and IDRichiesta = " & IDStoricoDemoClienti
                    myCmd4.Parameters.Add("@DestinatarioPrincipale", SqlDbType.VarChar).Value = MailContatto
                    myCmd4.Parameters.Add("@note", SqlDbType.VarChar).Value = strSqlCtr
                    myReader4 = myCmd4.ExecuteReader()
                    If myReader4.HasRows = False Then
                        bInviaMail = True
                    End If

                    myReader4.Close()
                    myConn3.Close()

                    MailContattoAgg = ""
                    If bInviaMail = True Then

                        Try

                            Inviamail(strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", DataDemo).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra).Replace("#commerciale", Commerciale), DataDemo, "", Nothing)
                            sw.WriteLine("Invio email partecipazione demo a " & MailContatto & " " & DateTime.Now)

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                    .Parameters.AddWithValue("@Esito", "OK")
                                    .Parameters.AddWithValue("@Note", "INVIO MAILPARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & strDemo & " del " & DataDemo)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", CInt(IDStoricoDemoClienti))
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemo).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#ora", strOra).Replace("#commerciale", Commerciale))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn2.Close()


                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                    .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                    .Parameters.AddWithValue("@Note", "INVIO MAILPARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & strDemo)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex1 As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn2.Close()


                        End Try

                    End If

                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try


                ' settare a 1 il flag SendPartecip2Guest
                ' update StoricoDemoClienti set SendPartecip2Guest=1 where ID=IDStoricoDemoClienti

                Dim queryU As String
                queryU = "update Storico_DemoClienti set SendPartecip2Guest=1 where ID=@IDStoricoDemoClienti"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = queryU
                        .Parameters.AddWithValue("@IDStoricoDemoClienti", CInt(IDStoricoDemoClienti))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using
                myConn2.Close()


                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailPartecipantiDemoOLD = True

        Exit Function

    End Function

    Private Function InviaMailPartecipantiDemo() As Boolean


        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDStoricoDemoClienti As String
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        Dim Commerciale As String
        Dim Argomento As String

        Dim DataDemo As Date
        Dim DataCompilazioneFeedbackQuestionario As String
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        Dim IDKey As Int32
        Dim Tabella As String
        Dim NoteDemo As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio invio mail ai partecipanti alle demo -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim testoHtmlOrig As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoPartecipazioneDemoGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()
        testoHtmlOrig = testoHtml
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectPartecipazioneDemoGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        strSQL = "set dateformat dmy "
        strSQL = strSQL & "Select s.DataCompilazioneFeedbackQuestionario, s.ID, pr.NomeAzienda, pr.NomeContatto, pr.MailContatto, s.idkey, s.tabella, s.notedemo, "
        strSQL = strSQL & "isnull((select top 1 a.Nominativo + ' (' + a.mail + ') ' from preventivi_richieste r Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale where r.NomeAzienda = pr.NomeAzienda And IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc),'') as Commerciale, "
        'strSQL = strSQL & "isnull((select top 1 a.Nominativo from preventivi_richieste r Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale where r.NomeAzienda = pr.NomeAzienda And IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc),'')) as Commerciale, "
        'strSQL = strSQL & "isnull(a.Nominativo, "
        'strSQL = strSQL & "(select top 1 a.Nominativo from preventivi_richieste r "
        'strSQL = strSQL & "Left Join Anagrafica_Commerciali a on a.ID=IDCommerciale "
        'strSQL = strSQL & "where r.NomeAzienda = pr.NomeAzienda And IDCommerciale Is Not null And IDCommerciale > 0 order by r.id desc)) as Commerciale, "
        strSQL = strSQL & "Case "
        strSQL = strSQL & "WHEN p.Tabella='Preventivi_Sezioni' THEN isnull(s.NoteDemo, sez.Sezione) "
        strSQL = strSQL & "WHEN p.Tabella='Preventivi_Funzioni' THEN isnull(s.NoteDemo,funz.Funzione) "
        strSQL = strSQL & "End Argomento, "
        strSQL = strSQL & "Convert(varchar, S.DATADEMO, 103) DataDemo, S.ORARIO	"
        strSQL = strSQL & "From Storico_DemoClienti s "
        strSQL = strSQL & "Join InfoSalutiDemoPresenti p on (p.IDKey=s.IDKey And p.Tabella=s.Tabella and p.NoteDemo=s.NoteDemo And isnull(p.AbilitaInvio,0)=1) "
        strSQL = strSQL & "Join Preventivi_Richieste pr on pr.id=s.IDRichiesta "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = pr.IDCommerciale "
        strSQL = strSQL & "Left Join Preventivi_Sezioni sez on sez.ID=s.IDKey "
        strSQL = strSQL & "Left Join Preventivi_Funzioni funz on funz.ID=s.IDKey "
        strSQL = strSQL & "where isnull(s.completata, 0) = 1 "
        strSQL = strSQL & "and isnull(s.SendPartecip2Guest,0)=0 "
        strSQL = strSQL & "And isnull(pr.ConfigurazionePerInterni ,0)=0 "
        strSQL = strSQL & "and DataRegistrazionePartecipazione is not null  "


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0
        IDStoricoDemoClienti = ""

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    testoHtml = testoHtmlOrig
                    IDStoricoDemoClienti = myReader.Item("ID").ToString()

                    Dim bImportato As Boolean

                    bImportato = True

                    Try
                        DataCompilazioneFeedbackQuestionario = myReader.Item("DataCompilazioneFeedbackQuestionario").ToString()
                    Catch ex As Exception
                        DataCompilazioneFeedbackQuestionario = ""
                    End Try
                    If DataCompilazioneFeedbackQuestionario.Equals("") Then
                        bImportato = False
                    End If

                    NomeAzienda = myReader.Item("NomeAzienda").ToString()
                    NomeContatto = myReader.Item("NomeContatto").ToString()
                    MailContatto = myReader.Item("MailContatto").ToString()
                    Commerciale = myReader.Item("Commerciale").ToString()
                    Argomento = myReader.Item("Argomento").ToString()
                    strDemo = myReader.Item("Argomento").ToString()
                    DataDemo = myReader.Item("DataDemo")
                    IDKey = myReader.Item("IdKey").ToString()
                    Tabella = myReader.Item("Tabella").ToString()
                    NoteDemo = myReader.Item("NoteDemo").ToString()

                    strOra = myReader.Item("ORARIO").ToString()

                    If (Commerciale = "" Or Commerciale Is Nothing) Then
                        Commerciale = " il tuo commerciale "
                    End If

                    Dim strTest As String
                    Dim strDest As String

                    strDest = ""
                    strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL PARTECIPAZIONE DEMO **************'
                    '**************************************************************'
                    MailContattoAgg = ""

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailContatto = strDest 'TEST

                    End If


                    Dim bInviaMail As Boolean
                    Dim myReader4 As SqlDataReader
                    Dim myCmd4 As New SqlCommand

                    Dim oDate As DateTime
                    oDate = Convert.ToDateTime(DataDemo)
                    oDate = oDate.AddDays(-1)

                    Dim myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    bInviaMail = False

                    Dim strSqlCtr As String
                    strSqlCtr = "INVIO MAILPARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & Argomento & " del " & DataDemo

                    myCmd4 = myConn3.CreateCommand
                    myCmd4.CommandText = "Select * from LogMailOperation where Oggetto='Configuratore Zucchetti: Grazie per avere partecipato alla demo' and DestinatarioPrincipale=@DestinatarioPrincipale and Note=@note and IDRichiesta = " & IDStoricoDemoClienti
                    myCmd4.Parameters.Add("@DestinatarioPrincipale", SqlDbType.VarChar).Value = MailContatto
                    myCmd4.Parameters.Add("@note", SqlDbType.VarChar).Value = strSqlCtr
                    myReader4 = myCmd4.ExecuteReader()
                    If myReader4.HasRows = False Then
                        bInviaMail = True
                    End If

                    myReader4.Close()

                    Dim strSQLFeedback As String
                    Dim strDomande As String
                    Dim idold As Integer
                    Dim id As Integer
                    Dim idRisposta As Integer
                    Dim Valore As Integer
                    Dim TestoRisposta As String
                    Dim IdTipo As Integer

                    If bImportato = False Then

                        myCmd4 = Nothing
                        myCmd4 = myConn3.CreateCommand

                        strSQLFeedback = "Select 0 TipoRecord,d.ID,d.IdKey,d.Tabella,d.NoteDemo,d.TestoDomanda TestoDomanda,d.IdTipo,d.Ordinamento,r.Testo,r.Valore,t.Descrizione Tipo,r.id IdRisposta "
                        strSQLFeedback = strSQLFeedback & " From Feedback_Domande_Weekly_Demo d "
                        strSQLFeedback = strSQLFeedback & " Join Feedback_Risposte_Domande_Weekly_Demo r on r.iddomanda=d.id "
                        strSQLFeedback = strSQLFeedback & " Join Feedback_Tipo_Domande_Weekly_Demo t on t.id=d.idtipo "
                        strSQLFeedback = strSQLFeedback & " where idkey=0 and tabella='' "
                        strSQLFeedback = strSQLFeedback & " UNION "
                        strSQLFeedback = strSQLFeedback & " Select 1 TipoRecord,d.ID,d.IdKey,d.Tabella,d.NoteDemo,d.TestoDomanda,d.IdTipo,d.Ordinamento,r.Testo,r.Valore,t.Descrizione Tipo,r.id IdRisposta "
                        strSQLFeedback = strSQLFeedback & " From Feedback_Domande_Weekly_Demo d "
                        strSQLFeedback = strSQLFeedback & " Join Feedback_Risposte_Domande_Weekly_Demo r on r.iddomanda=d.id "
                        strSQLFeedback = strSQLFeedback & " Join Feedback_Tipo_Domande_Weekly_Demo t on t.id=d.idtipo "
                        strSQLFeedback = strSQLFeedback & " where idkey=@IDKey and tabella=@Tabella "
                        strSQLFeedback = strSQLFeedback & " order by TipoRecord, d.ordinamento "
                        myCmd4.CommandText = strSQLFeedback
                        myCmd4.Parameters.Add("@IDKey", SqlDbType.VarChar).Value = IDKey
                        myCmd4.Parameters.Add("@Tabella", SqlDbType.VarChar).Value = Tabella
                        myReader4 = myCmd4.ExecuteReader()
                        If myReader4.HasRows = True Then
                            strDomande = ""
                            Do While myReader4.Read



                                id = Convert.ToInt32(myReader4.Item("ID"))
                                idRisposta = Convert.ToInt32(myReader4.Item("idRisposta"))
                                Valore = Convert.ToInt32(myReader4.Item("Valore"))
                                TestoRisposta = myReader4.Item("Testo")
                                IdTipo = Convert.ToInt32(myReader4.Item("IdTipo"))
                                If id <> idold Then

                                    strDomande = strDomande & "<br>"

                                    strDomande = strDomande & "<span style=""font-size: 20px;"">" & myReader4.Item("TestoDomanda").ToString() & ""
                                    If IdTipo = 1 Or IdTipo = 4 Then
                                        strDomande = strDomande & "<input type = ""radio"" name='" & id & "'  id='" & idRisposta & "'  value='" & Valore & "' /><label for='" & idRisposta & "'>'" & "<label for=''10'' style=""color:#0072BC;font-weight:bold;font-size:16px"">'" & TestoRisposta & "'</label>" & "'</label></span> "
                                    End If
                                Else
                                    If IdTipo = 1 Or IdTipo = 4 Then
                                        strDomande = strDomande & "<input type = ""radio"" name='" & id & "' id='" & idRisposta & "'  value='" & Valore & "' /><label for='" & idRisposta & "'>'" & "<label for=''10'' style=""color:#0072BC;font-weight:bold;font-size:16px"">'" & TestoRisposta & "'</label>" & "'</label></span> "
                                    End If
                                    If IdTipo = 4 Then
                                        strDomande = strDomande & "<label style=""font-size: 18;""><label><br>Note o suggerimenti</label><textarea name='" & id & "_2' maxlength=""512"" style=""width:100%"" ></textarea>"
                                    End If
                                    strDomande = strDomande & "<br><br>"
                                End If


                                idold = id
                            Loop
                        End If

                        '////////////////////////////////////////////////////////////////////////////////////////

                        myConn3.Close()

                    End If



                    Dim linkURLFeedback As String

                    linkURLFeedback = System.Configuration.ConfigurationManager.AppSettings("linkURLFeedback").ToString()

                    Dim MailCifrata As String

                    MailCifrata = CifraturaDecifratura.CifraturaDecifratura.EncryptX(MailContatto)

                    Dim strForm As String
                    strForm = "<div style=""display:none;color:#FFF""><input type=""hidden"" style=""display:none;color:#FFF"" name=""partecipante"" value='" & MailCifrata & "' /></div>"

                    Dim strInfo As String
                    Dim strInfoCifrata As String
                    strInfo = IDStoricoDemoClienti & ";" & IDRichiesta & ";" & IDKey & ";" & Tabella & ";" & NoteDemo & ";" & Convert.ToDateTime(DataDemo).ToShortDateString & ";" & strOra

                    strInfoCifrata = CifraturaDecifratura.CifraturaDecifratura.EncryptX(strInfo)
                    strForm = strForm & "<div style=""display:none;color:#FFF""><input type=""hidden"" style=""display:none;color:#FFF"" name=""info"" value='" & strInfoCifrata & "' /></div>"

                    strForm = strForm & strDomande

                    strForm = strForm & "<br><br>"

                    testoHtml = testoHtml.Replace("#feedbackguest", strForm)

                    testoHtml = testoHtml.Replace("#linkURLFeedback", linkURLFeedback)



                    If bImportato = False Then
                        testoHtml = testoHtml.Replace("#displayfeedback", "display:block")
                    Else
                        testoHtml = testoHtml.Replace("#displayfeedback", "display:none")
                    End If







                    'D:\Web\Feedback

                    'C:\inetpub\wwwroot\Feedback\
                    'linkHomePage


                    Dim strPathFeedback As String
                    Dim strlinkHomePage As String

                    strPathFeedback = System.Configuration.ConfigurationManager.AppSettings("pathFeedback").ToString()

                    strlinkHomePage = System.Configuration.ConfigurationManager.AppSettings("linkHomePage").ToString()
                    strlinkHomePage = strlinkHomePage & "Feedback/" & strInfoCifrata & ".html"

                    testoHtml = testoHtml.Replace("#data", DataDemo).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra).Replace("#commerciale", Commerciale)

                    Dim swOut As New StreamWriter(File.Open(strPathFeedback & strInfoCifrata & ".html", FileMode.Append))
                    swOut.WriteLine(testoHtml)
                    swOut.Close()

                    testoHtml = testoHtml.Replace("#webfeedback", strlinkHomePage)

                    MailContattoAgg = ""
                    If bInviaMail = True Then

                        Try

                            Inviamail(strOggetto.Replace("#data", DataDemo).Replace("#demo", strDemo), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", DataDemo).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra).Replace("#commerciale", Commerciale), DataDemo, "", Nothing)
                            sw.WriteLine("Invio email partecipazione demo a " & MailContatto & " " & DateTime.Now)

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", DataDemo))
                                    .Parameters.AddWithValue("@Esito", "OK")
                                    .Parameters.AddWithValue("@Note", "INVIO MAILPARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & strDemo & " del " & DataDemo)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", CInt(IDStoricoDemoClienti))
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemo).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#ora", strOra).Replace("#commerciale", Commerciale))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn2.Close()


                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                    .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                    .Parameters.AddWithValue("@Note", "INVIO MAILPARTECIPAZIONE DEMO  " & MailContatto & " per la demo " & strDemo)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex1 As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn2.Close()


                        End Try

                    End If

                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try


                'settare a 1 il flag SendPartecip2Guest
                Dim queryU As String
                queryU = "update Storico_DemoClienti Set SendPartecip2Guest=1, DataInvioFeedbackQuestionario=GetDate() where ID=@IDStoricoDemoClienti"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = queryU
                        .Parameters.AddWithValue("@IDStoricoDemoClienti", CInt(IDStoricoDemoClienti))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using
                myConn2.Close()


                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailPartecipantiDemo = True

        Exit Function

    End Function


    'Private Function HTMLToText(HTMLCode As String) As String
    '{
    '        '// Remove New lines since they are Not visible in HTML
    '        HTMLCode = HTMLCode.Replace("\n", " ")
    '        '// Remove tab spaces
    '        HTMLCode = HTMLCode.Replace("\t", " ")
    '        '// Remove multiple white spaces from HTML
    '        HTMLCode = Regex.Replace(HTMLCode, "\\s+", " ")
    '        '// Remove HEAD tag
    '        HTMLCode = Regex.Replace(HTMLCode, "<head.*?</head>", "", RegexOptions.IgnoreCase | RegexOptions.Singleline)


    '        '// Remove any JavaScript
    '        HTMLCode = Regex.Replace(HTMLCode, "<script.*?</script>", ""
    '          , RegexOptions.IgnoreCase | RegexOptions.Singleline);
    '        '// Replace special characters Like &, <, >, " etc.
    '        Dim sbHTML As new System.Text.StringBuilder(HTMLCode)

    '        '// Note: There are many more special characters, these are just
    '        '// most common. You can add new characters in this arrays if needed
    '        Dim OldWords As New List(Of String)()
    '        OldWords.Add("&nbsp;")
    '        OldWords.Add("&amp;")
    '        OldWords.Add("&quot;")
    '        OldWords.Add("&lt;")
    '        OldWords.Add("&gt;")
    '        OldWords.Add("&reg;")
    '        OldWords.Add("&copy;")
    '        OldWords.Add("&bull;")
    '        OldWords.Add("&trade;")
    '        OldWords.Add("&#39;")

    '        Dim NewWords As New List(Of String)()
    '        NewWords.Add(" ")
    '        NewWords.Add("&")
    '        NewWords.Add("\")
    '        NewWords.Add("<")
    '        NewWords.Add(">")
    '        NewWords.Add("Â®")
    '        NewWords.Add("â€¢")
    '        NewWords.Add("\'")


    '        'string[] OldWords = {"&nbsp;", "&amp;", "&quot;", "&lt;","&gt;", "&reg;", "&copy;", "&bull;", "&trade;","&#39;"}
    '        'string[] NewWords = { " ", "&", "\"", "<", ">", "Â®", "Â©", "â€¢", "â„¢", "\'" };

    '    For i=0 To Len(OldWords)
    '        sbHTML.Replace(OldWords(i), NewWords(i))
    '        i=i+1
    '    Next


    '        'For (Int() i = 0; i < OldWords.Length; i++)
    '        '{
    '        '   sbHTML.Replace(OldWords[i], NewWords[i]);
    '        '}

    '        dim linkDemo As String
    '        linkDemo= ""

    '        Dim mc As MatchCollection = Regex.Matches("Stringa di prova per le regex", "\bp\p*")

    '        String Regex = "href=\"(.*) \ ""
    '        Match Match = Regex.Match(sbHTML.ToString(), Regex);
    '        If (Match.Success) Then
    '                        {
    '            String link = Match.Groups[1].Value;
    '            If (link.Trim().ToLower().Contains("zucchetti.webex.com") == True) Then linkDemo = link.Replace("\">","<br/>");

    '        }


    '        '// Check if there are line breaks (<br>) or paragraph (<p>)
    '        '/*sbHTML.Replace("<br>", "\n");
    '        'sbHTML.Replace("<br>", "\r\n");
    '        'sbHTML.Replace("<br/>", "\n");
    '        'sbHTML.Replace("<br/>", "\r\n");
    '        'sbHTML.Replace("<p>", "\r\n");
    '        'sbHTML.Replace("</p>", "\r\n");

    '        '//rimane solo il link alle demo webex
    '        sbHTML.Replace("Clicca qui", linkDemo);
    '        '//sbHTML.Replace("clicca qui", linkDemo);
    '        '//sbHTML.Replace("Clicca Qui", linkDemo);



    '        '//sbHTML.Replace("<br ", "\n<br ");
    '        '//sbHTML.Replace("<p ", "\n<p ");
    '        '// pulisce l'html tranne i br e p e il link 
    '        ''/*string pattern1 = @"<a .*?href=['""](.+?)['""].*?>(.+?)</a>";
    '        'String replacePattern = "$2 ($1)";

    '        'String testoPulito = System.Text.RegularExpressions.Regex.Replace(sbHTML.ToString(), pattern1, replacePattern);

    '        'String pattern2 = @"<link .*?href=['""](.+?)['""].*?>";
    '        'String replacePattern2 = "";

    '        'testoPulito = System.Text.RegularExpressions.Regex.Replace(sbHTML.ToString(), pattern2, replacePattern2);

    '        'String pattern3 = @"<style .*?type=['""](.+?)['""].*?>(.+?)>(.+?)</style>";
    '        'String replacePattern3 = "";

    '        'testoPulito = System.Text.RegularExpressions.Regex.Replace(sbHTML.ToString(), pattern3, replacePattern3);
    '        '*/


    '        String pattern2 = @"(?(<[^>]*>)(?((<p>)|(<br>)|(</p>)|(</br>))([^\w\W])|(<[^>]*>))|(?(&nbsp;)(&nbsp;)|([^\w\W])))";

    '        String testoPulito = System.Text.RegularExpressions.Regex.Replace(sbHTML.ToString(), pattern2, "");

    '        testoPulito = testoPulito.Replace("Clicca qui per accedere alla demo", "");

    '        Return testoPulito;
    '         ' //sbHTML.ToString(), "<[^>]*>", "");


    '    }
    'End Function



    Private Function InviaMailDemoReminderGUEST() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim ConfigurazionePerInterni As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDDemo As Int32
        Dim IDStorico As Int32
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        'Dim MailPresales As String

        Dim strDataDemo As String
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        Dim IDKey As Int32
        Dim Tabella As String
        Dim NoteDemo As String

        NoteDemo = ""
        retImp = False

        Dim Path_Cartella As String = My.Application.Info.DirectoryPath & "\INFOGRAFICA"

        If (System.IO.Directory.Exists(Path_Cartella) = True) Then
            For Each deleteFile In Directory.GetFiles(Path_Cartella, "*.*", System.IO.SearchOption.TopDirectoryOnly)
                File.Delete(deleteFile)
            Next
        End If


        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ReminderInvioLink" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio controllo Date demo schedulate per il giorno successivo -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "Select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReaderContAgg As SqlDataReader
        Dim myCmdContAgg As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNotificaGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNotificaGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        strSQL = "Select P.ID IDRichiesta, c.ID IDDemo, s.ID IDStorico, NomeAzienda,NomeContatto,MailContatto,c.linkWebex,c.Tabella,CASE WHEN c.Tabella = 'Preventivi_Sezioni' THEN ps.Sezione WHEN c.Tabella = 'Preventivi_Funzioni' THEN pf.Funzione END AS Demo,"
        strSQL = strSQL & "isNull(a.Nominativo,'') Commerciale, "
        strSQL = strSQL & "c.id, s.DataDemo,Convert(varchar(10),s.DataDemo,103) DataDemoText,isNull(c.Note,'') Ora, isNull(Completata,0) Completata, isNull(Note2,'')NoteDemo, c.IDKey, c.Tabella, isnull(p.ConfigurazionePerInterni,0) ConfigurazionePerInterni "
        strSQL = strSQL & "From storico_democlienti s "
        strSQL = strSQL & "Left Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo and c.Note=s.Orario) "
        strSQL = strSQL & "Left Join Preventivi_Sezioni ps on ps.ID = c.IDKey And c.Tabella='Preventivi_Sezioni' "
        strSQL = strSQL & "Left Join Preventivi_Funzioni pf on pf.ID = c.IDKey And c.Tabella='Preventivi_Funzioni' "
        strSQL = strSQL & "where CONVERT(char(10), s.DataDemo,126) = CONVERT(char(10), GetDate()+1,126) "
        strSQL = strSQL & "And p.MailContatto<>'' "
        strSQL = strSQL & "order by p.MailContatto "

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    IDRichiesta = Int32.Parse(myReader.Item("IDRichiesta"))
                    IDDemo = Int32.Parse(myReader.Item("IDDemo"))
                    IDStorico = Int32.Parse(myReader.Item("IDStorico"))
                    NomeAzienda = myReader.Item("NomeAzienda").ToString()
                    NomeContatto = myReader.Item("NomeContatto").ToString()
                    MailContatto = myReader.Item("MailContatto").ToString()
                    strDemo = myReader.Item("Demo").ToString()
                    strDataDemo = myReader.Item("DataDemo").ToString()
                    strDataDemoTxt = myReader.Item("DataDemoText").ToString()
                    strOra = myReader.Item("Ora").ToString()
                    strLinkWebex = myReader.Item("linkWebex").ToString()
                    IDKey = Int32.Parse(myReader.Item("IDKey"))
                    Tabella = myReader.Item("Tabella").ToString()
                    NoteDemo = myReader.Item("NoteDemo").ToString()
                    ConfigurazionePerInterni = Int32.Parse(myReader.Item("ConfigurazionePerInterni"))

                    Dim strTest As String
                    Dim strDest As String
                    Dim strDest2 As String
                    Dim strDest3 As String
                    Dim strDest4 As String

                    strDest = ""
                    strDest2 = ""
                    strDest3 = ""
                    strDest4 = ""

                    strTest = System.Configuration.ConfigurationManager.AppSettings("TESTGuestTomorrow").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************' .Replace("#testo", testo)
                    MailContattoAgg = ""

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                        strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailContatto = strDest 'TEST

                    End If

                    '**************************************************************'
                    '****************** INVIO FILE INFOGRAFICA ********************'
                    '**************************************************************'
                    'Dim data As Net.Mail.Attachment = InvioFileInfografica(IDRichiesta)

                    Dim dataarray() As String = InvioFileInfografica(IDRichiesta, NoteDemo)

                    '**************************************************************'
                    '****************** INVIO FILE INFOGRAFICA ********************'
                    '**************************************************************'


                    MailContattoAgg = ""
                    Try

                        Inviamail(strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra), strDataDemoTxt, "", dataarray)

                        sw.WriteLine("Invio email contatto principale a " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", "OK")
                                If dataarray IsNot Nothing Then
                                    .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo & " con allegato ")
                                Else
                                    .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                End If

                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()

                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()

                    End Try

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'



                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO AGGIUNTIVO **************'
                    '**************************************************************'
                    MailContattoAgg = ""
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    myCmdContAgg = myConn2.CreateCommand

                    Dim strSQLcontagg As String


                    '''''''myCmdContAgg.CommandText = "Select Mail, Nome, Cognome from Preventivi_Richieste_Contatti where IDRichiesta = " & IDRichiesta.ToString()
                    'myCmdContAgg.CommandText = "Select Mail,Nominativo FROM Storico_Demo_Contatti where IDStoricoDemo = " & IDStorico.ToString()

                    strSQLcontagg = "Select Mail,Nominativo, '' Cognome FROM Storico_Demo_Contatti where IDStoricoDemo = " & IDStorico.ToString() & " "
                    strSQLcontagg = strSQLcontagg & " UNION "
                    strSQLcontagg = strSQLcontagg & "Select Mail, (Nome + ' ' + Cognome) Nominativo, Cognome from Preventivi_Richieste_Contatti where IDRichiesta = " & IDRichiesta.ToString()

                    myCmdContAgg.CommandText = strSQLcontagg

                    myReaderContAgg = myCmdContAgg.ExecuteReader()

                    Do While myReaderContAgg.Read()

                        MailContatto = myReaderContAgg.Item("Mail")
                        NomeContatto = myReaderContAgg.Item("Nominativo").ToString()
                        'NomeContatto = myReaderContAgg.Item("Nome").ToString() & " " & myReaderContAgg.Item("Cognome").ToString()

                        MailContattoAgg = ""

                        If strTest.ToUpper().Trim().ToString() = "S" Then
                            MailContatto = strDest 'TEST
                            strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMailContatto").ToString()
                        End If

                        Try
                            Inviamail(strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra), strDataDemoTxt, "", dataarray)
                            sw.WriteLine("Invio email contatto aggiuntivo a " & MailContatto & " " & DateTime.Now)


                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                    .Parameters.AddWithValue("@Esito", "OK")
                                    If dataarray IsNot Nothing Then
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO AGGIUNTIVO A  " & MailContatto & " per la demo " & strDemo & " con allegato ")
                                    Else
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO AGGIUNTIVO A  " & MailContatto & " per la demo " & strDemo)
                                    End If
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn2.Close()

                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO AGGIUNTIVO A  " & MailContatto & " " & DateTime.Now)

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                    .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                    .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO AGGIUNTIVO A  " & MailContatto & " per la demo " & strDemo)
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex1 As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn2.Close()

                        End Try
                        '**************************************************************'
                        '*************** INVIO EMAIL CONTATTO AGGIUNTIVO **************'
                        '**************************************************************'







                    Loop
                    myReaderContAgg.Close()
                    myConn2.Close()

                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailDemoReminderGUEST = True

        Exit Function

    End Function


    Private Function InviaMailPresaleAlertDemo1to1() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDDemo As Int32
        Dim IDStorico As Int32
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        'Dim MailPresales As String

        Dim strDataDemo As String
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        Dim IDKey As Int32
        Dim Tabella As String
        Dim NoteDemo As String

        Dim strRagioneSocialePartner As String
        Dim strMailPresale As String

        NoteDemo = ""
        retImp = False
        strRagioneSocialePartner = ""
        strMailPresale = ""

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio controllo date delle demo 1 to 1 -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReaderContAgg As SqlDataReader
        Dim myCmdContAgg As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoAlertPresales1to1'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectAlertPresales1to1'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        strSQL = "set dateformat dmy;DECLARE	@return_value int EXEC	@return_value = [dbo].[spSetupAlertPresales1to1] SELECT	'Return Value' = @return_value"

        'EXEC	@return_value = [dbo].[spSetupAlertPresales1to1]

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        NomeContatto = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    IDRichiesta = Int32.Parse(myReader.Item("IDRichiesta"))
                    NomeAzienda = myReader.Item("AziendaCliente").ToString()

                    If Not String.IsNullOrEmpty(myReader.Item("RagioneSocialePartner")) Then
                        NomeContatto = myReader.Item("RagioneSocialePartner").ToString()
                    End If


                    MailContatto = myReader.Item("MailPartner").ToString()
                    strMailPresale = myReader.Item("EmailPresale").ToString()
                    strDemo = myReader.Item("Demo").ToString().ToUpper()

                    Dim strTest As String
                    Dim strDest As String
                    Dim strDest2 As String
                    Dim strDest3 As String
                    Dim strDest4 As String

                    strDest = ""
                    strDest2 = ""
                    strDest3 = ""
                    strDest4 = ""

                    strTest = System.Configuration.ConfigurationManager.AppSettings("TESTGuestTomorrow").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************' .Replace("#testo", testo)
                    MailContattoAgg = strMailPresale

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()

                        If (MailContatto <> "") Then
                            MailContatto = strDest 'TEST
                        End If
                        If (MailContattoAgg <> "") Then
                            MailContattoAgg = strDest
                        End If
                    End If

                    Try

                        If (MailContatto = "") Then
                            Inviamail("Reminder: Errore - Mail senza destinatario", "Rilevato commerciale non presente sulla richiesta " & IDRichiesta.ToString() & " dell'azienda " & NomeAzienda, "massimo.benini@zucchetti.it", "silvia.giovannini@zucchetti.it;mandolfo@ebcconsulting.com", sw, "<html>Rilevato commerciale non presente sulla richiesta<br>Codice richiesta: " & IDRichiesta.ToString() & "<br>Nome azienda: " & NomeAzienda & "</html>", "", "", Nothing)
                        Else
                            Inviamail(strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo.ToUpper()).Replace("#azienda", strDemo.ToUpper()), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#azienda", NomeAzienda), strDataDemoTxt, "", Nothing)
                        End If



                        Threading.Thread.Sleep(1000)

                        sw.WriteLine("Invio email a presale " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        If (MailContatto <> "") Then
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#azienda", NomeAzienda))
                                    .Parameters.AddWithValue("@Esito", "OK")
                                    .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto & " per la demo 1 to 1 " & strDemo)


                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#azienda", NomeAzienda))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn2.Close()
                        End If


                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INVIO MAIL A PRESALE  " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()


                    End Try

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'





                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine controllo date delle demo 1 to 1 -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailPresaleAlertDemo1to1 = True

        Exit Function

    End Function

    Private Function InviaMailPresaleAlertDemo1to1(d As Int32) As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDDemo As Int32
        Dim IDStorico As Int32
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        'Dim MailPresales As String

        Dim strDataDemo As String
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        Dim IDKey As Int32
        Dim Tabella As String
        Dim NoteDemo As String

        NoteDemo = ""
        retImp = False



        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio controllo link webex demo schedulate per 2 giorni successivi -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReaderContAgg As SqlDataReader
        Dim myCmdContAgg As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoWebex'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectWebex'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        'strSQL = "Select P.ID IDRichiesta, c.ID IDDemo, s.ID IDStorico, NomeAzienda,case when s.tabella = 'Preventivi_Sezioni' then u1.descrizione when s.tabella = 'Preventivi_Funzioni' then u2.descrizione end NomePresale, "
        'strSQL = strSQL & "case when s.tabella = 'Preventivi_Sezioni' then u1.email when s.tabella = 'Preventivi_Funzioni' then u2.email end EmailPresale, "
        'strSQL = strSQL & "c.linkWebex,c.Tabella,CASE WHEN c.Tabella = 'Preventivi_Sezioni' THEN ps.Sezione WHEN c.Tabella = 'Preventivi_Funzioni' THEN pf.Funzione END AS Demo, "
        'strSQL = strSQL & "isNull(a.Nominativo,'') Commerciale, "
        'strSQL = strSQL & "c.id, s.DataDemo,Convert(varchar(10),s.DataDemo,103) DataDemoText,isNull(c.Note,'') Ora, isNull(Completata,0) Completata, isNull(Note2,'')NoteDemo, c.IDKey, c.Tabella "
        'strSQL = strSQL & "From storico_democlienti s "
        'strSQL = strSQL & "Left Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        'strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        'strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo and c.Note=s.Orario) "
        'strSQL = strSQL & "Left Join Preventivi_Sezioni ps on ps.ID = c.IDKey And c.Tabella='Preventivi_Sezioni' "
        'strSQL = strSQL & "left join presales presale1 on presale1.idkey=ps.id and presale1.tabella = 'Preventivi_Sezioni' "
        'strSQL = strSQL & "left join Users u1 on u1.Email=presale1.mail "
        'strSQL = strSQL & "Left Join Preventivi_Funzioni pf on pf.ID = c.IDKey And c.Tabella='Preventivi_Funzioni' "
        'strSQL = strSQL & "left join presales presale2 on presale2.idkey=pf.id and presale2.tabella = 'Preventivi_Funzioni' "
        'strSQL = strSQL & "left join Users u2 on u2.Email=presale2.mail "
        'strSQL = strSQL & "where CONVERT(char(10), s.DataDemo,126) = CONVERT(char(10), GetDate()+2,126) "
        'strSQL = strSQL & "And p.MailContatto<>'' "
        'strSQL = strSQL & "order by p.MailContatto "

        strSQL = "Select max(P.ID) IDRichiesta,max(NomeAzienda) NomeAzienda,case when max(s.tabella) = 'Preventivi_Sezioni' then u1.descrizione when max(s.tabella) = 'Preventivi_Funzioni' then u2.descrizione end NomePresale,  "
        strSQL = strSQL & "case when max(s.tabella) = 'Preventivi_Sezioni' then max(u1.email) when max(s.tabella) = 'Preventivi_Funzioni' then max(u2.email) end EmailPresale, "
        strSQL = strSQL & "max(c.linkWebex) linkWebex,CASE WHEN c.Tabella = 'Preventivi_Sezioni' THEN ps.Sezione WHEN c.Tabella = 'Preventivi_Funzioni' THEN pf.Funzione END AS Demo, "
        strSQL = strSQL & "max(s.DataDemo) DataDemo,Convert(varchar(10),max(s.DataDemo),103) DataDemoText,isNull(c.Note,'') Ora "
        strSQL = strSQL & "From storico_democlienti s "
        strSQL = strSQL & "Left Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo and c.Note=s.Orario) "
        strSQL = strSQL & "Left Join Preventivi_Sezioni ps on ps.ID = c.IDKey And c.Tabella='Preventivi_Sezioni' "
        strSQL = strSQL & "left join presales presale1 on presale1.idkey=ps.id and presale1.tabella = 'Preventivi_Sezioni' "
        strSQL = strSQL & "left join Users u1 on u1.Email=presale1.mail "
        strSQL = strSQL & "Left Join Preventivi_Funzioni pf on pf.ID = c.IDKey And c.Tabella='Preventivi_Funzioni' "
        strSQL = strSQL & "left join presales presale2 on presale2.idkey=pf.id and presale2.tabella = 'Preventivi_Funzioni' "
        strSQL = strSQL & "left join Users u2 on u2.Email=presale2.mail "
        strSQL = strSQL & "where CONVERT(char(10), s.DataDemo,126) = CONVERT(char(10), GetDate()+" & d.ToString() & ",126) "
        strSQL = strSQL & "And p.MailContatto<>'' "
        strSQL = strSQL & "group by c.Tabella, ps.Sezione, pf.Funzione, c.Note, u1.descrizione, u2.descrizione"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    IDRichiesta = Int32.Parse(myReader.Item("IDRichiesta"))
                    NomeAzienda = myReader.Item("NomeAzienda").ToString()
                    NomeContatto = myReader.Item("NomePresale").ToString()
                    MailContatto = myReader.Item("EmailPresale").ToString()
                    strDemo = myReader.Item("Demo").ToString().ToUpper()
                    strDataDemo = myReader.Item("DataDemo").ToString()
                    strDataDemoTxt = myReader.Item("DataDemoText").ToString()
                    strOra = myReader.Item("Ora").ToString()
                    strLinkWebex = myReader.Item("linkWebex").ToString()
                    'IDKey = Int32.Parse(myReader.Item("IDKey"))
                    'Tabella = myReader.Item("Tabella").ToString()
                    'NoteDemo = myReader.Item("NoteDemo").ToString()

                    Dim strTest As String
                    Dim strDest As String
                    Dim strDest2 As String
                    Dim strDest3 As String
                    Dim strDest4 As String

                    strDest = ""
                    strDest2 = ""
                    strDest3 = ""
                    strDest4 = ""

                    strTest = System.Configuration.ConfigurationManager.AppSettings("TESTGuestTomorrow").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************' .Replace("#testo", testo)
                    MailContattoAgg = ""

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                        'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailContatto = strDest 'TEST

                    End If



                    MailContattoAgg = ""
                    Try


                        Inviamail(strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo.ToUpper()), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra), strDataDemoTxt, "", Nothing)
                        sw.WriteLine("Invio email a presale " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto & " per la demo " & strDemo)


                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()


                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INVIO MAIL A PRESALE  " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()


                    End Try

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'





                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailPresaleAlertDemo1to1 = True

        Exit Function

    End Function



    Private Function InviaMailPresaleWebex(d As Int32) As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDDemo As Int32
        Dim IDStorico As Int32
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        'Dim MailPresales As String

        Dim strDataDemo As String
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        Dim IDKey As Int32
        Dim Tabella As String
        Dim NoteDemo As String

        NoteDemo = ""
        retImp = False



        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ControlloLinkWebex" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio controllo link webex demo schedulate per 2 giorni successivi -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReaderContAgg As SqlDataReader
        Dim myCmdContAgg As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoWebex'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectWebex'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        'strSQL = "Select P.ID IDRichiesta, c.ID IDDemo, s.ID IDStorico, NomeAzienda,case when s.tabella = 'Preventivi_Sezioni' then u1.descrizione when s.tabella = 'Preventivi_Funzioni' then u2.descrizione end NomePresale, "
        'strSQL = strSQL & "case when s.tabella = 'Preventivi_Sezioni' then u1.email when s.tabella = 'Preventivi_Funzioni' then u2.email end EmailPresale, "
        'strSQL = strSQL & "c.linkWebex,c.Tabella,CASE WHEN c.Tabella = 'Preventivi_Sezioni' THEN ps.Sezione WHEN c.Tabella = 'Preventivi_Funzioni' THEN pf.Funzione END AS Demo, "
        'strSQL = strSQL & "isNull(a.Nominativo,'') Commerciale, "
        'strSQL = strSQL & "c.id, s.DataDemo,Convert(varchar(10),s.DataDemo,103) DataDemoText,isNull(c.Note,'') Ora, isNull(Completata,0) Completata, isNull(Note2,'')NoteDemo, c.IDKey, c.Tabella "
        'strSQL = strSQL & "From storico_democlienti s "
        'strSQL = strSQL & "Left Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        'strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        'strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo and c.Note=s.Orario) "
        'strSQL = strSQL & "Left Join Preventivi_Sezioni ps on ps.ID = c.IDKey And c.Tabella='Preventivi_Sezioni' "
        'strSQL = strSQL & "left join presales presale1 on presale1.idkey=ps.id and presale1.tabella = 'Preventivi_Sezioni' "
        'strSQL = strSQL & "left join Users u1 on u1.Email=presale1.mail "
        'strSQL = strSQL & "Left Join Preventivi_Funzioni pf on pf.ID = c.IDKey And c.Tabella='Preventivi_Funzioni' "
        'strSQL = strSQL & "left join presales presale2 on presale2.idkey=pf.id and presale2.tabella = 'Preventivi_Funzioni' "
        'strSQL = strSQL & "left join Users u2 on u2.Email=presale2.mail "
        'strSQL = strSQL & "where CONVERT(char(10), s.DataDemo,126) = CONVERT(char(10), GetDate()+2,126) "
        'strSQL = strSQL & "And p.MailContatto<>'' "
        'strSQL = strSQL & "order by p.MailContatto "

        strSQL = "Select max(P.ID) IDRichiesta,max(NomeAzienda) NomeAzienda,case when max(s.tabella) = 'Preventivi_Sezioni' then u1.descrizione when max(s.tabella) = 'Preventivi_Funzioni' then u2.descrizione end NomePresale,  "
        strSQL = strSQL & "case when max(s.tabella) = 'Preventivi_Sezioni' then max(u1.email) when max(s.tabella) = 'Preventivi_Funzioni' then max(u2.email) end EmailPresale, "
        strSQL = strSQL & "max(c.linkWebex) linkWebex,CASE WHEN c.Tabella = 'Preventivi_Sezioni' THEN ps.Sezione WHEN c.Tabella = 'Preventivi_Funzioni' THEN pf.Funzione END AS Demo, "
        strSQL = strSQL & "max(s.DataDemo) DataDemo,Convert(varchar(10),max(s.DataDemo),103) DataDemoText,isNull(c.Note,'') Ora "
        strSQL = strSQL & "From storico_democlienti s "
        strSQL = strSQL & "Left Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo and c.Note=s.Orario) "
        strSQL = strSQL & "Left Join Preventivi_Sezioni ps on ps.ID = c.IDKey And c.Tabella='Preventivi_Sezioni' "
        strSQL = strSQL & "left join presales presale1 on presale1.idkey=ps.id and presale1.tabella = 'Preventivi_Sezioni' "
        strSQL = strSQL & "left join Users u1 on u1.Email=presale1.mail "
        strSQL = strSQL & "Left Join Preventivi_Funzioni pf on pf.ID = c.IDKey And c.Tabella='Preventivi_Funzioni' "
        strSQL = strSQL & "left join presales presale2 on presale2.idkey=pf.id and presale2.tabella = 'Preventivi_Funzioni' "
        strSQL = strSQL & "left join Users u2 on u2.Email=presale2.mail "
        strSQL = strSQL & "where CONVERT(char(10), s.DataDemo,126) = CONVERT(char(10), GetDate()+" & d.ToString() & ",126) "
        strSQL = strSQL & "And p.MailContatto<>'' "
        strSQL = strSQL & "group by c.Tabella, ps.Sezione, pf.Funzione, c.Note, u1.descrizione, u2.descrizione"


        'modifica del 11/04/2022
        '-----------------------------------------------------------------
        strSQL = "Select max(P.ID) IDRichiesta,max(NomeAzienda) NomeAzienda, "
        strSQL = strSQL & "u1.descrizione NomePresale, "
        strSQL = strSQL & "max(u1.email) EmailPresale, "
        strSQL = strSQL & "max(c.linkWebex) linkWebex, "
        strSQL = strSQL & "ps.Sezione as Demo, max(s.DataDemo) DataDemo, "
        strSQL = strSQL & "Convert(varchar(10), max(s.DataDemo), 103) DataDemoText, "
        strSQL = strSQL & "isNull(c.Note,'') Ora, "
        strSQL = strSQL & "max(c.IDKey) IDKey, 'Preventivi_Sezioni' Tabella "
        strSQL = strSQL & "From storico_democlienti s "
        strSQL = strSQL & "Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo And c.Note=s.Orario) "
        strSQL = strSQL & "Join Preventivi_Sezioni ps on ps.ID = c.IDKey And c.Tabella='Preventivi_Sezioni' "
        strSQL = strSQL & "Join presales presale1 on presale1.idkey=ps.id And presale1.tabella = 'Preventivi_Sezioni' "
        strSQL = strSQL & "Join Users u1 on u1.Email=presale1.mail "
        strSQL = strSQL & "Join Presales_CalendarioWeekly pcw1 on pcw1.Notedemo2=c.Note2 And pcw1.IdUtente=u1.ID "
        strSQL = strSQL & "where Convert(Char(10), s.DataDemo, 126) = Convert(Char(10), GetDate() + " & d.ToString() & ", 126) "
        strSQL = strSQL & "And p.MailContatto<>'' "
        strSQL = strSQL & "Group by c.Tabella, ps.Sezione, c.Note, u1.descrizione "

        strSQL = strSQL & " union "

        strSQL = strSQL & "Select max(P.ID) IDRichiesta,max(NomeAzienda) NomeAzienda, "
        strSQL = strSQL & "u2.descrizione NomePresale, "
        strSQL = strSQL & "max(u2.email) EmailPresale, "
        strSQL = strSQL & "max(c.linkWebex) linkWebex, "
        strSQL = strSQL & "pf.Funzione as Demo, "
        strSQL = strSQL & "max(s.DataDemo) DataDemo, "
        strSQL = strSQL & "Convert(varchar(10), max(s.DataDemo), 103) DataDemoText, "
        strSQL = strSQL & "isNull(c.Note,'') Ora, "
        strSQL = strSQL & "max(c.IDKey) IDKey, 'Preventivi_Funzioni' Tabella "
        strSQL = strSQL & "From storico_democlienti s "
        strSQL = strSQL & "Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo And c.Note=s.Orario) "
        strSQL = strSQL & "Join Preventivi_Funzioni pf on pf.ID = c.IDKey And c.Tabella='Preventivi_Funzioni' "
        strSQL = strSQL & "Join presales presale2 on presale2.idkey=pf.id And presale2.tabella = 'Preventivi_Funzioni' "
        strSQL = strSQL & "Join Users u2 on u2.Email=presale2.mail "
        strSQL = strSQL & "Join Presales_CalendarioWeekly pcw2 on pcw2.Notedemo2=c.Note2 And pcw2.IdUtente=u2.ID "
        strSQL = strSQL & "where Convert(Char(10), s.DataDemo, 126) = Convert(Char(10), GetDate() + " & d.ToString() & ", 126) "
        strSQL = strSQL & "And p.MailContatto<>'' "
        strSQL = strSQL & "Group by c.Tabella, pf.Funzione, c.Note, u2.descrizione"
        '-----------------------------------------------------------------



        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    IDRichiesta = Int32.Parse(myReader.Item("IDRichiesta"))
                    NomeAzienda = myReader.Item("NomeAzienda").ToString()
                    NomeContatto = myReader.Item("NomePresale").ToString()
                    MailContatto = myReader.Item("EmailPresale").ToString()
                    strDemo = myReader.Item("Demo").ToString().ToUpper()
                    strDataDemo = myReader.Item("DataDemo").ToString()
                    strDataDemoTxt = myReader.Item("DataDemoText").ToString()
                    strOra = myReader.Item("Ora").ToString()
                    strLinkWebex = myReader.Item("linkWebex").ToString()
                    IDKey = Int32.Parse(myReader.Item("IDKey"))
                    Tabella = myReader.Item("Tabella").ToString()
                    'NoteDemo = myReader.Item("NoteDemo").ToString()

                    Dim strTest As String
                    Dim strDest As String
                    Dim strDest2 As String
                    Dim strDest3 As String
                    Dim strDest4 As String

                    strDest = ""
                    strDest2 = ""
                    strDest3 = ""
                    strDest4 = ""

                    strTest = System.Configuration.ConfigurationManager.AppSettings("TESTGuestTomorrow").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************' .Replace("#testo", testo)
                    MailContattoAgg = ""

                    '20230509
                    Dim myConnCA As New SqlConnection
                    myConnCA = New SqlConnection(strConnectionDB)
                    myConnCA.Open()

                    If strTest.ToUpper().Trim().ToString() = "S" Then
                        MailContattoAgg = ""
                    Else
                        Dim sqlContAgg As String
                        Dim myCmdCA As SqlCommand
                        Dim myReaderCA As SqlDataReader
                        sqlContAgg = "Select u.Email from Presales_CalendarioWeekly p "
                        sqlContAgg = sqlContAgg & "Join users u on u.ID=p.IdUtente "
                        sqlContAgg = sqlContAgg & "where Tabella ='" & Tabella & "' "
                        sqlContAgg = sqlContAgg & "And IDKey = " & IDKey.ToString() & " And p.IdUtente Not in "
                        sqlContAgg = sqlContAgg & "(select u.ID from Presales p join users u on u.Email=p.mail "
                        sqlContAgg = sqlContAgg & "where Tabella ='" & Tabella & "' and idkey=" & IDKey.ToString() & ")"

                        myCmdCA = myConnCA.CreateCommand
                        myCmdCA.CommandText = sqlContAgg

                        myReaderCA = myCmdCA.ExecuteReader()

                        If myReaderCA.HasRows Then
                            Do While myReaderCA.Read
                                MailContattoAgg = MailContattoAgg & myReaderCA.Item("Email").ToString() & ";"
                            Loop
                            MailContattoAgg = MailContattoAgg.Substring(0, MailContattoAgg.Length - 1)
                        End If
                    End If
                    myConnCA.Close()

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                        'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailContatto = strDest 'TEST

                    End If




                    Try


                        Inviamail(strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo.ToUpper()), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra), strDataDemoTxt, "", Nothing)
                        sw.WriteLine("Invio email a presale " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto & " per la demo " & strDemo)


                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()


                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INVIO MAIL A PRESALE  " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn2.Close()


                    End Try

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'





                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailPresaleWebex = True

        Exit Function

    End Function

    Private Function InvioFileInfograficaEsigenze(idRichiesta As Integer, NoteDemo As String) As String()

        Dim ret As Int32
        Dim strSQL As String
        Dim strSQLcount As String
        Dim myReaderIG As SqlDataReader
        Dim myCmdIG As SqlCommand
        Dim myConnIG As SqlConnection

        'Dim myReaderIG1 As SqlDataReader
        Dim myCmdIG1 As SqlCommand

        Dim Path_Cartella As String = My.Application.Info.DirectoryPath & "\INFOGRAFICA"

        If Not Directory.Exists(Path_Cartella) Then
            Directory.CreateDirectory(Path_Cartella)
        End If


        myConnIG = New SqlConnection(strConnectionDB) 'Create a Connection object.
        myConnIG.Open()

        strSQL = "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Sezione else cd.Note2 End Demo , ps.Sezione Voce, isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID  IDInfo "
        strSQL = strSQL & "From Calendario_Demo cd "
        strSQL = strSQL & "Join Preventivi_Sezioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Sezioni' "
        strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Sezioni' "
        strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Sezioni' "
        strSQL = strSQL & "where AbilitazioneDemo = 1 And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
        strSQL = strSQL & "union "
        strSQL = strSQL & "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Funzione else cd.Note2 End Demo , ps.Funzione Voce,isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID IDInfo "
        strSQL = strSQL & "From Calendario_Demo cd "
        strSQL = strSQL & "Join Preventivi_Funzioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Funzioni' "
        strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Funzioni' "
        strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Funzioni' "
        strSQL = strSQL & "where ps.ID Not in (Select IDFunzione from Gruppi_Demo_Funzioni) And AbilitazioneDemo = 1 "
        strSQL = strSQL & "And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
        strSQL = strSQL & "and i.NoteDemo='" & NoteDemo & "'"

        'modifica 30/09/2021 per gestione demo "doppie"
        strSQL = "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Sezione else cd.Note2 End Demo , ps.Sezione Voce, isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID  IDInfo "
        strSQL = strSQL & "From Calendario_Demo cd "
        strSQL = strSQL & "Join Preventivi_Sezioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Sezioni' "
        strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Sezioni' and i.notedemo = cd.Note2 "
        strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Sezioni' and sd.NoteDemo = cd.Note2 "
        strSQL = strSQL & "where AbilitazioneDemo = 1 And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
        strSQL = strSQL & "and i.NoteDemo='" & NoteDemo & "' "
        strSQL = strSQL & "union "
        strSQL = strSQL & "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Funzione else cd.Note2 End Demo , ps.Funzione Voce,isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID IDInfo "
        strSQL = strSQL & "From Calendario_Demo cd "
        strSQL = strSQL & "Join Preventivi_Funzioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Funzioni' "
        strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Funzioni' and i.notedemo = cd.Note2 "
        strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Funzioni' and sd.NoteDemo = cd.Note2 "
        strSQL = strSQL & "where ps.ID Not in (Select IDFunzione from Gruppi_Demo_Funzioni) And AbilitazioneDemo = 1 "
        strSQL = strSQL & "And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
        strSQL = strSQL & "and i.NoteDemo='" & NoteDemo & "'"

        strSQLcount = "select count(*) ret from(" & strSQL & ") as t"

        myCmdIG1 = myConnIG.CreateCommand
        myCmdIG1.CommandText = strSQLcount
        myCmdIG1.Parameters.AddWithValue("@IDRichiesta", idRichiesta)

        ret = CInt(myCmdIG1.ExecuteScalar())

        If ret > 0 Then



            myCmdIG = myConnIG.CreateCommand
            myCmdIG.CommandText = strSQL
            myCmdIG.Parameters.AddWithValue("@IDRichiesta", idRichiesta)

            myReaderIG = myCmdIG.ExecuteReader()


            'Dim data1() As Net.Mail.Attachment

            Dim path(ret - 1) As String
            Dim y As Integer
            y = 0
            If myReaderIG.HasRows Then
                Do While myReaderIG.Read

                    Dim file As Byte() = myReaderIG(“FileAllegato”)

                    path(0) = Path_Cartella + "\" + myReaderIG(“Demo”) + ".pdf"

                    path(0) = path(0).Replace("'", "")
                    path(0) = path(0).Replace("%", "")
                    path(0) = path(0).Replace("&", "")

                    If System.IO.File.Exists(path(0)) = False Then
                        Dim fileStream As New FileStream(path(0), FileMode.Append, FileAccess.Write)

                        Dim utf8withNoBOM As New System.Text.UTF8Encoding(False)

                        Dim sw As New StreamWriter(fileStream, utf8withNoBOM)

                        sw.BaseStream.Write(file, 0, file.Length)

                        sw.Flush()

                        sw.Close()

                        fileStream.Close()
                    End If
                    System.IO.File.WriteAllBytes(path(0), file)

                    y = y + 1
                Loop

                'Dim dir As New DirectoryInfo(Path_Cartella)
                'For Each f In dir.GetFiles()

                '    Dim filePDF As String = f.Name
                '    Dim data As Net.Mail.Attachment = New Net.Mail.Attachment(Path_Cartella & "/" & filePDF)

                '    Return data
                '    'data.Name = f.Name
                '    'MailMessage.Attachments.Add(data)

                'Next


            End If
            myConnIG.Close()
            Return path

        Else
            myConnIG.Close()
            Return Nothing

        End If

    End Function


    Private Function InvioFileInfografica(idRichiesta As Integer, NoteDemo As String) As String()

        Dim ret As Int32
        Dim strSQL As String
        Dim strSQLcount As String
        Dim myReaderIG As SqlDataReader
        Dim myCmdIG As SqlCommand
        Dim myConnIG As SqlConnection

        'Dim myReaderIG1 As SqlDataReader
        Dim myCmdIG1 As SqlCommand

        Dim Path_Cartella As String = My.Application.Info.DirectoryPath & "\INFOGRAFICA"

        If Not Directory.Exists(Path_Cartella) Then
            Directory.CreateDirectory(Path_Cartella)
        End If

        Try

            myConnIG = New SqlConnection(strConnectionDB) 'Create a Connection object.
            myConnIG.Open()

            strSQL = "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Sezione else cd.Note2 End Demo , ps.Sezione Voce, isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID  IDInfo "
            strSQL = strSQL & "From Calendario_Demo cd "
            strSQL = strSQL & "Join Preventivi_Sezioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Sezioni' "
            strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Sezioni' "
            strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Sezioni' "
            strSQL = strSQL & "where AbilitazioneDemo = 1 And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
            strSQL = strSQL & "union "
            strSQL = strSQL & "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Funzione else cd.Note2 End Demo , ps.Funzione Voce,isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID IDInfo "
            strSQL = strSQL & "From Calendario_Demo cd "
            strSQL = strSQL & "Join Preventivi_Funzioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Funzioni' "
            strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Funzioni' "
            strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Funzioni' "
            strSQL = strSQL & "where ps.ID Not in (Select IDFunzione from Gruppi_Demo_Funzioni) And AbilitazioneDemo = 1 "
            strSQL = strSQL & "And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
            strSQL = strSQL & "and i.NoteDemo='" & NoteDemo & "'"

            'modifica 30/09/2021 per gestione demo "doppie"
            strSQL = "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Sezione else cd.Note2 End Demo , ps.Sezione Voce, isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID  IDInfo "
            strSQL = strSQL & "From Calendario_Demo cd "
            strSQL = strSQL & "Join Preventivi_Sezioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Sezioni' "
            strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Sezioni' and i.notedemo = cd.Note2 "
            strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Sezioni' and sd.NoteDemo = cd.Note2 "
            strSQL = strSQL & "where AbilitazioneDemo = 1 And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
            strSQL = strSQL & "and i.NoteDemo='" & NoteDemo & "' "
            strSQL = strSQL & "union "
            strSQL = strSQL & "Select distinct cd.IDKey, cd.Tabella, Case When cd.Note2 Is null Or cd.Note2 = '' then ps.Funzione else cd.Note2 End Demo , ps.Funzione Voce,isNull(i.AbilitaInvio,0) AbilitaInvio,i.FileAllegato, i.ID IDInfo "
            strSQL = strSQL & "From Calendario_Demo cd "
            strSQL = strSQL & "Join Preventivi_Funzioni ps on ps.ID = cd.IDKey And cd.Tabella='Preventivi_Funzioni' "
            strSQL = strSQL & "Left Join InfoGraficaDemo i on i.IDKey = cd.IDKey And i.Tabella = 'Preventivi_Funzioni' and i.notedemo = cd.Note2 "
            strSQL = strSQL & "Left Join Storico_DemoClienti  sd on sd.IDKey = cd.IDKey And sd.Tabella = 'Preventivi_Funzioni' and sd.NoteDemo = cd.Note2 "
            strSQL = strSQL & "where ps.ID Not in (Select IDFunzione from Gruppi_Demo_Funzioni) And AbilitazioneDemo = 1 "
            strSQL = strSQL & "And sd.IDRichiesta = @IDRichiesta And i.AbilitaInvio = 1 And i.FileAllegato Is Not null "
            strSQL = strSQL & "and i.NoteDemo='" & NoteDemo & "'"

            strSQLcount = "select count(*) ret from(" & strSQL & ") as t"

            myCmdIG1 = myConnIG.CreateCommand
            myCmdIG1.CommandText = strSQLcount
            myCmdIG1.Parameters.AddWithValue("@IDRichiesta", idRichiesta)

            ret = CInt(myCmdIG1.ExecuteScalar())

            If ret > 0 Then



                myCmdIG = myConnIG.CreateCommand
                myCmdIG.CommandText = strSQL
                myCmdIG.Parameters.AddWithValue("@IDRichiesta", idRichiesta)

                myReaderIG = myCmdIG.ExecuteReader()


                'Dim data1() As Net.Mail.Attachment

                Dim path(ret - 1) As String
                Dim y As Integer
                y = 0
                If myReaderIG.HasRows Then
                    Do While myReaderIG.Read

                        Dim file As Byte() = myReaderIG(“FileAllegato”)

                        path(0) = Path_Cartella + "\" + myReaderIG(“Demo”) + ".pdf"

                        path(0) = path(0).Replace("'", "")
                        path(0) = path(0).Replace("%", "")
                        path(0) = path(0).Replace("&", "")

                        If System.IO.File.Exists(path(0)) = False Then
                            Dim fileStream As New FileStream(path(0), FileMode.Append, FileAccess.Write)

                            Dim utf8withNoBOM As New System.Text.UTF8Encoding(False)

                            Dim sw As New StreamWriter(fileStream, utf8withNoBOM)

                            sw.BaseStream.Write(file, 0, file.Length)

                            sw.Flush()

                            sw.Close()

                            fileStream.Close()
                        End If
                        'System.IO.File.WriteAllBytes(path(0), file)

                        y = y + 1
                    Loop

                    'Dim dir As New DirectoryInfo(Path_Cartella)
                    'For Each f In dir.GetFiles()

                    '    Dim filePDF As String = f.Name
                    '    Dim data As Net.Mail.Attachment = New Net.Mail.Attachment(Path_Cartella & "/" & filePDF)

                    '    Return data
                    '    'data.Name = f.Name
                    '    'MailMessage.Attachments.Add(data)

                    'Next


                End If
                myConnIG.Close()
                Return path

            Else
                myConnIG.Close()
                Return Nothing

            End If

        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Private Function InviaMailDemoReminderGUESToggi() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim IDRichiesta As Int32
        Dim ConfigurazionePerInterni As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDDemo As Int32
        Dim IDStorico As Int32
        Dim NomeAzienda As String
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        'Dim MailPresales As String

        Dim strDataDemo As String
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        Dim IDKey As Int32
        Dim Tabella As String
        Dim NoteDemo As String

        NoteDemo = ""

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ReminderInvioLinkToday" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio controllo Date demo schedulate per il giorno successivo -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "Select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReaderContAgg As SqlDataReader
        Dim myCmdContAgg As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='TestoNotificaGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectNotificaGuest'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        strSQL = "Select P.ID IDRichiesta, c.ID IDDemo, s.ID IDStorico, NomeAzienda,NomeContatto,MailContatto,c.linkWebex,c.Tabella,CASE WHEN c.Tabella = 'Preventivi_Sezioni' THEN ps.Sezione WHEN c.Tabella = 'Preventivi_Funzioni' THEN pf.Funzione END AS Demo,"
        strSQL = strSQL & "isNull(a.Nominativo,'') Commerciale, "
        strSQL = strSQL & "c.id, s.DataDemo,Convert(varchar(10),s.DataDemo,103) DataDemoText,isNull(c.Note,'') Ora, isNull(Completata,0) Completata, isNull(Note2,'')NoteDemo, c.IDKey, c.Tabella, isnull(p.ConfigurazionePerInterni,0) ConfigurazionePerInterni "
        strSQL = strSQL & "From storico_democlienti s "
        strSQL = strSQL & "Left Join Preventivi_Richieste p on s.IDRichiesta = p.ID "
        strSQL = strSQL & "Left Join Anagrafica_commerciali a on a.ID = p.IDCommerciale "
        strSQL = strSQL & "Join Calendario_Demo c on (c.IDKey = s.IDkey And c.Tabella=s.tabella And c.DataDemo = s.DataDemo and c.Note=s.Orario) "
        strSQL = strSQL & "Left Join Preventivi_Sezioni ps on ps.ID = c.IDKey And c.Tabella='Preventivi_Sezioni' "
        strSQL = strSQL & "Left Join Preventivi_Funzioni pf on pf.ID = c.IDKey And c.Tabella='Preventivi_Funzioni' "
        strSQL = strSQL & "where CONVERT(char(10), s.DataDemo,126) = CONVERT(char(10), GetDate(),126) "
        strSQL = strSQL & "And p.MailContatto<>'' "
        strSQL = strSQL & "order by p.MailContatto "

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    IDRichiesta = Int32.Parse(myReader.Item("IDRichiesta"))
                    IDDemo = Int32.Parse(myReader.Item("IDDemo"))
                    IDStorico = Int32.Parse(myReader.Item("IDStorico"))
                    NomeAzienda = myReader.Item("NomeAzienda").ToString()
                    NomeContatto = myReader.Item("NomeContatto").ToString()
                    MailContatto = myReader.Item("MailContatto").ToString()
                    strDemo = myReader.Item("Demo").ToString()
                    strDataDemo = myReader.Item("DataDemo").ToString()
                    strDataDemoTxt = myReader.Item("DataDemoText").ToString()
                    strOra = myReader.Item("Ora").ToString()
                    strLinkWebex = myReader.Item("linkWebex").ToString()
                    IDKey = Int32.Parse(myReader.Item("IDKey"))
                    Tabella = myReader.Item("Tabella").ToString()
                    NoteDemo = myReader.Item("NoteDemo").ToString()
                    ConfigurazionePerInterni = Int32.Parse(myReader.Item("ConfigurazionePerInterni"))

                    Dim strTest As String
                    Dim strDest As String
                    Dim strDest2 As String
                    Dim strDest3 As String
                    Dim strDest4 As String
                    strDest = ""
                    strTest = System.Configuration.ConfigurationManager.AppSettings("TESTGuestToday").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'
                    MailContattoAgg = ""

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                        strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailContatto = strDest 'TEST

                    End If

                    MailContattoAgg = ""
                    Try

                        '
                        Dim bInviaMail As Boolean
                        Dim myReader4 As SqlDataReader
                        Dim myCmd4 As New SqlCommand

                        Dim oDate As DateTime
                        oDate = Convert.ToDateTime(strDataDemoTxt)
                        oDate = oDate.AddDays(-1)

                        Dim myConn3 = New SqlConnection(strConnectionDB)
                        myConn3.Open()
                        bInviaMail = False
                        'strOggetto = ""
                        'testoHtml = ""
                        myCmd4 = myConn3.CreateCommand
                        myCmd4.CommandText = "Select * from LogMailOperation where year(DataOra)=@anno and month(DataOra)=@mese and day(DataOra)>=@giorno and DestinatarioPrincipale=@DestinatarioPrincipale and IDRichiesta=@IDRichiesta"
                        myCmd4.Parameters.Add("@anno", SqlDbType.Int).Value = oDate.Year
                        myCmd4.Parameters.Add("@mese", SqlDbType.Int).Value = oDate.Month
                        myCmd4.Parameters.Add("@giorno", SqlDbType.Int).Value = oDate.Day

                        myCmd4.Parameters.Add("@DestinatarioPrincipale", SqlDbType.VarChar).Value = MailContatto
                        myCmd4.Parameters.Add("@IDRichiesta", SqlDbType.Int).Value = IDRichiesta
                        myReader4 = myCmd4.ExecuteReader()
                        If myReader4.HasRows = False Then
                            bInviaMail = True
                        End If

                        myReader4.Close()
                        myConn3.Close()
                        '

                        '**************************************************************'
                        '****************** INVIO FILE INFOGRAFICA ********************'
                        '**************************************************************'
                        'Dim data As Net.Mail.Attachment = InvioFileInfografica(IDRichiesta)

                        Dim dataarray() As String = InvioFileInfografica(IDRichiesta, NoteDemo)

                        '**************************************************************'
                        '****************** INVIO FILE INFOGRAFICA ********************'
                        '**************************************************************'

                        If bInviaMail = True Then

                            Inviamail(strOggetto.Replace("#data", strDataDemoTxt).Replace("per domani", "per oggi").Replace("#demo", strDemo), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra).Replace("per domani", "per oggi"), strDataDemoTxt, "", dataarray)

                            sw.WriteLine("Invio email contatto principale a " & MailContatto & " " & DateTime.Now)

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            myConn2 = New SqlConnection(strConnectionDB)
                            myConn2.Open()
                            Using comm As New SqlCommand()
                                With comm
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo).Replace("#demo", strDemo))
                                    .Parameters.AddWithValue("@Esito", "OK")
                                    If dataarray IsNot Nothing Then
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo & " con allegato ")
                                    Else
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                    End If

                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                End With
                                Try
                                    comm.ExecuteNonQuery()
                                Catch ex As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using

                        Else
                            sw.WriteLine("*** Mail già inviata ieri a " & MailContatto & " " & DateTime.Now)
                        End If

                    Catch ex As Exception
                        MsgBox(ex.Message)
                        sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using

                    End Try

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'



                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO AGGIUNTIVO **************'
                    '**************************************************************'
                    Dim strSQLcontagg As String

                    MailContattoAgg = ""
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    myCmdContAgg = myConn2.CreateCommand
                    'myCmdContAgg.CommandText = "Select Mail,Nominativo FROM Storico_Demo_Contatti where IDStoricoDemo = " & IDStorico.ToString()

                    strSQLcontagg = "Select Mail,Nominativo, '' Cognome FROM Storico_Demo_Contatti where IDStoricoDemo = " & IDStorico.ToString() & " "
                    strSQLcontagg = strSQLcontagg & " UNION "
                    strSQLcontagg = strSQLcontagg & "Select Mail, (Nome + ' ' + Cognome) Nominativo, Cognome from Preventivi_Richieste_Contatti where IDRichiesta = " & IDRichiesta.ToString()
                    myCmdContAgg.CommandText = strSQLcontagg

                    myReaderContAgg = myCmdContAgg.ExecuteReader()

                    Do While myReaderContAgg.Read()

                        MailContatto = myReaderContAgg.Item("Mail")
                        NomeContatto = myReaderContAgg.Item("Nominativo").ToString()

                        MailContattoAgg = ""

                        If strTest.ToUpper().Trim().ToString() = "S" Then
                            MailContatto = strDest 'TEST
                            strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMailContatto").ToString()
                        End If



                        '
                        Dim bInviaMail5 As Boolean
                        Dim myReader5 As SqlDataReader
                        Dim myCmd5 As New SqlCommand

                        Dim oDate5 As DateTime
                        oDate5 = Convert.ToDateTime(strDataDemoTxt)
                        oDate5 = oDate5.AddDays(-1)

                        Dim myConn5 = New SqlConnection(strConnectionDB)
                        myConn5.Open()
                        bInviaMail5 = False
                        'strOggetto = ""
                        'testoHtml = ""
                        myCmd5 = myConn5.CreateCommand
                        myCmd5.CommandText = "Select * from LogMailOperation where year(DataOra)=@anno and month(DataOra)=@mese and day(DataOra)=@giorno and DestinatarioPrincipale=@DestinatarioPrincipale and IDRichiesta=@IDRichiesta"
                        myCmd5.Parameters.Add("@anno", SqlDbType.Int).Value = oDate5.Year
                        myCmd5.Parameters.Add("@mese", SqlDbType.Int).Value = oDate5.Month
                        myCmd5.Parameters.Add("@giorno", SqlDbType.Int).Value = oDate5.Day

                        myCmd5.Parameters.Add("@DestinatarioPrincipale", SqlDbType.VarChar).Value = MailContatto
                        'myCmd4.Parameters.Add("@Oggetto", SqlDbType.VarChar).Value = strOggetto.Replace("#data", oDate.ToString())
                        myCmd5.Parameters.Add("@IDRichiesta", SqlDbType.Int).Value = IDRichiesta
                        myReader5 = myCmd5.ExecuteReader()
                        If myReader5.HasRows = False Then
                            bInviaMail5 = True
                        End If

                        myReader5.Close()
                        myConn5.Close()
                        '


                        If bInviaMail5 = True Then
                            Try
                                Inviamail(strOggetto.Replace("#data", strDataDemoTxt).Replace("per domani", "per oggi").Replace("#demo", strDemo), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra).Replace("per domani", "per oggi"), strDataDemoTxt, "", Nothing)
                                sw.WriteLine("Invio email contatto aggiuntivo a " & MailContatto & " " & DateTime.Now)

                                Dim query As String
                                query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                                'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,)"
                                myConn2 = New SqlConnection(strConnectionDB)
                                myConn2.Open()
                                Using comm As New SqlCommand()
                                    With comm
                                        .Connection = myConn2
                                        .CommandType = CommandType.Text
                                        .CommandText = query
                                        .Parameters.AddWithValue("@Dest", MailContatto)
                                        .Parameters.AddWithValue("@CC", MailContattoAgg)
                                        .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                        .Parameters.AddWithValue("@Esito", "OK")
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO AGGIUNTIVO A  " & MailContatto & " per la demo " & strDemo)
                                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                                        .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                    End With
                                    Try
                                        comm.ExecuteNonQuery()
                                    Catch ex As Exception
                                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                    End Try
                                End Using


                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO AGGIUNTIVO A  " & MailContatto & " " & DateTime.Now)

                                Dim query As String
                                query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                                'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                                myConn2 = New SqlConnection(strConnectionDB)
                                myConn2.Open()
                                Using comm As New SqlCommand()
                                    With comm
                                        .Connection = myConn2
                                        .CommandType = CommandType.Text
                                        .CommandText = query
                                        .Parameters.AddWithValue("@Dest", MailContatto)
                                        .Parameters.AddWithValue("@CC", MailContattoAgg)
                                        .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", strDataDemoTxt).Replace("#demo", strDemo))
                                        .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                                        .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                                    End With
                                    Try
                                        comm.ExecuteNonQuery()
                                    Catch ex1 As Exception
                                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                    End Try
                                End Using
                                myConn2.Close()

                            End Try
                        End If
                        '**************************************************************'
                        '*************** INVIO EMAIL CONTATTO AGGIUNTIVO **************'
                        '**************************************************************'

                    Loop
                    myReaderContAgg.Close()
                    myConn2.Close()

                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailDemoReminderGUESToggi = True

        Exit Function

    End Function




    Private Sub Inviamail(strVoce As String, strTesto As String, strMailDest As String, strMailAgg As String, sw As StreamWriter, testoHtml As String, strDataDemo As String, strLista As String, data As String())


        Dim mySmtp As New SmtpClient
        Dim myMail As New MailMessage()
        Dim strTest As String

        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()
        If strTest.ToUpper().Trim().ToString() = "S" Then
            strVoce = "[AMBIENTE DI TEST] " & strVoce
        End If

        'NB il server di demo di Zucchetti non ha la richiesta di autenticazione)
        If (Convert.ToBoolean(Mail_SmtpAuth) = True) Then
            mySmtp.UseDefaultCredentials = False
            mySmtp.Credentials = New System.Net.NetworkCredential(Mail_Username, stPasswordDecifrata)
        End If

        mySmtp.EnableSsl = Convert.ToBoolean(Mail_EnableSSL)
        mySmtp.Host = Mail_Host

        If (Mail_Host.Contains("smtp.office365.com") = True) Then
            mySmtp.TargetName = "STARTTLS/smtp.office365.com"
        End If

        Try

            myMail = New MailMessage()
            myMail.From = New MailAddress(Mail_Address)

            If strMailDest <> "" Then

                Dim Str1 As String = strMailDest
                Dim strarr1() As String
                strarr1 = Str1.Split(";")
                For Each s As String In strarr1
                    'myMail.CC.Add(s)
                    If s <> "" Then
                        myMail.To.Add(s)
                    End If

                Next
            End If
            'myMail.To.Add(strMailDest)
            If strMailAgg <> "" Then

                Dim Str As String = strMailAgg
                Dim strarr() As String
                strarr = Str.Split(";")
                For Each s As String In strarr
                    If s <> "" Then
                        myMail.CC.Add(s)
                    End If
                Next
            End If
            myMail.Subject = strVoce.ToString().ToUpper()
            myMail.IsBodyHtml = True



            'myMail.Body = "<p style='font-family:calibri'>" & strTesto & "</p>"

            'testoHtml = testoHtml.Replace("#DataDemo", strDataDemo)
            'testoHtml = testoHtml.Replace("#strLista", strLista)

            myMail.Body = "<p style='font-family:calibri'>" & testoHtml & "</p>"

            myMail.Priority = MailPriority.High
            myMail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure

            Try
                If Not (data Is Nothing) Then
                    For j = 0 To data.Count - 1
                        If Not (data(j) Is Nothing) Then
                            Dim file As New Net.Mail.Attachment(data(j))
                            Try
                                myMail.Attachments.Add(file)
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                End If
            Catch ex As Exception
                If Not (sw Is Nothing) Then
                    sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Errore su allegato per " + strMailDest)
                End If
            End Try

            If sInviaMail = "S" Then
                Try
                    mySmtp.Send(myMail)
                Catch ex As Exception
                    sw.WriteLine("Errore SMTP")
                    Threading.Thread.Sleep(3000)
                    sw.WriteLine("reinvio mail")
                    mySmtp.Send(myMail)
                    sw.WriteLine("reinviata mail")
                End Try

            End If

            myMail.Dispose()
            mySmtp.Dispose()

            Console.WriteLine("Email inviata")

        Catch se As SmtpException
            Console.WriteLine(se.ToString)

            If Not (sw Is Nothing) Then
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Errore Invio Mail Richiesta " & DateTime.Now)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Nominativo = " + Mail_Name)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Mail = " + Mail_Address)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Messaggio = " + testoHtml)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Oggetto = " + strVoce.ToString().ToUpper())
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Errore = " + se.Message + " " + se.StackTrace)
            End If

        Catch e As Exception
            Console.WriteLine(e.ToString)

            If Not (sw Is Nothing) Then
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Errore Invio Mail Richiesta " & DateTime.Now)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Nominativo = " + Mail_Name)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Mail = " + Mail_Address)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Messaggio = " + testoHtml)
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Oggetto = " + strVoce.ToString().ToUpper())
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " Errore = " + e.Message + " " + e.StackTrace)
            End If

        End Try

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim stConnessione As String

        sInviaMail = "S"

        sInviaMail = System.Configuration.ConfigurationManager.AppSettings("InviaMail").ToString()

        stConnessione = System.Configuration.ConfigurationManager.ConnectionStrings("connectionString").ToString()
        strConnectionDB = System.Configuration.ConfigurationManager.ConnectionStrings("connectionString").ToString()

        strDateFormat = System.Configuration.ConfigurationManager.AppSettings("dateformat").ToString()

        Me.Cursor = Cursors.WaitCursor

        inputName = ""
        inputIDAM = ""

        For Each s As String In My.Application.CommandLineArgs
            If s.ToLower.StartsWith(inputArgument) Then
                inputName = s.Remove(0, inputArgument.Length)
            End If
            If s.ToLower.StartsWith(inputArgument2) Then
                inputIDAM = s.Remove(0, inputArgument2.Length)
            End If
        Next

        ' ***********************************************************************************************************
        ' inputname=1 invio email ai guest per le demo di domani
        ' inputname=2 invio email ai guest per le demo di oggi
        ' inputname=3 invio mail ai presales per le demo di oggi con la lista dei contatti
        ' inputname=4 invio mail ai commerciali per le demo di oggi con la lista dei contatti
        ' inputname=5 controllo storico demo
        ' inputname=6 Backup della tabella StoricoDemoClienti 
        ' inputname=7 Invio mail ai partecipanti alle demo del giorno 
        ' inputname=8 Invio mail ai NON partecipanti alle demo del giorno e invio mail ai commerciali con l'elenco dei NON partecipanti alle demo del giorno 
        ' inputname=9 Invio mail RIEPILOGO MENSILE ai commerciali 
        ' inputname=10 Invio mail ai presale di controllo link webex
        ' inputname=11 Controllo anagrafiche duplicate e richierste senza funzioni collegate
        ' inputname=12 Invio email ai presales in caso di demo 1to1 cn data non valorizzata dopo 3 giorni
        ' inputname=13 Invio email ai presales in caso di demo 1to1 cn data valorizzata ma non eseguita dopo 2 giorni
        ' inputname=14 Invio mail RIEPILOGO MENSILE ai Presales
        ' inputname=15 PopolaStatiDemo
        ' inputname=16 PopolaGetDemoAdminTarget
        ' inputname=22 Invio mail RIEPILOGO MENSILE ai Product Manager
        ' inputname=23 Report Area manager
        ' inputname=24 Invio report Coordinatore AM
        ' inputname=25 INVIO MAIL AL COMMERCIALE CON LE ESIGENZE AGGIUNTE DAL PRESALE PER I CLIENTI
        ' inputname=26 INVIO MAIL AL PRESALE ESIGENZE AGGIUNTIVE
        ' inputname=27 INVIO MAIL ESIGENZE AGGIUNTIVE AL PROSPECT CON LE ESIGENZE AGGIUNTE DAL PRESALE
        ' inputname=28 Popola Attività Future dei presale
        ' inputname=29 Popolamento tabella Storico_Percentuali_Selezione_Prodotti
        ' inputname=30 ControlloIdCommerciale
        ' inputname=31 InvioMailCommercialiDemo1to1Eseguita
        ' inputname=32 InvioMailFeedbackDemo1to1
        ' ***********************************************************************************************************

        If inputName = "1" Then
            InviaMailDemoReminderGUEST()
            End
        End If
        If inputName = "2" Then
            InviaMailDemoReminderGUESToggi()
            End
        End If
        If inputName = "3" Then
            InviomailPresales()
            End
        End If
        If inputName = "4" Then
            InviomailCommerciali()
            End
        End If
        If inputName = "USERLOCKED" Then
            resetUserLocked()
            End
        End If
        If inputName = "5" Then
            ControlloStoricoDemo()
            End
        End If
        If inputName = "6" Then
            BackupStoricoDemo()
            End
        End If
        If inputName = "7" Then
            InviaMailPartecipantiDemo()
            End
        End If
        If inputName = "8" Then

            InviaMailNONPartecipantiDemoComm(DateTime.Now.ToString("dd/MM/yyyy"))
            Threading.Thread.Sleep(4000)
            InviaMailNONPartecipantiDemo(DateTime.Now.ToString("dd/MM/yyyy"))
            Threading.Thread.Sleep(4000)

            InviaMailNONPartecipantiDemoComm(DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"))
            Threading.Thread.Sleep(4000)
            InviaMailNONPartecipantiDemo(DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"))
            Threading.Thread.Sleep(4000)

            InviaMailNONPartecipantiDemoComm(DateTime.Now.AddDays(-2).ToString("dd/MM/yyyy"))
            Threading.Thread.Sleep(4000)
            InviaMailNONPartecipantiDemo(DateTime.Now.AddDays(-2).ToString("dd/MM/yyyy"))

            InviaMailNONPartecipantiDemoComm(DateTime.Now.AddDays(-3).ToString("dd/MM/yyyy"))
            Threading.Thread.Sleep(4000)
            InviaMailNONPartecipantiDemo(DateTime.Now.AddDays(-3).ToString("dd/MM/yyyy"))

            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminderNONPARTECIPANTI_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

            sw.WriteLine("Inizio invio mail ai NON partecipanti alle demo -- " & DateTime.Now)
            sw.Close()

            End
        End If
        If inputName = "9" Then

            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String

            strSQL = "SELECT ID,Nominativo,isnull(AbilitaInvioFileRiepilogo,0) AbilitaInvioFileRiepilogo FROM Anagrafica_Commerciali where isnull(AbilitaInvioRiepilogo,0)=1 and  Nominativo not in ('** BO PARTNER **', 'DEALER')"
            'strSQL = "SELECT ID,Nominativo,isnull(AbilitaInvioFileRiepilogo,0) AbilitaInvioFileRiepilogo FROM Anagrafica_Commerciali where isnull(AbilitaInvioRiepilogo,0)=1 and  Nominativo not in ('** BO PARTNER **', 'DEALER') and id in (45,46)"


            'Prendo solo i commerciali non revocati
            strSQL = "Select ac.ID,ac.Nominativo,isnull(AbilitaInvioFileRiepilogo,0) AbilitaInvioFileRiepilogo FROM Anagrafica_Commerciali ac "
            strSQL = strSQL & "Join users u on ac.mail=u.Email "
            strSQL = strSQL & "where isnull(AbilitaInvioRiepilogo, 0) = 1 And ac.Nominativo Not in ('** BO PARTNER **', 'DEALER') "
            strSQL = strSQL & "And (u.datarevoca Is null Or (u.datarevoca Is Not null And u.datarevoca>getdate())) "
            'strSQL = strSQL & "And ac.nominativo Like '%mastroianni%' "



            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim ID1 As Integer
            Dim Nominativo1 As String
            Dim AbilitaInvioFileRiepilogo As Integer

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                ID1 = myReader1.Item("ID")
                Nominativo1 = myReader1.Item("Nominativo")
                AbilitaInvioFileRiepilogo = myReader1.Item("AbilitaInvioFileRiepilogo")

                InviaMailRiepilgoMensileCommerciali(CInt(ID1), AbilitaInvioFileRiepilogo, Nominativo1)

                Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myConn.Close()

            End
        End If
        If inputName = "10" Then
            If Weekday(Now) = 1 Or Weekday(Now) = 7 Then
                End
            End If
            If Weekday(Now) = 6 Then
                InviaMailPresaleWebex(3)
                InviaMailPresaleWebex(4)
            Else
                InviaMailPresaleWebex(2)
            End If
            End
        End If
        If inputName = "11" Then
            ControlloAnagDuplicate()
            ControlloRichiesteSenzaFunzioni()
            End
        End If
        If inputName = "12" Then
            InviaMailPresaleAlertDemo1to1()
            End
        End If
        If inputName = "13" Then
            InviaMailPresalesAlertNonEseguita1to1()
            End
        End If
        If inputName = "14" Then

            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String

            'seleziono i presale presales (non product manager) non revocati
            strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=0 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim ID1 As Integer
            Dim Nominativo1 As String
            Dim Descrizione1 As String
            Dim Email1 As String

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                ID1 = myReader1.Item("ID")
                Nominativo1 = myReader1.Item("Nominativo")
                Descrizione1 = myReader1.Item("Descrizione")
                Email1 = myReader1.Item("Email")

                InviaMailRiepilgoMensilePresales(CInt(ID1), Nominativo1, Descrizione1, Email1)

                Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myConn.Close()

            End
        End If
        If inputName = "15" Then
            PopolaStatiDemo()
            End
        End If
        If inputName = "16" Then
            PopolaGetDemoAdminTarget()
            End
        End If
        If inputName = "18" Then 'Popola TimeLine commerciali

            PopolaTimeLineCommerciali()

            End
        End If
        If inputName = "19" Then 'Popola TimeLine presales

            PopolaTimeLinePresales()

            End
        End If
        If inputName = "20" Then 'Popola TimeLine DIREZIONE

            PopolaTimeLineDirezione()

            End
        End If
        If inputName = "21" Then

            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String

            strSQL = "SELECT ac.ID,ac.Nominativo FROM Anagrafica_Commerciali ac join Users u on u.Email=ac.Mail where isnull(ac.EscludiRiepilogoFeedback,0)=0 and ac.Nominativo not in ('** BO PARTNER **', 'DEALER') and u.idwebprofile=2"
            'strSQL = "SELECT ac.ID,ac.Nominativo FROM Anagrafica_Commerciali ac join Users u on u.Email=ac.Mail where isnull(ac.EscludiRiepilogoFeedback,0)=0 and ac.Nominativo not in ('** BO PARTNER **', 'DEALER') and u.idwebprofile=2 and ac.id IN (177)"

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL

            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim ID1 As Integer
            Dim Nominativo1 As String

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                ID1 = myReader1.Item("ID")
                Nominativo1 = myReader1.Item("Nominativo")

                InviaMailFeedbackCommerciali(CInt(ID1))

                Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myConn.Close()

            End
        End If

        If inputName = "22" Then

            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String

            'seleziono i product manager
            strSQL = "select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=1"
            'strSQL = "select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=1 and ID in (345)" 'TEST"

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim ID1 As Integer
            Dim Nominativo1 As String
            Dim Descrizione1 As String
            Dim Email1 As String

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                ID1 = myReader1.Item("ID")
                Nominativo1 = myReader1.Item("Nominativo")
                Descrizione1 = myReader1.Item("Descrizione")
                Email1 = myReader1.Item("Email")

                InviaMailRiepilgoMensilePM(CInt(ID1), Nominativo1, Descrizione1, Email1)

            Loop
            myReader1.Close()

            myConn.Close()

            End
        End If

        If inputName = "23" Then

            '/mode=23 /inputIDAM=340

            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String

            'seleziono gli area manager
            strSQL = "SELECT distinct IDUtenteAreaManager FROM AreaManager_Partner order by IDUtenteAreaManager"
            'strSQL = "SELECT distinct IDUtenteAreaManager FROM AreaManager_Partner where IDUtenteAreaManager in (340) order by IDUtenteAreaManager"
            strSQL = "SELECT distinct IDUtenteAreaManager FROM AreaManager_Partner where IDUtenteAreaManager in (" & inputIDAM & ") order by IDUtenteAreaManager"

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim IDAM As Integer
            'Dim IDP As Integer
            'Dim Nominativo1 As String
            'Dim Descrizione1 As String
            'Dim Email1 As String

            listID = New List(Of Integer)

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                IDAM = myReader1.Item("IDUtenteAreaManager")
                'IDP = myReader1.Item("IDUtentePartner")

                CreaFilesReportAreaManager(CInt(IDAM))

                'Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            'myConn.Open()

            'Dim myReader1 As SqlDataReader
            'Dim IDAM As Integer
            'Dim IDP As Integer
            'Dim Nominativo1 As String
            'Dim Descrizione1 As String
            'Dim Email1 As String

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                IDAM = myReader1.Item("IDUtenteAreaManager")
                'IDP = myReader1.Item("IDUtentePartner")

                InviaMailReportAreaManager(CInt(IDAM))

                'Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myConn.Close()

            Dim strPathGrafici As String

            strPathGrafici = System.Configuration.ConfigurationManager.AppSettings("pathReportAreaManager").ToString()

            SvuotaDir(strPathGrafici)

            Try
                Dim j As Integer
                Dim procID As Integer
                For j = 0 To listID.Count - 1
                    procID = listID(j)
                    Dim aProcess As System.Diagnostics.Process
                    aProcess = System.Diagnostics.Process.GetProcessById(procID)
                    aProcess.Kill()
                Next
            Catch ex As Exception

            End Try

            End
        End If

        If inputName = "24" Then

            '/mode=24 /inputIDAM=340

            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String

            'seleziono gli area manager
            'strSQL = "SELECT distinct IDUtenteCoordinatoreAM FROM Coordinatori_AreaManager order by IDUtenteCoordinatoreAM"
            'strSQL = "SELECT distinct IDUtenteCoordinatoreAM FROM Coordinatori_AreaManager WHERE IDUtenteCoordinatoreAM=403 order by IDUtenteCoordinatoreAM"
            strSQL = "SELECT distinct IDUtenteCoordinatoreAM FROM Coordinatori_AreaManager WHERE IDUtenteCoordinatoreAM=" & inputIDAM & " order by IDUtenteCoordinatoreAM"

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim IDCoordAM As Integer
            'Dim IDP As Integer
            'Dim Nominativo1 As String
            'Dim Descrizione1 As String
            'Dim Email1 As String

            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailCoordinatoreAM " & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))



            listID = New List(Of Integer)

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                IDCoordAM = myReader1.Item("IDUtenteCoordinatoreAM")
                'IDP = myReader1.Item("IDUtentePartner")

                sw.WriteLine("Crea file (inizio) per Coordinatore Area Manager  a " & IDCoordAM.ToString() & " " & DateTime.Now)
                CreaFilesReportCoordinatoreAM(CInt(IDCoordAM))
                sw.WriteLine("Crea file (fine) per Coordinatore Area Manager  a " & IDCoordAM.ToString() & " " & DateTime.Now)
                'Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            'myConn.Open()

            'Dim myReader1 As SqlDataReader
            'Dim IDAM As Integer
            'Dim IDP As Integer
            'Dim Nominativo1 As String
            'Dim Descrizione1 As String
            'Dim Email1 As String

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                IDCoordAM = myReader1.Item("IDUtenteCoordinatoreAM")
                'IDP = myReader1.Item("IDUtentePartner")

                sw.WriteLine("Invio email (inizio) per Coordinatore Area Manager  a " & IDCoordAM.ToString() & " " & DateTime.Now)
                InviaMailReportCoordinatoreAM(CInt(IDCoordAM))
                sw.WriteLine("Invio email (fine) per Coordinatore Area Manager  a " & IDCoordAM.ToString() & " " & DateTime.Now)

                'Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myConn.Close()

            sw.Close()
            sw = Nothing

            Dim strPathGrafici As String

            strPathGrafici = System.Configuration.ConfigurationManager.AppSettings("pathReportAreaManager").ToString()

            SvuotaDir(strPathGrafici)

            Try
                Dim j As Integer
                Dim procID As Integer
                For j = 0 To listID.Count - 1
                    procID = listID(j)
                    Dim aProcess As System.Diagnostics.Process
                    aProcess = System.Diagnostics.Process.GetProcessById(procID)
                    aProcess.Kill()
                Next
            Catch ex As Exception

            End Try

            End
        End If

        'INVIO MAIL AL COMMERCIALE CON LE ESIGENZE AGGIUNTE DAL PRESALE PER I CLIENTI
        If inputName = "25" Then

            InviaMailInteressiComm()

            InviaMailInteressiComm2()

            End
        End If

        'INVIO MAIL AL PRESALE ESIGENZE AGGIUNTIVE
        If inputName = "26" Then

            InviaMailInteressi()

            End
        End If

        'INVIO MAIL ESIGENZE AGGIUNTIVE al PROSPECT CON LE ESIGENZE AGGIUNTE DAL PRESALE 
        If inputName = "27" Then

            InviaMailInteressiCliente()

            End
        End If

        'REPORT ATTIVITA' FUTURE
        If inputName = "28" Then

            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String
            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\AttivitaFuture" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw1 As New StreamWriter(File.Open(strFile, FileMode.Append))

            Dim strSQLgen As String
            Dim query As String
            query = "TRUNCATE TABLE TabellaAttivitaFutureW; TRUNCATE TABLE TabellaAttivitaFuture1TO1; TRUNCATE TABLE TabellaAttivitaFuturePartecipanti"
            myConn = New SqlConnection(strConnectionDB)
            myConn.Open()
            Using commTRUNC As New SqlCommand()
                With commTRUNC
                    .Connection = myConn
                    .CommandType = CommandType.Text
                    .CommandText = query
                End With
                Try
                    commTRUNC.ExecuteNonQuery()
                Catch ex As Exception
                    sw1.WriteLine(Err.Description & " " & DateTime.Now)
                End Try
            End Using
            'myConn.Close()

            'seleziono i presale
            strSQL = "set dateformat dmy;select * from users where idwebprofile=3 and (DataUltimoAccesso >DATEADD(MONTH,-6,GETDATE()) or DataUltimoAccesso is null) order by id"

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            'myConn.Open()



            sw1.WriteLine("Inizio popolamento attività future" & DateTime.Now)

            Dim myReader1 As SqlDataReader
            Dim ID1 As Integer
            Dim Nominativo1 As String
            Dim Descrizione1 As String
            Dim Email1 As String

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                ID1 = myReader1.Item("ID")
                Nominativo1 = myReader1.Item("Nominativo")
                Descrizione1 = myReader1.Item("Descrizione")
                Email1 = myReader1.Item("Email")

                AttivitàFuture(CInt(ID1), Nominativo1, Descrizione1, Email1)

                sw1.WriteLine("Presale " & ID1.ToString() & " " & DateTime.Now)

                'Threading.Thread.Sleep(1500)

            Loop
            myReader1.Close()

            myConn.Close()

            sw1.WriteLine("Fine popolamento attività future" & DateTime.Now)

            sw1.Close()

            End

        End If
        If inputName = "29" Then
            'POPOLAMENTO TABELLA Storico_Percentuali_Selezione_Prodotti
            myConn = New SqlConnection(strConnectionDB)

            Dim strSQL As String
            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\Storico_Percentuali_Selezione_Prodotti_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw1 As New StreamWriter(File.Open(strFile, FileMode.Append))

            sw1.WriteLine("Inizio Popolamento Storico_Percentuali_Selezione_Prodotti " & DateTime.Now)

            Dim strSQLgen As String
            Dim query As String
            query = "spPopolaStoricoPercentualiSelezioneProdotti"
            myConn = New SqlConnection(strConnectionDB)
            myConn.Open()
            Using commTRUNC As New SqlCommand()
                With commTRUNC
                    .Connection = myConn
                    .CommandType = CommandType.StoredProcedure
                    .CommandText = query
                End With
                Try
                    commTRUNC.ExecuteNonQuery()
                Catch ex As Exception
                    sw1.WriteLine(Err.Description & " " & DateTime.Now)
                End Try
            End Using
            sw1.WriteLine("Fine Popolamento Storico_Percentuali_Selezione_Prodotti " & DateTime.Now)
            sw1.Close()
            'myConn.Close()

        End If
        If inputName = "30" Then
            ControlloIdCommerciale()
        End If
        If inputName = "31" Then
            InvioMailCommercialiDemo1to1Eseguita()
        End If
        If inputName = "32" Then
            InvioMailFeedbackDemo1to1()
        End If
        If inputName = "33" Then
            ControlloAziende()
        End If
        If inputName = "34" Then
            'POPOLAMENTO TABELLA eventi SleepingCompany
            myConn = New SqlConnection(strConnectionDB)

            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\Eventi_SleepingCompany_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw1 As New StreamWriter(File.Open(strFile, FileMode.Append))

            sw1.WriteLine("Inizio Popolamento Tabella eventi SleepingCompany " & DateTime.Now)


            '
            'Svuota TabellaEventiAziende
            Dim queryLQ As String
            queryLQ = ""
            queryLQ = "delete from TabellaEventiAziende where IDTipo in (1,2,3)"
            myConn3 = New SqlConnection(strConnectionDB)
            myConn3.Open()
            Using commins As New SqlCommand()
                With commins
                    .Connection = myConn3
                    .CommandType = CommandType.Text
                    .CommandText = queryLQ
                End With
                Try
                    commins.ExecuteNonQuery()
                Catch ex As Exception
                    sw1.WriteLine("*** ERRORE SU INSERT INTO TabellaEventiAziende " & DateTime.Now)
                    sw1.WriteLine("*** ERRORE SU INSERT INTO TabellaEventiAziende " & Err.Description)
                End Try
            End Using
            myConn3.Close()
            '


            Dim strSQL As String

            'seleziono i product manager
            strSQL = "select ID from Aziende"

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim ID1 As Integer

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                ID1 = myReader1.Item("ID")

                Try
                    PopolaTabellaEventiAziende(CInt(ID1), sw1)
                    sw1.WriteLine("OK PopolaTabellaEventiAziende Azienda " & ID1.ToString() & " " & DateTime.Now)
                Catch ex As Exception
                    sw1.WriteLine(" ")
                    sw1.WriteLine("***** ERRORE su PopolaTabellaEventiAziende Azienda " & ID1.ToString() & " " & DateTime.Now)
                    sw1.WriteLine(" ")
                End Try


            Loop
            myReader1.Close()

            myConn.Close()

            sw1.WriteLine("Fine Popolamento Tabella eventi SleepingCompany " & DateTime.Now)
            sw1.Close()

            End


        End If

        If inputName = "35" Then
            'INVIO MAIL AI COMMERCIALI CON I RISULTATI DEI FEEDBACK POST DEMO 1TO1 COMPILATI IN DATA ODIERNA
            InviaMailFeedback1to1Comm()
        End If
        If inputName = "36" Then
            'INVIO MAIL AI PRESALE CON I RISULTATI DEI FEEDBACK POST DEMO 1TO1 COMPILATI IN DATA ODIERNA
            InviaMailFeedback1to1Presale()
        End If



        If inputName = "37" Then

            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\LOGTotaliDashboard" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

            CalcolaTotaliDashboard1("GEN")
            CalcolaTotaliDashboard1("3M")
            CalcolaTotaliDashboard1("6M")
            CalcolaTotaliDashboard1("9M")
            CalcolaTotaliDashboard1("12M")

            sw.WriteLine("CalcolaTotaliDashboard 1" & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboard2("GEN")
            CalcolaTotaliDashboard2("3M")
            CalcolaTotaliDashboard2("6M")
            CalcolaTotaliDashboard2("9M")
            CalcolaTotaliDashboard2("12M")

            sw.WriteLine("CalcolaTotaliDashboard 2 " & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboard3("12M")
            CalcolaTotaliDashboard3("9M")
            CalcolaTotaliDashboard3("6M")
            CalcolaTotaliDashboard3("3M")
            CalcolaTotaliDashboard3("GEN")

            sw.WriteLine("CalcolaTotaliDashboard 3 " & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboard4("GEN")
            CalcolaTotaliDashboard4("3M")
            CalcolaTotaliDashboard4("6M")
            CalcolaTotaliDashboard4("9M")
            CalcolaTotaliDashboard4("12M")

            sw.WriteLine("CalcolaTotaliDashboard 4 " & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboard5("GEN")
            CalcolaTotaliDashboard5("3M")
            CalcolaTotaliDashboard5("6M")
            CalcolaTotaliDashboard5("9M")
            CalcolaTotaliDashboard5("12M")

            sw.WriteLine("CalcolaTotaliDashboard 5 " & Now.ToShortTimeString.ToString())


            CalcolaTotaliDashboardDett("GEN")
            CalcolaTotaliDashboardDett("3M")
            CalcolaTotaliDashboardDett("6M")
            CalcolaTotaliDashboardDett("9M")
            CalcolaTotaliDashboardDett("12M")

            sw.WriteLine("CalcolaTotaliDashboard Dett " & Now.ToShortTimeString.ToString())

            sw.Close()

            End
        End If

        If inputName = "38" Then

            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\LOGTotaliDashboardCOMM" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

            CalcolaTotaliDashboardCOMM6("GEN")
            CalcolaTotaliDashboardCOMM6("3M")
            CalcolaTotaliDashboardCOMM6("6M")
            CalcolaTotaliDashboardCOMM6("9M")
            CalcolaTotaliDashboardCOMM6("12M")

            sw.WriteLine("CalcolaTotaliDashboard Commerciali 1 di 6" & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboardCOMM5("GEN")
            CalcolaTotaliDashboardCOMM5("3M")
            CalcolaTotaliDashboardCOMM5("6M")
            CalcolaTotaliDashboardCOMM5("9M")
            CalcolaTotaliDashboardCOMM5("12M")

            sw.WriteLine("CalcolaTotaliDashboard Commerciali 2 di 6" & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboardCOMM4("GEN")
            CalcolaTotaliDashboardCOMM4("3M")
            CalcolaTotaliDashboardCOMM4("6M")
            CalcolaTotaliDashboardCOMM4("9M")
            CalcolaTotaliDashboardCOMM4("12M")

            sw.WriteLine("CalcolaTotaliDashboard Commerciali 3 di 6" & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboardCOMM3("GEN")
            CalcolaTotaliDashboardCOMM3("3M")
            CalcolaTotaliDashboardCOMM3("6M")
            CalcolaTotaliDashboardCOMM3("9M")
            CalcolaTotaliDashboardCOMM3("12M")

            sw.WriteLine("CalcolaTotaliDashboard Commerciali 4 di 6" & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboardCOMM2("GEN")
            CalcolaTotaliDashboardCOMM2("3M")
            CalcolaTotaliDashboardCOMM2("6M")
            CalcolaTotaliDashboardCOMM2("9M")
            CalcolaTotaliDashboardCOMM2("12M")

            sw.WriteLine("CalcolaTotaliDashboard Commerciali 5 di 6" & Now.ToShortTimeString.ToString())

            CalcolaTotaliDashboardCOMM1("GEN")
            CalcolaTotaliDashboardCOMM1("3M")
            CalcolaTotaliDashboardCOMM1("6M")
            CalcolaTotaliDashboardCOMM1("9M")
            CalcolaTotaliDashboardCOMM1("12M")

            sw.WriteLine("CalcolaTotaliDashboard Commerciali 6 di 6" & Now.ToShortTimeString.ToString())

            sw.Close()

            End
        End If
        If inputName = "39" Then
            'INVIA MAIL NUOVA RICHIESTA DEMO 1TO 1 A PRESALE DELEGATO

            myConn = New SqlConnection(strConnectionDB)

            Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\Richiesta1to1Delegati_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
            Dim fileExists As Boolean = File.Exists(strFile)
            Dim sw1 As New StreamWriter(File.Open(strFile, FileMode.Append))

            sw1.WriteLine("Inizio " & DateTime.Now)

            Dim strSQL As String

            'seleziono i product manager
            strSQL = "SELECT DISTINCT IDPresaleDelegato, u.Email FROM DelegaNotoficaRichiesta1to1 d join Users u on u.ID=d.IDPresaleDelegato"

            myCmd = myConn.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myConn.Open()

            Dim myReader1 As SqlDataReader
            Dim ID1 As Integer
            Dim Email1 As String

            myReader1 = myCmd.ExecuteReader()

            Do While myReader1.Read()
                ID1 = myReader1.Item("IDPresaleDelegato").ToString()
                Email1 = myReader1.Item("Email").ToString()

                Try
                    sw1.WriteLine("Inizio controllo per delegato " & ID1.ToString() & " " & Email1 & " " & DateTime.Now)
                    InviaMailNuovaRichiesta1to1(CInt(ID1), sw1)
                Catch ex As Exception
                    sw1.WriteLine(" ")
                    sw1.WriteLine("***** ERRORE su delegato " & ID1.ToString() & " " & DateTime.Now)
                    sw1.WriteLine(" ")
                End Try


            Loop
            myReader1.Close()

            myConn.Close()

            sw1.WriteLine("Fine " & DateTime.Now)
            sw1.Close()

            End

        End If


        Me.Cursor = Cursors.Default
        End

    End Sub

    Private Sub CalcolaTotaliDashboard2(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i presale presales (non product manager) non revocati
        strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 And isnull(ProductManager, 0) = 0 And (datarevoca Is Null Or (datarevoca Is Not Null And datarevoca > getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String




        Dim Num As Integer
        Dim Num2 As Integer
        Dim Testo1 As String
        Dim Testo2 As String
        Dim idkey As Integer
        Dim Tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")



            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDett where IDPresale=" & ID1.ToString & " And UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 170"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = "set dateformat dmy; "
            strSQL = strSQL & "Select rd.IDKey, rd.Tabella, count(*) Num, "
            strSQL = strSQL & "(select count(*) FROM RichiesteDemo_COP rd      join Richieste_COP r on r.ID=rd.IDRichiestaCop      outer APPLY dbo.TopStorico_RichiesteDemo_COP(rd.DataDemo,rd.IDKey,rd.OrarioDemo,rd.Tabella,r.ID,rd.Progressivo) s  left join Anagrafica_Commerciali ac on ac.id=r.IDCommerciale    where isnull(s.Eseguita,0)=1 And rd.DataDemo Is Not null And rd.datademo>=@data3 And rd.datademo <=@data4  /*And s.idkey=#idkey And s.tabella=#tabella*/  And IDUtentePresaleRiferimento=@IDPresale) Num2  , "
            strSQL = strSQL & "'Numero totale di demo 1 to 1 per il periodo selezionato' Testo1, "
            strSQL = strSQL & "'Variazione percentuale del numero totale di demo 1 to 1 per il periodo selezionato' Testo2 "
            strSQL = strSQL & "From RichiesteDemo_COP rd "
            strSQL = strSQL & "Join Richieste_COP r on r.ID=rd.IDRichiestaCop "
            strSQL = strSQL & "outer APPLY dbo.TopStorico_RichiesteDemo_COP(rd.DataDemo, rd.IDKey, rd.OrarioDemo, rd.Tabella, r.ID, rd.Progressivo) s "
            strSQL = strSQL & "Left Join Anagrafica_Commerciali ac on ac.id=r.IDCommerciale "
            strSQL = strSQL & "where isnull(s.Eseguita, 0) = 1 And rd.DataDemo Is Not Null "
            strSQL = strSQL & "And rd.datademo>=@data1 And rd.datademo <=@data2 "
            'strSQL = strSQL & "And s.idkey=#idkey And s.tabella=#tabella "
            strSQL = strSQL & "And IDUtentePresaleRiferimento=@IDPresale "
            strSQL = strSQL & "Group BY rd.IDKey, rd.Tabella"


            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDPresale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Num = Int32.Parse(myReader2.Item("Num"))
                Num2 = Int32.Parse(myReader2.Item("Num2"))
                Testo1 = myReader2.Item("Testo1")
                Testo2 = myReader2.Item("Testo2")
                IdKey = Int32.Parse(myReader2.Item("IdKey"))
                Tabella = myReader2.Item("Tabella")


                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDett (IDPresale,NumConf,NumConfPrec,Testo1,Testo2,Periodo, IdStat, Idkey, Tabella) VALUES (@IDPresale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat,@Idkey,@Tabella)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDPresale", ID1)
                        .Parameters.AddWithValue("@NumConf", Num)
                        .Parameters.AddWithValue("@NumConfPrec", Num2)
                        .Parameters.AddWithValue("@Testo1", Testo1)
                        .Parameters.AddWithValue("@Testo2", Testo2)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 170)
                        .Parameters.AddWithValue("@Idkey", idkey)
                        .Parameters.AddWithValue("@Tabella", Tabella)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub


    Private Sub CalcolaTotaliDashboardCOMM1(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i commerciali non revocati
        strSQL = "Select ac.ID, u.Nominativo, u.Descrizione, u.Email from users u join Anagrafica_Commerciali ac on ac.Mail=u.Email where idwebprofile=2 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String

        Dim Num As Integer
        Dim Num2 As Integer
        Dim Testo1 As String
        Dim Testo2 As String
        Dim idkey As Integer
        Dim tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")

            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDettComm where IDCommerciale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 162"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            'strSQL = "set dateformat dmy; exec spSTAT_ConfigurazioniPresale @IDPresale, @Data1,@Data2,@Data3,@Data4, 0, ''"

            strSQL = "set dateformat dmy; select count(*) Num, (select count(*) From Preventivi_Richieste pr where NomeAzienda<>''''
and NomeContatto<>'''' and MailContatto<>'''' And isnull(ConfigurazionePerInterni,0)=0 
And (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni
where IDRichiesta=pr.ID) >0 
And Data>=@data3 and Data <=DATEADD(day, 1, @data4) 
and pr.IDCommerciale=@IDCommerciale ) Num2 ,'Numero totale di configurazioni per il periodo selezionato' Testo1,
'Variazione percentuale delle configurazioni rispetto al periodo precedente' Testo2 

From Preventivi_Richieste pr 
where NomeAzienda<>'' and NomeContatto<>'' and MailContatto<>'' And isnull(ConfigurazionePerInterni,0)=0 
And (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0 
And Data>=@data1 and Data <=DATEADD(day, 1, @data2) And pr.IDCommerciale=@IDCommerciale"


            'strSQL = "set dateformat dmy; "
            'strSQL = strSQL & "select count(*) Num, "
            'strSQL = strSQL & "(select count(*) "
            'strSQL = strSQL & "From Preventivi_Richieste pr "
            'strSQL = strSQL & "where NomeAzienda<>'''' and NomeContatto<>'''' and MailContatto<>'''' "
            'strSQL = strSQL & "And isnull(ConfigurazionePerInterni,0)=0 "
            'strSQL = strSQL & "And (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0 "
            'strSQL = strSQL & "And Data>=@data3 and Data <=DATEADD(day, 1, @data4) and pr.IDCommerciale=@IDCommerciale "
            'strSQL = strSQL & ") Num2 ,''Numero totale di configurazioni per il periodo selezionato'' Testo1, ''Variazione percentuale delle configurazioni rispetto al periodo precedente'' Testo2 "
            'strSQL = strSQL & "From Preventivi_Richieste pr "
            'strSQL = strSQL & "where NomeAzienda<>'''' and NomeContatto<>'''' and MailContatto<>'''' "
            'strSQL = strSQL & "And isnull(ConfigurazionePerInterni,0)=0 "
            'strSQL = strSQL & "And (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0 "
            'strSQL = strSQL & "And Data>=@data1 and Data <=DATEADD(day, 1, @data2) "
            'strSQL = strSQL & "And pr.IDCommerciale=@IDCommerciale"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDCommerciale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Num = Int32.Parse(myReader2.Item("Num"))
                Num2 = Int32.Parse(myReader2.Item("Num2"))
                Testo1 = myReader2.Item("Testo1")
                Testo2 = myReader2.Item("Testo2")
                'idkey = Int32.Parse(myReader2.Item("Idkey"))
                'tabella = myReader2.Item("Tabella")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDettComm (IDCommerciale,NumConf,NumConfPrec,Testo1,Testo2,Periodo,IdStat) VALUES (@IDCommerciale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDCommerciale", ID1)
                        .Parameters.AddWithValue("@NumConf", Num)
                        .Parameters.AddWithValue("@NumConfPrec", Num2)
                        .Parameters.AddWithValue("@Testo1", Testo1)
                        .Parameters.AddWithValue("@Testo2", Testo2)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 162)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub

    Private Sub CalcolaTotaliDashboardCOMM2(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i commerciali non revocati
        strSQL = "Select ac.ID, u.Nominativo, u.Descrizione, u.Email from users u join Anagrafica_Commerciali ac on ac.Mail=u.Email where idwebprofile=2 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String

        Dim Num As Integer
        Dim Num2 As Integer
        Dim Testo1 As String
        Dim Testo2 As String
        Dim idkey As Integer
        Dim tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")

            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDettComm where IDCommerciale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 163"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = "set dateformat dmy;  
            select   count(*) Num,  (select count(*)   
            from Storico_DemoClienti s   
            left join Preventivi_Richieste r on r.id=s.IDRichiesta 
            where isnull(s.completata,0)=1   
            and r.IDCommerciale=@IDCommerciale
            and (r.NomeAzienda<>'' and r.NomeContatto<>'' and r.MailContatto<>'' 
            and isnull(ConfigurazionePerInterni,0)=0 and datademo>=@data3 and datademo <=@data4)  ) Num2
            ,'Numero totale di partecipanti alle demo weekly per il periodo selezionato' Testo1,
            'Variazione percentuale del numero totale di partecipanti alle demo weekly per il periodo selezionato' Testo2
            from Storico_DemoClienti s   
            left join Preventivi_Richieste r on r.id=s.IDRichiesta
            where isnull(s.completata,0)=1   
            and r.IDCommerciale=@IDCommerciale  
            and (r.NomeAzienda<>'''' and r.NomeContatto<>'''' and r.MailContatto<>'''' 
            and isnull(ConfigurazionePerInterni,0)=0 
            and datademo>=@data1 and datademo <=@data2)"




            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDCommerciale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Num = Int32.Parse(myReader2.Item("Num"))
                Num2 = Int32.Parse(myReader2.Item("Num2"))
                Testo1 = myReader2.Item("Testo1")
                Testo2 = myReader2.Item("Testo2")
                'idkey = Int32.Parse(myReader2.Item("Idkey"))
                'tabella = myReader2.Item("Tabella")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDettComm (IDCommerciale,NumConf,NumConfPrec,Testo1,Testo2,Periodo,IdStat) VALUES (@IDCommerciale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDCommerciale", ID1)
                        .Parameters.AddWithValue("@NumConf", Num)
                        .Parameters.AddWithValue("@NumConfPrec", Num2)
                        .Parameters.AddWithValue("@Testo1", Testo1)
                        .Parameters.AddWithValue("@Testo2", Testo2)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 163)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub


    Private Sub CalcolaTotaliDashboardCOMM3(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i commerciali non revocati
        strSQL = "Select ac.ID, u.Nominativo, u.Descrizione, u.Email from users u join Anagrafica_Commerciali ac on ac.Mail=u.Email where idwebprofile=2 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String

        Dim Num As Integer
        Dim Num2 As Integer
        Dim Testo1 As String
        Dim Testo2 As String
        Dim idkey As Integer
        Dim tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")

            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDettComm where IDCommerciale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 164"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = "set dateformat dmy;
            SELECT   count(*) Num,  
            (select count(*)   FROM RichiesteDemo_COP rd    join Richieste_COP r on r.ID=rd.IDRichiestaCop    
            outer APPLY dbo.TopStorico_RichiesteDemo_COP(rd.DataDemo,rd.IDKey,rd.OrarioDemo,rd.Tabella,r.ID,rd.Progressivo) s
            join Anagrafica_Commerciali ac on ac.id=r.IDCommerciale  where isnull(s.Eseguita,0)=1 AND rd.DataDemo is not null and rd.datademo>=@data3 and rd.datademo <=@data4  
            and r.IDCommerciale=@IDCommerciale  ) Num2
            ,'Numero totale di demo 1 to 1 per il periodo selezionato' Testo1, 'Variazione percentuale del numero totale di demo 1 to 1 per il periodo selezionato' Testo2
            FROM RichiesteDemo_COP rd    join Richieste_COP r on r.ID=rd.IDRichiestaCop    
            outer APPLY dbo.TopStorico_RichiesteDemo_COP(rd.DataDemo,rd.IDKey,rd.OrarioDemo,rd.Tabella,r.ID,rd.Progressivo) s
            join Anagrafica_Commerciali ac on ac.id=r.IDCommerciale  where isnull(s.Eseguita,0)=1 AND rd.DataDemo is not null 
            and rd.datademo>=@data1 and rd.datademo <=@data2 and r.IDCommerciale=@IDCommerciale"




            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDCommerciale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Num = Int32.Parse(myReader2.Item("Num"))
                Num2 = Int32.Parse(myReader2.Item("Num2"))
                Testo1 = myReader2.Item("Testo1")
                Testo2 = myReader2.Item("Testo2")
                'idkey = Int32.Parse(myReader2.Item("Idkey"))
                'tabella = myReader2.Item("Tabella")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDettComm (IDCommerciale,NumConf,NumConfPrec,Testo1,Testo2,Periodo,IdStat) VALUES (@IDCommerciale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDCommerciale", ID1)
                        .Parameters.AddWithValue("@NumConf", Num)
                        .Parameters.AddWithValue("@NumConfPrec", Num2)
                        .Parameters.AddWithValue("@Testo1", Testo1)
                        .Parameters.AddWithValue("@Testo2", Testo2)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 164)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub

    Private Sub CalcolaTotaliDashboardCOMM6(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i commerciali non revocati
        strSQL = "Select ac.ID, u.Nominativo, u.Descrizione, u.Email from users u join Anagrafica_Commerciali ac on ac.Mail=u.Email where idwebprofile=2 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String

        Dim SedeCliente As Integer
        Dim SedeLodi As Integer
        Dim SedePartner As Integer
        Dim Remoto As Integer
        Dim Tot As Integer
        Dim Label As String
        Dim Mese As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")

            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDettComm where IDCommerciale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 167"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = " set dateformat dmy;
 SELECT   SUBSTRING('GEN FEB MAR APR MAG GIU LUG AGO SET OTT NOV DIC ', 
 (month(CONVERT(VARCHAR, rd.DataDemo, 103)) * 4) - 3, 3) + ' ' + LTRIM(RTRIM(STR(YEAR(CONVERT(VARCHAR, MAX(rd.DataDemo), 103))))) [Mese],
 sum(AttivitaSedeCliente) [Sede Cliente],
 sum(AttivitaSedeLodi) [Sede Lodi],
 sum(AttivitaSedePartner) [Sede Partner],
 sum(AttivitaRemoto) Remoto,
 count(*) Tot,
 'Mese;Sede Cliente;Sede Lodi;Sede Partner;Remoto;Tot' Label
 FROM RichiesteDemo_COP rd
 join Richieste_COP r on r.ID=rd.IDRichiestaCop
 outer APPLY dbo.TopStorico_RichiesteDemo_COP(rd.DataDemo,rd.IDKey,rd.OrarioDemo,rd.Tabella,r.ID,rd.Progressivo) s
    join Anagrafica_Commerciali ac on ac.id=r.IDCommerciale  where isnull(s.Eseguita,0)=1
	AND rd.DataDemo is not null   
	and rd.datademo>=@data1 and rd.datademo <=@data2
	and r.IDCommerciale=@IDCommerciale
	group by YEAR(CONVERT(VARCHAR, rd.DataDemo, 103)), month(CONVERT(VARCHAR, rd.DataDemo, 103))
	ORDER by YEAR(CONVERT(VARCHAR, rd.DataDemo, 103)), month(CONVERT(VARCHAR, rd.DataDemo, 103))"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDCommerciale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Mese = myReader2.Item("Mese")
                SedeCliente = Int32.Parse(myReader2.Item("Sede Cliente"))
                SedeLodi = myReader2.Item("Sede Lodi")
                SedePartner = myReader2.Item("Sede Partner")
                Remoto = myReader2.Item("Remoto")
                Tot = myReader2.Item("Tot")
                Label = myReader2.Item("Label")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDettComm (IDCommerciale,SedeCliente,SedeLodi,SedePartner,Remoto,Tot,Label,Mese,Periodo,IdStat) VALUES (@IDCommerciale,@SedeCliente,@SedeLodi,@SedePartner,@Remoto,@Tot,@Label,@Mese,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDCommerciale", ID1)
                        .Parameters.AddWithValue("@SedeCliente", SedeCliente)
                        .Parameters.AddWithValue("@SedeLodi", SedeLodi)
                        .Parameters.AddWithValue("@SedePartner", SedePartner)
                        .Parameters.AddWithValue("@Remoto", Remoto)
                        .Parameters.AddWithValue("@Tot", Tot)
                        .Parameters.AddWithValue("@Label", Label)
                        .Parameters.AddWithValue("@Mese", Mese)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 167)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub

    Private Sub CalcolaTotaliDashboardCOMM5(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i commerciali non revocati
        strSQL = "Select ac.ID, u.Nominativo, u.Descrizione, u.Email from users u join Anagrafica_Commerciali ac on ac.Mail=u.Email where idwebprofile=2 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String

        Dim Num As Integer
        Dim Label As String
        Dim Mese As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")

            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDettComm where IDCommerciale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 166"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = " set dateformat dmy;  
 select   SUBSTRING('GEN FEB MAR APR MAG GIU LUG AGO SET OTT NOV DIC ', (month(CONVERT(VARCHAR, DataDemo, 103)) * 4) - 3, 3) + ' ' + LTRIM(RTRIM(STR(YEAR(CONVERT(VARCHAR, MAX(DataDemo), 103))))) [Mese],   count(*) Num ,'Data;Num' Label from Storico_DemoClienti s  
 left join Preventivi_Richieste r on r.id=s.IDRichiesta  where isnull(s.Completata,0)=1   and (r.NomeAzienda<>'' and r.NomeContatto<>'' and r.MailContatto<>''
 and isnull(ConfigurazionePerInterni,0)=0 and datademo>=@data1 and datademo <=@data2 and r.IDCommerciale=@IDCommerciale)   
 group by  YEAR(CONVERT(VARCHAR, DataDemo, 103)), month(CONVERT(VARCHAR, DataDemo, 103))   
 ORDER by  YEAR(CONVERT(VARCHAR, DataDemo, 103)), month(CONVERT(VARCHAR, DataDemo, 103))"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDCommerciale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Mese = myReader2.Item("Mese")
                Num = Int32.Parse(myReader2.Item("Num"))
                Label = myReader2.Item("Label")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDettComm (IDCommerciale,Num,Label,Mese,Periodo,IdStat) VALUES (@IDCommerciale,@Num,@Label,@Mese,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDCommerciale", ID1)
                        .Parameters.AddWithValue("@Num", Num)
                        .Parameters.AddWithValue("@Label", Label)
                        .Parameters.AddWithValue("@Mese", Mese)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 166)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub


    Private Sub CalcolaTotaliDashboardCOMM4(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i commerciali non revocati
        strSQL = "Select ac.ID, u.Nominativo, u.Descrizione, u.Email from users u join Anagrafica_Commerciali ac on ac.Mail=u.Email where idwebprofile=2 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String

        Dim Interne As Integer
        Dim Esterne As Integer
        Dim Totale As Integer
        Dim Label As String
        Dim Mese As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")

            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDettComm where IDCommerciale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 165"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = "if not exists (select * from sysobjects where id = object_id('#tmpTabellaStat01'))  
BEGIN   BEGIN TRY      
CREATE TABLE #tmpTabellaStat01 (Mese varchar(20), Sort varchar(6), Num1 int, Num2 int, Num3 int);   END TRY 
BEGIN CATCH     END CATCH   END 

truncate table #tmpTabellaStat01

set dateformat dmy  
insert into #tmpTabellaStat01 (Mese, Sort, Num1) 
select      SUBSTRING('GEN FEB MAR APR MAG GIU LUG AGO SET OTT NOV DIC ', (month(CONVERT(VARCHAR, Data, 103)) * 4) - 3, 3) + ' ' + LTRIM(RTRIM(STR(YEAR(CONVERT(VARCHAR, MAX(Data), 103))))) [MeseRichiesta],
Convert(CHAR(6),max(Data),112)     , count(*) Num     
FROM [Preventivi_Richieste] pr    where NomeAzienda<>'' and NomeContatto<>'' and MailContatto<>''     and isnull(ConfigurazionePerInterni,0)=0    
and pr.IDCommerciale=@IDcommerciale    
and Data>=@data1 and Data <=DATEADD(day, 1, @data2)     
and (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0        
and isnull(CompilazioneInterna,0)=1    group by month(CONVERT(VARCHAR, Data, 103))
set dateformat dmy
insert into #tmpTabellaStat01 (Mese, Sort, Num2)  
select      SUBSTRING('GEN FEB MAR APR MAG GIU LUG AGO SET OTT NOV DIC ', (month(CONVERT(VARCHAR, Data, 103)) * 4) - 3, 3) + ' ' + LTRIM(RTRIM(STR(YEAR(CONVERT(VARCHAR, MAX(Data), 103))))) [MeseRichiesta]
,Convert(CHAR(6),max(Data),112)     , count(*) Num    FROM [Preventivi_Richieste] pr    
where NomeAzienda<>'' and NomeContatto<>'' and MailContatto<>''     and isnull(ConfigurazionePerInterni,0)=0     
and pr.IDCommerciale=@IDCommerciale     and Data>=@data1 and Data <=DATEADD(day, 1, @data2)     
and (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0        
and isnull(CompilazioneInterna,0)=0    group by month(CONVERT(VARCHAR, Data, 103))  
select Mese,isnull(max(Num1),0) Interne,isnull(max(Num2),0) Esterne,(isnull(max(Num1),0) + isnull(max(Num2),0)) Totale,'Mese;Interne;Esterne;Totale' Label from #tmpTabellaStat01   group by Mese  order by max(Sort)   
"




            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDCommerciale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Mese = myReader2.Item("Mese")
                Interne = Int32.Parse(myReader2.Item("Interne"))
                Esterne = Int32.Parse(myReader2.Item("Esterne"))
                Totale = Int32.Parse(myReader2.Item("Totale"))
                Label = myReader2.Item("Label")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDettComm (IDCommerciale,Interne,Esterne,Totale,Label,Mese,Periodo,IdStat) VALUES (@IDCommerciale,@Interne,@Esterne,@Totale,@Label,@Mese,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDCommerciale", ID1)
                        .Parameters.AddWithValue("@Interne", Interne)
                        .Parameters.AddWithValue("@Esterne", Esterne)
                        .Parameters.AddWithValue("@Totale", Totale)
                        .Parameters.AddWithValue("@Label", Label)
                        .Parameters.AddWithValue("@Mese", Mese)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 165)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub


    Private Sub CalcolaTotaliDashboard1(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i presale presales (non product manager) non revocati
        strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=0 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String

        Dim Num As Integer
        Dim Num2 As Integer
        Dim Testo1 As String
        Dim Testo2 As String
        Dim idkey As Integer
        Dim tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")

            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDett where IDPresale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 169"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            'strSQL = "set dateformat dmy; exec spSTAT_ConfigurazioniPresale @IDPresale, @Data1,@Data2,@Data3,@Data4, 0, ''"

            strSQL = "set dateformat dmy; "
            strSQL = strSQL & "Select S.IDKey, S.Tabella, count(*) Num,    (Select count(*) "
            strSQL = strSQL & "From Storico_DemoClienti s "
            strSQL = strSQL & "Left Join Preventivi_Richieste r on r.id=s.IDRichiesta "
            strSQL = strSQL & "Left Join preventivi_sezioni sez on sez.id=s.IDKey And s.tabella = 'Preventivi_Sezioni' "
            strSQL = strSQL & "Left Join preventivi_funzioni f on f.id=s.IDKey And s.tabella = 'Preventivi_Funzioni' "
            strSQL = strSQL & "Join presales p on p.idkey=s.idkey And p.tabella=s.tabella "
            strSQL = strSQL & "Join users u On u.email=p.mail And u.id= @IDPresale  where isnull(s.completata, 0)=1     And r.NomeAzienda<>'' and r.NomeContatto<>'' and r.MailContatto<>'' "
            strSQL = strSQL & "And isnull(ConfigurazionePerInterni,0)=0 And datademo>=@Data3 and datademo <=@Data4 /*and s.idkey=#idkey and s.tabella=#tabella*/  ) Num2  , "
            strSQL = strSQL & "'Numero totale di partecipanti alle demo weekly per il periodo selezionato' Testo1,   'Variazione percentuale del numero totale di partecipanti alle demo weekly per il periodo selezionato' Testo2  "
            strSQL = strSQL & "From Storico_DemoClienti s   left Join Preventivi_Richieste r on r.id=s.IDRichiesta   left Join preventivi_sezioni sez on sez.id=s.IDKey And s.tabella = 'Preventivi_Sezioni' "
            strSQL = strSQL & "Left Join preventivi_funzioni f on f.id=s.IDKey And s.tabella = 'Preventivi_Funzioni'  join presales p on p.idkey=s.idkey and p.tabella=s.tabella "
            strSQL = strSQL & "Join users u On u.email=p.mail And u.id= @IDPresale  where isnull(s.completata, 0)=1   And r.NomeAzienda<>'' and r.NomeContatto<>'' and r.MailContatto<>'' "
            strSQL = strSQL & "And isnull(ConfigurazionePerInterni,0)=0 And datademo>=@Data1 and datademo <=@Data2 /*and s.idkey=#idkey and s.tabella=#tabella*/ "
            strSQL = strSQL & "Group BY S.IDKey, S.Tabella"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDPresale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Num = Int32.Parse(myReader2.Item("Num"))
                Num2 = Int32.Parse(myReader2.Item("Num2"))
                Testo1 = myReader2.Item("Testo1")
                Testo2 = myReader2.Item("Testo2")
                idkey = Int32.Parse(myReader2.Item("Idkey"))
                tabella = myReader2.Item("Tabella")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDett (IDPresale,NumConf,NumConfPrec,Testo1,Testo2,Periodo,IdStat, IdKey, Tabella) VALUES (@IDPresale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat,@IdKey,@Tabella)"
                'query = "INSERT INTO SetupTotaliDashboard(IDPresale,NumConf,NumConfPrec,Testo1,Testo2,Periodo, IdStat) VALUES (@IDPresale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDPresale", ID1)
                        .Parameters.AddWithValue("@NumConf", Num)
                        .Parameters.AddWithValue("@NumConfPrec", Num2)
                        .Parameters.AddWithValue("@Testo1", Testo1)
                        .Parameters.AddWithValue("@Testo2", Testo2)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 169)
                        .Parameters.AddWithValue("@IdKey", idkey)
                        .Parameters.AddWithValue("@Tabella", tabella)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub

    Private Sub CalcolaTotaliDashboard5(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i presale presales (non product manager) non revocati
        strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=0 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"
        'strSQL = "Select ID, Nominativo, Descrizione, Email from users where id=121"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String




        Dim SedeCliente As Integer
        Dim SedeLodi As Integer
        Dim SedePartner As Integer
        Dim Remoto As Integer
        Dim Tot As Integer
        Dim Mese As String
        Dim idkey As Integer
        Dim Tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")



            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDett where IDPresale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 173"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = "set dateformat dmy; "
            strSQL = strSQL & "SELECT rd.idkey,rd.tabella,SUBSTRING('GEN FEB MAR APR MAG GIU LUG AGO SET OTT NOV DIC ', (month(CONVERT(VARCHAR, rd.DataDemo, 103)) * 4) - 3, 3) + ' ' + LTRIM(RTRIM(STR(YEAR(CONVERT(VARCHAR, MAX(rd.DataDemo), 103))))) [Mese],
            sum(AttivitaSedeCliente) [Sede Cliente],  
            sum(AttivitaSedeLodi) [Sede Lodi], 
            sum(AttivitaSedePartner) [Sede Partner],  
            sum(AttivitaRemoto) Remoto,    count(*) Tot,
            'Mese;Sede Cliente;Sede Lodi;Sede Partner;Remoto;Tot' Label      
            FROM RichiesteDemo_COP rd      
            join Richieste_COP r on r.ID=rd.IDRichiestaCop      
            outer APPLY dbo.TopStorico_RichiesteDemo_COP(rd.DataDemo,rd.IDKey,rd.OrarioDemo,rd.Tabella,r.ID,rd.Progressivo) s  
            left join Anagrafica_Commerciali ac on ac.id=r.IDCommerciale   
            where isnull(s.Eseguita,0)=1 AND rd.DataDemo is not null   
            and rd.datademo>=@data1 and rd.datademo <=@data2
            /*and s.idkey=#idkey and s.tabella=#tabella*/
            and IDUtentePresaleRiferimento=@IDPresale
            group by rd.idkey, rd.tabella, YEAR(CONVERT(VARCHAR, rd.DataDemo, 103)), month(CONVERT(VARCHAR, rd.DataDemo, 103))    
            ORDER by rd.idkey, rd.tabella, YEAR(CONVERT(VARCHAR, rd.DataDemo, 103)), month(CONVERT(VARCHAR, rd.DataDemo, 103))"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDPresale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                SedeCliente = Int32.Parse(myReader2.Item("Sede Cliente"))
                SedeLodi = Int32.Parse(myReader2.Item("Sede Lodi"))
                SedePartner = Int32.Parse(myReader2.Item("Sede Partner"))
                Remoto = Int32.Parse(myReader2.Item("Remoto"))
                Tot = Int32.Parse(myReader2.Item("Tot"))
                Mese = myReader2.Item("Mese")
                IdKey = Int32.Parse(myReader2.Item("IdKey"))
                Tabella = myReader2.Item("Tabella")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDett (IDPresale,SedeCliente,SedeLodi,SedePartner,Remoto,Tot,Mese,Periodo,IdStat, IdKey, Tabella) VALUES (@IDPresale,@SedeCliente,@SedeLodi,@SedePartner,@Remoto,@Tot,@Mese,@Periodo,@IdStat,@IdKey,@Tabella)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDPresale", ID1)
                        .Parameters.AddWithValue("@SedeCliente", SedeCliente)
                        .Parameters.AddWithValue("@SedeLodi", SedeLodi)
                        .Parameters.AddWithValue("@SedePartner", SedePartner)
                        .Parameters.AddWithValue("@Remoto", Remoto)
                        .Parameters.AddWithValue("@Tot", Tot)
                        .Parameters.AddWithValue("@Mese", Mese)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 173)
                        .Parameters.AddWithValue("@IdKey", idkey)
                        .Parameters.AddWithValue("@Tabella", Tabella)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub



    Private Sub CalcolaTotaliDashboard4(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i presale presales (non product manager) non revocati
        strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=0 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"
        'strSQL = "Select ID, Nominativo, Descrizione, Email from users where id=121"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String




        Dim Num As Integer
        Dim Mese As String
        Dim IdKey As Integer
        Dim Tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")



            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDett where IDPresale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 172"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            'strSQL = "set dateformat dmy; exec spSTAT_GraficoConfigurazioniPresale @IDPresale, @Data1,@Data2, '', 0"

            strSQL = "set dateformat dmy; "
            strSQL = strSQL & "Select s.IDKey, s.Tabella, SUBSTRING('GEN FEB MAR APR MAG GIU LUG AGO SET OTT NOV DIC ', (month(CONVERT(VARCHAR, DataDemo, 103)) * 4) - 3, 3) + ' ' + LTRIM(RTRIM(STR(YEAR(CONVERT(VARCHAR, MAX(DataDemo), 103))))) [Mese],   count(*) Num ,'Data;Num' Label "
            strSQL = strSQL & "From Storico_DemoClienti s "
            strSQL = strSQL & "Left join Preventivi_Richieste r on r.id=s.IDRichiesta "
            strSQL = strSQL & "Left join preventivi_sezioni sez on sez.id=s.IDKey and s.tabella = 'Preventivi_Sezioni' "
            strSQL = strSQL & "Left join preventivi_funzioni f on f.id=s.IDKey and s.tabella = 'Preventivi_Funzioni' "
            strSQL = strSQL & "Join presales p on p.idkey=s.idkey and p.tabella=s.tabella "
            strSQL = strSQL & "Join users u on u.email=p.mail and u.id= @IDPresale "
            strSQL = strSQL & "where isnull(s.Completata,0)=1 "
            strSQL = strSQL & "And r.NomeAzienda<>'' and r.NomeContatto<>'' and r.MailContatto<>'' "
            strSQL = strSQL & "And isnull(ConfigurazionePerInterni,0)=0 "
            strSQL = strSQL & "and datademo>=@Data1 and datademo <=@Data2 "
            'strSQL = strSQL & "and s.idkey=#idkey and s.tabella=#tabella "
            strSQL = strSQL & "Group by s.IDKey, s.Tabella,YEAR(CONVERT(VARCHAR, DataDemo, 103)), month(CONVERT(VARCHAR, DataDemo, 103)) "
            strSQL = strSQL & "ORDER by s.IDKey, s.Tabella,YEAR(CONVERT(VARCHAR, DataDemo, 103)), month(CONVERT(VARCHAR, DataDemo, 103))"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDPresale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                Num = Int32.Parse(myReader2.Item("Num"))
                Mese = myReader2.Item("Mese")
                IDKey = myReader2.Item("IDKey")
                Tabella = myReader2.Item("Tabella")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDett (IDPresale,NumConf,Mese,Periodo,IdStat,IdKey,Tabella) VALUES (@IDPresale,@NumConf,@Mese,@Periodo,@IdStat,@IdKey,@Tabella)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDPresale", ID1)
                        .Parameters.AddWithValue("@NumConf", Num)
                        .Parameters.AddWithValue("@Mese", Mese)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 172)
                        .Parameters.AddWithValue("@IdKey", IdKey)
                        .Parameters.AddWithValue("@Tabella", Tabella)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()
            Loop
            myReader2.Close()
            myConn2.Close()

            Threading.Thread.Sleep(100)
        Loop
        myReader1.Close()

        myConn.Close()

    End Sub



    Private Sub CalcolaTotaliDashboard3(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i presale presales (non product manager) non revocati
        strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=0 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"
        'strSQL = "Select ID, Nominativo, Descrizione, Email from users where id=121"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String
        Dim IDKey As Integer
        Dim Tabella As String



        Dim NumInt As Integer
        Dim NumEst As Integer
        Dim Mese As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")



            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand
            myCmd4.CommandText = "DELETE FROM SetupTotaliDashboardDett where IDPresale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 171"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()

            strSQL = "set dateformat dmy; exec spSTAT_GraficoConfigurazioniPresale @IDPresale, @Data1,@Data2, '', 0"
            strSQL = "set dateformat dmy; exec spSTAT_GraficoConfigurazioniPresaleDett @IDPresale, @Data1,@Data2, '', 0"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDPresale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                NumInt = Int32.Parse(myReader2.Item("NumInt"))
                NumEst = Int32.Parse(myReader2.Item("NumEst"))
                Mese = myReader2.Item("Mese")
                IDKey = myReader2.Item("IDKey")
                Tabella = myReader2.Item("Tabella")


                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDett (IDPresale,NumConf,NumConfPrec,Mese,Periodo,IdStat, Idkey, Tabella) VALUES (@IDPresale,@NumConf,@NumConfPrec,@Mese,@Periodo,@IdStat,@Idkey,@Tabella)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDPresale", ID1)
                        .Parameters.AddWithValue("@NumConf", NumInt)
                        .Parameters.AddWithValue("@NumConfPrec", NumEst)
                        .Parameters.AddWithValue("@Mese", Mese)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 171)
                        .Parameters.AddWithValue("@IDKey", idkey)
                        .Parameters.AddWithValue("@Tabella", tabella)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub

    Private Sub CalcolaTotaliDashboardDett(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i presale presales (non product manager) non revocati
        strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=0 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String




        Dim NumConf As Integer
        Dim NumConfPrec As Integer
        Dim Testo1 As String
        Dim Testo2 As String
        Dim idkey As Integer
        Dim tabella As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")



            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            Dim sql4 As String
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand

            sql4 = "DELETE FROM SetupTotaliDashboardDett "
            sql4 = sql4 & "where IDPresale=" & ID1.ToString
            sql4 = sql4 & " and UPPER(Periodo)='" & Periodo.ToUpper()
            'sql4 = sql4 & " and IdKey=" & IdKey.ToString
            'sql4 = sql4 & " and Tabella='" & Tabella & "' "
            sql4 = sql4 & "' and IdStat = 168"

            myCmd4.CommandText = sql4 ' "DELETE FROM SetupTotaliDashboard where IDPresale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 168"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()



            strSQL = "set dateformat dmy; exec spSTAT_ConfigurazioniPresaleDett @IDPresale, @Data1,@Data2,@Data3,@Data4"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDPresale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                NumConf = Int32.Parse(myReader2.Item("NumConf"))
                NumConfPrec = Int32.Parse(myReader2.Item("NumConfPrec"))
                Testo1 = myReader2.Item("Testo1")
                Testo2 = myReader2.Item("Testo2")
                idkey = Int32.Parse(myReader2.Item("Idkey"))
                tabella = myReader2.Item("Tabella")

                Dim query As String
                query = "INSERT INTO SetupTotaliDashboardDett (IDPresale,NumConf,NumConfPrec,Testo1,Testo2,Periodo,IdStat, IdKey, Tabella) VALUES (@IDPresale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat,@IdKey,@Tabella)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDPresale", ID1)
                        .Parameters.AddWithValue("@NumConf", NumConf)
                        .Parameters.AddWithValue("@NumConfPrec", NumConfPrec)
                        .Parameters.AddWithValue("@Testo1", Testo1)
                        .Parameters.AddWithValue("@Testo2", Testo2)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 168)
                        .Parameters.AddWithValue("@IdKey", IdKey)
                        .Parameters.AddWithValue("@Tabella", Tabella)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            'Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub


    Private Sub CalcolaTotaliDashboard(Periodo As String)

        Dim data1 As Date
        Dim data2 As Date
        Dim data3 As Date
        Dim data4 As Date

        Dim sdata1 As String
        Dim sdata2 As String
        Dim sdata3 As String
        Dim sdata4 As String

        Select Case Periodo.ToUpper()
            Case "3M"
                data1 = DateAdd(DateInterval.Month, -3, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "6M"
                data1 = DateAdd(DateInterval.Month, -6, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "9M"
                data1 = DateAdd(DateInterval.Month, -9, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "12M"
                data1 = DateAdd(DateInterval.Month, -12, Now())
                data1 = DateAdd(DateInterval.Day, -data1.Day, data1)
                data1 = DateAdd(DateInterval.Day, 1, data1)
                sdata1 = data1.ToShortDateString()

                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

            Case "GEN"
                sdata1 = "01/01/" & Year(Now).ToString()
                data1 = CDate(sdata1)
                data2 = Now.ToShortDateString()
                data3 = DateAdd(DateInterval.Year, -1, data1)
                data4 = DateAdd(DateInterval.Year, -1, data2)

                sdata2 = data2.ToShortDateString()
                sdata3 = data3.ToShortDateString()
                sdata4 = data4.ToShortDateString()

        End Select

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono i presale presales (non product manager) non revocati
        strSQL = "Select ID, Nominativo, Descrizione, Email from users where idwebprofile=3 and isnull(ProductManager,0)=0 and (datarevoca is null or (datarevoca is not null and datarevoca>getdate()))"


        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim myReader2 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim Descrizione1 As String
        Dim Email1 As String




        Dim NumConf As Integer
        Dim NumConfPrec As Integer
        Dim Testo1 As String
        Dim Testo2 As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")
            Descrizione1 = myReader1.Item("Descrizione")
            Email1 = myReader1.Item("Email")



            Dim myReader4 As SqlDataReader
            Dim myCmd4 As New SqlCommand
            Dim myConnnew4 As New SqlConnection
            Dim sql4 As String
            myConnnew4 = New SqlConnection(strConnectionDB)
            myConnnew4.Open()
            myCmd4 = myConnnew4.CreateCommand

            sql4 = "DELETE FROM SetupTotaliDashboardDett "
            sql4 = sql4 & "where IDPresale=" & ID1.ToString
            sql4 = sql4 & " and UPPER(Periodo)='" & Periodo.ToUpper()
            sql4 = sql4 & "' and IdStat = 168"

            myCmd4.CommandText = sql4 ' "DELETE FROM SetupTotaliDashboard where IDPresale=" & ID1.ToString & " and UPPER(Periodo)='" & Periodo.ToUpper() & "' and IdStat = 168"
            myReader4 = myCmd4.ExecuteReader()
            myCmd4.Connection.Close()
            myCmd4.Dispose()
            myConnnew4.Close()



            strSQL = "set dateformat dmy; exec spSTAT_ConfigurazioniPresale @IDPresale, @Data1,@Data2,@Data3,@Data4, 0, ''"
            'strSQL = "set dateformat dmy; exec spSTAT_ConfigurazioniPresale @IDPresale, @Data1,@Data2,@Data3,@Data4, @idkey, @tabella"

            myCmd = myConn2.CreateCommand
            myCmd.CommandText = strSQL
            myCmd.CommandTimeout = 240
            myCmd.Parameters.AddWithValue("@IDPresale", ID1)
            myCmd.Parameters.AddWithValue("@Data1", sdata1)
            myCmd.Parameters.AddWithValue("@Data2", sdata2)
            myCmd.Parameters.AddWithValue("@Data3", sdata3)
            myCmd.Parameters.AddWithValue("@Data4", sdata4)

            myConn2.Open()

            myReader2 = myCmd.ExecuteReader()

            Do While myReader2.Read()
                NumConf = Int32.Parse(myReader2.Item("NumConf"))
                NumConfPrec = Int32.Parse(myReader2.Item("NumConfPrec"))
                Testo1 = myReader2.Item("Testo1")
                Testo2 = myReader2.Item("Testo2")


                Dim query As String
                query = "INSERT INTO SetupTotaliDashboard (IDPresale,NumConf,NumConfPrec,Testo1,Testo2,Periodo,IdStat) VALUES (@IDPresale,@NumConf,@NumConfPrec,@Testo1,@Testo2,@Periodo,@IdStat)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@IDPresale", ID1)
                        .Parameters.AddWithValue("@NumConf", NumConf)
                        .Parameters.AddWithValue("@NumConfPrec", NumConfPrec)
                        .Parameters.AddWithValue("@Testo1", Testo1)
                        .Parameters.AddWithValue("@Testo2", Testo2)
                        .Parameters.AddWithValue("@Periodo", Periodo)
                        .Parameters.AddWithValue("@IdStat", 168)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        'sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()

            Loop
            myReader2.Close()
            myConn2.Close()


            'Threading.Thread.Sleep(100)

        Loop
        myReader1.Close()

        myConn.Close()

    End Sub

    Private Function InvioMailFeedbackDemo1to1() As Boolean

        Dim strlinkHomePage As String

        'Dim strEmailPresale As String
        'Dim strDescrizionePresale As String

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        'Dim NominativoCommerciale As String
        Dim EmailCommerciale As String
        Dim EmailCommercialeINVIO As String
        Dim DataInvio As String
        Dim RispostaLibera As String

        Dim Email As String
        Dim EmailOLD As String
        Dim TestoDomanda As String
        Dim TestoRisposta As String
        Dim NomeAzienda As String
        Email = ""
        TestoDomanda = ""
        TestoRisposta = ""
        NomeAzienda = ""

        RispostaLibera = ""
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        EmailCommerciale = ""
        EmailCommercialeINVIO = ""
        DataInvio = ""
        EmailOLD = ""


        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailFeedbackDemo1to1" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio invio mail feedback demo 1to1 - " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim testoHtmlOrig As String
        Dim strOggetto As String

        Dim myReaderPresale As SqlDataReader
        Dim myCmdPresale As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        testoHtmlOrig = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioQuestionarioFeedback'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop
        testoHtmlOrig = testoHtml

        myReader3.Close()

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioQuestionarioFeedback'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String

        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String


        Dim Cognome As String
        Dim Nome As String
        Dim MailCliente As String
        Dim AziendaCliente As String
        Dim TelefonoCliente As String
        Dim CodiceCliente As String
        Dim IDRichiestaCOP As Integer

        Dim IdKey As Integer
        Dim DataDemo As String
        Dim OrarioDemo As String
        Dim Progressivo As Integer
        Dim Tabella As String
        Dim ArgomentoDemo As String

        Dim DataInizio As Date
        Dim DataFine As Date
        Dim IDQuestionario As Integer
        Dim GUIDRichiesta As String

        Dim IdKeyOLD As Integer
        Dim DataDemoOLD As String
        Dim OrarioDemoOLD As String
        Dim ProgressivoOLD As Integer
        Dim TabellaOLD As String

        Cognome = ""
        Nome = ""
        MailCliente = ""
        AziendaCliente = ""
        CodiceCliente = ""
        IDRichiestaCOP = 0
        IDQuestionario = 0
        ArgomentoDemo = ""

        IdKey = 0
        DataDemo = ""
        OrarioDemo = ""
        Tabella = ""
        Progressivo = 0

        IdKeyOLD = 0
        DataDemoOLD = ""
        OrarioDemoOLD = ""
        ProgressivoOLD = 0
        TabellaOLD = ""

        strDest = ""
        strDest2 = ""
        strDest3 = ""
        strDest4 = ""

        retID = 0

        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        If retID = "" Then
            InvioMailFeedbackDemo1to1 = False
            Exit Function
        End If

        strSQLgen = "Exec spGetFeedbackDemo1to1"

        myCmdGEN = myConn2.CreateCommand
        myCmdGEN.CommandText = strSQLgen

        sw.WriteLine("Inizio invio mail feedback al prospect")

        Dim IDQuestionari_Clienti As Integer
        Dim GUIDQuestionari_Clienti As String

        myReaderGEN = Nothing
        myReaderGEN = myCmdGEN.ExecuteReader()
        If myReaderGEN.HasRows Then
            Do While myReaderGEN.Read

                IDQuestionari_Clienti = 0
                GUIDQuestionari_Clienti = ""

                testoHtml = testoHtmlOrig

                Cognome = myReaderGEN.Item("Cognome")
                Nome = myReaderGEN.Item("Nome")
                MailCliente = myReaderGEN.Item("MailCliente")
                AziendaCliente = myReaderGEN.Item("AziendaCliente")
                TelefonoCliente = myReaderGEN.Item("TelefonoCliente")
                CodiceCliente = myReaderGEN.Item("CodiceCliente")
                IDRichiestaCOP = myReaderGEN.Item("IDRichiestaCOP")
                IdKey = myReaderGEN.Item("IdKey")
                Tabella = myReaderGEN.Item("Tabella")
                ArgomentoDemo = myReaderGEN.Item("ArgomentoDemo")
                DataDemo = myReaderGEN.Item("DataDemo")
                OrarioDemo = myReaderGEN.Item("OrarioDemo")
                Progressivo = myReaderGEN.Item("Progressivo")
                DataInizio = myReaderGEN.Item("DataInizio")
                DataFine = myReaderGEN.Item("DataFine")
                IDQuestionario = myReaderGEN.Item("IDQuestionario")
                GUIDRichiesta = myReaderGEN.Item("GUIDRichiesta")


                If DateTime.Parse(DataDemo) >= DateTime.Now().AddDays(-7) Then


                    MailContattoAgg = ""

                    Dim IDAnagrafica_Questionari As Integer

                    IDAnagrafica_Questionari = 0

                    ''''
                    myConn3.Open()
                    Dim myCmdAnag1 As SqlCommand
                    myCmdAnag1 = myConn3.CreateCommand
                    myCmdAnag1.CommandText = "select * from Anagrafica_Questionari where Email=@Email"
                    myCmdAnag1.Parameters.AddWithValue("@Email", MailCliente)
                    Dim myReaderAnag As SqlDataReader
                    myReaderAnag = myCmdAnag1.ExecuteReader()
                    If myReaderAnag.HasRows Then
                        Do While myReaderAnag.Read
                            IDAnagrafica_Questionari = myReaderAnag.Item("ID")
                        Loop
                    End If
                    myConn3.Close()
                    myConn3.Open()
                    ''''
                    Dim strAQ As String
                    strAQ = "set dateformat dmy;select * from Questionari_Clienti "
                    strAQ = strAQ & "where Email=@Email "
                    strAQ = strAQ & "and IDQuestionario=@IDQuestionario "
                    strAQ = strAQ & "and IDRichiestaCOP=@IDRichiestaCOP "
                    strAQ = strAQ & "and IDKeyDemo=@IDKeyDemo "
                    strAQ = strAQ & "and TabellaDemo=@TabellaDemo "
                    strAQ = strAQ & "And DataDemo=@DataDemo and Progressivo=@Progressivo "

                    Dim myCmdAnagQ As SqlCommand
                    Dim myReaderAnagQ As SqlDataReader
                    Dim ret As Integer

                    ret = 0
                    myCmdAnagQ = myConn3.CreateCommand
                    myCmdAnagQ.CommandText = strAQ
                    myCmdAnagQ.Parameters.AddWithValue("@Email", MailCliente)
                    myCmdAnagQ.Parameters.AddWithValue("@IDQuestionario", IDQuestionario)
                    myCmdAnagQ.Parameters.AddWithValue("@IDRichiestaCOP", IDRichiestaCOP)
                    myCmdAnagQ.Parameters.AddWithValue("@IDKeyDemo", IdKey)
                    myCmdAnagQ.Parameters.AddWithValue("@TabellaDemo", Tabella)
                    myCmdAnagQ.Parameters.AddWithValue("@DataDemo", DataDemo)
                    myCmdAnagQ.Parameters.AddWithValue("@Progressivo", Progressivo)
                    myReaderAnagQ = myCmdAnagQ.ExecuteReader()
                    If myReaderAnagQ.HasRows Then
                        Do While myReaderAnagQ.Read
                            ret = 1
                            GUIDQuestionari_Clienti = myReaderAnagQ.Item("GUID")
                            IDQuestionari_Clienti = myReaderAnagQ.Item("ID")
                        Loop
                    End If
                    myConn3.Close()
                    If ret <> 0 Then
                        Dim queryUpdateQC As String
                        queryUpdateQC = "set dateformat dmy;UPDATE Questionari_Clienti SET DataFinalizzazione=null,Completato=0,AperturaLink=1 "
                        queryUpdateQC = queryUpdateQC & "where Email=@Email "
                        queryUpdateQC = queryUpdateQC & "and IDQuestionario=@IDQuestionario "
                        queryUpdateQC = queryUpdateQC & "and IDRichiestaCOP=@IDRichiestaCOP "
                        queryUpdateQC = queryUpdateQC & "and IDKeyDemo=@IDKey "
                        queryUpdateQC = queryUpdateQC & "and TabellaDemo=@Tabella "
                        queryUpdateQC = queryUpdateQC & "And DataDemo=@DataDemo And OrarioDemo=@OrarioDemo and Progressivo=@Progressivo "
                        myConn3.Open()
                        Using commins As New SqlCommand()
                            With commins
                                .Connection = myConn3
                                .CommandType = CommandType.Text
                                .CommandText = queryUpdateQC
                                .Parameters.AddWithValue("@IDQuestionario", IDQuestionario)
                                .Parameters.AddWithValue("@Email", MailCliente)
                                .Parameters.AddWithValue("@Progressivo", Progressivo)
                                .Parameters.AddWithValue("@DataDemo", DataDemo)
                                .Parameters.AddWithValue("@OrarioDemo", OrarioDemo)
                                .Parameters.AddWithValue("@IDRichiestaCOP", IDRichiestaCOP)
                                .Parameters.AddWithValue("@Tabella", Tabella)
                                .Parameters.AddWithValue("@IDKey", IdKey)
                            End With
                            Try
                                commins.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU UPDATE Questionari_Clienti " & DateTime.Now)
                            End Try
                        End Using


                    Else

                        Dim query As String
                        query = "INSERT INTO Anagrafica_Questionari (Nome,Cognome,Email,GUID,RecapitiTelefonici) VALUES (@Nome,@Cognome,@Email,newid(),@Telefono)"
                        myConn3 = New SqlConnection(strConnectionDB)
                        myConn3.Open()
                        Using commins As New SqlCommand()
                            With commins
                                .Connection = myConn3
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Nome", Nome)
                                .Parameters.AddWithValue("@Cognome", Cognome)
                                .Parameters.AddWithValue("@Email", MailCliente.Replace(" [TEST]", ""))
                                .Parameters.AddWithValue("@Telefono", TelefonoCliente)
                            End With
                            Try
                                commins.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO Anagrafica_Questionari " & DateTime.Now)
                            End Try
                        End Using
                        Dim myReaderB As SqlDataReader
                        Dim cmdB = New SqlCommand("select TOP 1 ID from Anagrafica_Questionari order by id desc", myConn3)
                        myReaderB = cmdB.ExecuteReader()
                        If myReaderB.HasRows Then
                            Do While myReaderB.Read
                                IDAnagrafica_Questionari = Convert.ToInt32(myReaderB.Item(0))
                            Loop
                        End If

                        myConn3.Close()

                        IDQuestionari_Clienti = 0
                        GUIDQuestionari_Clienti = ""

                        query = "set dateformat dmy;INSERT INTO Questionari_Clienti (IDQuestionario,IDAnagraficaCliente,EMail,Nome,Cognome,DataCompilazione,GUID,IDutentePartner,Compilato,CodiceSige,RagioneSociale,Telefono,AperturaLink,DataRiferimento
                                        ,Progressivo, DataDemo, OrarioDemo, IDRichiestaCOP, TabellaDemo, IDKeyDemo) 
                                        VALUES 
                                        (@IDQuestionario,@IDAnagraficaCliente,@EMail,@Nome,@Cognome,getdate(),newid(),@IDUtentePartner,0,@CodiceSige,@RagioneSociale,@Telefono,1,getdate()
                                        ,@Progressivo, @DataDemo, @OrarioDemo, @IDRichiestaCOP,@TabellaDemo, @IDKeyDemo)"
                        'myConn3 = New SqlConnection(strConnectionDB)
                        myConn3.Open()
                        Using commins As New SqlCommand()
                            With commins
                                .Connection = myConn3
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@IDQuestionario", IDQuestionario)
                                .Parameters.AddWithValue("@IDAnagraficaCliente", IDAnagrafica_Questionari)
                                .Parameters.AddWithValue("@Email", MailCliente.Replace(" [TEST]", ""))
                                .Parameters.AddWithValue("@Nome", Nome)
                                .Parameters.AddWithValue("@Cognome", Cognome)
                                '.Parameters.AddWithValue("@IDUtentePartner", IDUtentePartner)
                                .Parameters.AddWithValue("@IDUtentePartner", 0)
                                .Parameters.AddWithValue("@CodiceSige", CodiceCliente)
                                .Parameters.AddWithValue("@RagioneSociale", AziendaCliente)
                                .Parameters.AddWithValue("@Telefono", TelefonoCliente)
                                .Parameters.AddWithValue("@AperturaLink", "AperturaLink")
                                .Parameters.AddWithValue("@Progressivo", Progressivo)
                                .Parameters.AddWithValue("@DataDemo", DataDemo)
                                .Parameters.AddWithValue("@OrarioDemo", OrarioDemo)
                                .Parameters.AddWithValue("@IDRichiestaCOP", IDRichiestaCOP)
                                .Parameters.AddWithValue("@TabellaDemo", Tabella)
                                .Parameters.AddWithValue("@IDKeyDemo", IdKey)

                            End With
                            Try
                                commins.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO Questionari_Clienti " & DateTime.Now)
                            End Try



                            Dim myReaderC As SqlDataReader
                            Dim cmdC = New SqlCommand("select TOP 1 ID, GUID from Questionari_Clienti order by id desc", myConn3)
                            myReaderC = cmdC.ExecuteReader()
                            If myReaderC.HasRows Then
                                Do While myReaderC.Read
                                    IDQuestionari_Clienti = Convert.ToInt32(myReaderC.Item(0))
                                    GUIDQuestionari_Clienti = myReaderC.Item(1).ToString()
                                Loop
                            End If

                        End Using
                        myConn3.Close()

                    End If
                    ''''


                    Dim link As String

                    link = System.Configuration.ConfigurationManager.AppSettings("linkHomePage").ToString().Trim() & "CompilazioneQuestionari/SchedaCompilazione?SESSIONID=" + GUIDQuestionari_Clienti
                    'link = link.Replace("Home/", "CompilazioneQuestionari/")
                    'link = link.Replace("CompilazioneQuestionari//", "CompilazioneQuestionari/")

                    Try
                        testoHtml = testoHtml.Replace("#Nome", Nome)
                        testoHtml = testoHtml.Replace("#Cognome", Cognome)
                        testoHtml = testoHtml.Replace("#datademo", DataDemo)
                        testoHtml = testoHtml.Replace("#link", link)
                        testoHtml = testoHtml.Replace("#argomentodemo", ArgomentoDemo)
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & EmailCommerciale & " " & DateTime.Now)
                    End Try

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'
                    Dim MailClienteInvio As String
                    Dim strNote As String
                    strNote = ""
                    MailClienteInvio = MailCliente

                    strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                        'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailClienteInvio = strDest 'TEST

                        Inviamail(strOggetto, "", MailClienteInvio, "", Nothing, testoHtml, DateTime.Now.ToString(), "", Nothing)

                        strNote = "[TEST] INVIO MAIL FEEDBACK DEMO 1to1 " & MailCliente

                    Else
                        Inviamail(strOggetto, "", MailCliente, "", Nothing, testoHtml, DateTime.Now.ToString(), "", Nothing)

                        strNote = "INVIO MAIL FEEDBACK DEMO 1to1 " & MailCliente
                    End If

                    sw.WriteLine("Invio email feedback demo 1 to 1 a prospect " & MailCliente & " " & DateTime.Now)


                    Dim queryLMO As String
                    queryLMO = ""
                    queryLMO = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
                    myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = queryLMO

                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                MailCliente = MailCliente & " [TEST]"
                            Else
                                MailCliente = MailCliente
                            End If
                            .Parameters.AddWithValue("@Dest", MailCliente.Replace(" [TEST]", ""))
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", strNote)
                            .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiesta)
                            .Parameters.AddWithValue("@TestoHTML", testoHtml)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & Err.Description)
                        End Try
                    End Using
                    'myConn3.Close()

                    'invalido il link precedente
                    Dim queryLQ As String
                    queryLQ = ""
                    queryLQ = "delete from LinkQuestionariInviati where IDQuestionario=@IDQ and mail = @mail and IDQuestionarioCliente=@IDQCliente"
                    'myConn3 = New SqlConnection(strConnectionDB)
                    'myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = queryLQ
                            .Parameters.AddWithValue("@IDQ", IDQuestionario)
                            .Parameters.AddWithValue("@mail", MailCliente.Replace(" [TEST]", ""))
                            .Parameters.AddWithValue("@IDQCliente", IDQuestionari_Clienti)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU DELETE LogMailOperation " & DateTime.Now)
                            sw.WriteLine("*** ERRORE SU DELETE LogMailOperation " & Err.Description)
                        End Try
                    End Using
                    'myConn3.Close()

                    'inserisco il link corretto
                    Dim queryLQI As String
                    queryLQI = ""
                    'queryLQI = "set dateformat dmy; insert into LinkQuestionariInviati values (newid(),@mail,getdate(),@dataInizio,@dataFine,@IDQC,@IDQ,@URL)"
                    queryLQI = "set dateformat dmy; insert into LinkQuestionariInviati (GUID,Mail,TimestampInvioLink,DataOraInizioValidita,DataOraFineValidita,IDQuestionarioCliente,IDQuestionario,Url) values (newid(),@mail,getdate(),@dataInizio,@dataFine,@IDQC,@IDQ,@URL)"
                    'myConn3 = New SqlConnection(strConnectionDB)
                    'myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = queryLQI
                            .Parameters.AddWithValue("@mail", MailCliente.Replace(" [TEST]", ""))
                            .Parameters.AddWithValue("@dataInizio", DataInizio)
                            .Parameters.AddWithValue("@dataFine", DataFine)
                            .Parameters.AddWithValue("@IDQC", IDQuestionari_Clienti)
                            .Parameters.AddWithValue("@IDQ", IDQuestionario)
                            .Parameters.AddWithValue("@URL", link)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & DateTime.Now)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & Err.Description)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & queryLQI)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & MailCliente)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & DataInizio)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & DataFine)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & IDQuestionari_Clienti)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & IDQuestionario)
                            sw.WriteLine("*** ERRORE SU insert into LinkQuestionariInviati " & link)
                        End Try
                    End Using
                    myConn3.Close()

                End If

                IdKeyOLD = IdKey
                DataDemoOLD = DataDemo
                OrarioDemoOLD = OrarioDemo
                ProgressivoOLD = Progressivo
                TabellaOLD = Tabella
                EmailOLD = Email

            Loop
        End If
        myReaderGEN.Close()
        myReaderGEN = Nothing



        ''**************************************************************'
        ''***************** INVIO EMAIL A COMMERCIALE ******************'
        ''**************************************************************'
        'MailContattoAgg = ""

        '        strDest = ""
        '        strDest2 = ""
        '        strDest3 = ""
        '        strDest4 = ""
        '        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

        '        If strTest.ToUpper().Trim().ToString() = "S" Then

        '            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
        '            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
        '            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
        '            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
        '            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

        '            EmailCommercialeINVIO = strDest 'TEST

        '        End If

        '        MailContattoAgg = ""

        '        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        '        Inviamail(strOggetto, "", EmailCommercialeINVIO, MailContattoAgg, Nothing, testoHtml, DataInvio, "", Nothing)
        '        sw.WriteLine("Invio email feedback a commerciale " & EmailCommercialeINVIO & " " & DateTime.Now)

        '        Dim query As String
        '        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
        '        myConn3 = New SqlConnection(strConnectionDB)
        '        myConn3.Open()
        '        Using commins As New SqlCommand()
        '            With commins
        '                .Connection = myConn3
        '                .CommandType = CommandType.Text
        '                .CommandText = query
        '                .Parameters.AddWithValue("@Dest", EmailCommercialeINVIO)
        '                .Parameters.AddWithValue("@CC", MailContattoAgg)
        '                .Parameters.AddWithValue("@Oggetto", strOggetto)
        '                .Parameters.AddWithValue("@Esito", "OK")
        '                .Parameters.AddWithValue("@Note", "INVIO MAIL FEEDBACK DEMO " & EmailCommerciale)
        '                .Parameters.AddWithValue("@GUIDRichiesta", "")
        '                .Parameters.AddWithValue("@TestoHTML", testoHtml)
        '            End With
        '            Try
        '                commins.ExecuteNonQuery()
        '            Catch ex As Exception
        '                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
        '            End Try
        '        End Using
        '        myConn3.Close()
        '''''''''''''''''''''''''''''''''''''''''''''''''''''


        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        InvioMailFeedbackDemo1to1 = True

        Exit Function

    End Function


    Private Function InviaMailInteressi() As Boolean

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim DescrizionePresale As String
        Dim IDRichiestaCOP As Integer
        Dim GUIDRichiesta As String
        Dim IdUtentePresale As Integer
        Dim IdCommerciale As Integer
        Dim IdCommercialeOLD As Integer

        Dim sCommerciale As String
        Dim sMailCommerciale As String
        Dim sAziendaCliente As String
        Dim sCommercialeOLD As String
        Dim sMailCommercialeOLD As String
        Dim sAziendaClienteOLD As String
        Dim DescrizionePresaleOLD As String
        Dim inviaremailOLD As String

        Dim IDRichiestaCOPOLD As Integer

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailInteressi_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio invio mail a presale con interessi aggiuntivi post demo 1 to 1 " & Now().ToString)

        myConn = New SqlConnection(strConnectionDB) 'Create a Connection object.

        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        Dim strSQLgen As String
        Dim myReader1 As SqlDataReader

        inviaremailOLD = ""

        strSQLgen = ""
        strSQLgen = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myCmd = myConn.CreateCommand 'Create a Command object.
        myCmd.CommandText = strSQLgen
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop

        myReader1.Close() 'Close the reader and the database connection.

        Dim testoHtml As String
        Dim testoHtmlOLD As String
        Dim strOggetto As String
        Dim strOggettoBCK As String
        Dim testoHtmlBCK As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        testoHtmlOLD = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioEsigenzePostDemo1to1'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop
        testoHtmlBCK = testoHtml

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioEsigenzePostDemo1to1'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo").ToString() '.Replace("#azienda", sAziendaCliente)
        Loop
        myReader3.Close()
        strOggettoBCK = strOggetto
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim strSQL As String
        strSQL = "set dateformat dmy Exec spGetInteressiReminderPresales"

        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240

        myConn1.Open()

        Dim IDEsigenzePostDemo1to1 As Integer
        Dim sIdKey As String
        Dim sIdKeyDemo As String
        Dim sTabella As String
        Dim sTabellaDemo As String
        Dim sProdottoDemo As String
        Dim sProdottoDemoOLD As String
        Dim sProdottoInteresse As String
        Dim sProdottoInteresseOLD As String
        Dim sTipologiaOLD As String
        Dim sInteresseEspresso As String
        Dim sTipologia As String
        Dim sAltroReferente As String
        Dim sAltroEmail As String
        Dim sNote As String
        Dim sRiferimento As String
        Dim sDataDemo As String

        Dim IDEsigenzePostDemo1to1OLD As Integer
        Dim sIdKeyOLD As String
        Dim sTabellaOLD As String
        Dim sIdKeyDemoOLD As String
        Dim sTabellaDemoOLD As String
        Dim sInteresseEspressoOLD As String
        Dim sAltroReferenteOLD As String
        Dim sAltroEmailOLD As String
        Dim sNoteOLD As String
        Dim sRiferimentoOLD As String
        Dim sDataDemoOLD As String

        myReader1 = myCmd.ExecuteReader()

        Dim sTabellaInteressi As String
        Dim sTabellaInteressiOLD As String

        Dim y As Integer


        Dim InviareMail As String
        Dim MailContatto As String
        Dim MailContattoOLD As String
        Dim MailContattoAgg As String
        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        sProdottoInteresse = ""
        sProdottoInteresseOLD = ""
        sProdottoDemo = ""
        sCommerciale = ""
        sCommercialeOLD = ""
        sMailCommercialeOLD = ""
        sAziendaClienteOLD = ""
        InviareMail = ""
        sTipologiaOLD = ""

        sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding:5px;text-align: left"">Azienda</th><th style=""padding:5px;text-align: left"">Tipologia</th><th style=""padding:5px;text-align: left"">Interesse Espresso</th><th style=""padding:5px;text-align: left"">Commerciale</th>
<th style=""padding:5px;text-align: left"">Prodotto Demo</th>
<th style=""padding:5px;text-align: left"">Presale Demo</th>
</tr></thead><tbody>"
        sTabellaInteressiOLD = ""
        IDEsigenzePostDemo1to1 = 0
        y = 0
        Do While myReader1.Read()
            Try
                IDEsigenzePostDemo1to1 = myReader1.Item("ID")
            Catch ex As Exception
                IDEsigenzePostDemo1to1 = 0
            End Try

            Try
                IdCommerciale = myReader1.Item("IDCommerciale")
            Catch ex As Exception
                IdCommerciale = 0
            End Try

            Try
                sCommerciale = myReader1.Item("Commerciale")
            Catch ex As Exception
                sCommerciale = ""
            End Try

            Try
                IDRichiestaCOP = myReader1.Item("IDRichiestaCOP")
            Catch ex As Exception
                IDRichiestaCOP = 0
            End Try

            Try
                GUIDRichiesta = myReader1.Item("GUIDRichiesta")
            Catch ex As Exception
                GUIDRichiesta = ""
            End Try

            Try
                sAziendaCliente = myReader1.Item("AziendaCliente")
            Catch ex As Exception
                sAziendaCliente = ""
            End Try

            Try
                DescrizionePresale = myReader1.Item("DescrizionePresale")
            Catch ex As Exception
                DescrizionePresale = ""
            End Try

            Try
                sIdKey = myReader1.Item("IdKey")
            Catch ex As Exception
                sIdKey = ""
            End Try

            Try
                sTabella = myReader1.Item("Tabella")
            Catch ex As Exception
                sTabella = ""
            End Try

            Try
                sIdKeyDemo = myReader1.Item("IdKeyDemo")
            Catch ex As Exception
                sIdKeyDemo = ""
            End Try

            Try
                sTabellaDemo = myReader1.Item("TabellaDemo")
            Catch ex As Exception
                sTabellaDemo = ""
            End Try

            Try
                sProdottoDemo = myReader1.Item("ProdottoDemo")
            Catch ex As Exception
                sProdottoDemo = ""
            End Try

            Try
                sProdottoInteresse = myReader1.Item("ProdottoInteresse")
            Catch ex As Exception
                sProdottoInteresse = ""
            End Try

            'Try
            '    sInteresseEspresso = myReader1.Item("InteresseEspresso").ToString()
            'Catch ex As Exception
            '    sInteresseEspresso = ""
            'End Try
            Try
                sTipologia = myReader1.Item("Tipologia").ToString()
            Catch ex As Exception
                sTipologia = ""
            End Try


            Try
                sAltroReferente = myReader1.Item("AltroReferente").ToString()
            Catch ex As Exception
                sAltroReferente = ""
            End Try
            Try
                sAltroEmail = myReader1.Item("AltroEmail").ToString()
            Catch ex As Exception
                sAltroEmail = ""
            End Try
            Try
                sNote = myReader1.Item("Note").ToString()
            Catch ex As Exception
                sNote = ""
            End Try
            Try
                sRiferimento = myReader1.Item("Riferimento").ToString()
            Catch ex As Exception
                sRiferimento = ""
            End Try
            Try
                sDataDemo = myReader1.Item("DataDemo").ToString()
            Catch ex As Exception
                sDataDemo = ""
            End Try
            Try
                InviareMail = myReader1.Item("InviareMail").ToString()
            Catch ex As Exception
                InviareMail = ""
            End Try

            If inviaremailOLD <> InviareMail And y <> 0 Then

                If inviaremailOLD <> "" Then
                    MailContattoAgg = ""
                    strTest = ""
                    strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()
                    If strTest.ToUpper().Trim().ToString() = "S" Then
                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                        MailContatto = strDest 'TEST
                    End If
                    strOggetto = ""
                    testoHtml = testoHtmlBCK
                    strOggetto = strOggettoBCK
                    sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Try
                        If strTest.ToUpper().Trim().ToString() = "S" Then
                            Inviamail(strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD), "", MailContatto, MailContattoAgg, sw, testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#azienda", sAziendaClienteOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD), sDataDemoOLD, "", Nothing)
                            sw.WriteLine("Mail inviata a " & MailContatto.ToString() & " IN TEST")
                        Else
                            Inviamail(strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD), "", inviaremailOLD, MailContattoAgg, sw, testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#azienda", sAziendaClienteOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace(DescrizionePresaleOLD, DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD), sDataDemoOLD, "", Nothing)
                            If sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                sw.WriteLine("Mail inviata a " & InviareMail.ToString() & " IN TEST")
                            Else
                                sw.WriteLine("Mail inviata a " & InviareMail.ToString())
                            End If
                        End If
                        Dim query As String
                        query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML, IdKeyEsigenza, TabellaEsigenza, IdKey, Tabella, DataDemo) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML, @IdKeyEsigenza, @TabellaEsigenza, @IdKey, @Tabella, @DataDemo)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query

                                Dim strDestinatario As String
                                If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                    strDestinatario = inviaremailOLD & " [TEST]"
                                Else
                                    strDestinatario = inviaremailOLD
                                End If
                                .Parameters.AddWithValue("@Dest", strDestinatario)
                                .Parameters.AddWithValue("@CC", "")
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD))
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL A " & inviaremailOLD)
                                .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiesta)
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOPOLD)
                                .Parameters.AddWithValue("@IDkeyEsigenza", sIdKeyOLD)
                                .Parameters.AddWithValue("@TabellaEsigenza", sTabellaOLD)
                                .Parameters.AddWithValue("@IDkey", sIdKeyDemoOLD)
                                .Parameters.AddWithValue("@Tabella", sTabellaDemoOLD)
                                .Parameters.AddWithValue("@DataDemo", sDataDemoOLD)
                                .Parameters.AddWithValue("@TestoHTML", testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now & " " & ex.Message.ToString() & " " & ex.StackTrace.ToString())
                            End Try
                            comm.Dispose()
                            myConn2.Close()
                        End Using
                    Catch ex As Exception
                        sw.WriteLine("Errore Invio mail a " & InviareMail.ToString())
                        Dim query As String
                        query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML, IdKeyEsigenza, TabellaEsigenza, IdKey, Tabella, DataDemo) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML, @IdKeyEsigenza, @TabellaEsigenza, @IdKey, @Tabella, @DataDemo)"
                        myConn2 = New SqlConnection(strConnectionDB)
                        myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                Dim strDestinatario As String
                                If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                    strDestinatario = inviaremailOLD & " [TEST]"
                                Else
                                    strDestinatario = inviaremailOLD
                                End If
                                .Parameters.AddWithValue("@Dest", strDestinatario)
                                .Parameters.AddWithValue("@CC", "")
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD))
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & inviaremailOLD)
                                .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiesta)
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOPOLD)
                                .Parameters.AddWithValue("@IDkeyEsigenza", sIdKeyOLD)
                                .Parameters.AddWithValue("@TabellaEsigenza", sTabellaOLD)
                                .Parameters.AddWithValue("@IDkey", sIdKeyDemoOLD)
                                .Parameters.AddWithValue("@Tabella", sTabellaDemoOLD)
                                .Parameters.AddWithValue("@DataDemo", sDataDemoOLD)
                                .Parameters.AddWithValue("@TestoHTML", testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now & " " & ex.Message.ToString() & " " & ex.StackTrace.ToString())
                            End Try
                        End Using

                    End Try

                End If
                sTabellaInteressi = ""
                sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding:5px;text-align: left"">Azienda</th><th style=""padding:5px;text-align: left"">Tipologia</th><th style=""padding:5px;text-align: left"">Interesse Espresso</th><th style=""padding:5px;text-align: left"">Commerciale</th>
<th style=""padding:5px;text-align: left"">Prodotto Demo</th>
<th style=""padding:5px;text-align: left"">Presale Demo</th>
</tr></thead><tbody>"
            Else

            End If

            sTabellaInteressi = sTabellaInteressi + "<tr>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sAziendaCliente & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sTipologia & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sProdottoInteresse & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sCommerciale & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sProdottoDemo & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & DescrizionePresale & "</td>"
            sTabellaInteressi = sTabellaInteressi + "</tr>"

            MailContatto = ""

            sCommercialeOLD = sCommerciale

            IDEsigenzePostDemo1to1OLD = IDEsigenzePostDemo1to1
            IdCommercialeOLD = IdCommerciale
            sIdKeyOLD = sIdKey
            sTabellaOLD = sTabella
            sIdKeyDemoOLD = sIdKeyDemo
            sTabellaDemoOLD = sTabellaDemo
            sInteresseEspressoOLD = sInteresseEspresso
            sAltroReferenteOLD = sAltroReferente
            sAltroEmailOLD = sAltroEmail
            sNoteOLD = sNote
            sRiferimentoOLD = sRiferimento
            sDataDemoOLD = sDataDemo
            sAziendaClienteOLD = sAziendaCliente
            testoHtmlOLD = testoHtml
            sTabellaInteressiOLD = sTabellaInteressi
            IDRichiestaCOPOLD = IDRichiestaCOP
            DescrizionePresaleOLD = DescrizionePresale
            inviaremailOLD = InviareMail
            sProdottoDemoOLD = sProdottoDemo
            sProdottoInteresseOLD = sProdottoInteresse
            sTipologiaOLD = sTipologia
            MailContattoOLD = MailContatto

            y = y + 1

        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()
        sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"

        '''''''''''''''''''''''''''''''''''''''''


        If inviaremailOLD <> "" Then
            MailContattoAgg = ""
            strTest = ""
            strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()
            If strTest.ToUpper().Trim().ToString() = "S" Then
                strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                MailContatto = strDest 'TEST
            End If
            strOggetto = ""
            testoHtml = testoHtmlBCK
            strOggetto = strOggettoBCK
            sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            Try
                If strTest.ToUpper().Trim().ToString() = "S" Then
                    Inviamail(strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD), "", MailContatto, MailContattoAgg, sw, testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#azienda", sAziendaClienteOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD), sDataDemoOLD, "", Nothing)
                    sw.WriteLine("Mail inviata a " & MailContatto.ToString() & " IN TEST")
                Else
                    Inviamail(strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD), "", inviaremailOLD, MailContattoAgg, sw, testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#azienda", sAziendaClienteOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD), sDataDemoOLD, "", Nothing)
                    If sInviaMail.ToUpper().Trim().ToString() = "N" Then
                        sw.WriteLine("Mail inviata a " & InviareMail.ToString() & " IN TEST")
                    Else
                        sw.WriteLine("Mail inviata a " & InviareMail.ToString())
                    End If

                End If
                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML, IdKeyEsigenza, TabellaEsigenza, IdKey, Tabella, DataDemo) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML, @IdKeyEsigenza, @TabellaEsigenza, @IdKey, @Tabella, @DataDemo)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = inviaremailOLD & " [TEST]"
                        Else
                            strDestinatario = inviaremailOLD
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD))
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A " & inviaremailOLD)
                        .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiesta)
                        .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOPOLD)
                        .Parameters.AddWithValue("@IDkeyEsigenza", sIdKeyOLD)
                        .Parameters.AddWithValue("@TabellaEsigenza", sTabellaOLD)
                        .Parameters.AddWithValue("@IDkey", sIdKeyDemoOLD)
                        .Parameters.AddWithValue("@Tabella", sTabellaDemoOLD)
                        .Parameters.AddWithValue("@DataDemo", sDataDemoOLD)
                        .Parameters.AddWithValue("@TestoHTML", testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now & " " & ex.Message.ToString() & " " & ex.StackTrace.ToString())
                    End Try
                    comm.Dispose()
                    myConn2.Close()
                End Using
            Catch ex As Exception
                sw.WriteLine("Errore Invio mail a " & InviareMail.ToString())
                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML, IdKeyEsigenza, TabellaEsigenza, IdKey, Tabella, DataDemo) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML, @IdKeyEsigenza, @TabellaEsigenza, @IdKey, @Tabella, @DataDemo)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = inviaremailOLD & " [TEST]"
                        Else
                            strDestinatario = inviaremailOLD
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", sDataDemoOLD).Replace("#azienda", sAziendaClienteOLD))
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & inviaremailOLD)
                        .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiesta)
                        .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOPOLD)
                        .Parameters.AddWithValue("@IDkeyEsigenza", sIdKeyOLD)
                        .Parameters.AddWithValue("@TabellaEsigenza", sTabellaOLD)
                        .Parameters.AddWithValue("@IDkey", sIdKeyDemoOLD)
                        .Parameters.AddWithValue("@Tabella", sTabellaDemoOLD)
                        .Parameters.AddWithValue("@DataDemo", sDataDemoOLD)
                        .Parameters.AddWithValue("@TestoHTML", testoHtmlOLD.Replace("#prodottointeresse", sProdottoInteresseOLD).Replace("#prodottodemo", sProdottoDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresaleOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#referente", DescrizionePresaleOLD).Replace("#azienda", sAziendaClienteOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now & " " & ex.Message.ToString() & " " & ex.StackTrace.ToString())
                    End Try
                End Using

            End Try

        End If

        sw.WriteLine("Fine invio mail a presale con interessi aggiuntivi post demo 1 to 1 " & Now().ToString)
        sw.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Function


    Private Function InviaMailInteressiCliente() As Boolean

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim DescrizionePresale As String
        Dim IDRichiestaCOP As Integer
        Dim IdUtentePresale As Integer
        Dim Commerciale As String
        Dim IdCommercialeOLD As Integer
        Dim fileInfografica As Byte()
        Dim fileInfograficaOLD As Byte()
        Dim sCommerciale As String
        Dim sMailCommerciale As String
        Dim sAziendaCliente As String
        Dim sContattoPrincipaleCliente As String
        Dim sMailCliente As String
        Dim sMailClienteOLD As String
        Dim sCommercialeOLD As String
        Dim sMailCommercialeOLD As String
        Dim sAziendaClienteOLD As String
        Dim sContattoPrincipaleClienteOLD As String
        Dim DescrizionePresaleOLD As String
        Dim IDRichiestaCOPOLD As Integer

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand

        Dim strOrario As String
        Dim strAnno As String
        Dim strMese As String
        Dim strGiorno As String

        strAnno = Now.Year()
        strMese = Now.Month()
        strGiorno = Now.Day()
        If strMese.Length() = 1 Then strMese = "0" & strMese
        If strGiorno.Length() = 1 Then strGiorno = "0" & strGiorno
        strOrario = strAnno & strMese & strGiorno & Now.ToShortTimeString().Replace(":", "")

        Dim Path_Cartella As String = My.Application.Info.DirectoryPath & "\TMP_INFOGRAFICA_ESIGENZE_" & strOrario

        If Not Directory.Exists(Path_Cartella) Then
            Directory.CreateDirectory(Path_Cartella)
        Else
            Dim di As IO.DirectoryInfo =
            New IO.DirectoryInfo(Path_Cartella)
            For Each oFile As IO.FileInfo In di.GetFiles()
                oFile.Delete()
            Next

        End If
        'Dim file As Byte() = fileInfografica


        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailInteressiCliente_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        myConn = New SqlConnection(strConnectionDB) 'Create a Connection object.

        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        Dim strSQLgen As String
        Dim myReader1 As SqlDataReader

        strSQLgen = ""
        strSQLgen = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myCmd = myConn.CreateCommand 'Create a Command object.
        myCmd.CommandText = strSQLgen
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop

        myReader1.Close() 'Close the reader and the database connection.

        Dim testoHtml As String
        Dim testoHtmlOLD As String
        Dim strOggetto As String
        Dim strOggettoBCK As String
        Dim testoHtmlBCK As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        testoHtmlOLD = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioEsigenzePostDemo1to1Cliente'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop
        testoHtmlBCK = testoHtml

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioEsigenzePostDemo1to1Cliente'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo").ToString() '.Replace("#azienda", sAziendaCliente)
        Loop
        myReader3.Close()
        strOggettoBCK = strOggetto
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim strSQL As String

        Dim strData As String
        strData = Now.ToShortDateString()

        sw.WriteLine("set dateformat dmy;exec spGetInteressiReminderCliente '" & strData & "'")

        'strData = "12/09/2022" 'test
        'strData = "Convert(NVARCHAR, '" & Now.ToShortDateString & "', 103)"

        strSQL = "set dateformat dmy;exec spGetInteressiReminderCliente '" & strData & "'"

        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240

        myConn1.Open()

        Dim IDEsigenzePostDemo1to1 As Integer
        Dim sIdKey As String
        Dim sTabella As String
        Dim sTabellaDemo As String
        Dim sInteresseEspresso As String
        Dim sAltroReferente As String
        Dim sAltroEmail As String
        Dim sNote As String
        Dim sRiferimento As String
        Dim sDataDemo As String
        Dim sNomeDemo As String

        Dim IDEsigenzePostDemo1to1OLD As Integer
        Dim sIdKeyOLD As String
        Dim sTabellaOLD As String
        Dim sTabellaDemoOLD As String
        Dim sInteresseEspressoOLD As String
        Dim sAltroReferenteOLD As String
        Dim sAltroEmailOLD As String
        Dim sNoteOLD As String
        Dim sRiferimentoOLD As String
        Dim sDataDemoOLD As String
        Dim sNomeDemoOLD As String

        myReader1 = myCmd.ExecuteReader()

        Dim sTabellaInteressi As String
        Dim sTabellaInteressiOLD As String

        Dim y As Integer


        Dim MailContatto As String
        Dim MailContattoAgg As String
        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        sCommerciale = ""
        sCommercialeOLD = ""
        sMailCommercialeOLD = ""
        sAziendaClienteOLD = ""
        sContattoPrincipaleClienteOLD = ""
        sMailClienteOLD = ""
        sNomeDemoOLD = ""

        'sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: Left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding: 5px;text-align: Left"" > Interesse Espresso</th><th style=""padding: 5px;text-align: Left"" > Azienda</th><th style=""padding: 5px;text-align: Left"" >Commerciale</th></tr></thead><tbody>"
        'sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: Left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding: 5px;text-align: Left"" > Interesse Espresso</th></tr></thead><tbody>"
        sTabellaInteressi = "<ul>"
        sTabellaInteressiOLD = ""
        IDEsigenzePostDemo1to1 = 0
        y = 0

        Dim indexInfografica As Integer
        Dim path(100) As String

        indexInfografica = 0

        Do While myReader1.Read()

            Try
                IDEsigenzePostDemo1to1 = myReader1.Item("ID")
            Catch ex As Exception
                IDEsigenzePostDemo1to1 = 0
            End Try
            Try
                sAziendaCliente = myReader1.Item("AziendaCliente")
            Catch ex As Exception
                sAziendaCliente = ""
            End Try
            Try
                sContattoPrincipaleCliente = myReader1.Item("ContattoPrincipaleCliente")
            Catch ex As Exception
                sContattoPrincipaleCliente = ""
            End Try
            Try
                sMailCliente = myReader1.Item("MailCliente")
            Catch ex As Exception
                sMailCliente = ""
            End Try
            Try
                sCommerciale = myReader1.Item("Commerciale")
            Catch ex As Exception
                sCommerciale = ""
            End Try
            Try
                sMailCommerciale = myReader1.Item("MailCommerciale")
            Catch ex As Exception
                sMailCommerciale = ""
            End Try
            Try
                IDRichiestaCOP = myReader1.Item("IDRichiestaCOP")
            Catch ex As Exception
                IDRichiestaCOP = 0
            End Try
            Try
                IdUtentePresale = myReader1.Item("IDutentePresale")
            Catch ex As Exception
                IdUtentePresale = 0
            End Try
            Try
                DescrizionePresale = myReader1.Item("DescrizionePresale")
            Catch ex As Exception
                DescrizionePresale = ""
            End Try
            Try
                sIdKey = myReader1.Item("IdKey")
            Catch ex As Exception
                sIdKey = ""
            End Try
            Try
                sTabella = myReader1.Item("Tabella")
            Catch ex As Exception
                sTabella = ""
            End Try
            Try
                sInteresseEspresso = myReader1.Item("InteresseEspresso").ToString()
            Catch ex As Exception
                sInteresseEspresso = ""
            End Try
            Try
                sTabellaDemo = myReader1.Item("TabellaDemo")
            Catch ex As Exception
                sTabellaDemo = ""
            End Try
            Try
                sDataDemo = myReader1.Item("DataDemo").ToString()
            Catch ex As Exception
                sDataDemo = ""
            End Try
            Try
                sNomeDemo = myReader1.Item("NomeDemo").ToString()
            Catch ex As Exception
                sNomeDemo = ""
            End Try
            Try
                sAltroReferente = myReader1.Item("AltroReferente").ToString()
            Catch ex As Exception
                sAltroReferente = ""
            End Try
            Try
                sAltroEmail = myReader1.Item("AltroEmail").ToString()
            Catch ex As Exception
                sAltroEmail = ""
            End Try
            Try
                sNote = myReader1.Item("Note").ToString()
            Catch ex As Exception
                sNote = ""
            End Try
            Try
                sRiferimento = myReader1.Item("Riferimento").ToString()
            Catch ex As Exception
                sRiferimento = ""
            End Try


            fileInfografica = myReader1.Item(“fileInfografica”)

            If (sAziendaClienteOLD = sAziendaCliente) Or y = 0 Then

                ' ReDim path(indexInfografica)
                path(indexInfografica) = Path_Cartella + "\" + sInteresseEspresso + ".pdf"

                path(indexInfografica) = path(indexInfografica).Replace("'", "")
                path(indexInfografica) = path(indexInfografica).Replace("%", "")
                path(indexInfografica) = path(indexInfografica).Replace("&", "")



                If System.IO.File.Exists(path(indexInfografica)) = False Then

                    Dim fileStream As New FileStream(path(indexInfografica), FileMode.Append, FileAccess.Write)

                    Dim utf8withNoBOM As New System.Text.UTF8Encoding(False)

                    Dim sw1 As New StreamWriter(fileStream, utf8withNoBOM)

                    sw1.BaseStream.Write(fileInfografica, 0, fileInfografica.Length)

                    sw1.Flush()

                    sw1.Close()

                    fileStream.Close()
                    fileStream = Nothing
                    sw1 = Nothing

                End If

            End If

            'System.IO.fileInfografica.WriteAllBytes(path(0), fileInfografica)

            'sTabellaInteressi = "<table>"

            If (sAziendaClienteOLD <> sAziendaCliente) And y > 0 Then
                'Invio la mail al commerciale sCommercialeOLD

                MailContattoAgg = ""
                strTest = ""
                MailContatto = sMailClienteOLD
                MailContattoAgg = sMailCommercialeOLD

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strTest.ToUpper().Trim().ToString() = "S" Then

                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                    MailContatto = strDest 'TEST
                    MailContattoAgg = strDest 'TEST

                End If

                strOggetto = ""
                testoHtml = testoHtmlBCK

                strOggetto = strOggettoBCK

                'sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"
                sTabellaInteressi = sTabellaInteressi + "</ul>"

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                MailContattoAgg = "" 'Non invio al commerciale in copia conoscenza
                Try
                    Dim TestoHTMLdainviare As String

                    TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemo).Replace("#listainteressi", sTabellaInteressiOLD).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspresso).Replace("#cliente", sContattoPrincipaleClienteOLD)
                    TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspressoOLD).Replace("#cliente", sContattoPrincipaleClienteOLD)
                    sw.WriteLine("*******TestoHTMLdainviare*******")
                    sw.WriteLine(TestoHTMLdainviare)
                    sw.WriteLine("*******TestoHTMLdainviare*******")




                    Inviamail(strOggetto.Replace("#data", sDataDemo), "", MailContatto, MailContattoAgg, sw, TestoHTMLdainviare, sDataDemoOLD, "", path)

                    'sw.WriteLine("Mail inviata ad azienda " & MailContatto.ToString())
                    If strTest.ToUpper().Trim().ToString() = "S" Then
                        sw.WriteLine("Mail inviata a " & MailContatto.ToString() & " IN TEST")
                    Else
                        If sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            sw.WriteLine("Mail inviata a " & MailContatto.ToString() & " IN TEST")
                        Else
                            sw.WriteLine("Mail inviata a " & MailContatto.ToString())
                        End If
                    End If

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", sDataDemo))
                            .Parameters.AddWithValue("@Esito", "OK")
                            If path IsNot Nothing Then
                                .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD & " con allegato ")
                            Else
                                .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD)
                            End If
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)
                            .Parameters.AddWithValue("@TestoHTML", TestoHTMLdainviare)
                            'testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspressoOLD).Replace("#cliente", sContattoPrincipaleClienteOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                        comm.Dispose()
                        myConn2.Close()
                    End Using

                Catch ex As Exception

                    Dim TestoHTMLdainviare As String

                    TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemo).Replace("#listainteressi", sTabellaInteressiOLD).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspresso).Replace("#cliente", sContattoPrincipaleClienteOLD)
                    TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspressoOLD).Replace("#cliente", sContattoPrincipaleClienteOLD)

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#data", sDataDemo))
                            .Parameters.AddWithValue("@Esito", "OK")
                            If path IsNot Nothing Then
                                .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD & " con allegato ")
                            Else
                                .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD)
                            End If
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)
                            .Parameters.AddWithValue("@TestoHTML", TestoHTMLdainviare)
                            'testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspressoOLD).Replace("#cliente", sContattoPrincipaleClienteOLD))
                            sw.WriteLine("Errore Invio mail a " & strDestinatario.ToString())
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                End Try

                'sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: Left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding: 5px;text-align: Left"" > Interesse Espresso</th><th style=""padding: 5px;text-align: Left"" > Azienda</th><th style=""padding: 5px;text-align: Left"" >Commerciale</th></tr></thead><tbody>"
                'sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: Left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding: 5px;text-align: Left"" > Interesse Espresso</th></tr></thead><tbody>"
                sTabellaInteressi = "<ul>"



                '''''''''''****** path
                indexInfografica = 0
                ReDim path(100)
                path(indexInfografica) = Path_Cartella + "\" + sInteresseEspresso + ".pdf"

                path(indexInfografica) = path(indexInfografica).Replace("'", "")
                path(indexInfografica) = path(indexInfografica).Replace("%", "")
                path(indexInfografica) = path(indexInfografica).Replace("&", "")



                If System.IO.File.Exists(path(indexInfografica)) = False Then

                    Dim fileStream As New FileStream(path(indexInfografica), FileMode.Append, FileAccess.Write)

                    Dim utf8withNoBOM As New System.Text.UTF8Encoding(False)

                    Dim sw1 As New StreamWriter(fileStream, utf8withNoBOM)

                    sw1.BaseStream.Write(fileInfografica, 0, fileInfografica.Length)

                    sw1.Flush()

                    sw1.Close()

                    fileStream.Close()
                    fileStream = Nothing
                    sw1 = Nothing

                End If
                '''''''''''****** path


            End If

            sCommercialeOLD = sCommerciale
            'sMailCommercialeOLD = sMailCommerciale

            IDEsigenzePostDemo1to1OLD = IDEsigenzePostDemo1to1
            'IdCommercialeOLD = IdCommerciale
            sIdKeyOLD = sIdKey
            sTabellaOLD = sTabella
            sTabellaDemoOLD = sTabellaDemo
            sInteresseEspressoOLD = sInteresseEspresso
            sAltroReferenteOLD = sAltroReferente
            sAltroEmailOLD = sAltroEmail
            sNoteOLD = sNote
            sRiferimentoOLD = sRiferimento
            sDataDemoOLD = sDataDemo
            sAziendaClienteOLD = sAziendaCliente
            sContattoPrincipaleClienteOLD = sContattoPrincipaleCliente
            sMailClienteOLD = sMailCliente
            testoHtmlOLD = testoHtml
            sTabellaInteressiOLD = sTabellaInteressi
            IDRichiestaCOPOLD = IDRichiestaCOP
            DescrizionePresaleOLD = DescrizionePresale
            fileInfograficaOLD = fileInfografica
            sNomeDemoOLD = sNomeDemo
            'sTabellaInteressi = sTabellaInteressi + "<tr>"
            'sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sInteresseEspressoOLD & "</td>"
            ''sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sAziendaClienteOLD & "</td>"
            ''sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sCommercialeOLD & "</td>"
            'sTabellaInteressi = sTabellaInteressi + "</tr>"


            sTabellaInteressi = sTabellaInteressi + "<li style='font-size: 20px;'>" & sInteresseEspressoOLD & "</li>"

            'C:\Source\ReminderDemoDriver\bin\Debug\INFOGRAFICA

            y = y + 1

            indexInfografica = indexInfografica + 1

        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()
        sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"

        ''''''''''''''''''''''''''''''''''''''''''

        If sMailClienteOLD <> "" Then

            sMailClienteOLD = ""
            'MailContattoAgg = sMailCommercialeOLD
            MailContattoAgg = "" 'Non invio al commerciale in copia conoscenza
            strTest = ""
            MailContatto = sMailClienteOLD
            strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()
            If strTest.ToUpper().Trim().ToString() = "S" Then
                strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()
                MailContatto = strDest 'TEST
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Try

                Dim TestoHTMLdainviare As String

                TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemo).Replace("#datademo", sDataDemo).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommerciale).Replace("#interesseespresso", sInteresseEspresso).Replace("#cliente", sContattoPrincipaleCliente)
                TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemo).Replace("#datademo", sDataDemo).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommerciale).Replace("#interesseespresso", sInteresseEspresso).Replace("#cliente", sContattoPrincipaleCliente)
                sw.WriteLine("*******TestoHTMLdainviare*******")
                sw.WriteLine(TestoHTMLdainviare)
                sw.WriteLine("*******TestoHTMLdainviare*******")

                Inviamail(strOggetto.Replace("#data", sDataDemo), "", MailContatto, MailContattoAgg, sw, TestoHTMLdainviare, sDataDemo, "", path)

                If strTest.ToUpper().Trim().ToString() = "S" Then
                    sw.WriteLine("Mail inviata a " & MailContatto.ToString() & " In TEST")
                Else
                    If sInviaMail.ToUpper().Trim().ToString() = "N" Then
                        sw.WriteLine("Mail inviata a " & MailContatto.ToString() & " In TEST")
                    Else
                        sw.WriteLine("Mail inviata a " & MailContatto.ToString())
                    End If
                End If

                Dim query As String
                query = "INSERT INTO LogMailOperation (DataOra, DestinatarioPrincipale, DestinatarioCC, Oggetto, Esito, Note, GUIDRichiesta, IDRichiesta, testoHtml) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", MailContattoAgg)
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        If path IsNot Nothing Then
                            .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD & " con allegato ")
                        Else
                            .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD)
                        End If
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)
                        .Parameters.AddWithValue("@TestoHTML", TestoHTMLdainviare)
                        'testoHtml.Replace("#nomedemo", sNomeDemo).Replace("#datademo", sDataDemo).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommerciale).Replace("#interesseespresso", sInteresseEspresso).Replace("#cliente", sContattoPrincipaleCliente))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                    comm.Dispose()
                    myConn2.Close()
                End Using

            Catch ex As Exception
                sw.WriteLine("Errore Invio mail a commerciali " & MailContatto.ToString())

                Dim TestoHTMLdainviare As String

                TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemo).Replace("#listainteressi", sTabellaInteressiOLD).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspresso).Replace("#cliente", sContattoPrincipaleClienteOLD)
                TestoHTMLdainviare = testoHtml.Replace("#nomedemo", sNomeDemoOLD).Replace("#datademo", sDataDemoOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommercialeOLD).Replace("#interesseespresso", sInteresseEspressoOLD).Replace("#cliente", sContattoPrincipaleClienteOLD)
                Dim query As String
                query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@Dest", MailContatto)
                        .Parameters.AddWithValue("@CC", MailContattoAgg)
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        If path IsNot Nothing Then
                            .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD & " con allegato ")
                        Else
                            .Parameters.AddWithValue("@Note", "INVIO MAIL AD AZIENDA " & sAziendaClienteOLD)
                        End If
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)
                        .Parameters.AddWithValue("@TestoHTML", TestoHTMLdainviare)
                        'testoHtml.Replace("#nomedemo", sNomeDemo).Replace("#datademo", sDataDemo).Replace("#listainteressi", sTabellaInteressi).Replace("#commerciale", sCommerciale).Replace("#interesseespresso", sInteresseEspresso).Replace("#cliente", sContattoPrincipaleCliente))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using
            End Try

        End If

        Threading.Thread.Sleep(1500)
        'SVUOTO LA CARTELLA
        If Directory.Exists(Path_Cartella) Then
            Dim di As IO.DirectoryInfo =
                New IO.DirectoryInfo(Path_Cartella)
            For Each oFile As IO.FileInfo In di.GetFiles()
                Try
                    oFile.Delete()
                Catch ex As Exception
                    sw.WriteLine("Errore " & ex.Message.ToString())
                End Try

            Next
            Directory.Delete(Path_Cartella)
        End If

        sw.WriteLine("Fine " & Now().ToString)
        sw.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Function

    Private Function InviaMailInteressiComm2() As Boolean

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim DescrizionePresale As String
        Dim GUIDRichiestaCOP As String
        Dim GUIDRichiestaCOPOLD As String
        Dim IDRichiestaCOP As Integer
        Dim IdUtentePresale As Integer
        Dim IdCommerciale As Integer
        Dim IdCommercialeOLD As Integer

        Dim sTipologia As String
        Dim sTipologiaOLD As String
        Dim sCommerciale As String
        Dim sMailCommerciale As String
        Dim sAziendaCliente As String
        Dim sCommercialeOLD As String
        Dim sMailCommercialeOLD As String
        Dim sAziendaClienteOLD As String
        Dim DescrizionePresaleOLD As String

        Dim IDRichiestaCOPOLD As Integer

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailInteressiComm2_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        myConn = New SqlConnection(strConnectionDB) 'Create a Connection object.

        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        Dim strSQLgen As String
        Dim myReader1 As SqlDataReader

        sTipologiaOLD = ""
        strSQLgen = ""
        strSQLgen = "Select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myCmd = myConn.CreateCommand 'Create a Command object.
        myCmd.CommandText = strSQLgen
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop

        myReader1.Close() 'Close the reader and the database connection.

        Dim testoHtml As String
        Dim testoHtmlOLD As String
        Dim strOggetto As String
        Dim strOggettoBCK As String
        Dim testoHtmlBCK As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        testoHtmlOLD = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioEsigenzePostDemo1to1Comm'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop
        testoHtmlBCK = testoHtml

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioEsigenzePostDemo1to1Comm'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo").ToString() '.Replace("#azienda", sAziendaCliente)
        Loop
        myReader3.Close()
        strOggettoBCK = strOggetto
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim strSQL As String
        strSQL = "set dateformat dmy Exec spGetInteressiReminderComm2"

        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240

        myConn1.Open()

        Dim IDEsigenzePostDemo1to1 As Integer
        Dim sIdKey As String
        Dim sIdKeyDemo As String
        Dim sTabella As String
        Dim sTabellaDemo As String
        Dim sInteresseEspresso As String
        Dim sAltroReferente As String
        Dim sAltroEmail As String
        Dim sNote As String
        Dim sRiferimento As String
        Dim sDataDemo As String

        Dim IDEsigenzePostDemo1to1OLD As Integer
        Dim sIdKeyOLD As String
        Dim sIdKeyDemoOLD As String
        Dim sTabellaOLD As String
        Dim sTabellaDemoOLD As String
        Dim sInteresseEspressoOLD As String
        Dim sAltroReferenteOLD As String
        Dim sAltroEmailOLD As String
        Dim sNoteOLD As String
        Dim sRiferimentoOLD As String
        Dim sDataDemoOLD As String

        myReader1 = myCmd.ExecuteReader()

        Dim sTabellaInteressi As String
        Dim sTabellaInteressiOLD As String

        Dim y As Integer


        Dim MailContatto As String
        Dim MailContattoAgg As String
        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        sCommerciale = ""
        sCommercialeOLD = ""
        sMailCommercialeOLD = ""
        sAziendaClienteOLD = ""

        sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding:5px;text-align: left"">Tipologia</th><th style=""padding:5px;text-align: left"">Interesse Espresso</th><th style=""padding:5px;text-align: left"">Azienda</th><th style=""padding:5px;text-align: left"">Presale segnalatore</th></tr></thead><tbody>"
        sTabellaInteressiOLD = ""
        IDEsigenzePostDemo1to1 = 0
        y = 0

        Do While myReader1.Read()

            Try
                IDEsigenzePostDemo1to1 = myReader1.Item("ID")
            Catch ex As Exception
                IDEsigenzePostDemo1to1 = 0
            End Try
            Try
                IdCommerciale = myReader1.Item("IDCommerciale")
            Catch ex As Exception
                IdCommerciale = 0
            End Try
            Try
                sCommerciale = myReader1.Item("Commerciale")
            Catch ex As Exception
                sCommerciale = ""
            End Try
            Try
                sTipologia = myReader1.Item("Tipologia").ToString()
            Catch ex As Exception
                sTipologia = ""
            End Try
            Try
                sMailCommerciale = myReader1.Item("MailCommerciale")
            Catch ex As Exception
                sMailCommerciale = ""
            End Try
            Try
                IDRichiestaCOP = myReader1.Item("IDRichiestaCOP")
            Catch ex As Exception
                IDRichiestaCOP = 0
            End Try
            Try
                GUIDRichiestaCOP = myReader1.Item("GUIDRichiestaCOP")
            Catch ex As Exception
                GUIDRichiestaCOP = 0
            End Try
            Try
                sIdKeyDemo = myReader1.Item("IdKeyDemo")
            Catch ex As Exception
                sIdKeyDemo = ""
            End Try
            Try
                sAziendaCliente = myReader1.Item("AziendaCliente")
            Catch ex As Exception
                sAziendaCliente = ""
            End Try

            Try
                DescrizionePresale = myReader1.Item("DescrizionePresale")
            Catch ex As Exception
                DescrizionePresale = ""
            End Try

            Try
                sIdKey = myReader1.Item("IdKey")
            Catch ex As Exception
                sIdKey = ""
            End Try



            Try
                sTabella = myReader1.Item("Tabella")
            Catch ex As Exception
                sTabella = ""
            End Try

            Try
                sTabellaDemo = myReader1.Item("TabellaDemo")
            Catch ex As Exception
                sTabellaDemo = ""
            End Try
            Try
                sInteresseEspresso = myReader1.Item("InteresseEspresso").ToString()
            Catch ex As Exception
                sInteresseEspresso = ""
            End Try
            Try
                sAltroReferente = myReader1.Item("AltroReferente").ToString()
            Catch ex As Exception
                sAltroReferente = ""
            End Try
            Try
                sAltroEmail = myReader1.Item("AltroEmail").ToString()
            Catch ex As Exception
                sAltroEmail = ""
            End Try
            Try
                sNote = myReader1.Item("Note").ToString()
            Catch ex As Exception
                sNote = ""
            End Try
            Try
                sRiferimento = myReader1.Item("Riferimento").ToString()
            Catch ex As Exception
                sRiferimento = ""
            End Try
            Try
                sDataDemo = myReader1.Item("DataDemo").ToString()
            Catch ex As Exception
                sDataDemo = ""
            End Try

            '''''''''''''''''''''''''''''''''''''''''
            Dim queryU As String
            queryU = "update EsigenzePostDemo1to1 set SollecitoCommercialeDataInvio=getdate() where ID=@id"
            myConn2 = New SqlConnection(strConnectionDB)
            myConn2.Open()
            Using comm As New SqlCommand()
                With comm
                    .Connection = myConn2
                    .CommandType = CommandType.Text
                    .CommandText = queryU
                    .Parameters.AddWithValue("@id", IDEsigenzePostDemo1to1)
                End With
                Try
                    comm.ExecuteNonQuery()
                Catch ex As Exception
                    sw.WriteLine("*** ERRORE SU update EsigenzePostDemo1to1 " & DateTime.Now)
                    sw.WriteLine(ex.Message.ToString())
                    sw.Close()
                End Try
                comm.Dispose()
                myConn2.Close()
            End Using
            '''''''''''''''''''''''''''''''''''''''''


            'sTabellaInteressi = "<table>"

            If (sCommercialeOLD <> sCommerciale) And y > 0 Then
                'Invio la mail al commerciale sCommercialeOLD

                MailContattoAgg = ""

                MailContattoAgg = ""
                strTest = ""
                MailContatto = sMailCommercialeOLD

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strTest.ToUpper().Trim().ToString() = "S" Then

                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                    MailContatto = strDest 'TEST

                End If

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                strOggetto = ""
                testoHtml = testoHtmlBCK

                strOggetto = strOggettoBCK

                sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Try
                    Inviamail(strOggetto.Replace("#data", sDataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD), sDataDemoOLD, "", Nothing)

                    sw.WriteLine("Mail inviata a commerciale " & MailContatto.ToString())



                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                            .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                            .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                            .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                            .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                            .Parameters.AddWithValue("@DataDemo", sDataDemo)
                            .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                            .Parameters.AddWithValue("@TabellaEsigenza", sTabella)


                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                        comm.Dispose()
                        myConn2.Close()
                    End Using

                Catch ex As Exception
                    sw.WriteLine("Errore Invio mail a commerciali " & sMailCommercialeOLD.ToString())

                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                            .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                            .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                            .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                            .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                            .Parameters.AddWithValue("@DataDemo", sDataDemo)
                            .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                            .Parameters.AddWithValue("@TabellaEsigenza", sTabella)

                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                End Try

                IDEsigenzePostDemo1to1OLD = IDEsigenzePostDemo1to1
                sCommercialeOLD = sCommerciale
                IdCommercialeOLD = IdCommerciale
                sIdKeyOLD = sIdKey
                sIdKeyDemoOLD = sIdKeyDemo
                sTabellaOLD = sTabella
                sTabellaDemoOLD = sTabellaDemo
                sInteresseEspressoOLD = sInteresseEspresso
                sAltroReferenteOLD = sAltroReferente
                sAltroEmailOLD = sAltroEmail
                sNoteOLD = sNote
                sRiferimentoOLD = sRiferimento
                sDataDemoOLD = sDataDemo
                sAziendaClienteOLD = sAziendaCliente
                testoHtmlOLD = testoHtml
                sTabellaInteressiOLD = sTabellaInteressi
                IDRichiestaCOP = IDRichiestaCOPOLD
                GUIDRichiestaCOP = GUIDRichiestaCOPOLD
                sMailCommercialeOLD = sMailCommerciale
                DescrizionePresaleOLD = DescrizionePresale
                sTipologiaOLD = sTipologia
                sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding:5px;text-align: left"">Tipologia</th><th style=""padding:5px;text-align: left"">Interesse Espresso</th><th style=""padding:5px;text-align: left"">Azienda</th><th style=""padding:5px;text-align: left"">Presale segnalatore</th></tr></thead><tbody>"


            End If

            sCommercialeOLD = sCommerciale
            sMailCommercialeOLD = sMailCommerciale

            IDEsigenzePostDemo1to1OLD = IDEsigenzePostDemo1to1
            IdCommercialeOLD = IdCommerciale
            sIdKeyOLD = sIdKey
            sIdKeyDemoOLD = sIdKeyDemo
            sTabellaOLD = sTabella
            sTabellaDemoOLD = sTabellaDemo
            sInteresseEspressoOLD = sInteresseEspresso
            sAltroReferenteOLD = sAltroReferente
            sAltroEmailOLD = sAltroEmail
            sNoteOLD = sNote
            sRiferimentoOLD = sRiferimento
            sDataDemoOLD = sDataDemo
            sAziendaClienteOLD = sAziendaCliente
            testoHtmlOLD = testoHtml
            sTabellaInteressiOLD = sTabellaInteressi
            IDRichiestaCOPOLD = IDRichiestaCOP
            GUIDRichiestaCOPOLD = IDRichiestaCOP
            DescrizionePresaleOLD = DescrizionePresale
            sTipologiaOLD = sTipologia

            sTabellaInteressi = sTabellaInteressi + "<tr>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sTipologia & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sInteresseEspresso & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sAziendaCliente & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & DescrizionePresale & "</td>"
            sTabellaInteressi = sTabellaInteressi + "</tr>"

            y = y + 1

        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()
        sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"


        '''''''''''''''''''''''''''''''''''''''''
        MailContattoAgg = ""

        MailContattoAgg = ""
        strTest = ""
        MailContatto = sMailCommercialeOLD

        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

        If strTest.ToUpper().Trim().ToString() = "S" Then

            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

            MailContatto = strDest 'TEST

        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()
        myCmd4 = myConn2.CreateCommand
        myCmd4.CommandText = "Select Nominativo,Mail from Anagrafica_Commerciali where ID=@IdCommerciale"
        myCmd4.Parameters.AddWithValue("@IdCommerciale", IdCommercialeOLD)

        myReader4 = myCmd4.ExecuteReader()

        Do While myReader4.Read()
            sMailCommerciale = myReader4.Item("Mail")
        Loop

        myReader4.Close()
        myCmd4.Connection.Close()
        myCmd4.Dispose()
        myConn2.Close()

        strOggetto = ""
        testoHtml = testoHtmlBCK

        strOggetto = strOggettoBCK

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If MailContatto <> "" And sCommercialeOLD <> "" Then

            Try
                Inviamail(strOggetto.Replace("#data", sDataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", "#presaledemo").Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD), sDataDemoOLD, "", Nothing)

                sw.WriteLine("Mail inviata a commerciale " & MailContatto.ToString())



                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                        .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                        .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                        .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                        .Parameters.AddWithValue("@DataDemo", sDataDemo)
                        .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                        .Parameters.AddWithValue("@TabellaEsigenza", sTabella)

                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", "#presaledemo").Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                    comm.Dispose()
                    myConn2.Close()
                End Using

            Catch ex As Exception
                sw.WriteLine("Errore Invio mail a commerciali " & sMailCommercialeOLD.ToString())

                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                        .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                        .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                        .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                        .Parameters.AddWithValue("@DataDemo", sDataDemo)
                        .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                        .Parameters.AddWithValue("@TabellaEsigenza", sTabella)

                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", "#presaledemo").Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using

            End Try
        End If

        sw.WriteLine("Fine " & Now().ToString)
        sw.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Function

    Private Function InviaMailFeedback1to1Presale() As Boolean

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailFeedback1to1Presale_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        myConn = New SqlConnection(strConnectionDB) 'Create a Connection object.

        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        Dim strSQLgen As String
        Dim myReader1 As SqlDataReader

        strSQLgen = ""
        strSQLgen = "Select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myCmd = myConn.CreateCommand 'Create a Command object.
        myCmd.CommandText = strSQLgen
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop

        myReader1.Close() 'Close the reader and the database connection.

        Dim testoHtml As String
        Dim testoHtmlOLD As String
        Dim strOggetto As String
        Dim strOggettoBCK As String
        Dim testoHtmlBCK As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        testoHtmlOLD = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioRisFeedback1to1Presale'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop
        testoHtmlBCK = testoHtml

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioRisFeedback1to1Presale'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo").ToString() '.Replace("#azienda", sAziendaCliente)
        Loop
        myReader3.Close()
        strOggettoBCK = strOggetto
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim strSQL As String
        strSQL = "set dateformat dmy;
        Select rc.AziendaCliente, rc.ContattoPrincipaleCliente, convert(varchar(10), qc.DataCompilazione,103) DataCompilazione,
          ac.Nominativo NominativoCommerciale,
          ac.Mail MailCommerciale,
          d.Testo TestoDomanda
          ,r.Testo TestoRisposta
          ,isNull(IDQuestionarioSezioneArgomentoDomanda,0) IDDomanda, isNull(IDRisposta,0) IDRisposta, isNull(TestoRisposta,'') TestoLibero,
          Case qc.tabellaDEMO When 'Preventivi_Sezioni' THEN (SELECT SEZIONE FROM Preventivi_Sezioni WHERE ID=qc.IDKeyDemo) ELSE (SELECT Funzione FROM Preventivi_Funzioni WHERE ID=qc.IDKeyDemo) END Demo
          ,convert(varchar(10),rd.DataDemo,103) DataDemo,
          rd.OrarioDemo,
          u.Descrizione DescrizionePresale,
          u.Email MailPresale,
          isnull(  (Select SUBSTRING (  (Select ', ' + ud.Email as 'data()'   FROM DelegaFeedback1to1 d join Users ud on ud.ID=d.IDPresaleDelegato where d.IDKey=rd.IDKey and d.tabella=rd.Tabella  
          For Xml Path('')  ),2,9999) SezioniDemo),'') EmailPresaleDelegati
        From Compilazione_Questionari cq
        Join Questionari_Clienti qc on qc.ID = cq.IDQuestionarioCliente
        Join Questionari q on q.ID=qc.IDQuestionario
        Join domande d on d.ID = cq.IDQuestionarioSezioneArgomentoDomanda
        Left Join RisposteDomandeMultiple r on r.ID = cq.IDRisposta
        Join Richieste_COP rc on rc.ID=qc.IDRichiestaCOP
        Join RichiesteDemo_COP rd on rd.IDKey=qc.IDKeyDemo And rd.Tabella=qc.TabellaDemo And rd.Progressivo=qc.Progressivo And rd.IDRichiestaCop=rc.ID
        Left Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale
        Left Join Users u on u.ID=rd.IDUtentePresaleRiferimento
        where isnull(q.Feedback, 0) = 1
        And isnull(qc.Compilato,0)=1
        and (year(qc.DataCompilazione) = year(getdate()) and month(qc.DataCompilazione) = month(getdate()) and day(qc.DataCompilazione) = day(getdate()))
        /*And convert(varchar(10), qc.DataCompilazione,103)>=convert(varchar(10), GETDATE(),103)*/
        /*And convert(varchar(10), qc.DataCompilazione,103)>=convert(varchar(10), DATEADD(day,-116, GETDATE()),103)*/
        and ac.Nominativo is not null
        order by rd.IDUtentePresaleRiferimento, rc.AziendaCliente/*, d.Ordinamento, r.Ordinamento*/"

        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240

        myConn1.Open()

        Dim AziendaCliente As String
        Dim ContattoPrincipaleCliente As String
        Dim NominativoCommerciale As String
        Dim MailCommerciale As String
        Dim TestoDomanda As String
        Dim TestoRisposta As String
        Dim IDDomanda As Integer
        Dim IDRisposta As Integer
        Dim TestoLibero As String
        Dim Demo As String
        Dim DataDemo As String
        Dim OrarioDemo As String
        Dim DescrizionePresale As String
        Dim MailPresale As String
        Dim EmailPresaleDelegati As String
        Dim DataCompilazione As String

        Dim AziendaClienteOLD As String
        Dim ContattoPrincipaleClienteOLD As String
        Dim NominativoCommercialeOLD As String
        Dim MailCommercialeOLD As String
        Dim TestoDomandaOLD As String
        Dim TestoRispostaOLD As String
        Dim IDDomandaOLD As Integer
        Dim IDRispostaOLD As Integer
        Dim TestoLiberoOLD As String
        Dim DemoOLD As String
        Dim DataDemoOLD As String
        Dim OrarioDemoOLD As String
        Dim DescrizionePresaleOLD As String
        Dim MailPresaleOLD As String
        Dim EmailPresaleDelegatiOLD As String
        Dim DataCompilazioneOLD As String

        myReader1 = myCmd.ExecuteReader()

        Dim sTabellaInteressi As String
        Dim sTabellaInteressiOLD As String

        Dim y As Integer


        Dim MailContatto As String
        Dim MailContattoAgg As String
        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;"">
        <table style='width:100%;margin:0 auto' border='1' bgcolor='#C8EBFA'>
        <tbody>"
        sTabellaInteressiOLD = ""

        '<thead>
        '<th style=""padding:5px;text-align: left"">Testo domanda</th>
        '<th style=""padding:5px;text-align: left"">Testo Risposta</th>
        '<th style=""padding:5px;text-align: left"">Testo libero</th>
        '</tr>
        '</thead>

        y = 0

        Do While myReader1.Read()

            Try
                AziendaCliente = myReader1.Item("AziendaCliente").ToString()
            Catch ex As Exception
                AziendaCliente = ""
            End Try
            Try
                ContattoPrincipaleCliente = myReader1.Item("ContattoPrincipaleCliente").ToString()
            Catch ex As Exception
                ContattoPrincipaleCliente = ""
            End Try
            Try
                NominativoCommerciale = myReader1.Item("NominativoCommerciale").ToString()
            Catch ex As Exception
                NominativoCommerciale = ""
            End Try
            Try
                MailCommerciale = myReader1.Item("MailCommerciale").ToString()
            Catch ex As Exception
                MailCommerciale = ""
            End Try
            Try
                TestoDomanda = myReader1.Item("TestoDomanda").ToString()
            Catch ex As Exception
                TestoDomanda = ""
            End Try
            Try
                TestoRisposta = myReader1.Item("TestoRisposta").ToString()
            Catch ex As Exception
                TestoRisposta = ""
            End Try
            Try
                IDDomanda = myReader1.Item("IDDomanda")
            Catch ex As Exception
                IDDomanda = 0
            End Try
            Try
                IDRisposta = myReader1.Item("IDRisposta")
            Catch ex As Exception
                IDRisposta = 0
            End Try
            Try
                TestoLibero = myReader1.Item("TestoLibero").ToString()
            Catch ex As Exception
                TestoLibero = ""
            End Try
            Try
                Demo = myReader1.Item("Demo").ToString()
            Catch ex As Exception
                Demo = ""
            End Try
            Try
                DataDemo = myReader1.Item("DataDemo").ToString()
            Catch ex As Exception
                DataDemo = ""
            End Try
            Try
                DataCompilazione = myReader1.Item("DataCompilazione").ToString()
            Catch ex As Exception
                DataCompilazione = ""
            End Try
            Try
                OrarioDemo = myReader1.Item("OrarioDemo").ToString()
            Catch ex As Exception
                OrarioDemo = ""
            End Try
            Try
                DescrizionePresale = myReader1.Item("DescrizionePresale").ToString()
            Catch ex As Exception
                DescrizionePresale = ""
            End Try
            Try
                MailPresale = myReader1.Item("MailPresale").ToString()
            Catch ex As Exception
                MailPresale = ""
            End Try
            Try
                EmailPresaleDelegati = myReader1.Item("EmailPresaleDelegati").ToString()
            Catch ex As Exception
                EmailPresaleDelegati = ""
            End Try



            ' sTabellaInteressi = "<table>"

            If AziendaClienteOLD <> AziendaCliente Or y = 0 Then
                sTabellaInteressi = sTabellaInteressi + "<tr>"

                If (DescrizionePresaleOLD = DescrizionePresale) Or y = 0 Then
                    sTabellaInteressi = sTabellaInteressi + "<td style='font-weight:bold;text-align: left;'>" & AziendaCliente & "<br>" & Demo & " " & DataDemo & "<br>" & ContattoPrincipaleCliente & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Risposta" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Testo libero" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "</tr>"
                End If

            End If

            If (DescrizionePresaleOLD <> DescrizionePresale) And y > 0 Then
                'Invio la mail al commerciale PRESALE

                MailContattoAgg = ""

                MailContattoAgg = EmailPresaleDelegatiOLD
                strTest = ""
                MailContatto = MailPresaleOLD

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strTest.ToUpper().Trim().ToString() = "S" Then
                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    If MailContatto <> "" Then
                        MailContatto = strDest 'TEST
                    End If
                End If

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                strOggetto = ""
                testoHtml = testoHtmlBCK
                strOggetto = strOggettoBCK
                sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Try
                    Inviamail(strOggetto.Replace("#data", DataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", DataDemoOLD).Replace("#presale", DescrizionePresaleOLD).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD), DataDemoOLD, "", Nothing)

                    sw.WriteLine("Mail inviata a PRESALE " & MailContatto.ToString())

                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailPresaleOLD)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)

                            .Parameters.AddWithValue("@IdKey", 0)
                            .Parameters.AddWithValue("@Tabella", "")
                            .Parameters.AddWithValue("@DataDemo", "")
                            .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                            .Parameters.AddWithValue("@TabellaEsigenza", "")

                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", MailCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                        comm.Dispose()
                        myConn2.Close()
                    End Using

                Catch ex As Exception
                    sw.WriteLine("Errore Invio mail a commerciali " & MailCommercialeOLD.ToString())

                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailPresaleOLD)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)

                            .Parameters.AddWithValue("@IdKey", 0)
                            .Parameters.AddWithValue("@Tabella", "")
                            .Parameters.AddWithValue("@DataDemo", "")
                            .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                            .Parameters.AddWithValue("@TabellaEsigenza", "")
                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", NominativoCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                End Try

                sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;"">
                <table style='width:100%;margin:0 auto' border='1' bgcolor='#C8EBFA'>
                <tbody>"

                If AziendaClienteOLD <> AziendaCliente Then
                    sTabellaInteressi = sTabellaInteressi + "<tr>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='font-weight:bold;text-align: left;'>" & AziendaCliente & "<br>" & Demo & "<br>" & DataDemo & "<br>" & ContattoPrincipaleCliente & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Risposta" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Testo libero" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "</tr>"
                End If

                AziendaClienteOLD = AziendaCliente
                ContattoPrincipaleClienteOLD = ContattoPrincipaleCliente
                NominativoCommercialeOLD = NominativoCommerciale
                MailCommercialeOLD = MailCommerciale
                TestoDomandaOLD = TestoDomanda
                TestoRispostaOLD = TestoRisposta
                IDDomandaOLD = IDDomanda
                IDRispostaOLD = IDRisposta
                TestoLiberoOLD = TestoLibero
                DemoOLD = Demo
                DataDemoOLD = DataDemo
                OrarioDemoOLD = OrarioDemo
                DescrizionePresaleOLD = DescrizionePresale
                MailPresaleOLD = MailPresale
                EmailPresaleDelegatiOLD = EmailPresaleDelegati
                DataCompilazioneOLD = DataCompilazione

            End If


            AziendaClienteOLD = AziendaCliente
            ContattoPrincipaleClienteOLD = ContattoPrincipaleCliente
            NominativoCommercialeOLD = NominativoCommerciale
            MailCommercialeOLD = MailCommerciale
            TestoDomandaOLD = TestoDomanda
            TestoRispostaOLD = TestoRisposta
            IDDomandaOLD = IDDomanda
            IDRispostaOLD = IDRisposta
            TestoLiberoOLD = TestoLibero
            DemoOLD = Demo
            DataDemoOLD = DataDemo
            OrarioDemoOLD = OrarioDemo
            DescrizionePresaleOLD = DescrizionePresale
            MailPresaleOLD = MailPresale
            EmailPresaleDelegatiOLD = EmailPresaleDelegati
            DataCompilazioneOLD = DataCompilazione

            sTabellaInteressi = sTabellaInteressi + "<tr>"

            sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & TestoDomandaOLD & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & TestoRispostaOLD & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & TestoLiberoOLD & "</td>"
            sTabellaInteressi = sTabellaInteressi + "</tr>"

            y = y + 1

        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()
        sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"


        '''''''''''''''''''''''''''''''''''''''''
        MailContattoAgg = ""

        MailContattoAgg = ""
        strTest = ""
        MailContatto = MailPresaleOLD
        MailContattoAgg = EmailPresaleDelegatiOLD

        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

        If strTest.ToUpper().Trim().ToString() = "S" Then

            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

            MailContatto = strDest 'TEST
            If MailContattoAgg <> "" Then
                MailContattoAgg = strDest 'TEST
            End If

        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        strOggetto = ""
        testoHtml = testoHtmlBCK

        strOggetto = strOggettoBCK

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If MailContatto <> "" And NominativoCommercialeOLD <> "" Then

            Try
                Inviamail(strOggetto.Replace("#data", DataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", DataDemoOLD).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#presale", DescrizionePresaleOLD).Replace("#presaledemo", "#presaledemo").Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#datacompilazione", DataCompilazioneOLD), DataDemoOLD, "", Nothing)




                sw.WriteLine("Mail inviata a PRESALE " & MailContatto.ToString())

                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", MailContattoAgg)
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)

                        .Parameters.AddWithValue("@IdKey", 0)
                        .Parameters.AddWithValue("@Tabella", "")
                        .Parameters.AddWithValue("@DataDemo", "")
                        .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                        .Parameters.AddWithValue("@TabellaEsigenza", "")
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", NominativoCommerciale).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#datacompilazione", DataCompilazioneOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                    comm.Dispose()
                    myConn2.Close()
                End Using

            Catch ex As Exception
                sw.WriteLine("Errore Invio mail a commerciali " & MailCommercialeOLD.ToString())

                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)

                        .Parameters.AddWithValue("@IdKey", 0)
                        .Parameters.AddWithValue("@Tabella", "")
                        .Parameters.AddWithValue("@DataDemo", "")
                        .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                        .Parameters.AddWithValue("@TabellaEsigenza", "")
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", NominativoCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#datacompilazione", DataCompilazioneOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using

            End Try
        End If

        sw.WriteLine("Fine " & Now().ToString)
        sw.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Function

    Private Function InviaMailFeedback1to1Comm() As Boolean

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailFeedback1to1Comm_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        myConn = New SqlConnection(strConnectionDB) 'Create a Connection object.

        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        Dim strSQLgen As String
        Dim myReader1 As SqlDataReader

        strSQLgen = ""
        strSQLgen = "Select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myCmd = myConn.CreateCommand 'Create a Command object.
        myCmd.CommandText = strSQLgen
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop

        myReader1.Close() 'Close the reader and the database connection.

        Dim testoHtml As String
        Dim testoHtmlOLD As String
        Dim strOggetto As String
        Dim strOggettoBCK As String
        Dim testoHtmlBCK As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        testoHtmlOLD = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioRisFeedback1to1Commerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop
        testoHtmlBCK = testoHtml

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioRisFeedback1to1Commerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo").ToString() '.Replace("#azienda", sAziendaCliente)
        Loop
        myReader3.Close()
        strOggettoBCK = strOggetto
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim strSQL As String
        strSQL = "set dateformat dmy;
        Select rc.AziendaCliente, rc.ContattoPrincipaleCliente, convert(varchar(10), qc.DataCompilazione,103) DataCompilazione,
          ac.Nominativo NominativoCommerciale,
          ac.Mail MailCommerciale,
          d.Testo TestoDomanda
          ,r.Testo TestoRisposta
          ,isNull(IDQuestionarioSezioneArgomentoDomanda,0) IDDomanda, isNull(IDRisposta,0) IDRisposta, isNull(TestoRisposta,'') TestoLibero,
          Case qc.tabellaDEMO When 'Preventivi_Sezioni' THEN (SELECT SEZIONE FROM Preventivi_Sezioni WHERE ID=qc.IDKeyDemo) ELSE (SELECT Funzione FROM Preventivi_Funzioni WHERE ID=qc.IDKeyDemo) END Demo
          ,convert(varchar(10),rd.DataDemo,103) DataDemo,
          rd.OrarioDemo,
          u.Descrizione DescrizionePresale,
          u.Email MailPresale
        From Compilazione_Questionari cq
        Join Questionari_Clienti qc on qc.ID = cq.IDQuestionarioCliente
        Join Questionari q on q.ID=qc.IDQuestionario
        Join domande d on d.ID = cq.IDQuestionarioSezioneArgomentoDomanda
        Left Join RisposteDomandeMultiple r on r.ID = cq.IDRisposta
        Join Richieste_COP rc on rc.ID=qc.IDRichiestaCOP
        Join RichiesteDemo_COP rd on rd.IDKey=qc.IDKeyDemo And rd.Tabella=qc.TabellaDemo And rd.Progressivo=qc.Progressivo And rd.IDRichiestaCop=rc.ID
        Left Join Anagrafica_Commerciali ac on ac.ID=rc.IDCommerciale
        Left Join Users u on u.ID=rd.IDUtentePresaleRiferimento
        where isnull(q.Feedback, 0) = 1
        And isnull(qc.Compilato,0)=1
        and (year(qc.DataCompilazione) = year(getdate()) and month(qc.DataCompilazione) = month(getdate()) and day(qc.DataCompilazione) = day(getdate()))
        /*And convert(varchar(10), qc.DataCompilazione,103)>=convert(varchar(10), GETDATE(),103)*/
        /*And convert(varchar(10), qc.DataCompilazione,103)>=convert(varchar(10), DATEADD(day,-116, GETDATE()),103)*/
        and ac.Nominativo is not null
        order by ac.id, rc.AziendaCliente/*, d.Ordinamento, r.Ordinamento*/"


        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240

        myConn1.Open()

        Dim AziendaCliente As String
        Dim ContattoPrincipaleCliente As String
        Dim NominativoCommerciale As String
        Dim MailCommerciale As String
        Dim TestoDomanda As String
        Dim TestoRisposta As String
        Dim IDDomanda As Integer
        Dim IDRisposta As Integer
        Dim TestoLibero As String
        Dim Demo As String
        Dim DataDemo As String
        Dim OrarioDemo As String
        Dim DescrizionePresale As String
        Dim MailPresale As String
        Dim DataCompilazione As String

        Dim AziendaClienteOLD As String
        Dim ContattoPrincipaleClienteOLD As String
        Dim NominativoCommercialeOLD As String
        Dim MailCommercialeOLD As String
        Dim TestoDomandaOLD As String
        Dim TestoRispostaOLD As String
        Dim IDDomandaOLD As Integer
        Dim IDRispostaOLD As Integer
        Dim TestoLiberoOLD As String
        Dim DemoOLD As String
        Dim DataDemoOLD As String
        Dim OrarioDemoOLD As String
        Dim DescrizionePresaleOLD As String
        Dim MailPresaleOLD As String
        Dim DataCompilazioneOLD As String

        myReader1 = myCmd.ExecuteReader()

        Dim sTabellaInteressi As String
        Dim sTabellaInteressiOLD As String

        Dim y As Integer


        Dim MailContatto As String
        Dim MailContattoAgg As String
        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;"">
        <table style='width:100%;margin:0 auto' border='1' bgcolor='#C8EBFA'>
        <tbody>"
        sTabellaInteressiOLD = ""

        '<thead>
        '<th style=""padding:5px;text-align: left"">Testo domanda</th>
        '<th style=""padding:5px;text-align: left"">Testo Risposta</th>
        '<th style=""padding:5px;text-align: left"">Testo libero</th>
        '</tr>
        '</thead>

        y = 0

        Do While myReader1.Read()

            Try
                AziendaCliente = myReader1.Item("AziendaCliente").ToString()
            Catch ex As Exception
                AziendaCliente = ""
            End Try
            Try
                ContattoPrincipaleCliente = myReader1.Item("ContattoPrincipaleCliente").ToString()
            Catch ex As Exception
                ContattoPrincipaleCliente = ""
            End Try
            Try
                NominativoCommerciale = myReader1.Item("NominativoCommerciale").ToString()
            Catch ex As Exception
                NominativoCommerciale = ""
            End Try
            Try
                MailCommerciale = myReader1.Item("MailCommerciale").ToString()
            Catch ex As Exception
                MailCommerciale = ""
            End Try
            Try
                TestoDomanda = myReader1.Item("TestoDomanda").ToString()
            Catch ex As Exception
                TestoDomanda = ""
            End Try
            Try
                TestoRisposta = myReader1.Item("TestoRisposta").ToString()
            Catch ex As Exception
                TestoRisposta = ""
            End Try
            Try
                IDDomanda = myReader1.Item("IDDomanda")
            Catch ex As Exception
                IDDomanda = 0
            End Try
            Try
                IDRisposta = myReader1.Item("IDRisposta")
            Catch ex As Exception
                IDRisposta = 0
            End Try
            Try
                TestoLibero = myReader1.Item("TestoLibero").ToString()
            Catch ex As Exception
                TestoLibero = ""
            End Try
            Try
                Demo = myReader1.Item("Demo").ToString()
            Catch ex As Exception
                Demo = ""
            End Try
            Try
                DataDemo = myReader1.Item("DataDemo").ToString()
            Catch ex As Exception
                DataDemo = ""
            End Try
            Try
                DataCompilazione = myReader1.Item("DataCompilazione").ToString()
            Catch ex As Exception
                DataCompilazione = ""
            End Try
            Try
                OrarioDemo = myReader1.Item("OrarioDemo").ToString()
            Catch ex As Exception
                OrarioDemo = ""
            End Try
            Try
                DescrizionePresale = myReader1.Item("DescrizionePresale").ToString()
            Catch ex As Exception
                DescrizionePresale = ""
            End Try
            Try
                MailPresale = myReader1.Item("MailPresale").ToString()
            Catch ex As Exception
                MailPresale = ""
            End Try

            ' sTabellaInteressi = "<table>"

            If AziendaClienteOLD <> AziendaCliente Or y = 0 Then
                sTabellaInteressi = sTabellaInteressi + "<tr>"

                If (NominativoCommercialeOLD = NominativoCommerciale) Or y = 0 Then
                    sTabellaInteressi = sTabellaInteressi + "<td style='font-weight:bold;text-align: left;'>" & AziendaCliente & "<br>" & Demo & " " & DataDemo & "<br>" & ContattoPrincipaleCliente & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Risposta" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Testo libero" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "</tr>"
                End If

            End If

            If (NominativoCommercialeOLD <> NominativoCommerciale) And y > 0 Then
                'Invio la mail al commerciale COMMERCIALE

                MailContattoAgg = ""

                MailContattoAgg = ""
                strTest = ""
                MailContatto = MailCommercialeOLD

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strTest.ToUpper().Trim().ToString() = "S" Then

                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                    MailContatto = strDest 'TEST

                End If

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                strOggetto = ""
                testoHtml = testoHtmlBCK

                strOggetto = strOggettoBCK

                sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Try
                    Inviamail(strOggetto.Replace("#data", DataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", NominativoCommercialeOLD).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD), DataDemoOLD, "", Nothing)

                    sw.WriteLine("Mail inviata a commerciale " & MailContatto.ToString())

                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommercialeOLD)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)

                            .Parameters.AddWithValue("@IdKey", 0)
                            .Parameters.AddWithValue("@Tabella", "")
                            .Parameters.AddWithValue("@DataDemo", "")
                            .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                            .Parameters.AddWithValue("@TabellaEsigenza", "")

                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", MailCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                        comm.Dispose()
                        myConn2.Close()
                    End Using

                Catch ex As Exception
                    sw.WriteLine("Errore Invio mail a commerciali " & MailCommercialeOLD.ToString())

                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@IDRichiesta", 0)

                            .Parameters.AddWithValue("@IdKey", 0)
                            .Parameters.AddWithValue("@Tabella", "")
                            .Parameters.AddWithValue("@DataDemo", "")
                            .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                            .Parameters.AddWithValue("@TabellaEsigenza", "")
                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", NominativoCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                End Try

                sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;"">
                <table style='width:100%;margin:0 auto' border='1' bgcolor='#C8EBFA'>
                <tbody>"

                If AziendaClienteOLD <> AziendaCliente Then
                    sTabellaInteressi = sTabellaInteressi + "<tr>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='font-weight:bold;text-align: left;'>" & AziendaCliente & "<br>" & Demo & "<br>" & DataDemo & "<br>" & ContattoPrincipaleCliente & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Risposta" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & "Testo libero" & "</td>"
                    sTabellaInteressi = sTabellaInteressi + "</tr>"
                End If

                AziendaClienteOLD = AziendaCliente
                ContattoPrincipaleClienteOLD = ContattoPrincipaleCliente
                NominativoCommercialeOLD = NominativoCommerciale
                MailCommercialeOLD = MailCommerciale
                TestoDomandaOLD = TestoDomanda
                TestoRispostaOLD = TestoRisposta
                IDDomandaOLD = IDDomanda
                IDRispostaOLD = IDRisposta
                TestoLiberoOLD = TestoLibero
                DemoOLD = Demo
                DataDemoOLD = DataDemo
                OrarioDemoOLD = OrarioDemo
                DescrizionePresaleOLD = DescrizionePresale
                MailPresaleOLD = MailPresale
                DataCompilazioneOLD = DataCompilazione

            End If


            AziendaClienteOLD = AziendaCliente
            ContattoPrincipaleClienteOLD = ContattoPrincipaleCliente
            NominativoCommercialeOLD = NominativoCommerciale
            MailCommercialeOLD = MailCommerciale
            TestoDomandaOLD = TestoDomanda
            TestoRispostaOLD = TestoRisposta
            IDDomandaOLD = IDDomanda
            IDRispostaOLD = IDRisposta
            TestoLiberoOLD = TestoLibero
            DemoOLD = Demo
            DataDemoOLD = DataDemo
            OrarioDemoOLD = OrarioDemo
            DescrizionePresaleOLD = DescrizionePresale
            MailPresaleOLD = MailPresale
            DataCompilazioneOLD = DataCompilazione

            sTabellaInteressi = sTabellaInteressi + "<tr>"

            sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & TestoDomandaOLD & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & TestoRispostaOLD & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style='text-align: left;'>" & TestoLiberoOLD & "</td>"
            sTabellaInteressi = sTabellaInteressi + "</tr>"

            y = y + 1

        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()
        sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"


        '''''''''''''''''''''''''''''''''''''''''
        MailContattoAgg = ""

        MailContattoAgg = ""
        strTest = ""
        MailContatto = MailCommercialeOLD

        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

        If strTest.ToUpper().Trim().ToString() = "S" Then

            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

            MailContatto = strDest 'TEST

        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


        strOggetto = ""
        testoHtml = testoHtmlBCK

        strOggetto = strOggettoBCK

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If MailContatto <> "" And NominativoCommercialeOLD <> "" Then

            Try
                Inviamail(strOggetto.Replace("#data", DataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", DataDemoOLD).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#commerciale", NominativoCommercialeOLD).Replace("#presaledemo", "#presaledemo").Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#datacompilazione", DataCompilazioneOLD), DataDemoOLD, "", Nothing)




                sw.WriteLine("Mail inviata a commerciale " & MailContatto.ToString())

                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)

                        .Parameters.AddWithValue("@IdKey", 0)
                        .Parameters.AddWithValue("@Tabella", "")
                        .Parameters.AddWithValue("@DataDemo", "")
                        .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                        .Parameters.AddWithValue("@TabellaEsigenza", "")
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", NominativoCommerciale).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#datacompilazione", DataCompilazioneOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                    comm.Dispose()
                    myConn2.Close()
                End Using

            Catch ex As Exception
                sw.WriteLine("Errore Invio mail a commerciali " & MailCommercialeOLD.ToString())

                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & MailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@IDRichiesta", 0)

                        .Parameters.AddWithValue("@IdKey", 0)
                        .Parameters.AddWithValue("@Tabella", "")
                        .Parameters.AddWithValue("@DataDemo", "")
                        .Parameters.AddWithValue("@IdKeyEsigenza", 0)
                        .Parameters.AddWithValue("@TabellaEsigenza", "")
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", DataDemoOLD).Replace("#commerciale", NominativoCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#datacompilazione", DataCompilazioneOLD).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD).Replace("#datacompilazione", DataCompilazioneOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using

            End Try
        End If

        sw.WriteLine("Fine " & Now().ToString)
        sw.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Function





    Private Function InviaMailInteressiComm() As Boolean

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim GUIDRichiestaCOP As String
        Dim sIdKeyDemo As Integer
        Dim GUIDRichiestaCOPold As String
        Dim sIdKeyDemoOLD As String
        Dim DescrizionePresale As String
        Dim IDRichiestaCOP As Integer
        Dim IdUtentePresale As Integer
        Dim IdCommerciale As Integer
        Dim IdCommercialeOLD As Integer

        Dim sTipologia As String
        Dim sTipologiaOLD As String
        Dim sCommerciale As String
        Dim sMailCommerciale As String
        Dim sAziendaCliente As String
        Dim sCommercialeOLD As String
        Dim sMailCommercialeOLD As String
        Dim sAziendaClienteOLD As String
        Dim DescrizionePresaleOLD As String

        Dim IDRichiestaCOPOLD As Integer

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailInteressiComm_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        myConn = New SqlConnection(strConnectionDB) 'Create a Connection object.

        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        Dim strSQLgen As String
        Dim myReader1 As SqlDataReader

        sTipologiaOLD = ""
        strSQLgen = ""
        strSQLgen = "Select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myCmd = myConn.CreateCommand 'Create a Command object.
        myCmd.CommandText = strSQLgen
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop

        myReader1.Close() 'Close the reader and the database connection.

        Dim testoHtml As String
        Dim testoHtmlOLD As String
        Dim strOggetto As String
        Dim strOggettoBCK As String
        Dim testoHtmlBCK As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        testoHtmlOLD = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioEsigenzePostDemo1to1Comm'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop
        testoHtmlBCK = testoHtml

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioEsigenzePostDemo1to1Comm'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo").ToString() '.Replace("#azienda", sAziendaCliente)
        Loop
        myReader3.Close()
        strOggettoBCK = strOggetto
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim strSQL As String
        strSQL = "set dateformat dmy Exec spGetInteressiReminderComm"

        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240

        myConn1.Open()

        Dim IDEsigenzePostDemo1to1 As Integer
        Dim sIdKey As String
        Dim sTabella As String
        Dim sTabellaDemo As String
        Dim sInteresseEspresso As String
        Dim sAltroReferente As String
        Dim sAltroEmail As String
        Dim sNote As String
        Dim sRiferimento As String
        Dim sDataDemo As String

        Dim IDEsigenzePostDemo1to1OLD As Integer
        Dim sIdKeyOLD As String
        Dim sTabellaOLD As String
        Dim sTabellaDemoOLD As String
        Dim sInteresseEspressoOLD As String
        Dim sAltroReferenteOLD As String
        Dim sAltroEmailOLD As String
        Dim sNoteOLD As String
        Dim sRiferimentoOLD As String
        Dim sDataDemoOLD As String

        myReader1 = myCmd.ExecuteReader()

        Dim sTabellaInteressi As String
        Dim sTabellaInteressiOLD As String

        Dim y As Integer


        Dim MailContatto As String
        Dim MailContattoAgg As String
        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        sCommerciale = ""
        sCommercialeOLD = ""
        sMailCommercialeOLD = ""
        sAziendaClienteOLD = ""

        sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding:5px;text-align: left"">Tipologia</th><th style=""padding:5px;text-align: left"">Interesse Espresso</th><th style=""padding:5px;text-align: left"">Azienda</th><th style=""padding:5px;text-align: left"">Presale segnalatore</th></tr></thead><tbody>"
        sTabellaInteressiOLD = ""
        IDEsigenzePostDemo1to1 = 0
        y = 0

        Do While myReader1.Read()

            Try
                IDEsigenzePostDemo1to1 = myReader1.Item("ID")
            Catch ex As Exception
                IDEsigenzePostDemo1to1 = 0
            End Try
            Try
                IdCommerciale = myReader1.Item("IDCommerciale")
            Catch ex As Exception
                IdCommerciale = 0
            End Try
            Try
                sCommerciale = myReader1.Item("Commerciale")
            Catch ex As Exception
                sCommerciale = ""
            End Try
            Try
                sTipologia = myReader1.Item("Tipologia").ToString()
            Catch ex As Exception
                sTipologia = ""
            End Try
            Try
                sMailCommerciale = myReader1.Item("MailCommerciale")
            Catch ex As Exception
                sMailCommerciale = ""
            End Try
            Try
                IDRichiestaCOP = myReader1.Item("IDRichiestaCOP")
            Catch ex As Exception
                IDRichiestaCOP = 0
            End Try
            Try
                GUIDRichiestaCOP = myReader1.Item("GUIDRichiestaCOP")
            Catch ex As Exception
                GUIDRichiestaCOP = 0
            End Try
            Try
                sIdKeyDemo = myReader1.Item("IdKeyDemo")
            Catch ex As Exception
                sIdKeyDemo = ""
            End Try

            Try
                sAziendaCliente = myReader1.Item("AziendaCliente")
            Catch ex As Exception
                sAziendaCliente = ""
            End Try

            Try
                DescrizionePresale = myReader1.Item("DescrizionePresale")
            Catch ex As Exception
                DescrizionePresale = ""
            End Try

            Try
                sIdKey = myReader1.Item("IdKey")
            Catch ex As Exception
                sIdKey = ""
            End Try

            Try
                sTabella = myReader1.Item("Tabella")
            Catch ex As Exception
                sTabella = ""
            End Try

            Try
                sTabellaDemo = myReader1.Item("TabellaDemo")
            Catch ex As Exception
                sTabellaDemo = ""
            End Try
            Try
                sInteresseEspresso = myReader1.Item("InteresseEspresso").ToString()
            Catch ex As Exception
                sInteresseEspresso = ""
            End Try
            Try
                sAltroReferente = myReader1.Item("AltroReferente").ToString()
            Catch ex As Exception
                sAltroReferente = ""
            End Try
            Try
                sAltroEmail = myReader1.Item("AltroEmail").ToString()
            Catch ex As Exception
                sAltroEmail = ""
            End Try
            Try
                sNote = myReader1.Item("Note").ToString()
            Catch ex As Exception
                sNote = ""
            End Try
            Try
                sRiferimento = myReader1.Item("Riferimento").ToString()
            Catch ex As Exception
                sRiferimento = ""
            End Try
            Try
                sDataDemo = myReader1.Item("DataDemo").ToString()
            Catch ex As Exception
                sDataDemo = ""
            End Try

            '''''''''''''''''''''''''''''''''''''''''
            Dim queryU As String
            queryU = "update EsigenzePostDemo1to1 set MailCommercialeInviata=1 where ID=@id"
            myConn2 = New SqlConnection(strConnectionDB)
            myConn2.Open()
            Using comm As New SqlCommand()
                With comm
                    .Connection = myConn2
                    .CommandType = CommandType.Text
                    .CommandText = queryU
                    .Parameters.AddWithValue("@id", IDEsigenzePostDemo1to1)
                End With
                Try
                    comm.ExecuteNonQuery()
                Catch ex As Exception
                    sw.WriteLine("*** ERRORE SU update EsigenzePostDemo1to1 " & DateTime.Now)
                    sw.WriteLine(ex.Message.ToString())
                    sw.Close()
                End Try
                comm.Dispose()
                myConn2.Close()
            End Using
            '''''''''''''''''''''''''''''''''''''''''


            'sTabellaInteressi = "<table>"

            If (sCommercialeOLD <> sCommerciale) And y > 0 Then
                'Invio la mail al commerciale sCommercialeOLD

                MailContattoAgg = ""

                MailContattoAgg = ""
                strTest = ""
                MailContatto = sMailCommercialeOLD

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strTest.ToUpper().Trim().ToString() = "S" Then

                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                    MailContatto = strDest 'TEST

                End If

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                strOggetto = ""
                testoHtml = testoHtmlBCK

                strOggetto = strOggettoBCK

                sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Try
                    Inviamail(strOggetto.Replace("#data", sDataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD), sDataDemoOLD, "", Nothing)

                    sw.WriteLine("Mail inviata a commerciale " & MailContatto.ToString())



                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                            .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                            .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                            .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                            .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                            .Parameters.AddWithValue("@DataDemo", sDataDemo)
                            .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                            .Parameters.AddWithValue("@TabellaEsigenza", sTabella)


                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                        comm.Dispose()
                        myConn2.Close()
                    End Using

                Catch ex As Exception
                    sw.WriteLine("Errore Invio mail a commerciali " & sMailCommercialeOLD.ToString())

                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                    myConn2 = New SqlConnection(strConnectionDB)
                    myConn2.Open()
                    Using comm As New SqlCommand()
                        With comm
                            .Connection = myConn2
                            .CommandType = CommandType.Text
                            .CommandText = query
                            Dim strDestinatario As String
                            If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                                strDestinatario = MailContatto & " [TEST]"
                            Else
                                strDestinatario = MailContatto
                            End If
                            .Parameters.AddWithValue("@Dest", strDestinatario)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                            .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                            .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                            .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                            .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                            .Parameters.AddWithValue("@DataDemo", sDataDemo)
                            .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                            .Parameters.AddWithValue("@TabellaEsigenza", sTabella)
                            .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                        End With
                        Try
                            comm.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using

                End Try

                IDEsigenzePostDemo1to1OLD = IDEsigenzePostDemo1to1
                sCommercialeOLD = sCommerciale
                IdCommercialeOLD = IdCommerciale
                sIdKeyOLD = sIdKey
                sTabellaOLD = sTabella
                sTabellaDemoOLD = sTabellaDemo
                sInteresseEspressoOLD = sInteresseEspresso
                sAltroReferenteOLD = sAltroReferente
                sAltroEmailOLD = sAltroEmail
                sNoteOLD = sNote
                sRiferimentoOLD = sRiferimento
                sDataDemoOLD = sDataDemo
                sAziendaClienteOLD = sAziendaCliente
                testoHtmlOLD = testoHtml
                sTabellaInteressiOLD = sTabellaInteressi
                IDRichiestaCOP = IDRichiestaCOPOLD
                GUIDRichiestaCOP = GUIDRichiestaCOPold
                sIdKeyDemo = sIdKeyDemoOLD
                sMailCommercialeOLD = sMailCommerciale
                DescrizionePresaleOLD = DescrizionePresale
                sTipologiaOLD = sTipologia
                sTabellaInteressi = "<p style=""font-size: 12px; line-height: 30px; text-align: left; margin: 0;""><table style=""width:100%;margin:0 auto"" border=""1"" bgcolor=""#C8EBFA""><thead><th style=""padding:5px;text-align: left"">Tipologia</th><th style=""padding:5px;text-align: left"">Interesse Espresso</th><th style=""padding:5px;text-align: left"">Azienda</th><th style=""padding:5px;text-align: left"">Presale Segnalatore</th></tr></thead><tbody>"


            End If

            sCommercialeOLD = sCommerciale
            sMailCommercialeOLD = sMailCommerciale

            IDEsigenzePostDemo1to1OLD = IDEsigenzePostDemo1to1
            IdCommercialeOLD = IdCommerciale
            sIdKeyOLD = sIdKey
            sTabellaOLD = sTabella
            sTabellaDemoOLD = sTabellaDemo
            sInteresseEspressoOLD = sInteresseEspresso
            sAltroReferenteOLD = sAltroReferente
            sAltroEmailOLD = sAltroEmail
            sNoteOLD = sNote
            sRiferimentoOLD = sRiferimento
            sDataDemoOLD = sDataDemo
            sAziendaClienteOLD = sAziendaCliente
            testoHtmlOLD = testoHtml
            sTabellaInteressiOLD = sTabellaInteressi
            IDRichiestaCOPOLD = IDRichiestaCOP
            GUIDRichiestaCOPold = GUIDRichiestaCOP
            sIdKeyDemoOLD = sIdKeyDemo
            DescrizionePresaleOLD = DescrizionePresale
            sTipologiaOLD = sTipologia

            sTabellaInteressi = sTabellaInteressi + "<tr>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sAziendaCliente & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sTipologia & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sInteresseEspresso & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & sAziendaCliente & "</td>"
            sTabellaInteressi = sTabellaInteressi + "<td style=""padding:5px;text-align: left; "">" & DescrizionePresale & "</td>"
            sTabellaInteressi = sTabellaInteressi + "</tr>"

            y = y + 1

        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()
        sTabellaInteressi = sTabellaInteressi + "</tbody></table></p><br/>"


        '''''''''''''''''''''''''''''''''''''''''
        MailContattoAgg = ""

        MailContattoAgg = ""
        strTest = ""
        MailContatto = sMailCommercialeOLD

        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

        If strTest.ToUpper().Trim().ToString() = "S" Then

            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

            MailContatto = strDest 'TEST

        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()
        myCmd4 = myConn2.CreateCommand
        myCmd4.CommandText = "Select Nominativo,Mail from Anagrafica_Commerciali where ID=@IdCommerciale"
        myCmd4.Parameters.AddWithValue("@IdCommerciale", IdCommercialeOLD)

        myReader4 = myCmd4.ExecuteReader()

        Do While myReader4.Read()
            sMailCommerciale = myReader4.Item("Mail")
        Loop

        myReader4.Close()
        myCmd4.Connection.Close()
        myCmd4.Dispose()
        myConn2.Close()

        strOggetto = ""
        testoHtml = testoHtmlBCK

        strOggetto = strOggettoBCK

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If MailContatto <> "" And sCommercialeOLD <> "" Then

            Try
                Inviamail(strOggetto.Replace("#data", sDataDemoOLD), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", "#presaledemo").Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD), sDataDemoOLD, "", Nothing)

                sw.WriteLine("Mail inviata a commerciale " & MailContatto.ToString())



                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                        .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                        .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                        .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                        .Parameters.AddWithValue("@DataDemo", sDataDemo)
                        .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                        .Parameters.AddWithValue("@TabellaEsigenza", sTabella)
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                    comm.Dispose()
                    myConn2.Close()
                End Using

            Catch ex As Exception
                sw.WriteLine("Errore Invio mail a commerciali " & sMailCommercialeOLD.ToString())

                Dim query As String
                query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML,IdKey,Tabella,DataDemo,IdKeyEsigenza,TabellaEsigenza) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IdKeyEsigenza,@TabellaEsigenza)"
                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        Dim strDestinatario As String
                        If strTest.ToUpper().Trim().ToString() = "S" Or sInviaMail.ToUpper().Trim().ToString() = "N" Then
                            strDestinatario = MailContatto & " [TEST]"
                        Else
                            strDestinatario = MailContatto
                        End If
                        .Parameters.AddWithValue("@Dest", strDestinatario)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL A COMMERCIALE " & sMailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", GUIDRichiestaCOP)
                        .Parameters.AddWithValue("@IDRichiesta", IDRichiestaCOP)

                        .Parameters.AddWithValue("@IdKey", sIdKeyDemo)
                        .Parameters.AddWithValue("@Tabella", sTabellaDemo)
                        .Parameters.AddWithValue("@DataDemo", sDataDemo)
                        .Parameters.AddWithValue("@IdKeyEsigenza", sIdKey)
                        .Parameters.AddWithValue("@TabellaEsigenza", sTabella)
                        .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#data", sDataDemoOLD).Replace("#commerciale", sCommercialeOLD).Replace("#presaledemo", DescrizionePresale).Replace("#listainteressi", sTabellaInteressi).Replace("#presale", DescrizionePresaleOLD))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex1 As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using

            End Try
        End If

        sw.WriteLine("Fine " & Now().ToString)
        sw.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Function






    Private Sub SvuotaDir(ByVal cartella As String)
        Dim di As IO.DirectoryInfo =
            New IO.DirectoryInfo(cartella)
        For Each oFile As IO.FileInfo In di.GetFiles()
            'MsgBox(oFile.Name)
            oFile.Delete()
        Next
    End Sub

    Private Function CreaFilesReportAreaManager(idAM As Integer) As Boolean

        Dim param1 As String
        Dim param2 As String
        Dim param3 As String
        Dim param4 As String
        Dim strlinkHomePage As String
        Dim strPathGrafici As String

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono gli area manager
        'strSQL = "Select IDUtenteAreaManager, IDUtentePartner FROM AreaManager_Partner Where IDUtenteAreaManager = " & idAM
        strSQL = "Select IDUtenteAreaManager, IDUtentePartner, ac.ID IdCommerciale, u.GUID FROM AreaManager_Partner  t1 join Users u On u.id=t1.IDUtentePartner join Anagrafica_Commerciali ac On ac.Mail=u.Email Where IDUtenteAreaManager =  " & idAM

        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn1.Open()

        Dim myReader1 As SqlDataReader
        Dim IDAM2 As Integer
        Dim IDP2 As Integer
        Dim IDCommerciale_Partner As Integer
        Dim GUID_Partner As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            IDAM2 = myReader1.Item("IDUtenteAreaManager")
            IDP2 = myReader1.Item("IDUtentePartner")
            IDCommerciale_Partner = myReader1.Item("IdCommerciale")
            GUID_Partner = myReader1.Item("GUID").ToString()

            Dim data1 As Date
            Dim data2 As Date

            'data2 = DateTime.Now.AddDays(-(DateTime.Now.Day))
            'data1 = data2.AddDays(-data2.Day + 1)
            'data1 = DateTime.Now.AddDays(-(DateTime.Now.Day) + 1)
            'data1 = data1.AddYears(-1)

            data2 = DateTime.Now.AddDays(-1)
            data1 = data2.AddYears(-1)
            data1 = data1.AddDays(-data1.Day)
            data1 = data1.AddDays(1)

            data1 = FormatDateTime(data1, DateFormat.ShortDate)
            data2 = FormatDateTime(data2, DateFormat.ShortDate)

            'Dim iData1 As String = DateTime.Parse(data1.ToString).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
            'Dim iData2 As String = DateTime.Parse(data2.ToString).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim iData1 As String = DateTime.Parse(data1.ToString).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)
            Dim iData2 As String = DateTime.Parse(data2.ToString).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)

            iData1 = data1.ToShortDateString()
            iData2 = data2.ToShortDateString()

            'MsgBox(iData1)
            'MsgBox(iData2)

            param1 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(IDAM2.ToString())
            param2 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(IDP2.ToString())
            param3 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(iData1)
            param4 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(iData2)

            strPathGrafici = System.Configuration.ConfigurationManager.AppSettings("pathReportAreaManager").ToString()

            strlinkHomePage = System.Configuration.ConfigurationManager.AppSettings("linkHomePage").ToString()
            strlinkHomePage = strlinkHomePage & "/Reminder/ReportAreaManager?param1=" & param1 & "&param2=" & param2 & "&param3=" & param3 & "&param4=" & param4

            'Process.Start(strlinkHomePage)

            Dim browser As String = String.Empty
            Dim key As RegistryKey = Nothing

            Try
                key = Registry.ClassesRoot.OpenSubKey("HTTP\shell\open\command")
                If Not key Is Nothing Then

                    ' Get default Browser
                    browser = key.GetValue(Nothing).ToString().ToLower().Trim(New Char() {""""c})
                End If
                If Not browser.EndsWith("exe") Then
                    'Remove all after the ".exe"
                    browser = browser.Substring(0, browser.LastIndexOf(".exe", StringComparison.InvariantCultureIgnoreCase) + 4)
                End If
            Finally
                If Not key Is Nothing Then
                    key.Close()
                End If
            End Try

            'Open the browser.
            Dim proc As Process = Process.Start(browser, strlinkHomePage)
            If Not proc Is Nothing Then
                Threading.Thread.Sleep(5000)
                'Close the browser.
                Try
                    'proc.Kill()
                    listID.Add(proc.Id)
                Catch ex As Exception
                    'MsgBox(Err.Description)
                End Try

            End If

            Threading.Thread.Sleep(10000)



        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Threading.Thread.Sleep(10000)

        IDP2 = 0
        param2 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(IDP2.ToString())
        strlinkHomePage = System.Configuration.ConfigurationManager.AppSettings("linkHomePage").ToString()
        strlinkHomePage = strlinkHomePage & "/Reminder/ReportAreaManager?param1=" & param1 & "&param2=" & param2 & "&param3=" & param3 & "&param4=" & param4

        Dim browser2 As String = String.Empty
        Dim key2 As RegistryKey = Nothing

        Try
            key2 = Registry.ClassesRoot.OpenSubKey("HTTP\shell\open\command")
            If Not key2 Is Nothing Then

                ' Get default Browser
                browser2 = key2.GetValue(Nothing).ToString().ToLower().Trim(New Char() {""""c})
            End If
            If Not browser2.EndsWith("exe") Then
                'Remove all after the ".exe"
                browser2 = browser2.Substring(0, browser2.LastIndexOf(".exe", StringComparison.InvariantCultureIgnoreCase) + 4)
            End If
        Finally
            If Not key2 Is Nothing Then
                key2.Close()
            End If
        End Try

        'Open the browser.
        Dim proc2 As Process = Process.Start(browser2, strlinkHomePage)
        If Not proc2 Is Nothing Then
            Threading.Thread.Sleep(1000)
            'Close the browser.
            Try
                'proc.Kill()
                listID.Add(proc2.Id)
            Catch ex As Exception
                'MsgBox(Err.Description)
            End Try

        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Threading.Thread.Sleep(10000)

    End Function

    Private Function CreaFilesReportCoordinatoreAM(idCoordAM As Integer) As Boolean

        Dim param1 As String
        Dim param2 As String
        Dim param3 As String
        Dim param4 As String
        Dim strlinkHomePage As String
        Dim strPathGrafici As String

        Dim myConn1 As SqlConnection
        myConn1 = New SqlConnection(strConnectionDB)
        Dim myConn2 As SqlConnection
        myConn2 = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'seleziono gli area manager

        'strSQL = "Select IDUtenteCoordinatoreAM, IDUtenteAreaManager, ac.ID IdCommerciale, u.GUID FROM Coordinatori_AreaManager  t1 join Users u On u.id=t1.IDUtenteAreaManager join Anagrafica_Commerciali ac On ac.Mail=u.Email Where IDUtenteCoordinatoreAM =  " & idCoordAM
        strSQL = "Select IDUtenteCoordinatoreAM, t1.IDUtenteAreaManager, ac.ID IdCommerciale, u.GUID, t2.IDUtentePartner FROM Coordinatori_AreaManager  t1
        Join AreaManager_Partner t2 on t1.IDUtenteAreaManager=t2.IDUtenteAreaManager
        Join Users u On u.id=t2.IDUtentePartner
        /*Join Users u On u.id=t1.IDUtenteAreaManager*/
        Join Anagrafica_Commerciali ac On ac.Mail=u.Email 
        Where IDUtenteCoordinatoreAM = " & idCoordAM

        myCmd = myConn1.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 240
        myConn1.Open()

        Dim myReader1 As SqlDataReader
        Dim IDAM2 As Integer
        Dim IDP2 As Integer
        Dim IDCommerciale_Partner As Integer
        Dim GUID_Partner As String

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            IDAM2 = myReader1.Item("IDUtenteCoordinatoreAM")
            'IDP2 = myReader1.Item("IDUtenteAreaManager")
            IDP2 = myReader1.Item("IDUtentePartner")
            IDCommerciale_Partner = myReader1.Item("IdCommerciale")
            GUID_Partner = myReader1.Item("GUID").ToString()

            Dim data1 As Date
            Dim data2 As Date

            'data2 = DateTime.Now.AddDays(-(DateTime.Now.Day))
            'data1 = data2.AddDays(-data2.Day + 1)
            'data1 = DateTime.Now.AddDays(-(DateTime.Now.Day) + 1)
            'data1 = data1.AddYears(-1)

            data2 = DateTime.Now.AddDays(-1)
            data1 = data2.AddYears(-1)
            data1 = data1.AddDays(-data1.Day)
            data1 = data1.AddDays(1)

            data1 = FormatDateTime(data1, DateFormat.ShortDate)
            data2 = FormatDateTime(data2, DateFormat.ShortDate)

            Dim iData1 As String = DateTime.Parse(data1.ToString).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim iData2 As String = DateTime.Parse(data2.ToString).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)

            iData1 = data1.ToShortDateString()
            iData2 = data2.ToShortDateString()

            param1 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(IDAM2.ToString())
            param2 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(IDP2.ToString())
            param3 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(iData1)
            param4 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(iData2)

            strPathGrafici = System.Configuration.ConfigurationManager.AppSettings("pathReportAreaManager").ToString()

            strlinkHomePage = System.Configuration.ConfigurationManager.AppSettings("linkHomePage").ToString()
            strlinkHomePage = strlinkHomePage & "/Reminder/ReportAreaManager?param1=" & param1 & "&param2=" & param2 & "&param3=" & param3 & "&param4=" & param4

            'Process.Start(strlinkHomePage)

            Dim browser As String = String.Empty
            Dim key As RegistryKey = Nothing

            Try
                key = Registry.ClassesRoot.OpenSubKey("HTTP\shell\open\command")
                If Not key Is Nothing Then

                    ' Get default Browser
                    browser = key.GetValue(Nothing).ToString().ToLower().Trim(New Char() {""""c})
                End If
                If Not browser.EndsWith("exe") Then
                    'Remove all after the ".exe"
                    browser = browser.Substring(0, browser.LastIndexOf(".exe", StringComparison.InvariantCultureIgnoreCase) + 4)
                End If
            Finally
                If Not key Is Nothing Then
                    key.Close()
                End If
            End Try

            'Open the browser.
            Dim proc As Process = Process.Start(browser, strlinkHomePage)
            If Not proc Is Nothing Then
                Threading.Thread.Sleep(1000)
                'Close the browser.
                Try
                    'proc.Kill()
                    listID.Add(proc.Id)
                Catch ex As Exception
                    'MsgBox(Err.Description)
                End Try

            End If

            Threading.Thread.Sleep(2500)



        Loop
        myReader1.Close()
        myConn1.Close()
        myConn2.Close()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Threading.Thread.Sleep(1000)

        IDP2 = 0
        param2 = CifraturaDecifratura.CifraturaDecifratura.EncryptX(IDP2.ToString())
        strlinkHomePage = System.Configuration.ConfigurationManager.AppSettings("linkHomePage").ToString()
        strlinkHomePage = strlinkHomePage & "/Reminder/ReportAreaManager?param1=" & param1 & "&param2=" & param2 & "&param3=" & param3 & "&param4=" & param4

        Dim browser2 As String = String.Empty
        Dim key2 As RegistryKey = Nothing

        Try
            key2 = Registry.ClassesRoot.OpenSubKey("HTTP\shell\open\command")
            If Not key2 Is Nothing Then

                ' Get default Browser
                browser2 = key2.GetValue(Nothing).ToString().ToLower().Trim(New Char() {""""c})
            End If
            If Not browser2.EndsWith("exe") Then
                'Remove all after the ".exe"
                browser2 = browser2.Substring(0, browser2.LastIndexOf(".exe", StringComparison.InvariantCultureIgnoreCase) + 4)
            End If
        Finally
            If Not key2 Is Nothing Then
                key2.Close()
            End If
        End Try

        'Open the browser.
        Dim proc2 As Process = Process.Start(browser2, strlinkHomePage)
        If Not proc2 Is Nothing Then
            Threading.Thread.Sleep(1000)
            'Close the browser.
            Try
                'proc.Kill()
                listID.Add(proc2.Id)
            Catch ex As Exception
                'MsgBox(Err.Description)
            End Try

        End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Threading.Thread.Sleep(10000)

    End Function

    Private Function InviaMailReportCoordinatoreAM(idCoordAM As Integer) As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoAggOLD As String
        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim GUIDAM As String
        Dim NomeAM As String
        Dim mailAM As String

        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        MailContattoAggOLD = ""

        myConn5 = New SqlConnection(strConnectionDB) 'Create a Connection object.
        myConn5.Open()
        Dim myReaderFile1 As SqlDataReader
        Dim myCmdFile1 As New SqlCommand

        myCmdFile1 = myConn5.CreateCommand
        myCmdFile1.CommandText = "Select GUID, Descrizione, Email from Users WHERE ID =" & idCoordAM & ""

        Try
            myReaderFile1 = myCmdFile1.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message.ToString())
        End Try


        Do While myReaderFile1.Read()
            GUIDAM = myReaderFile1.Item("GUID")
            NomeAM = myReaderFile1.Item("Descrizione")
            mailAM = myReaderFile1.Item("Email")
        Loop

        myReaderFile1.Close()
        myConn5.Close()

        Dim formattedDate As String
        Dim formattedYear As String
        Dim formattedDateStart As String
        Dim formattedDateEnd As String
        Dim myDate As Date

        formattedDateStart = ""
        formattedDateEnd = ""

        myDate = DateTime.Now
        formattedDate = Format$(myDate, "MM")
        formattedYear = Format$(myDate, "yyyy").ToString()

        If formattedDate.Equals("01") Then
            formattedDate = "12"
            formattedYear = (Format$(myDate, "yyyy") - 1).ToString()
        Else
            If formattedDate.Equals("02") Then formattedDate = "01"
            If formattedDate.Equals("03") Then formattedDate = "02"
            If formattedDate.Equals("04") Then formattedDate = "03"
            If formattedDate.Equals("05") Then formattedDate = "04"
            If formattedDate.Equals("06") Then formattedDate = "05"
            If formattedDate.Equals("07") Then formattedDate = "06"
            If formattedDate.Equals("08") Then formattedDate = "07"
            If formattedDate.Equals("09") Then formattedDate = "08"
            If formattedDate.Equals("10") Then formattedDate = "09"
            If formattedDate.Equals("11") Then formattedDate = "10"
            If formattedDate.Equals("12") Then formattedDate = "11"
        End If

        If formattedDate.Equals("01") Or formattedDate.Equals("03") Or formattedDate.Equals("05") Or formattedDate.Equals("07") Or formattedDate.Equals("08") Or formattedDate.Equals("10") Or formattedDate.Equals("12") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "31/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("02") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "28/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("04") Or formattedDate.Equals("06") Or formattedDate.Equals("09") Or formattedDate.Equals("11") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "30/" & formattedDate & "/" & formattedYear
        End If

        'formattedDateStart = "01/01/2021"
        'formattedDateEnd = "31/07/2021"

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail riepilogo mensile al commerciale - " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioReportAM'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioReportAM'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String
        Dim strElencoDemoPM As String
        Dim strElencoDemo1to1PM As String
        Dim strElencoaziendenopartecipato As String

        retID = 0
        strElencoDemoPM = ""
        strElencoDemo1to1PM = ""
        strElencoaziendenopartecipato = ""

        retID = idCoordAM

        Dim data1 As Date
        Dim data2 As Date

        data2 = DateTime.Now.AddDays(-(DateTime.Now.Day))
        data1 = data2.AddDays(-data2.Day + 1)
        data1 = DateTime.Now.AddDays(-(DateTime.Now.Day) + 1)
        data1 = data1.AddYears(-1)

        data1 = FormatDateTime(data1, DateFormat.ShortDate)
        data2 = FormatDateTime(data2, DateFormat.ShortDate)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim strPathGrafici As String


        strPathGrafici = System.Configuration.ConfigurationManager.AppSettings("pathReportAreaManager").ToString()

        ''''''''''''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''''''''''''''''''''''''''''''''


        Dim strHTML As String
        Dim NomePartner As String
        Dim sIndexSale As String
        Dim sTabellaHtml As String
        strHTML = ""
        NomePartner = ""

        If Not System.IO.Directory.Exists(strPathGrafici) Then
            System.IO.Directory.CreateDirectory(strPathGrafici)
        End If

        If System.IO.File.Exists(strPathGrafici & "\" & GUIDAM & ".txt") Then
            'System.IO.Directory.CreateDirectory(strPathGrafici)


            Dim array As String() = File.ReadAllLines(strPathGrafici & "\" & GUIDAM & ".txt")

            Dim linea As String
            Dim Y As Integer
            Y = 0
            For Each linea In array
                Dim linea2 As String = linea.ToString()

                Dim strlinea3 As String
                Dim j As Integer
                strlinea3 = ""
                j = 0
                Dim Str As String = linea2
                Dim strarr() As String
                strarr = Str.Split("|")
                For Each s As String In strarr
                    If j = 0 Then
                        strlinea3 = s
                    End If
                    If j = 1 Then
                        NomePartner = s
                    End If
                    If j = 2 Then
                        sIndexSale = s
                    End If
                    If j = 3 Then
                        sTabellaHtml = s
                    End If
                    j = j + 1
                Next

                If Y <> 0 Then
                    strHTML = strHTML & "<br><br><hr><br><b>" & NomePartner & "</b>"
                Else
                    strHTML = strHTML & "<b>" & NomePartner & "</b>"
                End If
                If NomePartner <> "VALORI TOTALI AGGREGATI" Then
                    strHTML = strHTML & " - " & "DemoDriver Index Sales = " & sIndexSale & "<br>"
                End If
                strHTML = strHTML & "<br><img src = " & strlinea3 & " alt='" & NomePartner & "' />"

                strHTML = strHTML & "<br><br>" & "<div style='text-align:center'>" & sTabellaHtml & "</div>"

                Y = Y + 1

            Next

        End If

        Threading.Thread.Sleep(1000)

        'Dim strHTML As String

        'Dim myDir As DirectoryInfo = New DirectoryInfo(strPathGrafici)
        'If (myDir.EnumerateFiles().Any()) Then

        '    Dim files() As String = IO.Directory.GetFiles(strPathGrafici)

        '    For Each file1 As String In files

        '        Dim myReaderFile As SqlDataReader
        '        Dim myCmdFile As New SqlCommand
        '        Dim sNomeFile As String
        '        Dim NomeAM As String
        '        NomeAM = ""

        '        Dim strarr() As String
        '        strarr = file1.Split("\")
        '        For Each s As String In strarr
        '            'MsgBox(s)
        '            sNomeFile = s.Replace(".txt", "")
        '            sNomeFile = s.Replace(".TXT", "")

        '        Next

        '        sNomeFile = sNomeFile.Replace(".txt", "")

        '        myCmdFile = myConn.CreateCommand
        '        myCmdFile.CommandText = "Select Descrizione from Users where GUID='" & sNomeFile & "'"

        '        myReaderFile = myCmdFile.ExecuteReader()

        '        Do While myReaderFile.Read()
        '            NomeAM = myReaderFile.Item("Descrizione")
        '        Loop

        '        myReaderFile.Close()


        '        Dim array As String() = File.ReadAllLines(file1)



        '        Dim linea As String

        '        strHTML = strHTML & " <p>" & NomeAM & "</p>"

        '        For Each linea In array
        '            Dim linea2 As String = linea.ToString()

        '            strHTML = strHTML & "<img src = " & linea2 & " alt='" & NomeAM & "' />"
        '        Next




        '    Next

        'End If

        testoHtml = testoHtml.Replace("#grafici_area_manager", strHTML)
        testoHtml = testoHtml.Replace("#areamanager", NomeAM)
        testoHtml = testoHtml.Replace("REPORT AREA MANAGER", "REPORT COORDINATORI DI AREA MANAGER")

        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String
        Dim MailContatto As String

        strDest = ""
        strDest2 = ""
        strDest3 = ""
        strDest4 = ""

        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

        '''**************************************************************'
        '''*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
        '''**************************************************************' .Replace("#testo", testo)
        MailContattoAgg = ""

        MailContatto = mailAM

        If strTest.ToUpper().Trim().ToString() = "S" Then

            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

            MailContatto = strDest 'TEST

        End If


        ''Dim sPartner As String
        ''Dim sAnnoMese As String
        ''Dim sConfInterne As String
        ''Dim sConfEsterne As String
        ''Dim sNumDemo1to1 As String
        ''Dim sNumPartecipantiWeekly As String


        MailContattoAgg = ""
        Try
            'testoHtml = testoHtml.Replace("#annonumerodemoweekly", myReaderGEN.Item("NumWeeklyAnno"))

            strOggetto = strOggetto & " " & NomeAM
            strOggetto = strOggetto.Replace("Statistiche per Area Manager", "Statistiche per Coordinatore Area Manager")

            Inviamail(strOggetto, "", MailContatto, MailContattoAgg, Nothing, testoHtml, "", "", Nothing)

            sw.WriteLine("Invio email per area manager a " & MailContatto & " " & DateTime.Now)

            Dim query As String
            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
            myConn3 = New SqlConnection(strConnectionDB)
            myConn3.Open()
            Using commins As New SqlCommand()
                With commins
                    .Connection = myConn3
                    .CommandType = CommandType.Text
                    .CommandText = query
                    .Parameters.AddWithValue("@Dest", MailContatto)
                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                    .Parameters.AddWithValue("@Oggetto", strOggetto)
                    .Parameters.AddWithValue("@Esito", "OK")
                    .Parameters.AddWithValue("@Note", "INVIO MAIL STATISTICA AREA MANAGER  " & MailContatto)
                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                    .Parameters.AddWithValue("@TestoHTML", testoHtml)
                End With
                Try
                    commins.ExecuteNonQuery()
                Catch ex As Exception
                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                End Try
            End Using
            myConn3.Close()


        Catch ex As Exception
            sw.WriteLine("*** INVIO MAIL STATISTICA AREA MANAGER A  " & MailContatto & " " & DateTime.Now)

            Dim query As String
            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
            'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
            myConn3 = New SqlConnection(strConnectionDB)
            myConn3.Open()
            Using commins As New SqlCommand()
                With commins
                    .Connection = myConn3
                    .CommandType = CommandType.Text
                    .CommandText = query
                    .Parameters.AddWithValue("@Dest", MailContatto)
                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                    .Parameters.AddWithValue("@Oggetto", strOggetto)
                    .Parameters.AddWithValue("@Esito", "ERR")
                    .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto)
                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                    .Parameters.AddWithValue("@TestoHTML", testoHtml)
                End With
                Try
                    commins.ExecuteNonQuery()
                Catch ex1 As Exception
                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                End Try
            End Using
            myConn3.Close()


        End Try

        '**************************************************************'
        '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
        '**************************************************************'
        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine invio mail riepilogo Area Manager - " & idCoordAM & " " & DateTime.Now)
        sw.Close()

        'Dim directoryName As String = strPathGrafici
        'For Each deleteFile In Directory.GetFiles(directoryName, "*.*", IO.SearchOption.TopDirectoryOnly)
        '    File.Delete(deleteFile)
        'Next

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailReportCoordinatoreAM = True

        Exit Function

    End Function



    Private Function InviaMailReportAreaManager(idAM As Integer) As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoAggOLD As String
        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim GUIDAM As String
        Dim NomeAM As String
        Dim mailAM As String

        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        MailContattoAggOLD = ""

        myConn5 = New SqlConnection(strConnectionDB) 'Create a Connection object.
        myConn5.Open()
        Dim myReaderFile1 As SqlDataReader
        Dim myCmdFile1 As New SqlCommand

        myCmdFile1 = myConn5.CreateCommand
        myCmdFile1.CommandText = "Select GUID, Descrizione, Email from Users WHERE ID =" & idAM & ""

        Try
            myReaderFile1 = myCmdFile1.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.Message.ToString())
        End Try


        Do While myReaderFile1.Read()
            GUIDAM = myReaderFile1.Item("GUID")
            NomeAM = myReaderFile1.Item("Descrizione")
            mailAM = myReaderFile1.Item("Email")
        Loop

        myReaderFile1.Close()
        myConn5.Close()

        Dim formattedDate As String
        Dim formattedYear As String
        Dim formattedDateStart As String
        Dim formattedDateEnd As String
        Dim myDate As Date

        formattedDateStart = ""
        formattedDateEnd = ""

        myDate = DateTime.Now
        formattedDate = Format$(myDate, "MM")
        formattedYear = Format$(myDate, "yyyy").ToString()

        If formattedDate.Equals("01") Then
            formattedDate = "12"
            formattedYear = (Format$(myDate, "yyyy") - 1).ToString()
        Else
            If formattedDate.Equals("02") Then formattedDate = "01"
            If formattedDate.Equals("03") Then formattedDate = "02"
            If formattedDate.Equals("04") Then formattedDate = "03"
            If formattedDate.Equals("05") Then formattedDate = "04"
            If formattedDate.Equals("06") Then formattedDate = "05"
            If formattedDate.Equals("07") Then formattedDate = "06"
            If formattedDate.Equals("08") Then formattedDate = "07"
            If formattedDate.Equals("09") Then formattedDate = "08"
            If formattedDate.Equals("10") Then formattedDate = "09"
            If formattedDate.Equals("11") Then formattedDate = "10"
            If formattedDate.Equals("12") Then formattedDate = "11"
        End If

        If formattedDate.Equals("01") Or formattedDate.Equals("03") Or formattedDate.Equals("05") Or formattedDate.Equals("07") Or formattedDate.Equals("08") Or formattedDate.Equals("10") Or formattedDate.Equals("12") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "31/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("02") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "28/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("04") Or formattedDate.Equals("06") Or formattedDate.Equals("09") Or formattedDate.Equals("11") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "30/" & formattedDate & "/" & formattedYear
        End If

        'formattedDateStart = "01/01/2021"
        'formattedDateEnd = "31/07/2021"

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail riepilogo mensile al commerciale - " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioReportAM'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioReportAM'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String
        Dim strElencoDemoPM As String
        Dim strElencoDemo1to1PM As String
        Dim strElencoaziendenopartecipato As String

        retID = 0
        strElencoDemoPM = ""
        strElencoDemo1to1PM = ""
        strElencoaziendenopartecipato = ""

        retID = idAM

        Dim data1 As Date
        Dim data2 As Date

        data2 = DateTime.Now.AddDays(-(DateTime.Now.Day))
        data1 = data2.AddDays(-data2.Day + 1)
        data1 = DateTime.Now.AddDays(-(DateTime.Now.Day) + 1)
        data1 = data1.AddYears(-1)

        data1 = FormatDateTime(data1, DateFormat.ShortDate)
        data2 = FormatDateTime(data2, DateFormat.ShortDate)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim strPathGrafici As String


        strPathGrafici = System.Configuration.ConfigurationManager.AppSettings("pathReportAreaManager").ToString()

        ''''''''''''''''''''''''''''''''''''''''''''''''

        '''''''''''''''''''''''''''''''''''''''''''''''


        Dim strHTML As String
        Dim NomePartner As String
        Dim sIndexSale As String
        strHTML = ""
        NomePartner = ""

        If Not System.IO.Directory.Exists(strPathGrafici) Then
            System.IO.Directory.CreateDirectory(strPathGrafici)
        End If

        If System.IO.File.Exists(strPathGrafici & "\" & GUIDAM & ".txt") Then
            'System.IO.Directory.CreateDirectory(strPathGrafici)


            Dim array As String() = File.ReadAllLines(strPathGrafici & "\" & GUIDAM & ".txt")

            Dim linea As String
            Dim Y As Integer
            Y = 0
            For Each linea In array
                Dim linea2 As String = linea.ToString()

                Dim strlinea3 As String
                Dim j As Integer
                strlinea3 = ""
                j = 0
                Dim Str As String = linea2
                Dim strarr() As String
                strarr = Str.Split("|")
                For Each s As String In strarr
                    If j = 0 Then
                        strlinea3 = s
                    End If
                    If j = 1 Then
                        NomePartner = s
                    End If
                    If j = 2 Then
                        sIndexSale = s
                    End If
                    j = j + 1
                Next

                If Y <> 0 Then
                    strHTML = strHTML & "<br><br><br><br><hr><br><b>" & NomePartner & "</b>"
                Else
                    strHTML = strHTML & "<b>" & NomePartner & "</b>"
                End If
                If NomePartner <> "VALORI TOTALI AGGREGATI" Then
                    strHTML = strHTML & " - " & "DemoDriver Index Sales = " & sIndexSale & "<br>"
                End If
                strHTML = strHTML & "<br><img src = " & strlinea3 & " alt='" & NomePartner & "' />"
                Y = Y + 1

            Next

        End If

        Threading.Thread.Sleep(1000)

        'Dim strHTML As String

        'Dim myDir As DirectoryInfo = New DirectoryInfo(strPathGrafici)
        'If (myDir.EnumerateFiles().Any()) Then

        '    Dim files() As String = IO.Directory.GetFiles(strPathGrafici)

        '    For Each file1 As String In files

        '        Dim myReaderFile As SqlDataReader
        '        Dim myCmdFile As New SqlCommand
        '        Dim sNomeFile As String
        '        Dim NomeAM As String
        '        NomeAM = ""

        '        Dim strarr() As String
        '        strarr = file1.Split("\")
        '        For Each s As String In strarr
        '            'MsgBox(s)
        '            sNomeFile = s.Replace(".txt", "")
        '            sNomeFile = s.Replace(".TXT", "")

        '        Next

        '        sNomeFile = sNomeFile.Replace(".txt", "")

        '        myCmdFile = myConn.CreateCommand
        '        myCmdFile.CommandText = "Select Descrizione from Users where GUID='" & sNomeFile & "'"

        '        myReaderFile = myCmdFile.ExecuteReader()

        '        Do While myReaderFile.Read()
        '            NomeAM = myReaderFile.Item("Descrizione")
        '        Loop

        '        myReaderFile.Close()


        '        Dim array As String() = File.ReadAllLines(file1)



        '        Dim linea As String

        '        strHTML = strHTML & " <p>" & NomeAM & "</p>"

        '        For Each linea In array
        '            Dim linea2 As String = linea.ToString()

        '            strHTML = strHTML & "<img src = " & linea2 & " alt='" & NomeAM & "' />"
        '        Next




        '    Next

        'End If

        testoHtml = testoHtml.Replace("#grafici_area_manager", strHTML)
        testoHtml = testoHtml.Replace("#areamanager", NomeAM)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        'strSQLgen = "set dateformat dmy Exec spSTAT_RiepilogoAreaManager @idam, @idp, @data1, @data2"

        'myCmdGEN = myConn2.CreateCommand
        'myCmdGEN.CommandText = strSQLgen
        'myCmdGEN.Parameters.AddWithValue("@idam", CInt(idAM))
        ''myCmdGEN.Parameters.AddWithValue("@idp", CInt(idP))
        'myCmdGEN.Parameters.AddWithValue("@data1", data1)
        'myCmdGEN.Parameters.AddWithValue("@data2", data2)

        'sw.WriteLine("Inizio invio mail riepilogo Area Manager - " & idAM)

        'Dim sAreaManager As String
        'Dim sEmailAM As String
        'Dim sCoordAM As String
        'Dim sEmailCoordAM As String

        'myReaderGEN = Nothing
        'myReaderGEN = myCmdGEN.ExecuteReader()
        'If myReaderGEN.HasRows Then
        '    Do While myReaderGEN.Read

        '        sAreaManager = myReaderGEN.Item("AreaManager")
        '        sEmailAM = myReaderGEN.Item("EmailAM")
        '        sCoordAM = myReaderGEN.Item("CoordAM")
        '        sEmailCoordAM = myReaderGEN.Item("EmailCoordAM")

        '        Exit Do
        '    Loop
        'End If
        'myReaderGEN.Close()
        'myReaderGEN = Nothing
        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String
        Dim MailContatto As String

        strDest = ""
        strDest2 = ""
        strDest3 = ""
        strDest4 = ""

        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

        '''**************************************************************'
        '''*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
        '''**************************************************************' .Replace("#testo", testo)
        MailContattoAgg = ""

        MailContatto = mailAM

        If strTest.ToUpper().Trim().ToString() = "S" Then

            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

            MailContatto = strDest 'TEST

        End If


        ''Dim sPartner As String
        ''Dim sAnnoMese As String
        ''Dim sConfInterne As String
        ''Dim sConfEsterne As String
        ''Dim sNumDemo1to1 As String
        ''Dim sNumPartecipantiWeekly As String


        MailContattoAgg = ""
        Try
            'testoHtml = testoHtml.Replace("#annonumerodemoweekly", myReaderGEN.Item("NumWeeklyAnno"))

            strOggetto = strOggetto & " " & NomeAM

            Inviamail(strOggetto, "", MailContatto, MailContattoAgg, Nothing, testoHtml, "", "", Nothing)

            sw.WriteLine("Invio email per area manager a " & MailContatto & " " & DateTime.Now)

            Dim query As String
            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
            myConn3 = New SqlConnection(strConnectionDB)
            myConn3.Open()
            Using commins As New SqlCommand()
                With commins
                    .Connection = myConn3
                    .CommandType = CommandType.Text
                    .CommandText = query
                    .Parameters.AddWithValue("@Dest", MailContatto)
                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                    .Parameters.AddWithValue("@Oggetto", strOggetto)
                    .Parameters.AddWithValue("@Esito", "OK")
                    .Parameters.AddWithValue("@Note", "INVIO MAIL STATISTICA AREA MANAGER  " & MailContatto)
                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                    .Parameters.AddWithValue("@TestoHTML", testoHtml)
                End With
                Try
                    commins.ExecuteNonQuery()
                Catch ex As Exception
                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                End Try
            End Using
            myConn3.Close()


        Catch ex As Exception
            sw.WriteLine("*** INVIO MAIL STATISTICA AREA MANAGER A  " & MailContatto & " " & DateTime.Now)

            Dim query As String
            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
            'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
            myConn3 = New SqlConnection(strConnectionDB)
            myConn3.Open()
            Using commins As New SqlCommand()
                With commins
                    .Connection = myConn3
                    .CommandType = CommandType.Text
                    .CommandText = query
                    .Parameters.AddWithValue("@Dest", MailContatto)
                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                    .Parameters.AddWithValue("@Oggetto", strOggetto)
                    .Parameters.AddWithValue("@Esito", "ERR")
                    .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto)
                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                    .Parameters.AddWithValue("@TestoHTML", testoHtml)
                End With
                Try
                    commins.ExecuteNonQuery()
                Catch ex1 As Exception
                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                End Try
            End Using
            myConn3.Close()


        End Try

        '**************************************************************'
        '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
        '**************************************************************'
        '''''''''''''''''''''''''''''''''''''''''''''''''''''

        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine invio mail riepilogo Area Manager - " & idAM & " " & DateTime.Now)
        sw.Close()

        'Dim directoryName As String = strPathGrafici
        'For Each deleteFile In Directory.GetFiles(directoryName, "*.*", IO.SearchOption.TopDirectoryOnly)
        '    File.Delete(deleteFile)
        'Next

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailReportAreaManager = True

        Exit Function

    End Function


    Private Function AttivitàFuture(IDPresale As Integer, Nominativo As String, Descrizione As String, Email As String) As Boolean

        Dim retImp As Boolean

        On Error GoTo ErrorTrap

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailContatto As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoAggOLD As String
        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim strDataDemoTxt As String

        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        MailContattoAggOLD = ""



        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim data1 As Date
        Dim data2 As Date

        data1 = DateTime.Now().AddYears(-2)
        data2 = DateTime.Now().AddYears(1)

        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()
        Dim strSQLgen As String
        Dim myCmdAF As SqlCommand
        myCmdAF = Nothing

        strSQLgen = "set dateformat dmy Exec spPopolaAttivitaFutureW @idpresale, @idkey, @tabella"
        myCmdAF = myConn2.CreateCommand
        myCmdAF.CommandText = strSQLgen
        myCmdAF.Parameters.AddWithValue("@idpresale", CInt(IDPresale))
        myCmdAF.Parameters.AddWithValue("@idkey", 0)
        myCmdAF.Parameters.AddWithValue("@tabella", "")

        Dim R As Integer
        R = myCmdAF.ExecuteNonQuery
        myCmdAF = Nothing

        myConn2.Close()

        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()
        myCmdAF = Nothing
        strSQLgen = "set dateformat dmy Exec spPopolaAttivitaFuture1TO1 @idpresale, @idkey, @tabella"
        myCmdAF = myConn2.CreateCommand
        myCmdAF.CommandText = strSQLgen
        myCmdAF.Parameters.AddWithValue("@idpresale", CInt(IDPresale))
        myCmdAF.Parameters.AddWithValue("@idkey", 0)
        myCmdAF.Parameters.AddWithValue("@tabella", "")

        R = myCmdAF.ExecuteNonQuery()
        myCmdAF = Nothing
        myConn2.Close()

        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()
        myCmdAF = Nothing
        strSQLgen = "set dateformat dmy Exec spPopolaAttivitaPartecipanti @idpresale, @idkey, @tabella"
        myCmdAF = myConn2.CreateCommand
        myCmdAF.CommandText = strSQLgen
        myCmdAF.Parameters.AddWithValue("@idpresale", CInt(IDPresale))
        myCmdAF.Parameters.AddWithValue("@idkey", 0)
        myCmdAF.Parameters.AddWithValue("@tabella", "")

        R = myCmdAF.ExecuteNonQuery()
        myCmdAF = Nothing
        myConn2.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        AttivitàFuture = True

        Exit Function

ErrorTrap:
        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Errore " & Err.Description & " IDPresale : " & IDPresale & " " & DateTime.Now)
        sw.Close()
        Exit Function


    End Function



    Private Function InviaMailRiepilgoMensilePM(idPM As Integer, Nominativo As String, Descrizione As String, Email As String) As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailContatto As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoAggOLD As String
        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim strDataDemoTxt As String

        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        MailContattoAggOLD = ""


        Dim formattedDate As String
        Dim formattedYear As String
        Dim formattedDateStart As String
        Dim formattedDateEnd As String
        Dim myDate As Date

        formattedDateStart = ""
        formattedDateEnd = ""

        myDate = DateTime.Now
        formattedDate = Format$(myDate, "MM")
        formattedYear = Format$(myDate, "yyyy").ToString()

        If formattedDate.Equals("01") Then
            formattedDate = "12"
            formattedYear = (Format$(myDate, "yyyy") - 1).ToString()
        Else
            If formattedDate.Equals("02") Then formattedDate = "01"
            If formattedDate.Equals("03") Then formattedDate = "02"
            If formattedDate.Equals("04") Then formattedDate = "03"
            If formattedDate.Equals("05") Then formattedDate = "04"
            If formattedDate.Equals("06") Then formattedDate = "05"
            If formattedDate.Equals("07") Then formattedDate = "06"
            If formattedDate.Equals("08") Then formattedDate = "07"
            If formattedDate.Equals("09") Then formattedDate = "08"
            If formattedDate.Equals("10") Then formattedDate = "09"
            If formattedDate.Equals("11") Then formattedDate = "10"
            If formattedDate.Equals("12") Then formattedDate = "11"
        End If

        If formattedDate.Equals("01") Or formattedDate.Equals("03") Or formattedDate.Equals("05") Or formattedDate.Equals("07") Or formattedDate.Equals("08") Or formattedDate.Equals("10") Or formattedDate.Equals("12") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "31/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("02") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "28/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("04") Or formattedDate.Equals("06") Or formattedDate.Equals("09") Or formattedDate.Equals("11") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "30/" & formattedDate & "/" & formattedYear
        End If

        'formattedDateStart = "01/01/2021"
        'formattedDateEnd = "31/07/2021"

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailPM_" & Nominativo & " " & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail riepilogo mensile al commerciale - " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioRiepilogoMensilePM'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioRiepilogoMensilePM'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String
        Dim strElencoDemoPM As String
        Dim strElencoDemo1to1PM As String
        Dim strElencoaziendenopartecipato As String

        retID = 0
        strElencoDemoPM = ""
        strElencoDemo1to1PM = ""
        strElencoaziendenopartecipato = ""

        '' MI SCORRO TUTTI I COMMERCIALI (AD ESCLUSIONE DI QUELLI FITTIZI BO PARTNER E DEALER
        'strSQL = "set dateformat dmy "
        ''strSQL = strSQL & "select * from Anagrafica_Commerciali where id=100 and  Nominativo not in ('** BO PARTNER **', 'DEALER') "
        'strSQL = strSQL & "select * from Anagrafica_Commerciali where id=" & idPM.ToString() & " and  Nominativo not in ('** BO PARTNER **', 'DEALER') "

        'myCmd = myConn.CreateCommand
        'myCmd.CommandText = strSQL

        'myReader = myCmd.ExecuteReader()
        'If myReader.HasRows Then
        '    Do While myReader.Read

        retID = idPM


        Dim data1 As Date
        Dim data2 As Date

        data2 = DateTime.Now.AddDays(-(DateTime.Now.Day))
        data1 = data2.AddDays(-data2.Day + 1)
        data1 = DateTime.Now.AddDays(-(DateTime.Now.Day) + 1)
        data1 = data1.AddYears(-1)

        data1 = FormatDateTime(data1, DateFormat.ShortDate)
        data2 = FormatDateTime(data2, DateFormat.ShortDate)


        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        If retID = "" Then
            InviaMailRiepilgoMensilePM = False
            Exit Function
        End If
        ' #listaclientidemo
        strSQLgen = "set dateformat dmy Exec spGetRiepilogoDemoPMWEEKLYMAIL @idpm, @data1, @data2, @order"
        myCmdGENMAIL = myConn2.CreateCommand
        myCmdGENMAIL.CommandText = strSQLgen
        myCmdGENMAIL.Parameters.AddWithValue("@idpm", CInt(retID))
        myCmdGENMAIL.Parameters.AddWithValue("@data1", data1)
        myCmdGENMAIL.Parameters.AddWithValue("@data2", data2)
        myCmdGENMAIL.Parameters.AddWithValue("@order", "")
        myCmdGENMAIL.CommandTimeout = 0
        myReaderGENMAIL = Nothing
        myReaderGENMAIL = myCmdGENMAIL.ExecuteReader()
        If myReaderGENMAIL.HasRows Then
            Do While myReaderGENMAIL.Read
                strElencoDemoPM = myReaderGENMAIL.Item(0)
            Loop
        End If
        myReaderGENMAIL.Close()
        myCmdGENMAIL = Nothing


        ' #listaclientidemo1to1
        strSQLgen = "set dateformat dmy Exec spGetRiepilogoDemoPM1TO1 @idpm, @data1, @data2, @order"
        myCmdGENMAIL = myConn2.CreateCommand
        myCmdGENMAIL.CommandText = strSQLgen
        myCmdGENMAIL.Parameters.AddWithValue("@idpm", CInt(idPM))
        myCmdGENMAIL.Parameters.AddWithValue("@data1", data1)
        myCmdGENMAIL.Parameters.AddWithValue("@data2", data2)
        myCmdGENMAIL.Parameters.AddWithValue("@order", "")
        myCmdGENMAIL.CommandTimeout = 0
        myReaderGENMAIL = Nothing
        myReaderGENMAIL = myCmdGENMAIL.ExecuteReader()
        If myReaderGENMAIL.HasRows Then
            Do While myReaderGENMAIL.Read
                strElencoDemo1to1PM = myReaderGENMAIL.Item(0)
            Loop
        End If
        myReaderGENMAIL.Close()
        myCmdGENMAIL = Nothing


        strSQLgen = "set dateformat dmy Exec spGetRiepilogoMensilePM @idpm"

        myCmdGEN = myConn2.CreateCommand
        myCmdGEN.CommandText = strSQLgen
        myCmdGEN.Parameters.AddWithValue("@idpm", CInt(retID))
        myCmdGEN.CommandTimeout = 0

        sw.WriteLine("Inizio invio mail riepilogo mensile al PM - " & retID)

        myReaderGEN = Nothing
        myReaderGEN = myCmdGEN.ExecuteReader()

        Threading.Thread.Sleep(3000)

        If myReaderGEN.HasRows Then
            Do While myReaderGEN.Read

                MailContatto = Email ' myReaderGEN.Item("MailCommerciale")

                Dim strTest As String
                Dim strDest As String
                Dim strDest2 As String
                Dim strDest3 As String
                Dim strDest4 As String

                strDest = ""
                strDest2 = ""
                strDest3 = ""
                strDest4 = ""

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                '**************************************************************'
                '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                '**************************************************************' .Replace("#testo", testo)
                MailContattoAgg = ""

                If strTest.ToUpper().Trim().ToString() = "S" Then

                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                    MailContatto = strDest 'TEST

                End If

                MailContattoAgg = ""

                If strElencoDemo1to1PM = "" Then
                    strElencoDemo1to1PM = "Nessuna demo 1to1 trovata"
                End If

                If strElencoDemoPM = "" Then
                    strElencoDemoPM = "Nessuna demo weekly trovata"
                End If

                strElencoaziendenopartecipato = myReaderGEN.Item("ElencoAziende").ToString()

                If strElencoaziendenopartecipato = "" Then
                    strElencoaziendenopartecipato = "Nessuna azienda trovata"
                End If

                MailContattoAgg = ""
                Try
                    testoHtml = testoHtml.Replace("#data1", formattedDateStart).Replace("#data2", formattedDateEnd).Replace("#numerodemoweekly", myReaderGEN.Item("NumWeekly")).Replace("#numerototaleiscritti", myReaderGEN.Item("Iscritti")).Replace("#numeropartecipanti", myReaderGEN.Item("Partecipanti")).Replace("#numerononpartecipanti", myReaderGEN.Item("Non partec")).Replace("#percentuale", myReaderGEN.Item("percentuale")).Replace("#numerodemo1to1", myReaderGEN.Item("NumDemo1to1"))
                    testoHtml = testoHtml.Replace("#annonumerodemoweekly", myReaderGEN.Item("NumWeeklyAnno"))
                    testoHtml = testoHtml.Replace("#annonumerototaleiscritti", myReaderGEN.Item("NumTotIscrittiWeeklyAnno"))
                    testoHtml = testoHtml.Replace("#annonumeropartecipanti", myReaderGEN.Item("NumPartecipantiAnno"))
                    testoHtml = testoHtml.Replace("#annonumerononpartecipanti", myReaderGEN.Item("NumNonPartecipantiAnno"))
                    testoHtml = testoHtml.Replace("#annopercentuale", myReaderGEN.Item("percentualeAnno"))
                    testoHtml = testoHtml.Replace("#annonumerodemo1to1", myReaderGEN.Item("NumDemo1to1Anno"))
                    testoHtml = testoHtml.Replace("#elencoaziendenopartecipato", strElencoaziendenopartecipato)
                    testoHtml = testoHtml.Replace("#listaclientidemo1to1", strElencoDemo1to1PM)
                    testoHtml = testoHtml.Replace("#listaclientidemo", strElencoDemoPM)
                    testoHtml = testoHtml.Replace("#presale", Descrizione)

                    If strElencoDemo1to1PM = "Nessuna demo 1to1 trovata" Then
                        testoHtml = testoHtml.Replace("LISTA CLIENTI/DEMO 1TO1 DEGLI ULTIMI 12 MESI", "")
                    End If

                    If strElencoDemoPM = "Nessuna demo weekly trovata" Then
                        testoHtml = testoHtml.Replace("LISTA CLIENTI/DEMO WEEKLY DEGLI ULTIMI 12 MESI", "")
                    End If

                    Inviamail(strOggetto, "", MailContatto, MailContattoAgg, Nothing, testoHtml, strDataDemoTxt, "", Nothing)
                    sw.WriteLine("Invio email contatto principale a " & MailContatto & " " & DateTime.Now)

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
                    myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailContatto)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@TestoHTML", testoHtml)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using
                    myConn3.Close()


                Catch ex As Exception
                    sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                    'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                    myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailContatto)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "ERR")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@TestoHTML", testoHtml)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using
                    myConn3.Close()


                End Try

                '**************************************************************'
                '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                '**************************************************************'


            Loop
        End If
        myReaderGEN.Close()
        myReaderGEN = Nothing
        '''''''''''''''''''''''''''''''''''''''''''''''''''''




        '    Loop
        'End If
        'myReader.Close()
        'myReader = Nothing




        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailRiepilgoMensilePM = True

        Exit Function

    End Function


    Private Sub PopolaTimeLineDirezione()

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio PopolaTimeLineDirezione -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()

        Dim str1 As String

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand
        Dim myConnnew4 As New SqlConnection
        myConnnew4 = New SqlConnection(strConnectionDB)
        myConnnew4.Open()
        myCmd4 = myConnnew4.CreateCommand
        myCmd4.CommandText = "truncate table TargetDirezione"
        myReader4 = myCmd4.ExecuteReader()
        myCmd4.Connection.Close()
        myCmd4.Dispose()
        myConnnew4.Close()

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand
        Dim myConnnew As New SqlConnection
        myConnnew = New SqlConnection(strConnectionDB)
        myConnnew.Open()
        myCmd3 = myConnnew.CreateCommand
        myCmd3.CommandText = "Exec spSTAT_PopolaTargetDirezione"
        myCmd3.CommandTimeout = 240

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            str1 = myReader3.Item(1)

            Dim query As String

            query = strDateFormat & "; INSERT INTO TargetDirezione (Codice,DataDemo,DataDemo1,DataDemo2,Demo,TipoDemo,NoteDemo,Prenotazioni,Iscritti,Azienda,Eseguita,IdKey,Tabella,GUID,Profilo,Commerciale,Presale,CodiceTL, IconaTL,Label) VALUES "
            query = query & " (@codice,@datademo,@datademo1,@datademo2,@Demo,@TipoDemo,@NoteDemo,@Prenotazioni,@Iscritti, @Azienda,@Eseguita,@IdKey,@Tabella,@GUID,@Profilo,@Commerciale,@Presale,@CodiceTL, @IconaTL, @Label "
            query = query & ")"

            'sw.WriteLine("query 1 -- " & query)

            myConn2 = New SqlConnection(strConnectionDB)
            myConn2.Open()
            Using comm As New SqlCommand()
                With comm

                    'sw.WriteLine("query -- " & query)

                    .Connection = myConn2
                    .CommandType = CommandType.Text
                    .CommandText = query
                    str1 = ""
                    Try
                        str1 = myReader3.Item(0).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    .Parameters.AddWithValue("@codice", str1)
                    Try
                        str1 = myReader3.Item(1).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    .Parameters.AddWithValue("@datademo", str1)
                    str1 = myReader3.Item(2)
                    Try
                        str1 = myReader3.Item(2).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    .Parameters.AddWithValue("@datademo1", str1)
                    Try
                        str1 = myReader3.Item(3).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    .Parameters.AddWithValue("@datademo2", str1)
                    Try
                        str1 = myReader3.Item(4).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    .Parameters.AddWithValue("@Demo", str1)
                    Try
                        str1 = myReader3.Item(5).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    .Parameters.AddWithValue("@TipoDemo", str1)
                    Try
                        str1 = myReader3.Item(6).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    '                    sw.WriteLine("str1 -- " & str1)
                    .Parameters.AddWithValue("@NoteDemo", str1)
                    Try
                        str1 = myReader3.Item(7).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("str1 -- " & str1)
                    .Parameters.AddWithValue("@Prenotazioni", str1)
                    Try
                        str1 = myReader3.Item(8).ToString()
                    Catch ex As Exception
                        'sw.WriteLine("*** ERRORE su PopolaTimeLineDirezioneaaaaaaaaaa " & ex.Message)
                        str1 = ""
                    End Try
                    'sw.WriteLine("str1 -- " & str1)
                    .Parameters.AddWithValue("@Iscritti", str1)
                    Try
                        str1 = myReader3.Item(9).ToString()
                    Catch ex As Exception
                        'sw.WriteLine("*** ERRORE su PopolaTimeLineDirezioneaaaaaaaaaa " & ex.Message)
                        str1 = ""
                    End Try
                    'sw.WriteLine("str1 -- " & str1)
                    .Parameters.AddWithValue("@Azienda", str1)
                    Try
                        str1 = myReader3.Item(10).ToString()
                    Catch ex As Exception
                        'sw.WriteLine("*** ERRORE su PopolaTimeLineDirezioneaaaaaaaaaa " & ex.Message)
                        str1 = ""
                    End Try
                    'sw.WriteLine("Eseguita -- " & str1)
                    .Parameters.AddWithValue("@Eseguita", str1)
                    Try
                        str1 = myReader3.Item(11).ToString()
                    Catch ex As Exception
                        'sw.WriteLine("*** ERRORE su PopolaTimeLineDirezioneaaaaaaaaaa " & ex.Message)
                        str1 = ""
                    End Try
                    'sw.WriteLine("IdKey -- " & str1)
                    .Parameters.AddWithValue("@IdKey", str1)
                    Try
                        str1 = myReader3.Item(12).ToString()
                    Catch ex As Exception
                        'sw.WriteLine("*** ERRORE su PopolaTimeLineDirezioneaaaaaaaaaa " & ex.Message)
                        str1 = ""
                    End Try
                    'sw.WriteLine("Tabella -- " & str1)
                    .Parameters.AddWithValue("@Tabella", str1)
                    Try
                        str1 = myReader3.Item(13).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("GUID -- " & str1)
                    .Parameters.AddWithValue("@GUID", str1)
                    Try
                        str1 = myReader3.Item(14).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("Commerciale -- " & str1)
                    .Parameters.AddWithValue("@Commerciale", str1)
                    Try
                        str1 = myReader3.Item(15).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("Presale -- " & str1)
                    .Parameters.AddWithValue("@Presale", str1)
                    Try
                        str1 = myReader3.Item(16).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("Profilo -- " & str1)
                    .Parameters.AddWithValue("@Profilo", str1)
                    Try
                        str1 = myReader3.Item(17).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("CodiceTL -- " & str1)
                    .Parameters.AddWithValue("@CodiceTL", str1)
                    Try
                        str1 = myReader3.Item(18).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("IconaTL -- " & str1)
                    .Parameters.AddWithValue("@IconaTL", str1)
                    Try
                        str1 = myReader3.Item(19).ToString()
                    Catch ex As Exception
                        str1 = ""
                    End Try
                    'sw.WriteLine("Label -- " & str1)
                    .Parameters.AddWithValue("@Label", str1)

                End With
                Try
                    'sw.WriteLine("Prima ExecuteNonQuery -- " & str1)
                    comm.ExecuteNonQuery()
                    'sw.WriteLine("Dopo ExecuteNonQuery -- " & str1)
                Catch ex As Exception
                    sw.WriteLine("*** ERRORE su PopolaTimeLineDirezione " & ex.Message)
                    sw.Close()
                    'MsgBox("*** ERRORE su PopolaTimeLineDirezione " & ex.Message)
                End Try
            End Using
            'sw.WriteLine("Prima di myConn2.Close")
            myConn2.Close()
            'sw.WriteLine("Dopo myConn2.Close")

        Loop

        myReader3.Close()
        myCmd3 = Nothing
        myConnnew.Close()
        Threading.Thread.Sleep(250)

        myConn.Close()

        sw.WriteLine("Fine PopolaTimeLineDirezione -- " & DateTime.Now)
        sw.Close()
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub PopolaTimeLinePresales()

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio PopolaTimeLinePresales -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        myConn = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        'strSQL = "SELECT ID,Nominativo FROM users where idwebprofile=3"
        strSQL = "SELECT ID,Nominativo FROM users where idwebprofile=3 AND (idwebprofile = 3) Or (idwebprofile=1 And ResponsabileDelivery=1)"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim str1 As String

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand
        Dim myConnnew4 As New SqlConnection
        myConnnew4 = New SqlConnection(strConnectionDB)
        myConnnew4.Open()
        myCmd4 = myConnnew4.CreateCommand
        myCmd4.CommandText = "truncate table TargetPresale"
        myReader4 = myCmd4.ExecuteReader()
        myConnnew4.Close()

        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")


            Dim myReader3 As SqlDataReader
            Dim myCmd3 As New SqlCommand
            Dim myConnnew As New SqlConnection
            myConnnew = New SqlConnection(strConnectionDB)
            myConnnew.Open()
            myCmd3 = myConnnew.CreateCommand
            myCmd3.CommandText = "Exec spSTAT_PopolaTargetPresale @idutentepresale"
            myCmd3.Parameters.AddWithValue("@idutentepresale", CInt(ID1.ToString()))
            myReader3 = myCmd3.ExecuteReader()

            Do While myReader3.Read()
                str1 = myReader3.Item(1)

                Dim query As String
                'query = "INSERT INTO TargetPresale (IdPresale,Codice,DataDemo,DataDemo1,DataDemo2,Demo,TipoDemo,NoteDemo,Prenotazioni,Iscritti,Azienda,Eseguita,IdKey,Tabella,GUID,Profilo,CodiceTL,IconaTL,Commerciale,Presale,Label) VALUES "
                'query = query & "(@IdPresale,@codice,@datademo,@datademo1,@datademo2,@Demo,@TipoDemo,@NoteDemo,@Prenotazioni, @Iscritti,@Azienda,@Eseguita,@IdKey,@Tabella,@GUID,@Profilo,@CodiceTL,@IconaTL,@Commerciale,@Presale,@Label "
                'query = query & ")"

                query = strDateFormat & "; INSERT INTO TargetPresale (IdPresale,Codice,DataDemo,DataDemo1,DataDemo2,Demo,TipoDemo,NoteDemo,Prenotazioni,Iscritti,Azienda,Eseguita,IdKey,Tabella,GUID,Profilo,Commerciale,Presale,Label, IDDemo) VALUES "
                query = query & "(@IdPresale,@codice,@datademo,@datademo1,@datademo2,@Demo,@TipoDemo,@NoteDemo,@Prenotazioni,@Iscritti, @Azienda,@Eseguita,@IdKey,@Tabella,@GUID,@Profilo,@Commerciale,@Presale,@Label,@IDDemo "
                query = query & ")"


                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query
                        '.Parameters.AddWithValue("@IdPresale", CInt(ID1.ToString()))
                        .Parameters.AddWithValue("@codice", myReader3.Item(0))
                        .Parameters.AddWithValue("@datademo", myReader3.Item(1))
                        .Parameters.AddWithValue("@datademo1", myReader3.Item(2))
                        .Parameters.AddWithValue("@datademo2", myReader3.Item(3))
                        .Parameters.AddWithValue("@Demo", myReader3.Item(4))
                        .Parameters.AddWithValue("@TipoDemo", myReader3.Item(5))
                        .Parameters.AddWithValue("@NoteDemo", myReader3.Item(6))
                        .Parameters.AddWithValue("@Prenotazioni", myReader3.Item(7))
                        .Parameters.AddWithValue("@Iscritti", myReader3.Item(8))
                        .Parameters.AddWithValue("@Azienda", myReader3.Item(9))
                        .Parameters.AddWithValue("@Eseguita", myReader3.Item(10))
                        .Parameters.AddWithValue("@IdKey", myReader3.Item(11))
                        .Parameters.AddWithValue("@Tabella", myReader3.Item(12))
                        .Parameters.AddWithValue("@GUID", myReader3.Item(13))
                        .Parameters.AddWithValue("@Profilo", myReader3.Item(14))
                        '.Parameters.AddWithValue("@CodiceTL", myReader3.Item(15))
                        '.Parameters.AddWithValue("@IconaTL", myReader3.Item(16))
                        .Parameters.AddWithValue("@Commerciale", myReader3.Item(15))
                        .Parameters.AddWithValue("@Presale", myReader3.Item(16))
                        .Parameters.AddWithValue("@IdPresale", myReader3.Item(17))
                        .Parameters.AddWithValue("@IDDemo", myReader3.Item(18))
                        ''''.Parameters.AddWithValue("@IDRichiesteDemoCOP", myReader3.Item(19))
                        .Parameters.AddWithValue("@Label", myReader3.Item(19))
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        'sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        MsgBox(ex.Message & DateTime.Now)
                    End Try
                End Using
                myConn2.Close()

            Loop

            myReader3.Close()
            myCmd3 = Nothing
            myConnnew.Close()
            Threading.Thread.Sleep(250)

        Loop
        myReader1.Close()

        myConn.Close()

        sw.WriteLine("Fine PopolaTimeLinePresales -- " & DateTime.Now)
        sw.Close()
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub PopolaTimeLineCommerciali()

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\PopolaTimeLineCommerciali" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio PopolaTimeLineCommerciali -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        myConn = New SqlConnection(strConnectionDB)

        Dim strSQL As String

        strSQL = "SELECT ac.ID,ac.Nominativo FROM Anagrafica_Commerciali ac join Users u on u.Email=ac.Mail where u.idwebprofile=2 and ac.id not in (26)"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim myReader1 As SqlDataReader
        Dim ID1 As Integer
        Dim Nominativo1 As String
        Dim str1 As String

        Dim myReader4 As SqlDataReader
        Dim myCmd4 As New SqlCommand
        Dim myConnnew4 As New SqlConnection
        myConnnew4 = New SqlConnection(strConnectionDB)
        myConnnew4.Open()
        myCmd4 = myConnnew4.CreateCommand
        myCmd4.CommandText = "truncate table TargetCommerciale"
        myReader4 = myCmd4.ExecuteReader()
        myConnnew4.Close()

        myReader1 = myCmd.ExecuteReader()
        Do While myReader1.Read()
            ID1 = myReader1.Item("ID")
            Nominativo1 = myReader1.Item("Nominativo")

            Dim myReader3 As SqlDataReader
            Dim myCmd3 As New SqlCommand
            Dim myConnnew As New SqlConnection
            myConnnew = New SqlConnection(strConnectionDB)
            myConnnew.Open()
            myCmd3 = myConnnew.CreateCommand

            Dim strFile1 As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & Nominativo1.Trim() & ".log"

            Dim fileExists1 As Boolean = File.Exists(strFile1)
            Dim sw1 As New StreamWriter(File.Open(strFile1, FileMode.Append))

            sw1.WriteLine("Inizio PopolaTimeLineCommerciali -- " & ID1 & " " & Nominativo1 & " " & DateTime.Now)

            myCmd3.CommandText = "set dateformat dmy; Exec spSTAT_PopolaTargetCommerciale @idcommerciale"
            myCmd3.Parameters.AddWithValue("@idcommerciale", CInt(ID1.ToString()))

            myReader3 = myCmd3.ExecuteReader()

            Do While myReader3.Read()
                str1 = myReader3.Item(1)

                Dim query As String
                Dim codicetest As String
                Dim demotest As String
                query = strDateFormat & "; INSERT INTO TargetCommerciale (IdCommerciale,Codice,DataDemo,DataDemo1,DataDemo2,Demo,TipoDemo,NoteDemo,Prenotazioni,Iscritti,Azienda,Eseguita,IdKey,Tabella,GUID,Profilo,CodiceTL,IconaTL,Commerciale,Presale,Mode,Label) VALUES "
                query = query & "(@IdCommerciale,@codice,@datademo,@datademo1,@datademo2,@Demo,@TipoDemo,@NoteDemo,@Prenotazioni, @Iscritti,@Azienda,@Eseguita,@IdKey,@Tabella,@GUID,@Profilo,@CodiceTL,@IconaTL,@Commerciale,@Presale,@mode,@Label "
                query = query & ")"

                'da rimettere la precedente con il parametro @mode
                'query = strDateFormat & "; INSERT INTO TargetCommerciale (IdCommerciale,Codice,DataDemo,DataDemo1,DataDemo2,Demo,TipoDemo,NoteDemo,Prenotazioni,Iscritti,Azienda,Eseguita,IdKey,Tabella,GUID,Profilo,CodiceTL,IconaTL,Commerciale,Presale,Label) VALUES "
                'query = query & "(@IdCommerciale,@codice,@datademo,@datademo1,@datademo2,@Demo,@TipoDemo,@NoteDemo,@Prenotazioni, @Iscritti,@Azienda,@Eseguita,@IdKey,@Tabella,@GUID,@Profilo,@CodiceTL,@IconaTL,@Commerciale,@Presale,@Label "
                'query = query & ")"
                'da rimettere la precedente con il parametro @mode

                myConn2 = New SqlConnection(strConnectionDB)
                myConn2.Open()
                Using comm As New SqlCommand()
                    With comm
                        .Connection = myConn2
                        .CommandType = CommandType.Text
                        .CommandText = query

                        codicetest = myReader3.Item(0).ToString()
                        demotest = myReader3.Item(4).ToString()
                        .Parameters.AddWithValue("@IdCommerciale", CInt(ID1.ToString()))
                        .Parameters.AddWithValue("@codice", myReader3.Item(0))
                        .Parameters.AddWithValue("@datademo", myReader3.Item(1))
                        .Parameters.AddWithValue("@datademo1", myReader3.Item(2))
                        .Parameters.AddWithValue("@datademo2", myReader3.Item(3))
                        .Parameters.AddWithValue("@Demo", myReader3.Item(4))
                        .Parameters.AddWithValue("@TipoDemo", myReader3.Item(5))
                        .Parameters.AddWithValue("@NoteDemo", myReader3.Item(6))
                        .Parameters.AddWithValue("@Prenotazioni", myReader3.Item(7))
                        .Parameters.AddWithValue("@Iscritti", myReader3.Item(8))
                        .Parameters.AddWithValue("@Azienda", myReader3.Item(9))
                        .Parameters.AddWithValue("@Eseguita", myReader3.Item(10))
                        .Parameters.AddWithValue("@IdKey", myReader3.Item(11))
                        .Parameters.AddWithValue("@Tabella", myReader3.Item(12))
                        .Parameters.AddWithValue("@GUID", myReader3.Item(13))
                        .Parameters.AddWithValue("@Profilo", myReader3.Item(14))
                        .Parameters.AddWithValue("@CodiceTL", myReader3.Item(15))
                        .Parameters.AddWithValue("@IconaTL", myReader3.Item(16))
                        .Parameters.AddWithValue("@Commerciale", myReader3.Item(17))
                        .Parameters.AddWithValue("@Presale", myReader3.Item(18))
                        '.Parameters.AddWithValue("@Label", myReader3.Item(19))

                        'da rimettere
                        .Parameters.AddWithValue("@mode", myReader3.Item(19))
                        .Parameters.AddWithValue("@Label", myReader3.Item(20))
                        'da rimettere
                    End With
                    Try
                        comm.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(Err.Description & " " & codicetest & " " & demotest & DateTime.Now)
                    End Try
                End Using
                myConn2.Close()



            Loop
            sw1.WriteLine("Fine PopolaTimeLineCommerciali -- " & ID1 & " " & Nominativo1 & " " & DateTime.Now)
            sw1.Close()
            'MsgBox("4")
            myReader3.Close()
            myCmd3 = Nothing
            myConnnew.Close()
            Threading.Thread.Sleep(250)



        Loop
        myReader1.Close()

        myConn.Close()

        sw.WriteLine("Fine PopolaTimeLineCommerciali -- " & DateTime.Now)
        sw.Close()
        Me.Cursor = Cursors.Default

    End Sub
    Private Function InviaMailPresalesAlertNonEseguita1to1() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim GUID As String
        Dim IDRichiesta As Int32
        Dim IDRichiestaCOP As Int32
        Dim IDRichiestaOLD As Int32
        Dim IDDemo As Int32
        Dim IDStorico As Int32
        Dim NomeAzienda As String
        Dim IdUtentePresale As Integer
        Dim NomeContatto As String
        Dim MailContatto As String
        Dim strDemo As String
        Dim strLinkWebex As String
        Dim MailContattoAgg As String
        'Dim MailPresales As String

        Dim strDataDemo As String
        Dim strOra As String
        Dim strDataDemoTxt As String
        Dim strPresale As String
        Dim strContatto As String
        Dim strMailGuest As String
        Dim IDKey As Int32
        Dim Tabella As String
        Dim NoteDemo As String
        Dim strT1 As String
        Dim strProgressivo As String

        NoteDemo = ""
        retImp = False



        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        sw.WriteLine("Inizio controllo date delle demo 1 to 1 -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))


        Dim strSQL As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReaderContAgg As SqlDataReader
        Dim myCmdContAgg As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoAlertPresales1to1NonEseguita'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='ObjectAlertPresales1to1NonEseguita'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        strSQL = "set dateformat dmy;DECLARE	@return_value int EXEC	@return_value = [dbo].[spSetupAlertPresalesNonEseguita1to1] SELECT	'Return Value' = @return_value"

        'EXEC	@return_value = [dbo].[spSetupAlertPresales1to1]

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()

        strContatto = ""
        strMailGuest = ""
        strPresale = ""
        strDataDemoTxt = ""
        IDRichiestaOLD = 0
        IDRichiesta = 0

        myConn2 = New SqlConnection(strConnectionDB)
        myConn2.Open()

        If myReader.HasRows Then
            Do While myReader.Read

                Try

                    GUID = myReader.Item("GUIDRICHIESTA").ToString()
                    IDRichiesta = Int32.Parse(myReader.Item("IDRichiesta"))
                    IDRichiestaCOP = Int32.Parse(myReader.Item("ID"))
                    IDKey = Int32.Parse(myReader.Item("IDKey"))
                    Tabella = myReader.Item("Tabella").ToString()
                    NomeAzienda = myReader.Item("AziendaCliente").ToString()
                    IdUtentePresale = myReader.Item("IdUtentePresale")
                    NomeContatto = myReader.Item("Presale").ToString()
                    MailContatto = myReader.Item("EmailPresale").ToString()
                    strDemo = myReader.Item("Demo").ToString().ToUpper()
                    strDataDemo = myReader.Item("DataDemo").ToString()
                    strDataDemoTxt = myReader.Item("DataDemoText").ToString()
                    strT1 = myReader.Item("T1").ToString().ToUpper()
                    strProgressivo = myReader.Item("Progressivo").ToString().ToUpper()

                    strOra = myReader.Item("Ora").ToString()
                    'strLinkWebex = myReader.Item("linkWebex").ToString()
                    'IDKey = Int32.Parse(myReader.Item("IDKey"))
                    'Tabella = myReader.Item("Tabella").ToString()
                    'NoteDemo = myReader.Item("NoteDemo").ToString()

                    Dim strTest As String
                    Dim strDest As String
                    Dim strDest2 As String
                    Dim strDest3 As String
                    Dim strDest4 As String

                    strDest = ""
                    strDest2 = ""
                    strDest3 = ""
                    strDest4 = ""

                    strTest = System.Configuration.ConfigurationManager.AppSettings("TESTGuestTomorrow").ToString()

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************' .Replace("#testo", testo)
                    MailContattoAgg = ""

                    If strTest.ToUpper().Trim().ToString() = "S" Then

                        strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                        strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                        strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                        strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                        'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                        MailContatto = strDest 'TEST

                    End If

                    MailContattoAgg = ""
                    Try
                        Dim linkDemo As String
                        linkDemo = apriDirettamenteSchedaDemo(GUID, IDKey, Tabella)

                        linkDemo = System.Configuration.ConfigurationManager.AppSettings("linkURL").ToString() & linkDemo



                        Dim bInviaMail As Boolean

                        bInviaMail = False

                        Dim strSqlCtr As String
                        strSqlCtr = "INVIO MAIL A PRESALE " & MailContatto & " per la demo 1 to 1 " & strDemo

                        Dim NumGiorni As String

                        NumGiorni = System.Configuration.ConfigurationManager.AppSettings("NumGiorniInvioControlloEseguita").ToString()

                        Dim myCmd4 As New SqlCommand

                        myCmd4 = myConn2.CreateCommand
                        myCmd4.CommandText = "Select * from LogMailOperation where DATEDIFF(dd,DataOra,GETDATE())<=" & NumGiorni & " AND Oggetto=@oggetto and DestinatarioPrincipale=@DestinatarioPrincipale and Note=@note and IDRichiesta = " & IDRichiesta
                        myCmd4.Parameters.AddWithValue("@DestinatarioPrincipale", SqlDbType.VarChar).Value = MailContatto
                        myCmd4.Parameters.AddWithValue("@note", SqlDbType.VarChar).Value = strSqlCtr
                        myCmd4.Parameters.AddWithValue("@Oggetto", SqlDbType.VarChar).Value = strOggetto.Replace("#azienda", NomeAzienda)
                        myReader4 = myCmd4.ExecuteReader()
                        If myReader4.HasRows = False Then
                            bInviaMail = True
                        End If
                        myReader4.Close()

                        'GAMIFICATION E INVIO MAIL AL PRESLAE SOLO OGNI N GIORNI
                        If bInviaMail = True Then

                            Dim queryG1 As String
                            queryG1 = "Select count(ID) From [dbo].[Gamification_Valutazione_Presales_SollecitoFlagEseguita] "
                            queryG1 = queryG1 & "Where IDRichiestaCOP =@IDRichiestaCOP And IDKey=@IDKey And Tabella=@Tabella And Progressivo=@Progressivo And MailPresales=@MailPresales "
                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''myConn2 = New SqlConnection(strConnectionDB)
                            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''myConn2.Open()
                            Using commG1 As New SqlCommand()
                                With commG1
                                    .Connection = myConn2
                                    .CommandType = CommandType.Text
                                    .CommandText = queryG1
                                    .Parameters.AddWithValue("IDRichiestaCOP", IDRichiestaCOP)
                                    .Parameters.AddWithValue("@IDKey", IDKey)
                                    .Parameters.AddWithValue("@Tabella", Tabella)
                                    .Parameters.AddWithValue("@Progressivo", Int(strProgressivo))
                                    .Parameters.AddWithValue("@MailPresales", MailContatto)
                                End With
                                Try
                                    Dim check1 As Integer
                                    check1 = Int(commG1.ExecuteScalar())
                                    If check1 = 0 Then
                                        'INSERT Gamification_Valutazione_Presales_SollecitoFlagEseguita
                                        Dim queryG As String
                                        queryG = "set dateformat dmy;INSERT INTO [dbo].[Gamification_Valutazione_Presales_SollecitoFlagEseguita] "
                                        queryG = queryG & " ([IDRichiestaCOP],[IDKey],[Tabella],[Progressivo],[DataDemo],[orarioDemo],[NumeroTentativi],[PunteggioDemoDriver],[DataAggiornamentoPunteggioDemoDriver],[IDUtentePresale],[MailPresales]) "
                                        queryG = queryG & " VALUES "
                                        queryG = queryG & " (@IDRichiestaCOP,@IDKey,@Tabella,@Progressivo,@DataDemo,@orarioDemo,1,0,getDate(),@IDUtentePresale,@MailPresales) "

                                        'myConn2 = New SqlConnection(strConnectionDB)
                                        'myConn2.Open()
                                        Using comm As New SqlCommand()
                                            With comm
                                                .Connection = myConn2
                                                .CommandType = CommandType.Text
                                                .CommandText = queryG
                                                .Parameters.AddWithValue("IDRichiestaCOP", IDRichiestaCOP)
                                                .Parameters.AddWithValue("@IDKey", IDKey)
                                                .Parameters.AddWithValue("@Tabella", Tabella)
                                                .Parameters.AddWithValue("@Progressivo", Int(strProgressivo))
                                                .Parameters.AddWithValue("@DataDemo", strDataDemo)
                                                .Parameters.AddWithValue("@orarioDemo", strOra)
                                                .Parameters.AddWithValue("@IDUtentePresale", IdUtentePresale)
                                                .Parameters.AddWithValue("@MailPresales", MailContatto)
                                            End With
                                            Try
                                                comm.ExecuteNonQuery()
                                            Catch ex As Exception
                                                sw.WriteLine("*** ERRORE SU INSERT INTO Gamification_Valutazione_Presales_SollecitoFlagEseguita " & DateTime.Now)
                                            End Try
                                        End Using
                                        'myConn2.Close()
                                    Else
                                        'UPDATE Gamification_Valutazione_Presales_SollecitoFlagEseguita
                                        Dim strQueryG3 As String

                                        strQueryG3 = "Declare @ValoreDaTogliere DECIMAL(5,2) "
                                        strQueryG3 = strQueryG3 & " Select @ValoreDaTogliere=Valore "
                                        strQueryG3 = strQueryG3 & " From [dbo].[Gamification_Punteggio_Presales_SollecitoFlagEseguita] "
                                        strQueryG3 = strQueryG3 & " WHERE NumeroTentativi > 1 "
                                        strQueryG3 = strQueryG3 & " Update [dbo].[Gamification_Valutazione_Presales_SollecitoFlagEseguita] "
                                        strQueryG3 = strQueryG3 & " Set [NumeroTentativi] = [NumeroTentativi] + 1,[PunteggioDemoDriver] = [PunteggioDemoDriver] + @ValoreDaTogliere, "
                                        strQueryG3 = strQueryG3 & " [DataAggiornamentoPunteggioDemoDriver] = getdate() "
                                        strQueryG3 = strQueryG3 & " WHERE IDRichiestaCOP = @IDRichiestaCOP And IDKey = @IDKey "
                                        strQueryG3 = strQueryG3 & " And Tabella =@Tabella "
                                        strQueryG3 = strQueryG3 & " And Progressivo=@Progressivo And MailPresales=@MailPresales "
                                        'myConn4 = New SqlConnection(strConnectionDB)
                                        'myConn4.Open()
                                        Using comm4 As New SqlCommand()
                                            With comm4
                                                .Connection = myConn2
                                                .CommandType = CommandType.Text
                                                .CommandText = strQueryG3
                                                .Parameters.AddWithValue("IDRichiestaCOP", IDRichiestaCOP)
                                                .Parameters.AddWithValue("@IDKey", IDKey)
                                                .Parameters.AddWithValue("@Tabella", Tabella)
                                                .Parameters.AddWithValue("@Progressivo", Int(strProgressivo))
                                                .Parameters.AddWithValue("@MailPresales", MailContatto)
                                            End With
                                            Try
                                                comm4.ExecuteNonQuery()
                                            Catch ex1 As Exception
                                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                            End Try
                                        End Using
                                        'myConn4.Close()

                                    End If
                                Catch
                                    sw.WriteLine("*** ERRORE SU INSERT INTO Gamification_Valutazione_Presales_SollecitoFlagEseguita " & DateTime.Now)
                                End Try
                            End Using

                            Inviamail(strOggetto.Replace("#dataDemo", strDataDemoTxt).Replace("#demo", strDemo.ToUpper()).Replace("#azienda", strDemo.ToUpper()), "", MailContatto, MailContattoAgg, sw, testoHtml.Replace("#dataDemo", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#azienda", NomeAzienda).Replace("#dataDemo", strDataDemoTxt).Replace("#linkURL", linkDemo), strDataDemo, "", Nothing)

                            sw.WriteLine("Invio email a presale " & MailContatto & " " & DateTime.Now)

                        End If


                        Threading.Thread.Sleep(1000)



                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra, DestinatarioPrincipale, DestinatarioCC, Oggetto, Esito, Note, GUIDRichiesta, IDRichiesta, testoHtml) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'myConn2 = New SqlConnection(strConnectionDB)
                        'myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#azienda", NomeAzienda))
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL A PRESALE " & MailContatto & " per la demo 1 to 1 " & strDemo)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#dataDemo", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#azienda", NomeAzienda).Replace("#dataDemo", strDataDemoTxt).Replace("#linkURL", linkDemo))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        'myConn2.Close()

                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INVIO MAIL A PRESALE  " & MailContatto & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                        'myConn2 = New SqlConnection(strConnectionDB)
                        'myConn2.Open()
                        Using comm As New SqlCommand()
                            With comm
                                .Connection = myConn2
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", MailContatto)
                                .Parameters.AddWithValue("@CC", MailContattoAgg)
                                .Parameters.AddWithValue("@Oggetto", strOggetto.Replace("#dataDemo", strDataDemoTxt).Replace("#demo", strDemo))
                                .Parameters.AddWithValue("@Esito", ex.Message.ToString())
                                .Parameters.AddWithValue("@Note", "INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " per la demo " & strDemo)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@IDRichiesta", IDRichiesta)
                                .Parameters.AddWithValue("@TestoHTML", testoHtml.Replace("#dataDemo", strDataDemoTxt).Replace("#nome", NomeContatto).Replace("#demo", strDemo).Replace("#linkwebex", strLinkWebex).Replace("#ora", strOra))
                            End With
                            Try
                                comm.ExecuteNonQuery()
                            Catch ex1 As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        'myConn2.Close()


                    End Try

                    '**************************************************************'
                    '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                    '**************************************************************'





                Catch ex As Exception
                    sw.WriteLine(ex.Message.ToString())
                End Try

                IDRichiestaOLD = IDRichiesta

            Loop
        End If

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine controllo date delle demo 1 to 1 -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailPresalesAlertNonEseguita1to1 = True

        Exit Function

    End Function

    Private Function InviaMailFeedbackCommerciali(idComm As Integer) As Boolean


        Dim retSP As Boolean
        Dim strEmailPresale As String
        Dim strDescrizionePresale As String
        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim NominativoCommerciale As String
        Dim EmailCommerciale As String
        Dim EmailCommercialeINVIO As String
        Dim DataInvio As String
        Dim RispostaLibera As String

        Dim Email As String
        Dim EmailOLD As String
        Dim TestoDomanda As String
        Dim TestoRisposta As String
        Dim NomeAzienda As String
        Email = ""
        TestoDomanda = ""
        TestoRisposta = ""
        NomeAzienda = ""

        RispostaLibera = ""
        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        EmailCommerciale = ""
        EmailCommercialeINVIO = ""
        DataInvio = ""
        EmailOLD = ""

        retSP = False

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail riepilogo mensile al commerciale - " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim testoHtmlPresale As String
        Dim strOggettoPresale As String
        Dim strOggetto As String

        Dim myReaderPresale As SqlDataReader
        Dim myCmdPresale As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioFeedbackCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        testoHtmlPresale = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioFeedbackPresale'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtmlPresale = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioFeedbackCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        strOggettoPresale = ""
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioFeedbackPresale'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggettoPresale = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String
        Dim strElencoRisposte As String

        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        Dim IdKey As Integer
        Dim DataDemo As String
        Dim OrarioDemo As String
        Dim NoteDemo As String
        Dim Tabella As String

        Dim IdKeyOLD As Integer
        Dim DataDemoOLD As String
        Dim OrarioDemoOLD As String
        Dim NoteDemoOLD As String
        Dim TabellaOLD As String

        IdKey = 0
        DataDemo = ""
        OrarioDemo = ""
        NoteDemo = ""
        Tabella = ""

        IdKeyOLD = 0
        DataDemoOLD = ""
        OrarioDemoOLD = ""
        NoteDemoOLD = ""
        TabellaOLD = ""

        strDest = ""
        strDest2 = ""
        strDest3 = ""
        strDest4 = ""

        retID = 0
        strElencoRisposte = ""

        retID = idComm

        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        If retID = "" Then
            InviaMailFeedbackCommerciali = False
            Exit Function
        End If

        retSP = False

        strSQLgen = "set dateformat dmy Exec spGetFeedbackCommerciali @idcommerciale, @data"

        myCmdGEN = myConn2.CreateCommand
        myCmdGEN.CommandText = strSQLgen
        myCmdGEN.Parameters.AddWithValue("@idcommerciale", CInt(retID))
        myCmdGEN.Parameters.AddWithValue("@data", CDate(Now.ToString("dd'/'MM'/'yyyy")))

        sw.WriteLine("Inizio invio mail feedback al commerciale - " & retID)

        strElencoRisposte = "<tr><td style = ""width:200px;text-align:Left;text-align:center"" >"

        myReaderGEN = Nothing
        myReaderGEN = myCmdGEN.ExecuteReader()
        If myReaderGEN.HasRows Then
            Do While myReaderGEN.Read


                retSP = True
                retImp = True
                EmailCommerciale = myReaderGEN.Item("MailCommerciale")
                EmailCommercialeINVIO = EmailCommerciale
                Email = myReaderGEN.Item("Mail")
                NomeAzienda = myReaderGEN.Item("NomeAzienda")
                NominativoCommerciale = myReaderGEN.Item("NominativoCommerciale")
                DataInvio = myReaderGEN.Item("DataInvio")
                TestoDomanda = myReaderGEN.Item("TestoDomanda")
                TestoRisposta = myReaderGEN.Item("Testo")
                RispostaLibera = myReaderGEN.Item("RispostaLibera")

                IdKey = myReaderGEN.Item("IdKey")
                Tabella = myReaderGEN.Item("Tabella")
                DataDemo = myReaderGEN.Item("DataDemo")
                OrarioDemo = myReaderGEN.Item("OrarioDemo")
                NoteDemo = myReaderGEN.Item("NoteDemo")

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strElencoRisposte = "" Then
                    strElencoRisposte = "Nessuna risposta trovata"
                End If

                MailContattoAgg = ""
                Try
                    testoHtml = testoHtml.Replace("#datacompilazione", DataInvio)
                    testoHtmlPresale = testoHtmlPresale.Replace("#datacompilazione", DataInvio)
                    testoHtml = testoHtml.Replace("#commerciale", NominativoCommerciale)

                    If (IdKeyOLD <> IdKey Or DataDemoOLD <> DataDemo Or OrarioDemoOLD <> OrarioDemo Or NoteDemoOLD <> NoteDemo Or Email <> Emailold) Then
                        strElencoRisposte = strElencoRisposte & "<tr><td style=""font-size:12px; font-weight: bold;"">" & NoteDemo & " " & DataDemo.ToString() & " " & OrarioDemo & "</td></tr><tr><td style=""font-size:12px; font-weight: bold;"">" & Email & "</td></tr><tr><td style=""font-size:12px; font-weight: bold;"">" & NomeAzienda & "</td></tr>"
                    End If

                    strElencoRisposte = strElencoRisposte & "<tr><td style = ""font-size:12px; width:300px;text-align:Left;text-align:Left"" > " & TestoDomanda & "</td><td style=""font-size:12px; width: 100px;text-align:center;text-align:center"">" & TestoRisposta & "</td><td style=""font-size:12px; width: 230px;text-align:center;text-align:center"">" & RispostaLibera & "</td></tr>"

                Catch ex As Exception
                    sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & EmailCommerciale & " " & DateTime.Now)

                End Try

                '**************************************************************'
                '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                '**************************************************************'

                strEmailPresale = ""
                strDescrizionePresale = ""
                If IdKeyOLD <> IdKey And TabellaOLD <> Tabella And IdKeyOLD <> 0 And TabellaOLD <> "" Then
                    testoHtmlPresale = testoHtmlPresale.Replace("#elencorisposte", strElencoRisposte)
                    'invio mail al presale

                    'recupero l'idUtente del presale di riferimento da questa
                    'invio la mail al presale

                    'myReaderPresale.Close()

                    If (IdKeyOLD = 91 And TabellaOLD = "Preventivi_Sezioni") Or (IdKeyOLD = 317 And TabellaOLD = "Preventivi_Sezioni") Then 'se presenze o paghe guardo se c'è un presale di riferimento, e nel caso la mando solo a lui
                        myCmdPresale = myConn.CreateCommand
                        myCmdPresale.CommandText = "Select U.Email, U.Descrizione from Calendario_Demo c Left Join Users U ON U.ID=C.IDUtentePresaleRiferimento where IdKey = @IdKeyOLD And Tabella =@TabellaOLD AND DataDemo= @DataDemoOLD AND Note2=@NoteDemoOLD"
                        myCmdPresale.Parameters.AddWithValue("@IdKeyOLD", Int(IdKeyOLD))
                        myCmdPresale.Parameters.AddWithValue("@TabellaOLD", TabellaOLD)
                        myCmdPresale.Parameters.AddWithValue("@DataDemoOLD", CDate(DataDemoOLD))
                        myCmdPresale.Parameters.AddWithValue("@NoteDemoOLD", NoteDemoOLD)
                        myReaderPresale = myCmdPresale.ExecuteReader()
                        strEmailPresale = ""
                        While myReaderPresale.Read()
                            If strEmailPresale <> "" Then
                                strEmailPresale = strEmailPresale & ";" & myReaderPresale.Item("Email")
                                strDescrizionePresale = strDescrizionePresale & ";" & myReaderPresale.Item("Descrizione")
                            Else
                                strEmailPresale = myReaderPresale.Item("Email")
                                strDescrizionePresale = myReaderPresale.Item("Descrizione")
                            End If
                        End While
                        myReaderPresale.Close()
                    End If

                    myCmdPresale = myConn.CreateCommand
                    If strEmailPresale = "" Then
                        myCmdPresale.CommandText = "select u.Email Email, u.Descrizione Descrizione from Presales_CalendarioWeekly p join Users u on u.ID=p.IdUtente where p.IdKey=" & Str(IdKeyOLD) & " And p.Tabella = '" & TabellaOLD & "'"
                        myReaderPresale = myCmdPresale.ExecuteReader()
                        strEmailPresale = ""
                        While myReaderPresale.Read()
                            If strEmailPresale <> "" Then
                                strEmailPresale = strEmailPresale & ";" & myReaderPresale.Item("Email")
                                strDescrizionePresale = strDescrizionePresale & ";" & myReaderPresale.Item("Descrizione")
                            Else
                                strEmailPresale = myReaderPresale.Item("Email")
                                strDescrizionePresale = myReaderPresale.Item("Descrizione")
                            End If
                        End While
                        myReaderPresale.Close()
                    End If


                    'myCmdPresale = myConn.CreateCommand
                    'myCmdPresale.CommandText = "select u.Email Email, u.Descrizione Descrizione Email from Presales_CalendarioWeekly p join Users u on u.ID=p.IdUtente where p.IdKey=@idkey and p.Tabella=@tabella"

                    'myCmdPresale.Parameters.AddWithValue("@idkey", CInt(IdKeyOLD))
                    'myCmdPresale.Parameters.AddWithValue("@tabella", TabellaOLD)

                    'myReaderPresale = myCmdPresale.ExecuteReader()
                    'strEmailPresale = ""
                    'strDescrizionePresale = ""
                    'Do While myReaderPresale.Read()
                    '    strEmailPresale = strEmailPresale & ";" & myReaderPresale.Item("Email")
                    '    strDescrizionePresale = strDescrizionePresale & ";" & myReaderPresale.Item("Descrizione")
                    'Loop
                    myReaderPresale.Close()

                    strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                    If strTest.ToUpper().Trim().ToString() = "S" Then
                        strEmailPresale = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    End If
                    If strDescrizionePresale <> "" Then
                        testoHtmlPresale = testoHtmlPresale.Replace("#elencorisposte", strElencoRisposte)
                    End If

                    If strEmailPresale <> "" Then
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''
                        testoHtmlPresale = testoHtmlPresale.Replace("Ciao #presale,", "")
                        Inviamail(strOggettoPresale, "", strEmailPresale, MailContattoAgg, Nothing, testoHtmlPresale, DataInvio, "", Nothing)
                        sw.WriteLine("Invio email feedback a presale " & strEmailPresale & " " & DateTime.Now)

                        Dim query As String
                        query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
                        myConn3 = New SqlConnection(strConnectionDB)
                        myConn3.Open()
                        Using commins As New SqlCommand()
                            With commins
                                .Connection = myConn3
                                .CommandType = CommandType.Text
                                .CommandText = query
                                .Parameters.AddWithValue("@Dest", strEmailPresale)
                                .Parameters.AddWithValue("@CC", "")
                                .Parameters.AddWithValue("@Oggetto", strOggettoPresale)
                                .Parameters.AddWithValue("@Esito", "OK")
                                .Parameters.AddWithValue("@Note", "INVIO MAIL FEEDBACK DEMO " & strEmailPresale)
                                .Parameters.AddWithValue("@GUIDRichiesta", "")
                                .Parameters.AddWithValue("@TestoHTML", testoHtmlPresale)
                            End With
                            Try
                                commins.ExecuteNonQuery()
                            Catch ex As Exception
                                sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                            End Try
                        End Using
                        myConn3.Close()
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''

                    End If
                    strEmailPresale = ""

                End If

                IdKeyOLD = IdKey
                DataDemoOLD = DataDemo
                OrarioDemoOLD = OrarioDemo
                NoteDemoOLD = NoteDemo
                TabellaOLD = Tabella
                EmailOLD = Email

            Loop
        End If
        myReaderGEN.Close()
        myReaderGEN = Nothing

        If retSP = True Then

            strEmailPresale = ""
            strDescrizionePresale = ""
            '**************************************************************'
            '******************* INVIO EMAIL A PRESALE ********************'
            '**************************************************************'
            testoHtmlPresale = testoHtmlPresale.Replace("#elencorisposte", strElencoRisposte)

            'recupero l'idUtente del presale di riferimento da questa
            'invio la mail al presale

            myCmdPresale = myConn.CreateCommand
            myCmdPresale.CommandText = "Select U.Email, U.Descrizione from Calendario_Demo c Left Join Users U ON U.ID=C.IDUtentePresaleRiferimento where IdKey = @IdKeyOLD And Tabella =@TabellaOLD AND DataDemo= @DataDemoOLD AND Note2=@NoteDemoOLD"
            If (IdKeyOLD = 91 And TabellaOLD = "Preventivi_Sezioni") Or (IdKeyOLD = 317 And TabellaOLD = "Preventivi_Sezioni") Then 'se presenze o paghe guardo se c'è un presale di riferimento, e nel caso la mando solo a lui
                myCmdPresale = myConn.CreateCommand
                myCmdPresale.CommandText = "Select U.Email, U.Descrizione from Calendario_Demo c Left Join Users U ON U.ID=C.IDUtentePresaleRiferimento where IdKey = @IdKeyOLD And Tabella =@TabellaOLD AND DataDemo= @DataDemoOLD AND Note2=@NoteDemoOLD"
                myCmdPresale.Parameters.AddWithValue("@IdKeyOLD", Int(IdKeyOLD))
                myCmdPresale.Parameters.AddWithValue("@TabellaOLD", TabellaOLD)
                myCmdPresale.Parameters.AddWithValue("@DataDemoOLD", CDate(DataDemoOLD))
                myCmdPresale.Parameters.AddWithValue("@NoteDemoOLD", NoteDemoOLD)
                myReaderPresale = myCmdPresale.ExecuteReader()
                strEmailPresale = ""
                While myReaderPresale.Read()
                    If strEmailPresale <> "" Then
                        strEmailPresale = strEmailPresale & ";" & myReaderPresale.Item("Email")
                        strDescrizionePresale = strDescrizionePresale & ";" & myReaderPresale.Item("Descrizione")
                    Else
                        strEmailPresale = myReaderPresale.Item("Email")
                        strDescrizionePresale = myReaderPresale.Item("Descrizione")
                    End If
                End While
                myReaderPresale.Close()
            End If
            myCmdPresale = myConn.CreateCommand

            If strEmailPresale = "" Then
                myCmdPresale.CommandText = "select u.Email Email, u.Descrizione Descrizione from Presales_CalendarioWeekly p join Users u on u.ID=p.IdUtente where p.IdKey=" & Str(IdKeyOLD) & " And p.Tabella = '" & TabellaOLD & "'"
                myReaderPresale = myCmdPresale.ExecuteReader()
                strEmailPresale = ""
                While myReaderPresale.Read()
                    If strEmailPresale <> "" Then
                        strEmailPresale = strEmailPresale & ";" & myReaderPresale.Item("Email")
                        strDescrizionePresale = strDescrizionePresale & ";" & myReaderPresale.Item("Descrizione")
                    Else
                        strEmailPresale = myReaderPresale.Item("Email")
                        strDescrizionePresale = myReaderPresale.Item("Descrizione")
                    End If
                End While
                myReaderPresale.Close()
            End If

            'If strDescrizionePresale <> "" Then
            '    testoHtmlPresale = testoHtmlPresale.Replace("#presale", "")
            'End If

            strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

            If strTest.ToUpper().Trim().ToString() = "S" Then
                strEmailPresale = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
            End If

            If strEmailPresale <> "" Then
                '''''''''''''''''''''''''''''''''''''''''''''''''''''
                '''Ciao #presale,
                testoHtmlPresale = testoHtmlPresale.Replace("Ciao #presale,", "")
                Inviamail(strOggettoPresale, "", strEmailPresale, "", Nothing, testoHtmlPresale, DataInvio, "", Nothing)
                sw.WriteLine("Invio email feedback a presale " & strEmailPresale & " " & DateTime.Now)

                Dim query As String
                query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@Dest", strEmailPresale)
                        .Parameters.AddWithValue("@CC", "")
                        .Parameters.AddWithValue("@Oggetto", strOggettoPresale)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL FEEDBACK DEMO " & strEmailPresale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@TestoHTML", testoHtmlPresale)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''

            End If
            strEmailPresale = ""

            If retImp = True Then

                strElencoRisposte = strElencoRisposte & "</td></tr>"
                testoHtml = testoHtml.Replace("#elencorisposte", strElencoRisposte)

                '**************************************************************'
                '***************** INVIO EMAIL A COMMERCIALE ******************'
                '**************************************************************'
                MailContattoAgg = ""

                strDest = ""
                strDest2 = ""
                strDest3 = ""
                strDest4 = ""
                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strTest.ToUpper().Trim().ToString() = "S" Then

                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                    EmailCommercialeINVIO = strDest 'TEST

                End If

                MailContattoAgg = ""

                '''''''''''''''''''''''''''''''''''''''''''''''''''''
                Inviamail(strOggetto, "", EmailCommercialeINVIO, MailContattoAgg, Nothing, testoHtml, DataInvio, "", Nothing)
                sw.WriteLine("Invio email feedback a commerciale " & EmailCommercialeINVIO & " " & DateTime.Now)

                Dim query As String
                query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
                myConn3 = New SqlConnection(strConnectionDB)
                myConn3.Open()
                Using commins As New SqlCommand()
                    With commins
                        .Connection = myConn3
                        .CommandType = CommandType.Text
                        .CommandText = query
                        .Parameters.AddWithValue("@Dest", EmailCommercialeINVIO)
                        .Parameters.AddWithValue("@CC", MailContattoAgg)
                        .Parameters.AddWithValue("@Oggetto", strOggetto)
                        .Parameters.AddWithValue("@Esito", "OK")
                        .Parameters.AddWithValue("@Note", "INVIO MAIL FEEDBACK DEMO " & EmailCommerciale)
                        .Parameters.AddWithValue("@GUIDRichiesta", "")
                        .Parameters.AddWithValue("@TestoHTML", testoHtml)
                    End With
                    Try
                        commins.ExecuteNonQuery()
                    Catch ex As Exception
                        sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                    End Try
                End Using
                myConn3.Close()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''

            End If

        End If

        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailFeedbackCommerciali = True

        Exit Function

    End Function

    Private Function InviaMailNuovaRichiesta1to1(idUtentePresale As Integer, sw1 As StreamWriter) As Boolean

        Dim retSP As Boolean
        Dim strEmailPresale As String
        Dim strDescrizionePresale As String
        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim NominativoCommerciale As String
        Dim EmailCommerciale As String
        Dim EmailCommercialeINVIO As String
        Dim DataInvio As String
        Dim RispostaLibera As String

        Dim Email As String
        Dim EmailOLD As String
        Dim TestoDomanda As String
        Dim TestoRisposta As String
        Dim NomeAzienda As String
        Email = ""
        TestoDomanda = ""
        TestoRisposta = ""
        NomeAzienda = ""

        RispostaLibera = ""
        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        EmailCommerciale = ""
        EmailCommercialeINVIO = ""
        DataInvio = ""
        EmailOLD = ""

        retSP = False

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReaderPresale As SqlDataReader
        Dim myCmdPresale As New SqlCommand

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoCOP1'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectCOP1'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String
        Dim strElencoRisposte As String

        Dim strTest As String
        Dim strDest As String
        Dim strDest2 As String
        Dim strDest3 As String
        Dim strDest4 As String

        Dim IdKey As Integer
        Dim DataDemo As String
        Dim DataRichiestaCOP As String
        Dim OrarioDemo As String
        Dim NoteDemo As String
        Dim NomeDemo As String
        Dim Tabella As String

        Dim IdKeyOLD As Integer
        Dim DataDemoOLD As String
        Dim OrarioDemoOLD As String
        Dim NoteDemoOLD As String
        Dim TabellaOLD As String

        Dim IDUtentePresaleRiferimento As Integer
        Dim EmailPresale As String
        Dim DescrizionePresale As String
        Dim AziendaCliente As String
        Dim TelefonoCliente As String
        Dim CodiceCliente As String
        Dim IndirizzoCliente As String
        Dim LocalitaCliente As String
        Dim ContattoPrincipaleCliente As String
        Dim NumDipendentiCliente As String
        Dim MailCliente As String
        Dim IDRichiestaCop As Integer
        Dim CodiceRichiestaCop As String
        Dim AltreInfoUtili As String
        Dim NomeCommerciale As String
        Dim TelefonoCommerciale As String
        Dim MailCommerciale As String
        Dim IDDemo As Integer

        IdKey = 0
        DataDemo = ""
        OrarioDemo = ""
        NoteDemo = ""
        Tabella = ""

        IdKeyOLD = 0
        DataDemoOLD = ""
        OrarioDemoOLD = ""
        NoteDemoOLD = ""
        TabellaOLD = ""

        strDest = ""
        strDest2 = ""
        strDest3 = ""
        strDest4 = ""

        retID = 0
        strElencoRisposte = ""

        retID = idUtentePresale

        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        If retID = "" Then
            InviaMailNuovaRichiesta1to1 = False
            Exit Function
        End If

        retSP = False

        strSQLgen = "set dateformat dmy;
        SELECT convert(varchar(10),rc.DataRichiesta,103) DataRichiestaCOP, isnull(rc.AltreInfoUtili,'') AltreInfoUtili, rd.ID IDDemo,
        isnull(ac.Nominativo,'') NomeCommerciale, isnull(ac.Telefono,'') TelefonoCommerciale, isnull(ac.Mail,'') MailCommerciale, 
        rc.ID IDRichiestaCop,IDUtentePresaleRiferimento, u.Email EmailPresale, u.Descrizione DescrizionePresale, convert(varchar(10),DataDemo,103) DataDemo, OrarioDemo, IDKey, Tabella, rc.AziendaCliente, rc.TelefonoCliente, 
        rc.CodiceCliente, rc.IndirizzoCliente, rc.LocalitaCliente, rc.ContattoPrincipaleCliente, isnull(rc.NumDipendentiCliente,0) NumDipendentiCliente, rc.MailCliente,
		case
			when rd.tabella = 'Preventivi_Sezioni' then (select sezione from Preventivi_Sezioni where ID=rd.IDKey)
			when rd.tabella = 'Preventivi_Funzioni' then (select Funzione from Preventivi_Funzioni where ID=rd.IDKey)
            else ''
		end NomeDemo
        FROM RichiesteDemo_COP rd
        join Richieste_COP rc on rc.id=rd.IDRichiestaCop
        join Preventivi_Richieste pr on pr.id=rc.IDRichiesta
        join Users u on u.ID=@IdUtente
        left join Anagrafica_Commerciali ac on ac.ID = pr.IDCommerciale
        where IDUtentePresaleRiferimento in 
        (select IdUtente from Presales_COP pc1 
        where ltrim(rtrim(str(pc1.IdKey))) + pc1.Tabella 
        in (select ltrim(rtrim(str(pc2.IdKey))) + pc2.Tabella from Presales_COP pc2 where IdUtente=@IdUtente))
        /*
        and rc.DataUltimaModifica >= '19/05/2023'
        and rc.DataUltimaModifica < '20/05/2023'
        */
        and rc.DataUltimaModifica >= convert(varchar(10),GETDATE(),103)
        and rc.DataUltimaModifica < convert(varchar(10),DATEADD(dd,+1,getdate()),103)
        and rd.ID not in (select IDDemoPerDelega1to1 from LogMailOperation where IDDemoPerDelega1to1 is Not null)
        and IDUtentePresaleRiferimento <> @IdUtente
        and rd.DataDemo is not null
        and isnull(pr.ConfigurazionePerInterni,0)=0
        order by rd.ID desc"

        myCmdGEN = myConn2.CreateCommand
        myCmdGEN.CommandText = strSQLgen
        myCmdGEN.Parameters.AddWithValue("@IdUtente", CInt(retID))

        sw1.WriteLine("Inizio invio mail al presale - " & retID)

        strEmailPresale = ""
        strDescrizionePresale = ""

        myReaderGEN = Nothing
        myReaderGEN = myCmdGEN.ExecuteReader()
        If myReaderGEN.HasRows Then
            Do While myReaderGEN.Read

                IDUtentePresaleRiferimento = myReaderGEN.Item("IDUtentePresaleRiferimento")
                EmailPresale = myReaderGEN.Item("EmailPresale")
                DescrizionePresale = myReaderGEN.Item("DescrizionePresale")
                NomeDemo = myReaderGEN.Item("NomeDemo")
                OrarioDemo = myReaderGEN.Item("OrarioDemo")
                DataDemo = myReaderGEN.Item("DataDemo").ToString()
                OrarioDemo = myReaderGEN.Item("OrarioDemo")
                IdKey = myReaderGEN.Item("IdKey")
                Tabella = myReaderGEN.Item("Tabella")
                NomeAzienda = myReaderGEN.Item("AziendaCliente")
                TelefonoCliente = myReaderGEN.Item("TelefonoCliente")
                CodiceCliente = myReaderGEN.Item("CodiceCliente")
                IndirizzoCliente = myReaderGEN.Item("IndirizzoCliente")
                LocalitaCliente = myReaderGEN.Item("LocalitaCliente")
                ContattoPrincipaleCliente = myReaderGEN.Item("ContattoPrincipaleCliente")
                NumDipendentiCliente = myReaderGEN.Item("NumDipendentiCliente")
                MailCliente = myReaderGEN.Item("MailCliente")
                IDRichiestaCop = myReaderGEN.Item("IDRichiestaCop")
                AltreInfoUtili = myReaderGEN.Item("AltreInfoUtili")
                NomeCommerciale = myReaderGEN.Item("NomeCommerciale")
                TelefonoCommerciale = myReaderGEN.Item("TelefonoCommerciale")
                MailCommerciale = myReaderGEN.Item("MailCommerciale")
                IDDemo = myReaderGEN.Item("IDDemo")
                DataRichiestaCOP = myReaderGEN.Item("DataRichiestaCOP").ToString()


                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                MailContattoAgg = ""

                '**************************************************************'
                '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                '**************************************************************'

                strEmailPresale = EmailPresale
                strDescrizionePresale = DescrizionePresale

                'invio mail al presale
                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                If strTest.ToUpper().Trim().ToString() = "S" Then
                    strEmailPresale = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                End If

                CodiceRichiestaCop = IDRichiestaCop.ToString("X")

                If strEmailPresale <> "" Then
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''
                    testoHtml = testoHtml.Replace("#nomedemo", NomeDemo)
                    testoHtml = testoHtml.Replace("#dataorademo", DataDemo & " " & OrarioDemo)
                    testoHtml = testoHtml.Replace("#codicerichiestacop", CodiceRichiestaCop)
                    testoHtml = testoHtml.Replace("#nomeazienda", NomeAzienda)
                    testoHtml = testoHtml.Replace("#numerodipendenti", NumDipendentiCliente)
                    testoHtml = testoHtml.Replace("#referente", ContattoPrincipaleCliente)
                    testoHtml = testoHtml.Replace("#recapitotelefonico", TelefonoCliente)
                    testoHtml = testoHtml.Replace("#emailreferente", MailCliente)
                    testoHtml = testoHtml.Replace("#altreInfo", AltreInfoUtili)
                    testoHtml = testoHtml.Replace("#nomecommerciale", NomeCommerciale)
                    testoHtml = testoHtml.Replace("#telefonocommerciale", TelefonoCommerciale)
                    testoHtml = testoHtml.Replace("#emailcommerciale", MailCommerciale)
                    testoHtml = testoHtml.Replace("#codiceRichesta", CodiceRichiestaCop)
                    testoHtml = testoHtml.Replace("#datarichiestacop", DataRichiestaCOP)
                    testoHtml = testoHtml.Replace("#presale", DescrizionePresale)

                    strOggetto = strOggetto.Replace("#ragionesociale", NomeAzienda)


                    Inviamail(strOggetto, "", strEmailPresale, MailContattoAgg, Nothing, testoHtml, DataInvio, "", Nothing)
                    sw1.WriteLine("Inviata mail a " & strEmailPresale)

                    sw1.WriteLine("Invio email di nuova richiesta 1to1 a presale " & strEmailPresale & " " & DateTime.Now)

                    Dim query As String
                    query = "set dateformat dmy;INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML, IdKey, Tabella, DataDemo, IDDemoPerDelega1to1) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML,@IdKey,@Tabella,@DataDemo,@IDDemoPerDelega1to1)"
                    myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", strEmailPresale)
                            .Parameters.AddWithValue("@CC", "")
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO NUOVA RICHIESTA DEMO 1TO1 " & strEmailPresale)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@TestoHTML", testoHtml)
                            .Parameters.AddWithValue("@IdKey", IdKey)
                            .Parameters.AddWithValue("@Tabella", Tabella)
                            .Parameters.AddWithValue("@DataDemo", DataDemo)
                            .Parameters.AddWithValue("@IDDemoPerDelega1to1", IDDemo)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex As Exception
                            sw1.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using
                    myConn3.Close()
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''

                End If
                strEmailPresale = ""

                IdKeyOLD = IdKey
                DataDemoOLD = DataDemo
                OrarioDemoOLD = OrarioDemo
                NoteDemoOLD = NoteDemo
                TabellaOLD = Tabella
                EmailOLD = Email

            Loop
        End If
        myReaderGEN.Close()
        myReaderGEN = Nothing

        myConn.Close()
        myConn2.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailNuovaRichiesta1to1 = True

        Exit Function

    End Function

    Private Function InviaMailRiepilgoMensileCommerciali(idComm As Integer, AbilitaInvioFileRiepilogo As Boolean, Nominativo1 As String) As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailContatto As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoAggOLD As String
        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim strDataDemoTxt As String

        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        MailContattoAggOLD = ""


        Dim formattedDate As String
        Dim formattedYear As String
        Dim formattedDateStart As String
        Dim formattedDateEnd As String
        Dim myDate As Date

        formattedDateStart = ""
        formattedDateEnd = ""

        myDate = DateTime.Now
        formattedDate = Format$(myDate, "MM")
        formattedYear = Format$(myDate, "yyyy").ToString()

        If formattedDate.Equals("01") Then
            formattedDate = "12"
            formattedYear = (Format$(myDate, "yyyy") - 1).ToString()
        Else
            If formattedDate.Equals("02") Then formattedDate = "01"
            If formattedDate.Equals("03") Then formattedDate = "02"
            If formattedDate.Equals("04") Then formattedDate = "03"
            If formattedDate.Equals("05") Then formattedDate = "04"
            If formattedDate.Equals("06") Then formattedDate = "05"
            If formattedDate.Equals("07") Then formattedDate = "06"
            If formattedDate.Equals("08") Then formattedDate = "07"
            If formattedDate.Equals("09") Then formattedDate = "08"
            If formattedDate.Equals("10") Then formattedDate = "09"
            If formattedDate.Equals("11") Then formattedDate = "10"
            If formattedDate.Equals("12") Then formattedDate = "11"
        End If

        If formattedDate.Equals("01") Or formattedDate.Equals("03") Or formattedDate.Equals("05") Or formattedDate.Equals("07") Or formattedDate.Equals("08") Or formattedDate.Equals("10") Or formattedDate.Equals("12") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "31/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("02") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "28/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("04") Or formattedDate.Equals("06") Or formattedDate.Equals("09") Or formattedDate.Equals("11") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "30/" & formattedDate & "/" & formattedYear
        End If

        'formattedDateStart = "01/01/2021"
        'formattedDateEnd = "31/07/2021"

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Nominativo1 = Nominativo1.Trim()
        Nominativo1 = Nominativo1.Replace("''", "")


        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailRiepilogoMensileCommercali_[" & Nominativo1 & "]_" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail riepilogo mensile al commerciale - " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioRiepilogoMensileCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioRiepilogoMensileCommerciali'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String
        Dim strElencoDemoCommerciale As String
        Dim strElencoDemo1to1Commerciale As String

        retID = 0
        strElencoDemoCommerciale = ""
        strElencoDemo1to1Commerciale = ""

        ' MI SCORRO TUTTI I COMMERCIALI (AD ESCLUSIONE DI QUELLI FITTIZI BO PARTNER E DEALER
        strSQL = "set dateformat dmy "
        'strSQL = strSQL & "select * from Anagrafica_Commerciali where id=100 and  Nominativo not in ('** BO PARTNER **', 'DEALER') "
        strSQL = strSQL & "select * from Anagrafica_Commerciali where id=" & idComm.ToString() & " and  Nominativo not in ('** BO PARTNER **', 'DEALER') "

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myReader = myCmd.ExecuteReader()
        If myReader.HasRows Then
            Do While myReader.Read
                retID = myReader.Item("ID").ToString()

                '''''''''''''''''''''''''''''''''''''''''''''''''''''
                If retID = "" Then
                    InviaMailRiepilgoMensileCommerciali = False
                    Exit Function
                End If
                ' #listaclientidemo
                strSQLgen = "set dateformat dmy Exec spGetRiepilogoDemoCommercialiMAIL @idcommerciale, @data1, @data2, @order"
                myCmdGENMAIL = myConn2.CreateCommand
                myCmdGENMAIL.CommandText = strSQLgen
                myCmdGENMAIL.Parameters.AddWithValue("@idcommerciale", CInt(retID))
                myCmdGENMAIL.Parameters.AddWithValue("@data1", DateTime.Now.AddYears(-1))
                myCmdGENMAIL.Parameters.AddWithValue("@data2", DateTime.Now)
                myCmdGENMAIL.Parameters.AddWithValue("@order", "")
                myReaderGENMAIL = Nothing
                myReaderGENMAIL = myCmdGENMAIL.ExecuteReader()
                If myReaderGENMAIL.HasRows Then
                    Do While myReaderGENMAIL.Read
                        strElencoDemoCommerciale = myReaderGENMAIL.Item(0)
                    Loop
                End If
                myReaderGENMAIL.Close()
                myCmdGENMAIL = Nothing

                Dim strFileXLS1 As String
                Dim fileExistsXLS1 As Boolean

                If AbilitaInvioFileRiepilogo = True Then
                    strFileXLS1 = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ElencoDemoCommerciale" & Now().ToString("yyyyMMdd_HHmmss") & ".xls"
                    fileExistsXLS1 = File.Exists(strFileXLS1)
                    Dim swXLS1 As New StreamWriter(File.Open(strFileXLS1, FileMode.Append))
                    swXLS1.WriteLine(strElencoDemoCommerciale)
                    swXLS1.Close()
                End If

                '''' #listaclientidemo1to1
                '''strSQLgen = "set dateformat dmy Exec spGetRiepilogoDemoCommerciali1TO1 @idcommerciale, @data1, @data2, @order"
                '''myCmdGENMAIL = myConn2.CreateCommand
                '''myCmdGENMAIL.CommandText = strSQLgen
                '''myCmdGENMAIL.Parameters.AddWithValue("@idcommerciale", CInt(retID))
                '''myCmdGENMAIL.Parameters.AddWithValue("@data1", DateTime.Now.AddYears(-1))
                '''myCmdGENMAIL.Parameters.AddWithValue("@data2", DateTime.Now)
                '''myCmdGENMAIL.Parameters.AddWithValue("@order", "")
                '''myReaderGENMAIL = Nothing
                '''myReaderGENMAIL = myCmdGENMAIL.ExecuteReader()
                '''If myReaderGENMAIL.HasRows Then
                '''    Do While myReaderGENMAIL.Read
                '''        strElencoDemo1to1Commerciale = myReaderGENMAIL.Item(0)
                '''    Loop
                '''End If
                '''myReaderGENMAIL.Close()
                '''myCmdGENMAIL = Nothing


                strSQLgen = "set dateformat dmy Exec spGetRiepilogoMensileCommerciali @idcommerciale"

                myCmdGEN = myConn2.CreateCommand
                myCmdGEN.CommandText = strSQLgen
                myCmdGEN.Parameters.AddWithValue("@idcommerciale", CInt(retID))

                sw.WriteLine("Inizio invio mail riepilogo mensile al commerciale - " & retID)

                myReaderGEN = Nothing
                myReaderGEN = myCmdGEN.ExecuteReader()

                If myReaderGEN.HasRows Then
                    Do While myReaderGEN.Read

                        MailContatto = myReaderGEN.Item("MailCommerciale")

                        Dim strTest As String
                        Dim strDest As String
                        Dim strDest2 As String
                        Dim strDest3 As String
                        Dim strDest4 As String

                        strDest = ""
                        strDest2 = ""
                        strDest3 = ""
                        strDest4 = ""

                        strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                        '**************************************************************'
                        '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                        '**************************************************************' .Replace("#testo", testo)
                        MailContattoAgg = ""

                        If strTest.ToUpper().Trim().ToString() = "S" Then

                            strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                            strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                            strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                            strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                            'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                            MailContatto = strDest 'TEST

                        End If

                        MailContattoAgg = ""

                        'If strElencoDemo1to1Commerciale = "" Then
                        '    strElencoDemo1to1Commerciale = "Nessuna demo 1to1 trovata"
                        'End If

                        If strElencoDemoCommerciale = "" Then
                            strElencoDemoCommerciale = "Nessuna demo trovata"
                        End If

                        Dim strFileXLS2 As String
                        Dim fileExistsXLS2 As Boolean
                        If AbilitaInvioFileRiepilogo = True Then
                            strFileXLS2 = AppDomain.CurrentDomain.BaseDirectory + "\LOG\ElencoAziendeCommerciale" & Now().ToString("yyyyMMdd_HHmmss") & ".xls"
                            fileExistsXLS2 = File.Exists(strFileXLS2)
                            Dim swXLS2 As New StreamWriter(File.Open(strFileXLS2, FileMode.Append))
                            swXLS2.WriteLine(myReaderGEN.Item("ElencoAziende"))
                            swXLS2.Close()
                        End If

                        MailContattoAgg = ""
                        Try
                            testoHtml = testoHtml.Replace("#data1", formattedDateStart).Replace("#data2", formattedDateEnd).Replace("#numeroconfigurazioni", myReaderGEN.Item("NumConfigurazioni")).Replace("#numerototaleiscritti", myReaderGEN.Item("Iscritti")).Replace("#numeropartecipanti", myReaderGEN.Item("Partecipanti")).Replace("#numerononpartecipanti", myReaderGEN.Item("Non partec")).Replace("#percentuale", myReaderGEN.Item("Perc")).Replace("#commerciale", myReaderGEN.Item("Commerciale"))
                            testoHtml = testoHtml.Replace("#annonumeroconfigurazioni", myReaderGEN.Item("NumConfigurazioni da inizio anno"))
                            testoHtml = testoHtml.Replace("#numconfest", myReaderGEN.Item("NumConfigurazioniEsterne"))
                            testoHtml = testoHtml.Replace("#numconfannoest", myReaderGEN.Item("NumConfigurazioniEsterne da inizio anno"))
                            testoHtml = testoHtml.Replace("#data1", formattedDateStart).Replace("#data2", formattedDateEnd).Replace("#annonumerototaleiscritti", myReaderGEN.Item("Iscritti da inizio anno")).Replace("#annonumeropartecipanti", myReaderGEN.Item("Partecipanti da inizio anno")).Replace("#annonumerononpartecipanti", myReaderGEN.Item("Non partec da inizio anno")).Replace("#annopercentuale", myReaderGEN.Item("Perc da inizio anno")).Replace("#commerciale", myReaderGEN.Item("Commerciale"))
                            testoHtml = testoHtml.Replace("#elencoaziende", myReaderGEN.Item("ElencoAziende"))
                            testoHtml = testoHtml.Replace("#listaclientidemo1to1", strElencoDemo1to1Commerciale)
                            testoHtml = testoHtml.Replace("#listaclientidemo", strElencoDemoCommerciale)

                            If strElencoDemo1to1Commerciale = "Nessuna demo 1to1 trovata" Then
                                testoHtml = testoHtml.Replace("LISTA CLIENTI/DEMO 1TO1 DEGLI ULTIMI 12 MESI", "")
                            End If

                            If strElencoDemoCommerciale = "Nessuna demo weekly trovata" Then
                                testoHtml = testoHtml.Replace("LISTA CLIENTI/DEMO WEEKLY DEGLI ULTIMI 12 MESI", "")
                            End If



                            ''''''''''''''''''''''''''''''''''
                            Dim dataarray(1) As String
                            If AbilitaInvioFileRiepilogo = True Then
                                dataarray(0) = strFileXLS1
                                dataarray(1) = strFileXLS2

                                dataarray(0) = dataarray(0).Replace("'", "")
                                dataarray(0) = dataarray(0).Replace("%", "")
                                dataarray(0) = dataarray(0).Replace("&", "")

                                dataarray(1) = dataarray(1).Replace("'", "")
                                dataarray(1) = dataarray(1).Replace("%", "")
                                dataarray(1) = dataarray(1).Replace("&", "")

                                testoHtml = testoHtml.Replace("Se vuoi ricevere questi dati in formato XLS contatta andrea.sabattini@zucchetti.it", "")
                            End If
                            ''''''''''''''''''''''''''''''''''

                            If AbilitaInvioFileRiepilogo = True Then
                                Inviamail(strOggetto, "", MailContatto, MailContattoAgg, Nothing, testoHtml, strDataDemoTxt, "", dataarray)
                            Else
                                Inviamail(strOggetto, "", MailContatto, MailContattoAgg, Nothing, testoHtml, strDataDemoTxt, "", Nothing)
                            End If

                            sw.WriteLine("Invio email contatto principale a " & MailContatto & " " & DateTime.Now)






                            Try
                                File.Delete(strFileXLS1)
                            Catch ex As Exception

                            End Try
                            Try
                                File.Delete(strFileXLS2)
                            Catch ex As Exception

                            End Try

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
                            myConn3 = New SqlConnection(strConnectionDB)
                            myConn3.Open()
                            Using commins As New SqlCommand()
                                With commins
                                    .Connection = myConn3
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto & " [" & Nominativo1 & "]")
                                    .Parameters.AddWithValue("@Esito", "OK")
                                    If AbilitaInvioFileRiepilogo = True Then
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto & " con allegato ")
                                    Else
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto & " ")
                                    End If
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml)
                                End With
                                Try
                                    commins.ExecuteNonQuery()
                                Catch ex As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn3.Close()

                            dataarray = Nothing

                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                            Dim query As String
                            query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                            'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                            myConn3 = New SqlConnection(strConnectionDB)
                            myConn3.Open()
                            Using commins As New SqlCommand()
                                With commins
                                    .Connection = myConn3
                                    .CommandType = CommandType.Text
                                    .CommandText = query
                                    .Parameters.AddWithValue("@Dest", MailContatto)
                                    .Parameters.AddWithValue("@CC", MailContattoAgg)
                                    .Parameters.AddWithValue("@Oggetto", strOggetto)
                                    .Parameters.AddWithValue("@Esito", "ERR")
                                    If AbilitaInvioFileRiepilogo = True Then
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto & " con allegato ")
                                    Else
                                        .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto & " ")
                                    End If
                                    .Parameters.AddWithValue("@GUIDRichiesta", "")
                                    .Parameters.AddWithValue("@TestoHTML", testoHtml)
                                End With
                                Try
                                    commins.ExecuteNonQuery()
                                Catch ex1 As Exception
                                    sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                                End Try
                            End Using
                            myConn3.Close()


                        End Try



                        '**************************************************************'
                        '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                        '**************************************************************'


                    Loop
                End If
                myReaderGEN.Close()
                myReaderGEN = Nothing
                '''''''''''''''''''''''''''''''''''''''''''''''''''''




            Loop
        End If
        myReader.Close()
        myReader = Nothing




        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailRiepilgoMensileCommerciali = True

        Exit Function

    End Function
    '12/10/2021 sg2

    Private Function InviaMailRiepilgoMensilePresales(idPM As Integer, Nominativo As String, Descrizione As String, Email As String) As Boolean
        'si chiama PM ma è relativa ai PRESALES
        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim cnstring As String
        Dim MailContatto As String
        Dim MailCommercialeOLD As String

        Dim MailContattoAgg As String
        Dim CommercialeOLD As String

        Dim MailContattoAggOLD As String
        Dim MailContattoOLD As String
        Dim strLinkWebexOLD As String
        Dim strOraOLD As String

        Dim strDataDemoTxt As String

        retImp = False
        CommercialeOLD = ""
        MailCommercialeOLD = ""
        strLinkWebexOLD = ""
        strOraOLD = ""
        MailContattoOLD = ""
        MailContattoAggOLD = ""


        Dim formattedDate As String
        Dim formattedYear As String
        Dim formattedDateStart As String
        Dim formattedDateEnd As String
        Dim myDate As Date

        formattedDateStart = ""
        formattedDateEnd = ""

        myDate = DateTime.Now
        formattedDate = Format$(myDate, "MM")
        formattedYear = Format$(myDate, "yyyy").ToString()

        If formattedDate.Equals("01") Then
            formattedDate = "12"
            formattedYear = (Format$(myDate, "yyyy") - 1).ToString()
        Else
            If formattedDate.Equals("02") Then formattedDate = "01"
            If formattedDate.Equals("03") Then formattedDate = "02"
            If formattedDate.Equals("04") Then formattedDate = "03"
            If formattedDate.Equals("05") Then formattedDate = "04"
            If formattedDate.Equals("06") Then formattedDate = "05"
            If formattedDate.Equals("07") Then formattedDate = "06"
            If formattedDate.Equals("08") Then formattedDate = "07"
            If formattedDate.Equals("09") Then formattedDate = "08"
            If formattedDate.Equals("10") Then formattedDate = "09"
            If formattedDate.Equals("11") Then formattedDate = "10"
            If formattedDate.Equals("12") Then formattedDate = "11"
        End If

        If formattedDate.Equals("01") Or formattedDate.Equals("03") Or formattedDate.Equals("05") Or formattedDate.Equals("07") Or formattedDate.Equals("08") Or formattedDate.Equals("10") Or formattedDate.Equals("12") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "31/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("02") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "28/" & formattedDate & "/" & formattedYear
        End If
        If formattedDate.Equals("04") Or formattedDate.Equals("06") Or formattedDate.Equals("09") Or formattedDate.Equals("11") Then
            formattedDateStart = "01/" & formattedDate & "/" & formattedYear
            formattedDateEnd = "30/" & formattedDate & "/" & formattedYear
        End If

        'formattedDateStart = "01/01/2021"
        'formattedDateEnd = "31/07/2021"

        Dim strData As String
        strData = "Convert(NVARCHAR, '" & DateTime.Now.ToString("dd/MM/yyyy") & "', 103)"

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaRiepilogomensilePresales" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.Append))

        'sw.WriteLine("Inizio invio mail riepilogo mensile al commerciale - " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        cnstring = strConnect2

        Dim strToday As String

        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String
        Dim strSQLgen As String

        strSQL = ""

        strSQL = "select Mail_Host, Mail_Name,Mail_Address,Mail_Port, Mail_Username, Mail_Password, Mail_EnableSSL,Mail_SmtpAuth from Configurazione_SMTP "

        myConn = New SqlConnection(strConnectionDB)
        myConn2 = New SqlConnection(strConnectionDB)
        myConn3 = New SqlConnection(strConnectionDB)

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL

        myConn.Open()
        myConn2.Open()

        Dim testoHtml As String
        Dim strOggetto As String

        Dim myReader3 As SqlDataReader
        Dim myCmd3 As New SqlCommand

        strOggetto = ""
        testoHtml = ""
        myCmd3 = myConn.CreateCommand
        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='testoInvioRiepilogoMensilePresales'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            testoHtml = myReader3.Item("Testo")
        Loop

        myReader3.Close()

        myCmd3.CommandText = "Select * from Etichette_mail  where Quale='objectInvioRiepilogoMensilePresales'"

        myReader3 = myCmd3.ExecuteReader()

        Do While myReader3.Read()
            strOggetto = myReader3.Item("Testo")
        Loop
        myReader3.Close()

        Dim myReader1 As SqlDataReader

        myReader1 = myCmd.ExecuteReader()

        Do While myReader1.Read()
            Mail_Host = myReader1.Item("Mail_Host")
            Mail_Name = myReader1.Item("Mail_Name")
            Mail_Address = myReader1.Item("Mail_Address")
            Mail_Port = myReader1.Item("Mail_Port")
            Mail_Username = myReader1.Item("Mail_Username")
            Mail_Password = myReader1.Item("Mail_Password")
            stPasswordDecifrata = CifraturaDecifratura.CifraturaDecifratura.Decrypt(Mail_Password)
            Mail_EnableSSL = myReader1.Item("Mail_EnableSSL")
            Mail_SmtpAuth = myReader1.Item("Mail_SmtpAuth")
        Loop
        myReader1.Close()

        Dim retID As String
        Dim strElencoDemoPM As String
        Dim strElencoDemo1to1PM As String
        Dim strElencoaziendenopartecipato As String

        retID = 0
        strElencoDemoPM = ""
        strElencoDemo1to1PM = ""
        strElencoaziendenopartecipato = ""

        retID = idPM

        Dim data1 As Date
        Dim data2 As Date

        data2 = DateTime.Now.AddDays(-(DateTime.Now.Day))
        data1 = data2.AddDays(-data2.Day + 1)
        data1 = DateTime.Now.AddDays(-(DateTime.Now.Day) + 1)
        data1 = data1.AddYears(-1)

        data1 = FormatDateTime(data1, DateFormat.ShortDate)
        data2 = FormatDateTime(data2, DateFormat.ShortDate)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''
        If retID = "" Then
            InviaMailRiepilgoMensilePresales = False
            Exit Function
        End If
        ' #listaclientidemo
        strSQLgen = "set dateformat dmy Exec spGetRiepilogoDemoPresaleWEEKLYMAIL @idpm, @data1, @data2, @order"
        myCmdGENMAIL = myConn2.CreateCommand
        myCmdGENMAIL.CommandText = strSQLgen
        myCmdGENMAIL.Parameters.AddWithValue("@idpm", CInt(retID))
        myCmdGENMAIL.Parameters.AddWithValue("@data1", data1)
        myCmdGENMAIL.Parameters.AddWithValue("@data2", data2)
        myCmdGENMAIL.Parameters.AddWithValue("@order", "")
        myCmdGENMAIL.CommandTimeout = 240
        myReaderGENMAIL = Nothing
        myReaderGENMAIL = myCmdGENMAIL.ExecuteReader()
        If myReaderGENMAIL.HasRows Then
            Do While myReaderGENMAIL.Read
                strElencoDemoPM = myReaderGENMAIL.Item(0)
            Loop
        End If
        myReaderGENMAIL.Close()
        myCmdGENMAIL = Nothing


        ' #listaclientidemo1to1
        strSQLgen = "set dateformat dmy Exec spGetRiepilogoDemoPresale1TO1 @idpm, @data1, @data2, @order"
        myCmdGENMAIL = myConn2.CreateCommand
        myCmdGENMAIL.CommandText = strSQLgen
        myCmdGENMAIL.Parameters.AddWithValue("@idpm", CInt(idPM))
        myCmdGENMAIL.Parameters.AddWithValue("@data1", data1)
        myCmdGENMAIL.Parameters.AddWithValue("@data2", data2)
        myCmdGENMAIL.Parameters.AddWithValue("@order", "")
        myCmdGENMAIL.CommandTimeout = 240
        myReaderGENMAIL = Nothing
        myReaderGENMAIL = myCmdGENMAIL.ExecuteReader()
        If myReaderGENMAIL.HasRows Then
            Do While myReaderGENMAIL.Read
                strElencoDemo1to1PM = myReaderGENMAIL.Item(0)
            Loop
        End If
        myReaderGENMAIL.Close()
        myCmdGENMAIL = Nothing

        strSQLgen = "set dateformat dmy Exec spGetRiepilogoMensilePresale @idpm"

        myCmdGEN = myConn2.CreateCommand
        myCmdGEN.CommandText = strSQLgen
        myCmdGEN.Parameters.AddWithValue("@idpm", CInt(retID))

        sw.WriteLine("Inizio invio mail riepilogo mensile al presale - " & retID & " - " & Nominativo)


        myReaderGEN = Nothing
        myCmdGEN.CommandTimeout = 240
        myReaderGEN = myCmdGEN.ExecuteReader()
        If myReaderGEN.HasRows Then
            Do While myReaderGEN.Read

                MailContatto = Email ' myReaderGEN.Item("MailCommerciale")

                Dim strTest As String
                Dim strDest As String
                Dim strDest2 As String
                Dim strDest3 As String
                Dim strDest4 As String

                strDest = ""
                strDest2 = ""
                strDest3 = ""
                strDest4 = ""

                strTest = System.Configuration.ConfigurationManager.AppSettings("TEST").ToString()

                '**************************************************************'
                '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                '**************************************************************' .Replace("#testo", testo)
                MailContattoAgg = ""

                If strTest.ToUpper().Trim().ToString() = "S" Then

                    strDest = System.Configuration.ConfigurationManager.AppSettings("Dest").ToString()
                    strDest2 = System.Configuration.ConfigurationManager.AppSettings("Dest2").ToString()
                    strDest3 = System.Configuration.ConfigurationManager.AppSettings("Dest3").ToString()
                    strDest4 = System.Configuration.ConfigurationManager.AppSettings("Dest4").ToString()
                    'strOggetto = System.Configuration.ConfigurationManager.AppSettings("OggettoMail").ToString()

                    MailContatto = strDest 'TEST

                End If

                MailContattoAgg = ""

                If strElencoDemo1to1PM = "" Then
                    strElencoDemo1to1PM = "Nessuna demo 1to1 trovata"
                End If

                If strElencoDemoPM = "" Then
                    strElencoDemoPM = "Nessuna demo weekly trovata"
                End If

                strElencoaziendenopartecipato = myReaderGEN.Item("ElencoAziende").ToString()

                If strElencoaziendenopartecipato = "" Then
                    strElencoaziendenopartecipato = "Nessuna azienda trovata"
                End If

                MailContattoAgg = ""
                Try
                    testoHtml = testoHtml.Replace("#data1", formattedDateStart).Replace("#data2", formattedDateEnd).Replace("#numerodemoweekly", myReaderGEN.Item("NumWeekly")).Replace("#numerototaleiscritti", myReaderGEN.Item("Iscritti")).Replace("#numeropartecipanti", myReaderGEN.Item("Partecipanti")).Replace("#numerononpartecipanti", myReaderGEN.Item("Non partec")).Replace("#percentuale", myReaderGEN.Item("percentuale")).Replace("#numerodemo1to1", myReaderGEN.Item("NumDemo1to1"))
                    testoHtml = testoHtml.Replace("#annonumerodemoweekly", myReaderGEN.Item("NumWeeklyAnno"))
                    testoHtml = testoHtml.Replace("#annonumerototaleiscritti", myReaderGEN.Item("NumTotIscrittiWeeklyAnno"))
                    testoHtml = testoHtml.Replace("#annonumeropartecipanti", myReaderGEN.Item("NumPartecipantiAnno"))
                    testoHtml = testoHtml.Replace("#annonumerononpartecipanti", myReaderGEN.Item("NumNonPartecipantiAnno"))
                    testoHtml = testoHtml.Replace("#annopercentuale", myReaderGEN.Item("percentualeAnno"))
                    testoHtml = testoHtml.Replace("#annonumerodemo1to1", myReaderGEN.Item("NumDemo1to1Anno"))
                    testoHtml = testoHtml.Replace("#elencoaziendenopartecipato", strElencoaziendenopartecipato)
                    testoHtml = testoHtml.Replace("#listaclientidemo1to1", strElencoDemo1to1PM)
                    testoHtml = testoHtml.Replace("#listaclientidemo", strElencoDemoPM)
                    testoHtml = testoHtml.Replace("#presale", Descrizione)

                    If strElencoDemo1to1PM = "Nessuna demo 1to1 trovata" Then
                        testoHtml = testoHtml.Replace("LISTA CLIENTI/DEMO 1TO1 DEGLI ULTIMI 12 MESI", "")
                    End If

                    If strElencoDemoPM = "Nessuna demo weekly trovata" Then
                        testoHtml = testoHtml.Replace("LISTA CLIENTI/DEMO WEEKLY DEGLI ULTIMI 12 MESI", "")
                    End If

                    Inviamail(strOggetto, "", MailContatto, MailContattoAgg, Nothing, testoHtml, strDataDemoTxt, "", Nothing)
                    sw.WriteLine("Invio email contatto principale a " & MailContatto & " " & DateTime.Now)

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta,@TestoHTML)"
                    myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailContatto)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "OK")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@TestoHTML", testoHtml)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using
                    myConn3.Close()


                Catch ex As Exception
                    sw.WriteLine("*** ERRORE SU INVIO MAIL CONTATTO PRINCIPALE A  " & MailContatto & " " & DateTime.Now)

                    Dim query As String
                    query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta,TestoHTML) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta,@TestoHTML)"
                    'query = "INSERT INTO LogMailOperation (DataOra,DestinatarioPrincipale,DestinatarioCC,Oggetto,Esito,Note,GUIDRichiesta,IDRichiesta) VALUES (getdate(),@Dest,@CC,@Oggetto,@Esito,@Note,@GUIDRichiesta, @IDRichiesta)"
                    myConn3 = New SqlConnection(strConnectionDB)
                    myConn3.Open()
                    Using commins As New SqlCommand()
                        With commins
                            .Connection = myConn3
                            .CommandType = CommandType.Text
                            .CommandText = query
                            .Parameters.AddWithValue("@Dest", MailContatto)
                            .Parameters.AddWithValue("@CC", MailContattoAgg)
                            .Parameters.AddWithValue("@Oggetto", strOggetto)
                            .Parameters.AddWithValue("@Esito", "ERR")
                            .Parameters.AddWithValue("@Note", "INVIO MAIL RIEPILOGO MENSILE A  " & MailContatto)
                            .Parameters.AddWithValue("@GUIDRichiesta", "")
                            .Parameters.AddWithValue("@TestoHTML", testoHtml)
                        End With
                        Try
                            commins.ExecuteNonQuery()
                        Catch ex1 As Exception
                            sw.WriteLine("*** ERRORE SU INSERT INTO LogMailOperation " & DateTime.Now)
                        End Try
                    End Using
                    myConn3.Close()


                End Try

                '**************************************************************'
                '*************** INVIO EMAIL CONTATTO PRINCIPALE **************'
                '**************************************************************'


            Loop
        End If
        myReaderGEN.Close()
        myReaderGEN = Nothing
        '''''''''''''''''''''''''''''''''''''''''''''''''''''



        '    Loop
        'End If
        'myReader.Close()
        'myReader = Nothing




        myConn.Close()
        myConn2.Close()

        sw.WriteLine("Fine operazioni ore -- " & " " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        InviaMailRiepilgoMensilePresales = True

        Exit Function

    End Function
    Private Function PopolaGetDemoAdminTarget() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\InviaMailDemoReminder" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio PopolaGetDemoAdminTarget -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String


        Dim myCmd9 As New SqlCommand

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()

        myCmd9 = myConn.CreateCommand
        myCmd9.CommandText = "spPopolaGetDemoAdminTarget"
        myCmd9.CommandType = CommandType.StoredProcedure

        myCmd9.CommandTimeout = 240

        myReader = myCmd9.ExecuteReader()

        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine PopolaGetDemoAdminTarget ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        Exit Function

    End Function


    Private Function PopolaStatiDemo() As Boolean

        Dim retImp As Boolean

        Me.Cursor = Cursors.WaitCursor

        Dim IdAttivita As String
        Dim IdCorsoOLD As String
        Dim arrayIdCorso As New ArrayList

        Dim cnstring As String
        Dim elencoIdCorso As String

        retImp = False

        If (Not System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")) Then
            System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\LOG\")
        End If

        Dim strFile As String = AppDomain.CurrentDomain.BaseDirectory + "\LOG\PopolaStatiDemo" & Now().ToString("yyyyMMdd_HHmmss") & ".log"
        Dim fileExists As Boolean = File.Exists(strFile)
        Dim sw As New StreamWriter(File.Open(strFile, FileMode.OpenOrCreate))

        sw.WriteLine("Inizio PopolaStatiDemo -- " & DateTime.Now)
        Me.Cursor = Cursors.WaitCursor

        IdAttivita = ""
        IdCorsoOLD = ""
        cnstring = strConnect2
        elencoIdCorso = ""

        Dim strToday As String
        strToday = DateTime.Now.ToString("yyyyMMddhhmmss", System.Globalization.CultureInfo.GetCultureInfo("en-US"))

        Dim strSQL As String


        Dim myCmd3 As New SqlCommand

        'Create a Connection object.
        myConn = New SqlConnection(strConnectionDB)
        myConn.Open()
        'Create a Command object.
        strSQL = "Declare @Q VARCHAR(MAX) Set @Q='EXEC spPopolaStatiDemo '''', '''', '''', '''', '''', '''', '''', ''''' SELECT @Q EXEC(@Q)"

        myCmd = myConn.CreateCommand
        myCmd.CommandText = strSQL
        myCmd.CommandTimeout = 0

        Try
            sw.WriteLine("Inizio ExecuteReader -- " & DateTime.Now)
            myReader = myCmd.ExecuteReader()
            sw.WriteLine("Fine ExecuteReader -- " & DateTime.Now)
        Catch ex As Exception
            sw.WriteLine("Errore PopolaStatiDemo -- " & ex.Message)
        End Try


        'Close the reader and the database connection.
        myReader.Close()
        myConn.Close()

        sw.WriteLine("Fine PopolaStatiDemo ore -- " & DateTime.Now)
        sw.Close()

        Me.Cursor = Cursors.Default

        retImp = True

        Exit Function

    End Function


    Private Function CSVBuilder(dt As DataTable) As String
        Dim sCSV As New System.Text.StringBuilder()

        'Headers
        Dim delimeter As String = ""
        For Each col In dt.Columns '.Select(Func(col) col.ColumnName)
            If col.ToString().Contains(",") Then col = """" & col & """"
            sCSV.Append(delimeter).Append(col)
            delimeter = ","
        Next
        sCSV.AppendLine()

        For Each row As DataRow In dt.Rows
            sCSV.AppendLine(String.Join(",", (From rw In row.ItemArray Select If(rw.ToString.Trim.Contains(","), String.Format("""{0}""", rw.ToString.Trim), rw.ToString.Trim))))
        Next

        Return sCSV.ToString
    End Function


End Class
