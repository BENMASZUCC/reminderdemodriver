/*SVUOTA LA TABELLA PER IDTIPOEVENTO 1,2,3*/
DELETE FROM TabellaEventiAziende WHERE IDTIPO IN (1,2,3)



declare @data1 datetime
declare @data2 datetime
set @data1=DATEADD(YEAR,-2,GETDATE())
set @data2=GETDATE()


DECLARE @id int
declare @idazienda int



DECLARE db_cursor CURSOR FOR 
select top 1 ID from Aziende

OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO @id  

WHILE @@FETCH_STATUS = 0  
BEGIN  
      
	  print @id
	  SET @idazienda=@id

	if not exists (select * from sysobjects where id = object_id('#TabellaStatEventiDett')) 
	BEGIN
		BEGIN TRY  
			 CREATE TABLE #TabellaStatEventiDett (IdRichiesta int, Tipo varchar(1), DataEvento varchar(8), NomeAzienda varchar(100),NomeContatto varchar(100),MailContatto varchar(100), DataDemo date, NoteDemo varchar(100),Demo varchar(100), NumSettimana int, Anno int, keydate varchar(14), idkey int, tabella varchar(100), idcommerciale int, partecipantiweekly int);
		END TRY  
		BEGIN CATCH  
		END CATCH
	 END
	 truncate table #TabellaStatEventiDett

 

	set dateformat dmy
	insert into #TabellaStatEventiDett (IdRichiesta, Tipo, DataEvento, NomeAzienda,NomeContatto,MailContatto, DataDemo, NoteDemo,Demo, Anno, NumSettimana, idcommerciale, partecipantiweekly)
	SELECT pr.ID, 'A', format(cast(pr.data as date),'yyyyMMdd'), pr.NomeAzienda, pr.NomeContatto, pr.MailContatto, NULL,'','',YEAR(pr.Data), Format(DATEPART(week, pr.Data),'00'), pr.IDCommerciale, 0
	   FROM [Preventivi_Richieste] pr
	  where NomeAzienda<>'' and NomeContatto<>'' and MailContatto<>''
	  and IDAzienda=@idazienda
	  and Data>=@data1 and Data <=DATEADD(day, 1, @data2)
	  and (select COUNT(IDRichiesta) from Preventivi_Richieste_Funzioni where IDRichiesta=pr.ID) >0 
	/*
	   and (
	(#tabella = 'Preventivi_Sezioni' AND pr.ID in (select IDRichiesta from Preventivi_Sezioni_Funzioni psf join Preventivi_Richieste_Funzioni prf on psf.IDFunzione=prf.IDFunzione where psf.IDSezione=#idkey)) 
	OR (#tabella = 'Preventivi_Funzioni' AND pr.ID in (select IDRichiesta from Preventivi_Sezioni_Funzioni psf join Preventivi_Richieste_Funzioni prf on psf.IDFunzione=prf.IDFunzione where prf.IDFunzione=#idkey))
	) 
	*/
	  ORDER BY YEAR(pr.Data), DATEPART(week, pr.Data)



	set dateformat dmy
	insert into #TabellaStatEventiDett (IdRichiesta, Tipo, DataEvento, NomeAzienda,NomeContatto,MailContatto, DataDemo, NoteDemo,Demo, Anno, NumSettimana,idcommerciale, partecipantiweekly)
	SELECT max(PR.ID), 'B', format(cast(s.DataDemo as date),'yyyyMMdd'), max(pr.NomeAzienda), max(pr.NomeContatto), max(pr.MailContatto), s.DataDemo, max(s.NoteDemo),
	case max(s.tabella)
	 when 'Preventivi_Sezioni' then max(sez.Sezione)
	 when 'Preventivi_Funzioni' then max(funz.Funzione)
	 else ''
	end Demo,YEAR(max(l.DataOra)), DATEPART(week, max(L.DataOra)),max(pr.IDCommerciale)
	,
	(Select isnull(count(s1.ID),0)
	from Storico_democlienti s1
	join Preventivi_Richieste r on r.id=s1.IDRichiesta
	--where s.IDKey = s1.IDkey and s.Tabella=s1.tabella and s.DataDemo = s1.DataDemo and max(s.Orario) = s1.Orario 
	where s1.IDKey = s.idkey and s1.Tabella=s.tabella and s1.DataDemo = s.datademo and s1.Orario = s.orario
	and isnull(s1.Completata,0)=1
	and r.NomeAzienda <>'' and r.NomeContatto <>'' and r.MailContatto<>''
	and r.NomeAzienda = max(pr.NomeAzienda)
	)


	from Storico_DemoClienti s
	join Preventivi_Richieste pr on pr.id=s.IDRichiesta
	join Log_TableOp L on s.ID = L.KeyValue and L.Tabella='Storico_DemoClienti' and L.Operation='I'
	join Calendario_Demo c on c.IDKey=s.IDKey and c.Tabella=s.Tabella and c.DataDemo=s.DataDemo and c.Note=s.Orario
	left join Preventivi_Sezioni sez on s.IDKey=sez.ID and s.Tabella='Preventivi_Sezioni'
	left join Preventivi_Funzioni funz on s.IDKey=funz.ID and s.Tabella='Preventivi_Funzioni'
	where pr.NomeAzienda<>'' and pr.NomeContatto<>'' and pr.MailContatto<>''
	and pr.IDAzienda=@idazienda
	and s.DataDemo>=@data1 and s.DataDemo <=DATEADD(day, 1, @data2)
	group by s.IDKey, s.Tabella, s.DataDemo, s.Orario
	ORDER BY YEAR(s.DataDemo), DATEPART(week, s.DataDemo)


	--numero demo prenotate in stato eseguita
	set dateformat dmy
	insert into #TabellaStatEventiDett (IdRichiesta, Tipo, DataEvento, NomeAzienda,NomeContatto,MailContatto, DataDemo, NoteDemo,Demo, Anno, NumSettimana,idcommerciale, partecipantiweekly)
	SELECT MAX(PR.ID), 'C', format(cast(MAX(rd.DataDemo) as date),'yyyyMMdd'), MAX(pr.NomeAzienda), MAX(pr.NomeContatto), MAX(pr.MailContatto), MAX(rd.DataDemo), MAX(s.Note),
	case rd.tabella
	 when 'Preventivi_Sezioni' then MAX(sez.Sezione )
	 when 'Preventivi_Funzioni' then MAX(funz.Funzione)
	 else ''
	end Demo,YEAR(MAX(rd.DataDemo)), DATEPART(week, MAX(L.DataOra)),max(pr.IDCommerciale), 0
	from RichiesteDemo_COP rd
	join Richieste_COP rc on rd.IDRichiestaCop= rc.ID
	join Preventivi_Richieste pr on pr.id=rc.IDRichiesta
	outer APPLY dbo.TopStorico_RichiesteDemo_COP(rd.DataDemo,rd.IDKey,rd.OrarioDemo,rd.Tabella,rc.ID,rd.Progressivo) s
	join LogOperation_COP L on L.IDRichiestaCOP=rc.ID  and L.Operazione='I'
	left join Preventivi_Sezioni sez on s.IDKey=sez.ID and s.Tabella='Preventivi_Sezioni'
	left join Preventivi_Funzioni funz on s.IDKey=funz.ID and s.Tabella='Preventivi_Funzioni'
	where rd.IDRichiestaCop=rc.id and rd.datademo is not null
	and isnull(s.eseguita,0) not in (2,3)
	and pr.IDAzienda=@idazienda
	and rd.DataDemo>=@data1 and rd.DataDemo <=DATEADD(day, 1, @data2)
	group by rd.IDKey, rd.tabella, rd.IDRichiestaCop

	--GROUP BY YEAR(pr.Data), DATEPART(week, pr.Data)
	ORDER BY YEAR(max(rd.DataDemo)), DATEPART(week, max(rd.DataDemo))


	set dateformat dmy;

	INSERT INTO [dbo].[TabellaEventiAziende]
			   ([IdRichiesta]
			   ,[IDTipo]
			   ,[DataEvento]
			   ,[IdAzienda]
			   ,[NomeAzienda]
			   ,[NomeContatto]
			   ,[MailContatto]
			   ,[NoteDemo]
			   ,[Demo]
			   ,[Anno]
			   ,[NumSettimana]
			   ,[idcommerciale]
			   ,[partecipantiweekly])

	select 
	IdRichiesta,
	case Tipo
	 when 'A' then 1
	 when 'B' then 2
	 when 'C' then 3
	 end
	IDTipo, 
	format(cast(DataEvento as date),'dd/MM/yyyy') DataEvento,
	@idazienda,
	NomeAzienda,NomeContatto,MailContatto, NoteDemo,Demo, Anno, NumSettimana, idcommerciale, partecipantiweekly
	--select *
	from #TabellaStatEventiDett t
	left join Anagrafica_Commerciali ac on ac.ID=t.idcommerciale
	--order by id desc-- format(cast(DataEvento as date),'dd/MM/yyyy') --desc
	order by year(DataEvento), month(DataEvento), day(DataEvento)


	FETCH NEXT FROM db_cursor INTO @id	 
END 

CLOSE db_cursor  
DEALLOCATE db_cursor