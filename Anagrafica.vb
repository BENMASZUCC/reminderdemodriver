﻿Imports System.Data.OleDb

Public Class Anagrafica

    Private ID As Integer
    Private IDAzienda As Integer
    Private CodiceFiscale As String

    Public Property m_ObjID() As Integer
        Get
            ' Gets the property value.
            Return ID
        End Get
        Set(ByVal Value As Integer)
            ' Sets the property value.
            ID = Value
        End Set
    End Property

    Public Property m_ObjIDAzienda() As Integer
        Get
            ' Gets the property value.
            Return IDAzienda
        End Get
        Set(ByVal Value As Integer)
            ' Sets the property value.
            IDAzienda = Value
        End Set
    End Property

    Public Property m_ObjCodiceFiscale() As String
        Get
            ' Gets the property value.
            Return CodiceFiscale
        End Get
        Set(ByVal Value As String)
            ' Sets the property value.
            CodiceFiscale = Value
        End Set
    End Property

    Public Function Load(cnstring As String) As Integer

        On Error GoTo ErrorTrap

        Dim cn As New OleDb.OleDbConnection(cnstring) 'oledb connection

        Dim objAnagrafica As Anagrafica

        cn.Open()

        'Ricavo l'anagrafica se esiste
        Dim cmdAnag As New OleDb.OleDbCommand("select ISNULL(ID,0) AS ID, ISNULL(IDAzienda,0) AS IDAzienda, ISNULL(CodiceFiscale,0) as CodiceFiscale from Anagrafica")
        cmdAnag.Connection = cn
        Dim oledbReader As OleDbDataReader = cmdAnag.ExecuteReader()
        objAnagrafica = New Anagrafica
        objAnagrafica.m_ObjID = 0
        objAnagrafica.m_ObjIDAzienda = 0
        objAnagrafica.m_ObjCodiceFiscale = ""
        While oledbReader.Read
            objAnagrafica.m_ObjID = Convert.ToInt32(oledbReader.Item(0))
            objAnagrafica.m_ObjIDAzienda = Convert.ToInt32(oledbReader.Item(1))
            objAnagrafica.m_ObjCodiceFiscale = oledbReader(2)
        End While
        oledbReader.Close()

        cn.Close()

        Exit Function

ErrorTrap:
        MsgBox(Err.Description)
        Exit Function
        Resume

    End Function

End Class
